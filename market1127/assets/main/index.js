System.register("chunks:///_virtual/ArrayUtils.ts", ['cc'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      exports({
        advance: advance,
        asString: asString,
        backward: backward,
        combine: combine,
        flatten: flatten,
        forward: forward,
        pickElementFrom: pickElementFrom,
        pickElementsFrom: pickElementsFrom,
        pickFromHead: pickFromHead,
        pickIndexFrom: pickIndexFrom,
        pickToTail: pickToTail,
        productOf: productOf,
        range: range,
        removeDuplicated: removeDuplicated,
        shuffle1: shuffle1,
        shuffle2: shuffle2,
        shuffle3: shuffle3,
        sortByAscending: sortByAscending,
        sortByDescending: sortByDescending,
        sumOf: sumOf,
        zip: zip
      });

      cclegacy._RF.push({}, "c7c3en+a5ZM8pjxTe/Psc1y", "ArrayUtils", undefined);
      /**
       * 洗牌算法1
       * Fisher-Yates Shuffle 随机置乱算法
       * @param list 目标数组
       */


      function shuffle1(list) {
        var count = list.length;

        while (count) {
          var index = Math.floor(Math.random() * count--);
          var temp = list[count];
          list[count] = list[index];
          list[index] = temp;
        }

        return list;
      }
      /**
       * 洗牌算法2
       * @param list 目标数组
       */


      function shuffle2(list) {
        var count = list.length;

        for (var i = 0; i < count; i++) {
          var index = Math.floor(Math.random() * count);
          var temp = list[index];
          list[index] = list[0];
          list[0] = temp;
        }

        return list;
      }
      /**
       * 洗牌算法3
       * @param list 目标数组
       */


      function shuffle3(list) {
        return list.sort(function () {
          return Math.random() - 0.5;
        });
      }
      /**
       * 从小到大排序
       * @param list 数字数组
       * @returns
       */


      function sortByAscending(list) {
        return list.sort(function (a, b) {
          return a - b;
        });
      }
      /**
       * 从大到小排序
       * @param list 数字数组
       * @returns
       */


      function sortByDescending(list) {
        return list.sort(function (a, b) {
          return b - a;
        });
      }
      /**
       * 数组去重
       * @param list 原始数组
       * @returns
       */


      function removeDuplicated(list) {
        return list.filter(function (item, index, arr) {
          return arr.indexOf(item, 0) === index;
        });
      }
      /**
       * 获得指定范围内的数值数组
       * @param start 起始数值
       * @param ended 终止数值
       * @param step 增进步幅
       * @returns
       */


      function range(start, ended, step) {
        if (step === void 0) {
          step = 1;
        }

        start = start | 0;
        ended = ended | 0;
        step = step | 0;
        var ret = [];

        if (step > 0) {
          var _ref = ended > start ? [start, ended] : [ended, start];

          start = _ref[0];
          ended = _ref[1];

          for (var i = start; i <= ended; i += step) {
            ret.push(i);
          }
        } else if (step === 0) {
          ret.push(start, ended);
        } else {
          var _ref2 = ended > start ? [start, ended] : [ended, start];

          start = _ref2[0];
          ended = _ref2[1];

          for (var _i = ended; _i >= start; _i += step) {
            ret.push(_i);
          }
        }

        return ret;
      }
      /**
       * 累加
       * @param list 数值数组
       * @returns
       */


      function sumOf(list) {
        return list.reduce(function (a, b) {
          return a + b;
        }, 0);
      }
      /**
       * 乘积
       * @param list 数值数组
       * @returns
       */


      function productOf(list) {
        return list.reduce(function (a, b) {
          return a * b;
        }, 1);
      }
      /**
       * 截取第一个到指定个数的数组
       * @param list 数组
       * @param count 个数
       */


      function pickFromHead(list, count) {
        return list.slice(0, count | 0);
      }
      /**
       * 截取从最后一个到指定个数的数组
       * @param list 数组
       * @param count 个数
       */


      function pickToTail(list, count) {
        return list.slice(list.length - count | 0, list.length);
      }
      /**
       * 前进一步
       * @param list 数组
       */


      function forward(list) {
        list.length > 1 && list.unshift(list.pop());
      }
      /**
       * 后退一步
       * @param list 数组
       */


      function backward(list) {
        list.length > 1 && list.push(list.shift());
      }
      /**
       * 前进/后退 n 步
       * @param list 数组
       * @param step 步数
       */


      function advance(list, step) {
        step = step | 0;

        if (step !== 0 && list.length > 1) {
          if (step > 0) {
            list.unshift.apply(list, list.splice(list.length - step, step));
          } else {
            list.push.apply(list, list.splice(0, step));
          }
        }

        return list;
      }
      /**
       * 转换为字符串
       * @returns
       */


      function asString(list) {
        return list.length > 0 ? list.map(function (v) {
          return v.toString();
        }).join(",") : 0;
      }
      /**
       * 数组合并
       * @param arrays 数组列表
       * @returns
       */


      function zip() {
        for (var _len = arguments.length, arrays = new Array(_len), _key = 0; _key < _len; _key++) {
          arrays[_key] = arguments[_key];
        }

        return Array.apply(null, Array(arrays[0].length)).map(function (_, i) {
          return arrays.map(function (array) {
            return array[i];
          });
        });
      }
      /**
       * 数组扁平化
       * @param array 目标数组
       */


      function flatten(array) {
        while (array.some(function (v) {
          return Array.isArray(v);
        })) {
          array = [].concat.apply([], array);
        }

        return array;
      }
      /**
       * 合并数组
       * @param array1 目标数组1
       * @param array2 目标数组2
       */


      function combine(array1, array2) {
        return [].concat(array1, array2);
      }
      /**
       * 随机获取数组索引
       * @param array 目标数组
       */


      function pickIndexFrom(array) {
        if (array.length > 0) {
          return Math.random() * array.length | 0;
        }
      }
      /**
       * 随机获取数组成员
       * @param array 目标数组
       */


      function pickElementFrom(array) {
        var size = array.length;
        if (size === 0) return undefined;else if (size === 1) return array[0];
        return array[pickIndexFrom(array)];
      }
      /**
       * 随机获取数组成员
       * @param array 目标数组
       */


      function pickElementsFrom(array, count) {
        count = Math.min(array.length, Math.max(1, count | 0));
        return shuffle1(array.slice()).slice(0, count);
      }

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/AssetLocator.ts", ['cc'], function (exports) {
  var cclegacy, Prefab, sp, SceneAsset;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
      Prefab = module.Prefab;
      sp = module.sp;
      SceneAsset = module.SceneAsset;
    }],
    execute: function () {
      cclegacy._RF.push({}, "b3b2fSGRkpM3I0TbkidSfLL", "AssetLocator", undefined);
      /**
       * 资源定位器
       */


      var AssetLocator = exports('AssetLocator', /*#__PURE__*/function () {
        function AssetLocator() {}
        /** 获取 Prefab */


        AssetLocator.seekPrefab = function seekPrefab(from) {
          return {
            path: "prefab/" + from,
            type: Prefab
          };
        }
        /** 获取 SkeletonData */
        ;

        AssetLocator.seekSkeleton = function seekSkeleton(from) {
          return {
            path: "res/" + from,
            type: sp.SkeletonData
          };
        }
        /** 获取 Scene */
        ;

        AssetLocator.seekScene = function seekScene(from) {
          return {
            path: from,
            type: SceneAsset
          };
        };

        return AssetLocator;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/AudioEngine.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './ResManager.ts'], function (exports) {
  var _createClass, cclegacy, AudioClip, res;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      AudioClip = module.AudioClip;
    }, function (module) {
      res = module.res;
    }],
    execute: function () {
      cclegacy._RF.push({}, "0c91f5JqTpCuLkY4eNd6Yah", "AudioEngine", undefined);
      /**
       * 音频引擎
       */


      var AudioEngine = /*#__PURE__*/function () {
        function AudioEngine() {
          this._audioSource = void 0;
          this._audioRes = new Map();
          this._volume = 1;
          this._clickEffectSound = void 0;
          this._bgmSwitch = true;
          this._effectSwitch = true;
        }

        var _proto = AudioEngine.prototype;

        _proto.init = function init(source) {
          this._audioSource = source;
          this._audioSource.playOnAwake = false;
        };
        /**
         * @en
         * play short audio, such as strikes,explosions
         * @zh
         * 播放短音频,比如 打击音效，爆炸音效等
         * @param sound clip or url for the audio
         * @param volume
         */


        _proto.playOneShot = function playOneShot(sound, volume, bundle) {
          var _this = this;

          if (volume === void 0) {
            volume = 1.0;
          }

          if (bundle === void 0) {
            bundle = 'MainModule';
          }

          if (sound instanceof AudioClip) {
            if (this.effectSwitch) this._audioSource.playOneShot(sound, volume);
          } else {
            if (this._audioRes.has(sound)) {
              var clip = this._audioRes.get(sound);

              if (this.effectSwitch) this._audioSource.playOneShot(clip, volume);
            } else {
              res.load(bundle, sound, AudioClip, function (clip) {
                _this._audioRes.set(sound, clip);

                if (_this.effectSwitch) _this._audioSource.playOneShot(clip, volume);
              });
            }
          }
        };

        _proto.playDefaultClickEffect = function playDefaultClickEffect() {
          this._clickEffectSound && this.playOneShot(this._clickEffectSound);
        }
        /**
         * @en
         * play long audio, such as the bg music
         * @zh
         * 播放长音频，比如 背景音乐
         * @param sound clip or url for the sound
         * @param volume
         */
        ;

        _proto.play = function play(sound, loop, bundle) {
          var _this2 = this;

          if (bundle === void 0) {
            bundle = 'MainModule';
          }

          if (sound instanceof AudioClip) {
            if (this._audioSource.clip != sound) {
              this._playByClip(sound);
            }
          } else {
            if (this._audioRes.has(sound)) {
              var clip = this._audioRes.get(sound);

              if (this._audioSource.clip != clip) {
                this._playByClip(clip);
              }
            } else {
              res.load(bundle, sound, AudioClip, function (clip) {
                _this2._audioRes.set(sound, clip);

                _this2._playByClip(clip);
              });
            }
          }
        };

        _proto._playByClip = function _playByClip(clip, loop) {
          if (loop === void 0) {
            loop = false;
          }

          this._audioSource.stop();

          this._audioSource.loop = loop;
          this._audioSource.clip = clip;
          this._audioSource.volume = this._volume;
          if (this.bgmSwitch) this._audioSource.play();
        }
        /**
         * stop the audio play
         */
        ;

        _proto.stop = function stop() {
          this._audioSource.stop();
        }
        /**
         * pause the audio play
         */
        ;

        _proto.pause = function pause() {
          this._audioSource.pause();
        }
        /**
         * resume the audio play
         */
        ;

        _proto.resume = function resume() {
          if (this.bgmSwitch) this._audioSource.play();
        };

        _createClass(AudioEngine, [{
          key: "audioSource",
          get: function get() {
            return this._audioSource;
          }
        }, {
          key: "volume",
          get: function get() {
            return this._volume;
          },
          set: function set(value) {
            this._volume = value;
            this._audioSource.volume = value;
          }
        }, {
          key: "clickEffectSound",
          get: function get() {
            return this._clickEffectSound;
          },
          set: function set(value) {
            this._clickEffectSound = value;
          }
        }, {
          key: "bgmSwitch",
          get: function get() {
            return this._bgmSwitch;
          },
          set: function set(value) {
            if (this._bgmSwitch && !value) {
              this.pause();
            }

            if (!this._bgmSwitch && value) {
              this.resume();
            }

            this._bgmSwitch = value;
          }
        }, {
          key: "effectSwitch",
          get: function get() {
            return this._effectSwitch;
          },
          set: function set(value) {
            this._effectSwitch = value;
          }
        }]);

        return AudioEngine;
      }();

      var audioEngine = exports('audioEngine', new AudioEngine());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/base-application.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _inheritsLoose, _createClass, _asyncToGenerator, _regeneratorRuntime, _createForOfIteratorHelperLoose, cclegacy, EventTarget;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
      _createForOfIteratorHelperLoose = module.createForOfIteratorHelperLoose;
    }, function (module) {
      cclegacy = module.cclegacy;
      EventTarget = module.EventTarget;
    }],
    execute: function () {
      cclegacy._RF.push({}, "7669fh5HKdK85cu5r812/L1", "base-application", undefined);
      /**
       * 插件系统（插件管理器）
       * - 负责统一挂载和卸载插件
       * - 可以在此处挂载（原本可以设计为单例的）管理器，例如：音频播放器、资源管理器，等等
       **/


      var PluginsManager = /*#__PURE__*/function (_EventTarget) {
        _inheritsLoose(PluginsManager, _EventTarget);
        /** 插件系统事件 */

        /** 插件列表 */

        /** 构造 */


        function PluginsManager() {
          var _this;

          _this = _EventTarget.call(this) || this;
          _this.plugins = void 0;
          _this.plugins = new Map();
          return _this;
        }

        var _proto = PluginsManager.prototype;

        _proto.mount = /*#__PURE__*/function () {
          var _mount = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(cls) {
            var instance,
                _instance,
                _len,
                args,
                _key,
                _args = arguments;

            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  instance = this.plugins.get(cls.name);

                  if (instance) {
                    _context.next = 8;
                    break;
                  }

                  instance = new cls();

                  for (_len = _args.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
                    args[_key - 1] = _args[_key];
                  }

                  _context.next = 6;
                  return (_instance = instance).init.apply(_instance, args);

                case 6:
                  this.plugins.set(cls.name, instance);
                  this.emit(PluginsManager.EventType.Mounted, cls.name);

                case 8:
                  return _context.abrupt("return", Promise.resolve(instance));

                case 9:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));

          function mount(_x) {
            return _mount.apply(this, arguments);
          }

          return mount;
        }();

        _proto.unmount = /*#__PURE__*/function () {
          var _unmount = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(cls) {
            var instance;
            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                  if (this.plugins.has(cls.name)) {
                    _context2.next = 2;
                    break;
                  }

                  return _context2.abrupt("return");

                case 2:
                  instance = this.plugins.get(cls.name);
                  _context2.next = 5;
                  return instance.deinit();

                case 5:
                  this.plugins["delete"](cls.name);
                  this.emit(PluginsManager.EventType.UnMounted, cls.name);

                case 7:
                case "end":
                  return _context2.stop();
              }
            }, _callee2, this);
          }));

          function unmount(_x2) {
            return _unmount.apply(this, arguments);
          }

          return unmount;
        }();

        _proto.deinit = /*#__PURE__*/function () {
          var _deinit = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3() {
            var _iterator, _step, plugin;

            return _regeneratorRuntime().wrap(function _callee3$(_context3) {
              while (1) switch (_context3.prev = _context3.next) {
                case 0:
                  _iterator = _createForOfIteratorHelperLoose(this.plugins.values());

                case 1:
                  if ((_step = _iterator()).done) {
                    _context3.next = 7;
                    break;
                  }

                  plugin = _step.value;
                  _context3.next = 5;
                  return plugin.deinit();

                case 5:
                  _context3.next = 1;
                  break;

                case 7:
                  this.plugins.clear();

                case 8:
                case "end":
                  return _context3.stop();
              }
            }, _callee3, this);
          }));

          function deinit() {
            return _deinit.apply(this, arguments);
          }

          return deinit;
        }();

        return PluginsManager;
      }(EventTarget);
      /**
       * 基础应用骨架
       */


      PluginsManager.EventType = {
        Mounted: 'plugin-mounted',
        UnMounted: 'plugin-unmounted'
      };
      var BaseApplication = exports('BaseApplication', /*#__PURE__*/function (_EventTarget2) {
        _inheritsLoose(BaseApplication, _EventTarget2);
        /** 运行状态 */

        /** 环境配置 */

        /** 插件系统 */

        /**
         * 构造
         * @param env 环境配置
         **/


        function BaseApplication(env) {
          var _this2;

          _this2 = _EventTarget2.call(this) || this;
          _this2.status = void 0;
          _this2.environment = void 0;
          _this2.plugins = void 0;
          _this2.status = 'none';
          _this2.environment = env;
          _this2.plugins = new PluginsManager();

          _this2.init();

          return _this2;
        }

        var _proto2 = BaseApplication.prototype;

        _proto2.init = function init() {
          if (this.status === 'none') {
            this.status = 'inited';
            this.onInited();
            this.emit('inited');
          }
        };

        _proto2.launch = function launch() {
          if (this.status === 'inited') {
            this.status = 'launched';
            this.onLaunched();
            this.emit('lauched');
          }
        };

        _proto2.resume = function resume() {
          if (this.status === 'paused') {
            this.status = 'launched';
            this.onResumed();
            this.emit('resumed');
          }
        };

        _proto2.pause = function pause() {
          if (this.status === 'launched') {
            this.status = 'paused';
            this.onPaused();
            this.emit('paused');
          }
        };

        _proto2.stop = function stop() {
          if (this.status !== 'stopped') {
            this.status = 'stopped';
            this.onStopped();
            this.emit('stopped');
          }
        }
        /** 初始化后回调 */
        ;

        _createClass(BaseApplication, [{
          key: "inited",
          get: function get() {
            return this.status === 'inited';
          }
        }, {
          key: "launched",
          get: function get() {
            return this.status === 'launched';
          }
        }, {
          key: "paused",
          get: function get() {
            return this.status === 'paused';
          }
        }, {
          key: "stopped",
          get: function get() {
            return this.status === 'stopped';
          }
        }]);

        return BaseApplication;
      }(EventTarget));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/BatchItems.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, UITransform, director, Director, geometry, Mask, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      UITransform = module.UITransform;
      director = module.director;
      Director = module.Director;
      geometry = module.geometry;
      Mask = module.Mask;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _class3;

      cclegacy._RF.push({}, "bbec1Hw1IJCkp9qZgG/emli", "BatchItems", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property,
          menu = _decorator.menu;
      var BatchItems = exports('BatchItems', (_dec = ccclass('BatchItems'), _dec2 = ccmodifier('BatchItems'), _dec3 = menu('UIEx/BatchItems'), _dec4 = property(UITransform), _dec(_class = _dec2(_class = _dec3(_class = (_class2 = (_class3 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(BatchItems, _BaseComponent);

        function BatchItems() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          _this.queue = new Queue(); //优先级分层队列

          _this.children = []; //记录原节点结构

          _initializerDefineProperty(_this, "culling", _descriptor, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = BatchItems.prototype;

        _proto.update = function update(dt) {
          BatchItems.nodes.push(this.node);
          BatchItems.queues.push(this.queue);
        };

        return BatchItems;
      }(BaseComponent), _class3.nodes = [], _class3.queues = [], _class3), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "culling", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _class2)) || _class) || _class) || _class)); //绘画层

      var Draw = //以节点名为标记
      //是否有mask遮盖组件
      //绘画节点容器
      //绘图节点原子节点数据
      function Draw(key) {
        this.key = '';
        this.mask = false;
        this.nodes = [];
        this.localOpacitys = [];
        this.childrens = [];
        this.next = null;
        this.prev = null;
        this.key = key;
        this.mask = false;
        this.nodes = [];
        this.childrens = [];
      }; //绘画层队列


      var Queue = /*#__PURE__*/function () {
        function Queue() {
          this.items = {};
          this.head = null;
          this.items = {};
          this.head = null;
        }

        var _proto2 = Queue.prototype;

        _proto2.get = function get(key) {
          return this.items[key];
        };

        _proto2.set = function set(prev, data) {
          this.items[data.key] = data;
          if (!this.head) this.head = data;else {
            data.next = prev.next;
            prev.next = data;
            data.prev = prev;
          }
        };

        _proto2.clear = function clear() {
          var _this2 = this;

          Object.keys(this.items).forEach(function (key) {
            delete _this2.items[key];
          });
          this.head = null;
          this.items = {};
        };

        return Queue;
      }(); //遍历建立绘图层队，并收集绘画节点, 全程以节点名字来作为唯一识别标记


      var DFS = function DFS(prev, node, queue, active, level, opacity) {
        if (level === void 0) {
          level = 0;
        }

        if (opacity === void 0) {
          opacity = 1.0;
        }

        var uiProps = node._uiProps;
        var render = uiProps.uiComp; // as Renderable2D;

        opacity = opacity * uiProps.localOpacity; //注意：根据节点的名字进行分层，务必确保名字的唯一性

        var key = node.name; //添加层级前缀加强同名过滤

        if (level == 0) key = '98K'; //自定义收集首节点(允许Item首节点异名)

        var draw = queue.get(key);

        if (!draw) {
          draw = new Draw(key);
          queue.set(prev, draw); //检测是否带有mask组件，不建议item内有mask

          if (render) //mask 会打断合批，会增加draw call
            draw.mask = node.getComponent(Mask) != null;
        }

        prev = draw;

        if (render) {
          var nodes = draw.nodes;
          var localOpacitys = draw.localOpacitys;
          var selfOpacity = render.color ? render.color.a / 255 : 1; // let currOpactity = opacity * selfOpacity * uiProps.localOpacity;

          if (active) {
            //node.active && active
            nodes.push(node); //收集节点

            localOpacitys.push(uiProps.localOpacity); //保存透明度

            uiProps.localOpacity = opacity; //opacity * uiProps.localOpacity; //设置当前透明度
          } //opacity = currOpactity;


          opacity = opacity * selfOpacity;
        }

        if (draw.mask) return prev;
        var childs = node.children;

        for (var i = 0; i < childs.length; i++) {
          var isActive = childs[i].active ? active : false;
          prev = DFS(prev, childs[i], queue, isActive, level + 1, opacity);
        }

        return prev;
      };

      var changeTree = function changeTree(parent, queue) {
        // queue.clear();
        var aabb0 = new geometry.AABB();
        var aabb1 = new geometry.AABB();
        var btn = parent.getComponent(BatchItems);
        btn.culling && btn.culling.getComputeAABB(aabb0); //遍历所有绘画节点，按顺序分层

        var nodes = parent.children;

        for (var i = 0; i < nodes.length; i++) {
          var node = nodes[i];

          if (node.activeInHierarchy) {
            //剔除显示范围外的item
            if (btn.culling) {
              var uiProps = node._uiProps;
              var trans = uiProps.uiTransformComp;

              if (trans) {
                trans.getComputeAABB(aabb1);
                if (!geometry.intersect.aabbWithAABB(aabb0, aabb1)) continue;
              }
            }

            DFS(null, node, queue, true);
          }
        } // //记录item的父节点的子节点结构
        // let btn = parent.getComponent(BatchItems)!;


        btn.children = parent['_children']; //记录原来节点结构

        var childs = parent['_children'] = []; //创建动态分层节点结构
        //拼接动态分层的绘画节点

        var curr = queue.head;

        while (curr) {
          var mask = curr.mask;
          var _nodes = curr.nodes;
          var childrens = curr.childrens;

          for (var _i = 0; _i < _nodes.length; _i++) {
            childrens[_i] = _nodes[_i]['_children']; //记录原来节点结构

            !mask && (_nodes[_i]['_children'] = []); //清空切断下层节点
          } //按顺序拼接分层节点


          childs.push.apply(childs, _nodes);
          curr = curr.next;
        }
      };

      var resetTree = function resetTree(parent, queue) {
        //恢复父节点结构
        var btn = parent.getComponent(BatchItems);
        parent['_children'].length = 0; //清空动态分层节点结构

        parent['_children'] = btn.children; //恢复原来节点结构
        //恢复原来节点结构

        var curr = queue.head;

        while (curr) {
          var nodes = curr.nodes;
          var childrens = curr.childrens;
          var localOpacitys = curr.localOpacitys;

          for (var i = 0; i < nodes.length; i++) {
            nodes[i]['_children'] = childrens[i]; //恢复原来节点结构
            //恢复原来透明度

            var uiProps = nodes[i]._uiProps;
            uiProps.localOpacity = localOpacitys[i]; // childrens[i] = [];
          }

          childrens.length = 0;
          nodes.length = 0;
          curr = curr.next;
        }

        queue.clear();
      };

      director.on(Director.EVENT_BEFORE_DRAW, function (dt) {
        //绘画前拦截修改节点结构
        var nodes = BatchItems.nodes;
        var queues = BatchItems.queues;

        for (var i = 0; i < nodes.length; i++) {
          var node = nodes[i];

          if (node.active && node.isValid) {
            changeTree(node, queues[i]);
          }
        }
      });
      director.on(Director.EVENT_AFTER_DRAW, function (dt) {
        //绘画结束后恢复节点结构
        var nodes = BatchItems.nodes;
        var queues = BatchItems.queues;

        for (var i = 0; i < nodes.length; i++) {
          var node = nodes[i];

          if (node && node.isValid) {
            resetTree(node, queues[i]);
          }
        }

        nodes.length = 0;
        queues.length = 0;
      });

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/BezierCurve.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _createClass, cclegacy, v3;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      v3 = module.v3;
    }],
    execute: function () {
      cclegacy._RF.push({}, "5869cOY60NCUZaAwZJyH1Tg", "BezierCurve", undefined);
      /** 贝塞尔曲线 */


      var BezierCurve = exports('default', /*#__PURE__*/function () {
        function BezierCurve(pointAs_) {
          /* --------------- private --------------- */
          this._distanceNS = [];
          this._funcFSS = [];
          /** 控制点 */

          this._pointV3S = void 0;
          this.pointV3S = pointAs_;

          this._resetData();
        }

        var _proto = BezierCurve.prototype;
        /* ------------------------------- 功能函数 ------------------------------- */

        /** 重置数据 */

        _proto._resetData = function _resetData() {
          if (this._pointV3S.length < 2) {
            return;
          }
          /** 首尾相等 */


          var equalsB = this._pointV3S[0].strictEquals(this._pointV3S[this._pointV3S.length - 1]);
          /** 总距离 */


          var sumDistanceN = 0;
          /** 临时变量 */

          var tempV3;
          var temp2V3;
          var temp3V3;
          var temp4V3;

          for (var kN = 0, lenN = this._pointV3S.length - 1; kN < lenN; kN++) {
            if (kN === 0) {
              tempV3 = equalsB ? this._pointV3S[this._pointV3S.length - 2] : this._pointV3S[0];
            } else {
              tempV3 = this._pointV3S[kN - 1];
            }

            temp2V3 = this._pointV3S[kN];
            temp3V3 = this._pointV3S[kN + 1];

            if (kN + 1 === this._pointV3S.length - 1) {
              temp4V3 = equalsB ? this._pointV3S[1] : this._pointV3S[this._pointV3S.length - 1];
            } else {
              temp4V3 = this._pointV3S[kN + 2];
            }

            this._funcFSS[kN] = [];

            var _this$_curve = this._curve(tempV3, temp2V3, temp3V3, temp4V3);

            this._funcFSS[kN][0] = _this$_curve[0];
            this._funcFSS[kN][1] = _this$_curve[1];
            sumDistanceN += this._gaussLegendre(this._funcFSS[kN][1], 0, 1);
            this._distanceNS[kN] = sumDistanceN;
          }
        }
        /**
         * 递归阶乘
         * @param valueN_
         * @returns
         */
        ;

        _proto._factorial = function _factorial(valueN_) {
          var resultN = 1;

          for (var kN = 2; kN <= valueN_; ++kN) {
            resultN *= kN;
          }

          return resultN;
        }
        /**
         * 高斯—勒让德积分公式可以用较少节点数得到高精度的计算结果
         * @param valueF_ 曲线长度变化率,用于匀速曲线运动
         * @param valueN_ 左区间
         * @param value2N_ 右区间
         * @returns
         */
        ;

        _proto._gaussLegendre = function _gaussLegendre(valueF_, valueN_, value2N_) {
          // 3次系数
          var gauFactor = {
            0.7745966692: 0.555555556,
            0: 0.8888888889
          }; // 5次系数
          // let GauFactor = {0.9061798459:0.2369268851,0.5384693101:0.4786286705,0:0.5688888889}
          // 积分

          var gauSumN = 0;
          var keyN;

          for (var key in gauFactor) {
            if (Object.prototype.hasOwnProperty.call(gauFactor, key)) {
              keyN = Number(key);
              var v = gauFactor[key];
              var t = ((value2N_ - valueN_) * keyN + valueN_ + value2N_) / 2;
              var der = valueF_(t);
              gauSumN = gauSumN + der * v;

              if (keyN > 0) {
                t = ((value2N_ - valueN_) * -key + valueN_ + value2N_) / 2;
                der = valueF_(t);
                gauSumN = gauSumN + der * v;
              }
            }
          }

          return gauSumN * (value2N_ - valueN_) / 2;
        };

        _proto._curve = function _curve(pointV3_, point2V3_, point3V3_, point4V3_) {
          // 基本样条线插值算法
          // 弹性
          var sN = 0.5; // 计算三次样条线函数系数

          var bV3 = pointV3_.clone().multiplyScalar(-sN).add(point2V3_.clone().multiplyScalar(2 - sN)).add(point3V3_.clone().multiplyScalar(sN - 2)).add(point4V3_.clone().multiplyScalar(sN));
          var b2V3 = pointV3_.clone().multiplyScalar(2 * sN).add(point2V3_.clone().multiplyScalar(sN - 3)).add(point3V3_.clone().multiplyScalar(3 - 2 * sN)).add(point4V3_.clone().multiplyScalar(-sN));
          var b3V3 = pointV3_.clone().multiplyScalar(-sN).add(point3V3_.clone().multiplyScalar(sN));
          var b4V3 = point2V3_; // 函数曲线

          function fx(xN) {
            return bV3.clone().multiplyScalar(Math.pow(xN, 3)).add(b2V3.clone().multiplyScalar(Math.pow(xN, 2))).add(b3V3.clone().multiplyScalar(xN)).add(b4V3.clone());
          } // 曲线长度变化率,用于匀速曲线运动


          function ds(xN) {
            var derV3 = bV3.clone().multiplyScalar(3 * Math.pow(xN, 2)).add(b2V3.clone().multiplyScalar(2 * xN)).add(b3V3.clone());
            return Math.sqrt(Math.pow(derV3.x, 2) + Math.pow(derV3.y, 2) + Math.pow(derV3.z, 2));
          }

          return [fx, ds];
        }
        /**
         * 获取曲线上某点的位置
         * @param posN_ min: 0, max: 1
         */
        ;

        _proto.point = function point(posN_) {
          var _this = this;

          var posN = posN_;

          if (this._pointV3S.length < 2) {
            return null;
          }

          if (posN < 0 || posN > 1) {
            posN = posN < 0 ? 0 : 1;
          } // 首个和最后点直接返回


          if (posN === 0) {
            return this._pointV3S[0];
          } else if (posN === 1) {
            return this._pointV3S[this._pointV3S.length - 1];
          }

          var resultV3 = v3();
          var indexN = this._pointV3S.length - 1;

          this._pointV3S.forEach(function (v, kS) {
            if (!kS) {
              resultV3.x += v.x * Math.pow(1 - posN, indexN - kS) * Math.pow(posN, kS);
              resultV3.y += v.y * Math.pow(1 - posN, indexN - kS) * Math.pow(posN, kS);
              resultV3.z += v.z * Math.pow(1 - posN, indexN - kS) * Math.pow(posN, kS);
            } else {
              resultV3.x += _this._factorial(indexN) / _this._factorial(kS) / _this._factorial(indexN - kS) * v.x * Math.pow(1 - posN, indexN - kS) * Math.pow(posN, kS);
              resultV3.y += _this._factorial(indexN) / _this._factorial(kS) / _this._factorial(indexN - kS) * v.y * Math.pow(1 - posN, indexN - kS) * Math.pow(posN, kS);
              resultV3.z += _this._factorial(indexN) / _this._factorial(kS) / _this._factorial(indexN - kS) * v.z * Math.pow(1 - posN, indexN - kS) * Math.pow(posN, kS);
            }
          });

          return resultV3;
        }
        /** 匀速点 */
        ;

        _proto.uniformPoint = function uniformPoint(posN_) {
          var posN = posN_;

          if (this._pointV3S.length < 2) {
            return null;
          }

          if (posN < 0 || posN > 1) {
            posN = posN < 0 ? 0 : 1;
          } // 首个和最后点直接返回


          if (posN === 0) {
            return this._pointV3S[0];
          } else if (posN === 1) {
            return this._pointV3S[this._pointV3S.length - 1];
          } // 平均距离


          var averDistN = posN * this._distanceNS[this._pointV3S.length - 2];
          var indexN = 0;
          var beyondN = 0;
          var percentN = 0;

          for (var kN = 0; kN < this._pointV3S.length - 1; kN++) {
            if (averDistN < this._distanceNS[kN]) {
              var preDis = kN === 0 ? 0 : this._distanceNS[kN - 1];
              indexN = kN;
              beyondN = averDistN - preDis;
              percentN = beyondN / (this._distanceNS[kN] - preDis);
              break;
            }
          } // 牛顿切线法求根


          var aN = percentN;
          var bN; // 最多迭代6次

          for (var i = 0; i < 6; i++) {
            var actualLen = this._gaussLegendre(this._funcFSS[indexN][1], 0, aN);

            bN = aN - (actualLen - beyondN) / this._funcFSS[indexN][1](aN);

            if (Math.abs(aN - bN) < 0.0001) {
              break;
            }

            aN = bN;
          }

          percentN = bN;
          return this._funcFSS[indexN][0](percentN);
        };

        _createClass(BezierCurve, [{
          key: "pointV3S",
          get:
          /* --------------- public --------------- */

          /** 控制点 */
          function get() {
            return this._pointV3S;
          },
          set: function set(valueV3S) {
            this._pointV3S = valueV3S;

            this._resetData();
          }
        }]);

        return BezierCurve;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/BezierCurveAnimation.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './BezierCurve.ts', './BezierCurveAnimationTweenUnit.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, EventHandler, _decorator, easing, tween, ccmodifier, BaseComponent, BezierCurve, BezierCurveAnimationTweenUnit, easingEnum;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      EventHandler = module.EventHandler;
      _decorator = module._decorator;
      easing = module.easing;
      tween = module.tween;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      BezierCurve = module.default;
    }, function (module) {
      BezierCurveAnimationTweenUnit = module.BezierCurveAnimationTweenUnit;
      easingEnum = module.easingEnum;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4;

      cclegacy._RF.push({}, "a882cWsty1LuLZvP+uB62z9", "BezierCurveAnimation", undefined);

      var _cc$_decorator = _decorator,
          ccclass = _cc$_decorator.ccclass,
          property = _cc$_decorator.property,
          help = _cc$_decorator.help;
      /** 贝塞尔曲线通用动画组件 */

      var BezierCurveAnimation = exports('BezierCurveAnimation', (_dec = ccclass('BezierCurveAnimation'), _dec2 = ccmodifier('BezierCurveAnimation'), _dec3 = help('https://www.desmos.com/calculator/cahqdxeshd?lang=zh-CN'), _dec4 = property({
        displayName: '缓动单元',
        type: [BezierCurveAnimationTweenUnit]
      }), _dec5 = property({
        displayName: '缓动切换事件',
        tooltip: '(当前缓动下标_indexN)',
        type: [EventHandler]
      }), _dec6 = property({
        displayName: '更新事件',
        tooltip: '(当前缓动曲线Y_yN, 当前缓动下标_indexN, 总曲线Y_yN)',
        type: [EventHandler]
      }), _dec7 = property({
        displayName: '结束事件',
        type: [EventHandler]
      }), _dec(_class = _dec2(_class = _dec3(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(BezierCurveAnimation, _BaseComponent);

        function BezierCurveAnimation() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          /* --------------- 属性 --------------- */

          /** 缓动单元 */

          _initializerDefineProperty(_this, "tweenUnitAS", _descriptor, _assertThisInitialized(_this));
          /** 缓动切换事件 */


          _initializerDefineProperty(_this, "tweenSwitchEventAS", _descriptor2, _assertThisInitialized(_this));
          /** 更新事件 */


          _initializerDefineProperty(_this, "updateEventAS", _descriptor3, _assertThisInitialized(_this));
          /** 结束事件 */


          _initializerDefineProperty(_this, "endEventAS", _descriptor4, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = BezierCurveAnimation.prototype;
        /* --------------- private --------------- */

        /* ------------------------------- 功能 ------------------------------- */

        /** 触发事件 */

        _proto.emit = function emit(eventKey_) {
          for (var _len2 = arguments.length, argsAS_ = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
            argsAS_[_key2 - 1] = arguments[_key2];
          }

          var eventAS = this[eventKey_];

          if (!eventAS) {
            return;
          }

          eventAS.forEach(function (v) {
            v.emit(argsAS_);
          });
        }
        /**
         * 开始缓动
         * @param tweenIndex_ 指定缓动或缓动队列
         * @returns
         */
        ;

        _proto.startTween = function startTween(tweenIndex_) {
          var _this2 = this;
          /** 缓动队列 */


          var tweenUnitAs = this.tweenUnitAS; // 获取缓动队列

          if (tweenIndex_ !== undefined) {
            if (typeof tweenIndex_ === 'number') {
              tweenUnitAs = tweenUnitAs.slice(tweenIndex_, 1);
            } else {
              tweenUnitAs = [];
              tweenIndex_.forEach(function (vN) {
                tweenUnitAs.push(_this2.tweenUnitAS[vN]);
              });
            }

            tweenUnitAs = tweenUnitAs.filter(function (v) {
              return Boolean(v);
            });
          }

          if (!tweenUnitAs.length) {
            return null;
          }
          /** 总时间（秒） */


          var totalTimeSN = tweenUnitAs.reduce(function (preValue, currValue) {
            return preValue + currValue.timeSN;
          }, 0);
          /** 时间占比 */

          var timeRatioNs = [];
          {
            var currN = 0;
            tweenUnitAs.forEach(function (v, kN) {
              var ratioN = v.timeSN / totalTimeSN;
              currN += ratioN;
              timeRatioNs.push(currN);
            });
          }
          /** 曲线函数 */

          var curveFS = tweenUnitAs.map(function (v) {
            if (v.customCurveB) {
              var curve = new BezierCurve(v.controlPointV3S);
              return function (kN) {
                return curve.point(kN).y;
              };
            } else {
              return easing[easingEnum[v.easing]].bind(easing);
            }
          });
          /** 上次缓动下标 */

          var lastTweenIndexN = 0;
          /** 缓动对象 */

          var tweenTarget = {
            valueN: 0
          };
          /** 缓动 */

          var tween$1 = tween(tweenTarget).to(totalTimeSN, {
            valueN: 1
          }, {
            onUpdate: function onUpdate(target, ratioN) {
              /** 当前缓动下标 */
              var tweenIndexN = timeRatioNs.findIndex(function (vN) {
                return ratioN <= vN;
              });

              if (tweenIndexN === -1) {
                return;
              }
              /** 上个时间占比 */


              var lastTimeRatioN = tweenIndexN ? timeRatioNs[tweenIndexN - 1] : 0;
              /** 当前时间范围 */

              var timeRangeN = timeRatioNs[tweenIndexN] - lastTimeRatioN;
              /** 曲线位置 */

              var posN = (ratioN - lastTimeRatioN) / timeRangeN;
              /** 当前曲线 Y */

              var currCurveYN = curveFS[tweenIndexN](posN);
              /** 总曲线 Y */

              var totalCurveYN = currCurveYN * timeRangeN + lastTimeRatioN; // 缓动切换事件触发

              if (lastTweenIndexN !== tweenIndexN) {
                _this2.emit('tweenSwitchEventAS', lastTweenIndexN);
              } // 更新事件触发


              _this2.emit('updateEventAS', currCurveYN, tweenIndexN, totalCurveYN); // 更新缓动下标


              lastTweenIndexN = tweenIndexN;
            }
          }).call(function () {
            // 结束事件触发
            _this2.emit('endEventAS');
          }).start();
          return tween$1;
        }
        /**
         * 获取曲线 Y
         * @param ratioN_ 进度
         * @param tweenIndex_ 指定缓动或缓动队列
         * @returns
         */
        ;

        _proto.getCurveY = function getCurveY(ratioN_, tweenIndex_) {
          var _this3 = this;
          /** 缓动队列 */


          var tweenUnitAs = this.tweenUnitAS; // 获取缓动队列

          if (tweenIndex_ !== undefined) {
            if (typeof tweenIndex_ === 'number') {
              tweenUnitAs = tweenUnitAs.slice(tweenIndex_, 1);
            } else {
              tweenUnitAs = [];
              tweenIndex_.forEach(function (vN) {
                tweenUnitAs.push(_this3.tweenUnitAS[vN]);
              });
            }

            tweenUnitAs = tweenUnitAs.filter(function (v) {
              return Boolean(v);
            });
          }

          if (!tweenUnitAs.length) {
            return 0;
          }
          /** 总时间（秒） */


          var totalTimeSN = tweenUnitAs.reduce(function (preValue, currValue) {
            return preValue + currValue.timeSN;
          }, 0);
          /** 时间占比 */

          var timeRatioNs = [];
          {
            var currN = 0;
            tweenUnitAs.forEach(function (v, kN) {
              var ratioN = v.timeSN / totalTimeSN;
              currN += ratioN;
              timeRatioNs.push(currN);
            });
          }
          /** 当前缓动下标 */

          var tweenIndexN = timeRatioNs.findIndex(function (vN) {
            return ratioN_ <= vN;
          });

          if (tweenIndexN === -1) {
            return 0;
          }
          /** 曲线函数 */


          var curveFS = tweenUnitAs.map(function (v) {
            if (v.customCurveB) {
              var curve = new BezierCurve(v.controlPointV3S);
              return function (kN) {
                return curve.point(kN).y;
              };
            } else {
              return easing[easingEnum[v.easing]].bind(easing);
            }
          });
          /** 上个时间占比 */

          var lastTimeRatioN = tweenIndexN ? timeRatioNs[tweenIndexN - 1] : 0;
          /** 当前时间范围 */

          var timeRangeN = timeRatioNs[tweenIndexN] - lastTimeRatioN;
          /** 曲线位置 */

          var posN = (ratioN_ - lastTimeRatioN) / timeRangeN;
          return curveFS[tweenIndexN](posN);
        };

        return BezierCurveAnimation;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "tweenUnitAS", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "tweenSwitchEventAS", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "updateEventAS", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "endEventAS", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      })), _class2)) || _class) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/BezierCurveAnimationTweenUnit.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, easing, Enum, Vec3, _decorator, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      easing = module.easing;
      Enum = module.Enum;
      Vec3 = module.Vec3;
      _decorator = module._decorator;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4;

      cclegacy._RF.push({}, "c0676/xhQRMi50JKuHT7luq", "BezierCurveAnimationTweenUnit", undefined);

      var _cc$_decorator = _decorator,
          ccclass = _cc$_decorator.ccclass,
          property = _cc$_decorator.property,
          help = _cc$_decorator.help;
      /** 缓动枚举 */

      var easingEnum = exports('easingEnum', {});
      {
        var tempN = 0;

        for (var kS in easing) {
          if (Object.prototype.hasOwnProperty.call(easing, kS)) {
            easingEnum[kS] = tempN;
            easingEnum[tempN] = kS;
            tempN++;
          }
        }
      }
      /** 缓动单元 */

      var BezierCurveAnimationTweenUnit = exports('BezierCurveAnimationTweenUnit', (_dec = ccclass('BezierCurveAnimationTweenUnit'), _dec2 = ccmodifier('BezierCurveAnimationTweenUnit'), _dec3 = property({
        displayName: '自定义缓动曲线'
      }), _dec4 = property({
        displayName: '缓动曲线',
        type: Enum(easingEnum),
        visible: function visible() {
          return !this.customCurveB;
        }
      }), _dec5 = property({
        displayName: '控制点',
        type: [Vec3],
        visible: function visible() {
          return this.customCurveB;
        }
      }), _dec6 = property({
        displayName: '时间（秒）'
      }), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(BezierCurveAnimationTweenUnit, _BaseComponent);

        function BezierCurveAnimationTweenUnit() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          /* --------------- 属性 --------------- */

          /** 自定义缓动曲线 */

          _initializerDefineProperty(_this, "customCurveB", _descriptor, _assertThisInitialized(_this));
          /** 缓动曲线 */


          _initializerDefineProperty(_this, "easing", _descriptor2, _assertThisInitialized(_this));
          /** 缓动控制点 */


          _initializerDefineProperty(_this, "controlPointV3S", _descriptor3, _assertThisInitialized(_this));
          /** 时间（秒） */


          _initializerDefineProperty(_this, "timeSN", _descriptor4, _assertThisInitialized(_this));

          return _this;
        }

        return BezierCurveAnimationTweenUnit;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "customCurveB", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return false;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "easing", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 0;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "controlPointV3S", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "timeSN", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 0;
        }
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/BoneOfGirl.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './RoleBone.ts', './RoleBoneOfNvhai.ts'], function (exports) {
  var _inheritsLoose, cclegacy, _decorator, ccmodifier, RoleBone, RoleBoneOfNvhai;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
    }, function (module) {
      ccmodifier = module.ccmodifier;
    }, function (module) {
      RoleBone = module.RoleBone;
    }, function (module) {
      RoleBoneOfNvhai = module.RoleBoneOfNvhai;
    }],
    execute: function () {
      var _dec, _dec2, _class;

      cclegacy._RF.push({}, "de12fdm9dFGSYV6Gu4ZLXVa", "BoneOfGirl", undefined);

      var ccclass = _decorator.ccclass;
      var BoneOfGirl = exports('BoneOfGirl', (_dec = ccclass('BoneOfGirl'), _dec2 = ccmodifier('BoneOfGirl'), _dec(_class = _dec2(_class = /*#__PURE__*/function (_RoleBone) {
        _inheritsLoose(BoneOfGirl, _RoleBone);

        function BoneOfGirl() {
          return _RoleBone.apply(this, arguments) || this;
        }

        var _proto = BoneOfGirl.prototype;

        _proto.start = function start() {
          this.init(RoleBoneOfNvhai);
        };

        return BoneOfGirl;
      }(RoleBone)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ButtonChangeAni.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './BoneOfGirl.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, ccmodifier, BaseComponent, BoneOfGirl;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      BoneOfGirl = module.BoneOfGirl;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class, _class2, _descriptor;

      cclegacy._RF.push({}, "21a752w41hLJqZApqLh0V85", "ButtonChangeAni", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var ButtonChangeAni = exports('ButtonChangeAni', (_dec = ccclass('ButtonChangeAni'), _dec2 = ccmodifier('ButtonChangeAni'), _dec3 = property(BoneOfGirl), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(ButtonChangeAni, _BaseComponent);

        function ButtonChangeAni() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          _this.animations = ['idle', 'run'];

          _initializerDefineProperty(_this, "girl", _descriptor, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = ButtonChangeAni.prototype;

        _proto.start = function start() {
          this.onNext();
        };

        _proto.onNext = function onNext() {
          var ani = this.animations.shift();
          this.animations.push(ani);
          this.girl.changeAnimation(ani);
        };

        return ButtonChangeAni;
      }(BaseComponent), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "girl", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ButtonChangeDir.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './BoneOfGirl.ts', './RoleBoneExports.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, ccmodifier, BaseComponent, BoneOfGirl, DirectionType;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      BoneOfGirl = module.BoneOfGirl;
    }, function (module) {
      DirectionType = module.DirectionType;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class, _class2, _descriptor;

      cclegacy._RF.push({}, "78e50cVWnBF+ZoelXWftUEI", "ButtonChangeDir", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var ButtonChangeDir = exports('ButtonChangeDir', (_dec = ccclass('ButtonChangeDir'), _dec2 = ccmodifier('ButtonChangeDir'), _dec3 = property(BoneOfGirl), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(ButtonChangeDir, _BaseComponent);

        function ButtonChangeDir() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          _this.dirs = [DirectionType.Shang, DirectionType.YouShang, DirectionType.You, DirectionType.YouXia, DirectionType.Xia, DirectionType.ZuoXia, DirectionType.Zuo, DirectionType.ZuoShang];

          _initializerDefineProperty(_this, "girl", _descriptor, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = ButtonChangeDir.prototype;

        _proto.start = function start() {
          this.onNext();
        };

        _proto.onNext = function onNext() {
          var dir = this.dirs.shift();
          this.dirs.push(dir);
          this.girl.changeDiretion(dir);
        };

        return ButtonChangeDir;
      }(BaseComponent), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "girl", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ButtonDecorator.ts", ['cc', './Logger.ts'], function (exports) {
  var cclegacy, js, logger;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
      js = module.js;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      cclegacy._RF.push({}, "818a1O3whNFlp7O0zasSvzQ", "ButtonDecorator", undefined);
      /** 点击信息 */

      /**
       * Button点击事件装饰器
       */


      var ButtonDecorator = /*#__PURE__*/function () {
        function ButtonDecorator() {
          /** 按钮注册信息 */
          this.__btnInfoMap = {};
          this.__btnFunctions = [];
        }

        var _proto = ButtonDecorator.prototype;
        /**
         * 按钮点击事件注册
         * @param func 按钮点击回调
         * @param clickDelay 点击间隔
         * @param sound 音量
         * @returns
         */

        _proto.inject = function inject(func, clickDelay, sound) {
          var _this = this;

          if (clickDelay === void 0) {
            clickDelay = 500;
          }

          return function (ui, key) {
            _this.add(ui, key, {
              func: func,
              clickDelay: clickDelay,
              sound: sound
            });
          };
        }
        /**
         * 添加按钮注册信息
         * @param ui 所属 UI
         * @param btnName 按钮名称
         * @param clickInfo 按钮注册信息
         */
        ;

        _proto.add = function add(ui, btnName, clickInfo) {
          var _this2 = this; // TS 装饰器执行顺序：属性装饰器，方法装饰器，方法参数装饰器，类装饰器；这决定了这个做法会有问题，
          // 因为在构建后脚本被混淆了，此时类装饰器还没有注入，而方法装饰器先行，就会获取不到正确的类名。正
          // 确的做法应该要修改组件的 onLoad 方法，在 onLoad 中执行按钮回调初始化。
          // 这是你的作业哦~


          this.__btnFunctions.push({
            ui: ui,
            fn: function fn() {
              var name = js.getClassName(ui);
              _this2.__btnInfoMap[name] = _this2.__btnInfoMap[name] || {};
              _this2.__btnInfoMap[name][btnName] = clickInfo;
              logger.i(name + " \u6CE8\u518C\u6309\u94AE " + btnName, clickInfo, ui);
            }
          });
        }
        /**
         * 获取按钮注册信息
         * @param ui 所属 UI
         * @returns
         */
        ;

        _proto.get = function get(ui) {
          for (var i = 0; i < this.__btnFunctions.length; i++) {
            if (this.__btnFunctions[i].ui.name === ui) {
              this.__btnFunctions[i].fn();
            }
          }

          return this.__btnInfoMap[ui];
        }
        /**
         * 移除按钮注册信息
         * @param ui 所属 UI
         * @returns
         */
        ;

        _proto.del = function del(ui) {
          logger.log("\u9500\u6BC1 " + ui + " \u6CE8\u518C\u7684\u6309\u94AE\u4E8B\u4EF6");
          delete this.__btnInfoMap[ui];
        };

        return ButtonDecorator;
      }();

      var buttonDecorator = exports('buttonDecorator', new ButtonDecorator());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ByteArray.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _createClass, cclegacy, errorID;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      errorID = module.errorID;
    }],
    execute: function () {
      cclegacy._RF.push({}, "43188wNqd5FOqDjVq646RxN", "ByteArray", undefined);

      var ENDIAN = exports('ENDIAN', {
        LITTLE_ENDIAN: 0,
        BIG_ENDIAN: 1
      });
      var ByteArraySize = {
        SIZE_OF_BOOLEAN: 1,
        SIZE_OF_INT8: 1,
        SIZE_OF_INT16: 2,
        SIZE_OF_INT32: 4,
        SIZE_OF_UINT8: 1,
        SIZE_OF_UINT16: 2,
        SIZE_OF_UINT32: 4,
        SIZE_OF_FLOAT32: 4,
        SIZE_OF_FLOAT64: 8
      };
      /**
       * The ByteArray class provides methods and attributes for optimized reading and writing as well as dealing with binary data.
       * Note: The ByteArray class is applied to the advanced developers who need to access data at the byte layer.
       * @platform Web,Native
       * @language en_US
       */

      /**
       * ByteArray 类提供用于优化读取、写入以及处理二进制数据的方法和属性。
       * 注意：ByteArray 类适用于需要在字节层访问数据的高级开发人员。
       * @platform Web,Native
       * @language zh_CN
       */

      var ByteArray = exports('ByteArray', /*#__PURE__*/function () {
        /**
         * @platform Web,Native
         */
        function ByteArray(buffer, bufferExtSize) {
          if (bufferExtSize === void 0) {
            bufferExtSize = 0;
          }
          /**
           * @private
           */


          this.bufferExtSize = 0; //Buffer expansion size

          this.data = void 0;
          this._bytes = void 0;
          /**
           * @private
           */

          this._position = void 0;
          /**
           * 
           * 已经使用的字节偏移量
           * @protected
           * @type {number}
           * @memberOf ByteArray
           */

          this.write_position = void 0;
          this.$endian = void 0;
          /**
           * @private
           */

          this.EOF_byte = -1;
          /**
           * @private
           */

          this.EOF_code_point = -1;

          if (bufferExtSize < 0) {
            bufferExtSize = 0;
          }

          this.bufferExtSize = bufferExtSize;
          var bytes,
              wpos = 0;

          if (buffer) {
            //有数据，则可写字节数从字节尾开始
            var uint8;

            if (buffer instanceof Uint8Array) {
              uint8 = buffer;
              wpos = buffer.length;
            } else {
              wpos = buffer.byteLength;
              uint8 = new Uint8Array(buffer);
            }

            if (bufferExtSize == 0) {
              bytes = new Uint8Array(wpos);
            } else {
              var multi = (wpos / bufferExtSize | 0) + 1;
              bytes = new Uint8Array(multi * bufferExtSize);
            }

            bytes.set(uint8);
          } else {
            bytes = new Uint8Array(bufferExtSize);
          }

          this.write_position = wpos;
          this._position = 0;
          this._bytes = bytes;
          this.data = new DataView(bytes.buffer);
          this.endian = ENDIAN.BIG_ENDIAN;
        }
        /**
         * @deprecated
         * @platform Web,Native
         */


        var _proto = ByteArray.prototype;

        _proto.setArrayBuffer = function setArrayBuffer(buffer) {}
        /**
         * 可读的剩余字节数
         * 
         * @returns 
         * 
         * @memberOf ByteArray
         */
        ;

        _proto._validateBuffer = function _validateBuffer(value) {
          if (this.data.byteLength < value) {
            var be = this.bufferExtSize;
            var tmp;

            if (be == 0) {
              tmp = new Uint8Array(value);
            } else {
              var nLen = ((value / be >> 0) + 1) * be;
              tmp = new Uint8Array(nLen);
            }

            tmp.set(this._bytes);
            this._bytes = tmp;
            this.data = new DataView(tmp.buffer);
          }
        }
        /**
         * The number of bytes that can be read from the current position of the byte array to the end of the array data.
         * When you access a ByteArray object, the bytesAvailable property in conjunction with the read methods each use to make sure you are reading valid data.
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 可从字节数组的当前位置到数组末尾读取的数据的字节数。
         * 每次访问 ByteArray 对象时，将 bytesAvailable 属性与读取方法结合使用，以确保读取有效的数据。
         * @platform Web,Native
         * @language zh_CN
         */
        ;
        /**
         * Clears the contents of the byte array and resets the length and position properties to 0.
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 清除字节数组的内容，并将 length 和 position 属性重置为 0。
         * @platform Web,Native
         * @language zh_CN
         */


        _proto.clear = function clear() {
          var buffer = new ArrayBuffer(this.bufferExtSize);
          this.data = new DataView(buffer);
          this._bytes = new Uint8Array(buffer);
          this._position = 0;
          this.write_position = 0;
        }
        /**
         * Read a Boolean value from the byte stream. Read a simple byte. If the byte is non-zero, it returns true; otherwise, it returns false.
         * @return If the byte is non-zero, it returns true; otherwise, it returns false.
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取布尔值。读取单个字节，如果字节非零，则返回 true，否则返回 false
         * @return 如果字节不为零，则返回 true，否则返回 false
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readBoolean = function readBoolean() {
          if (this.validate(ByteArraySize.SIZE_OF_BOOLEAN)) return !!this._bytes[this.position++];
        }
        /**
         * Read signed bytes from the byte stream.
         * @return An integer ranging from -128 to 127
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取带符号的字节
         * @return 介于 -128 和 127 之间的整数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readByte = function readByte() {
          if (this.validate(ByteArraySize.SIZE_OF_INT8)) return this.data.getInt8(this.position++);
        }
        /**
         * Read data byte number specified by the length parameter from the byte stream. Starting from the position specified by offset, read bytes into the ByteArray object specified by the bytes parameter, and write bytes into the target ByteArray
         * @param bytes ByteArray object that data is read into
         * @param offset Offset (position) in bytes. Read data should be written from this position
         * @param length Byte number to be read Default value 0 indicates reading all available data
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取 length 参数指定的数据字节数。从 offset 指定的位置开始，将字节读入 bytes 参数指定的 ByteArray 对象中，并将字节写入目标 ByteArray 中
         * @param bytes 要将数据读入的 ByteArray 对象
         * @param offset bytes 中的偏移（位置），应从该位置写入读取的数据
         * @param length 要读取的字节数。默认值 0 导致读取所有可用的数据
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readBytes = function readBytes(bytes, offset, length) {
          if (offset === void 0) {
            offset = 0;
          }

          if (length === void 0) {
            length = 0;
          }

          if (!bytes) {
            //由于bytes不返回，所以new新的无意义
            return;
          }

          var pos = this._position;
          var available = this.write_position - pos;

          if (available < 0) {
            errorID(1025);
            return;
          }

          if (length == 0) {
            length = available;
          } else if (length > available) {
            errorID(1025);
            return;
          }

          var position = bytes._position;
          bytes._position = 0;
          bytes.validateBuffer(offset + length);
          bytes._position = position;

          bytes._bytes.set(this._bytes.subarray(pos, pos + length), offset);

          this.position += length;
        }
        /**
         * Read an IEEE 754 double-precision (64 bit) floating point number from the byte stream
         * @return Double-precision (64 bit) floating point number
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取一个 IEEE 754 双精度（64 位）浮点数
         * @return 双精度（64 位）浮点数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readDouble = function readDouble() {
          if (this.validate(ByteArraySize.SIZE_OF_FLOAT64)) {
            var value = this.data.getFloat64(this._position, this.$endian == ENDIAN.LITTLE_ENDIAN);
            this.position += ByteArraySize.SIZE_OF_FLOAT64;
            return value;
          }
        }
        /**
         * Read an IEEE 754 single-precision (32 bit) floating point number from the byte stream
         * @return Single-precision (32 bit) floating point number
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取一个 IEEE 754 单精度（32 位）浮点数
         * @return 单精度（32 位）浮点数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readFloat = function readFloat() {
          if (this.validate(ByteArraySize.SIZE_OF_FLOAT32)) {
            var value = this.data.getFloat32(this._position, this.$endian == ENDIAN.LITTLE_ENDIAN);
            this.position += ByteArraySize.SIZE_OF_FLOAT32;
            return value;
          }
        }
        /**
         * Read a 32-bit signed integer from the byte stream.
         * @return A 32-bit signed integer ranging from -2147483648 to 2147483647
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取一个带符号的 32 位整数
         * @return 介于 -2147483648 和 2147483647 之间的 32 位带符号整数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readInt = function readInt() {
          if (this.validate(ByteArraySize.SIZE_OF_INT32)) {
            var value = this.data.getInt32(this._position, this.$endian == ENDIAN.LITTLE_ENDIAN);
            this.position += ByteArraySize.SIZE_OF_INT32;
            return value;
          }
        }
        /**
         * Read a 16-bit signed integer from the byte stream.
         * @return A 16-bit signed integer ranging from -32768 to 32767
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取一个带符号的 16 位整数
         * @return 介于 -32768 和 32767 之间的 16 位带符号整数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readShort = function readShort() {
          if (this.validate(ByteArraySize.SIZE_OF_INT16)) {
            var value = this.data.getInt16(this._position, this.$endian == ENDIAN.LITTLE_ENDIAN);
            this.position += ByteArraySize.SIZE_OF_INT16;
            return value;
          }
        }
        /**
         * Read unsigned bytes from the byte stream.
         * @return A unsigned integer ranging from 0 to 255
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取无符号的字节
         * @return 介于 0 和 255 之间的无符号整数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readUnsignedByte = function readUnsignedByte() {
          if (this.validate(ByteArraySize.SIZE_OF_UINT8)) return this._bytes[this.position++];
        }
        /**
         * Read a 32-bit unsigned integer from the byte stream.
         * @return A 32-bit unsigned integer ranging from 0 to 4294967295
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取一个无符号的 32 位整数
         * @return 介于 0 和 4294967295 之间的 32 位无符号整数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readUnsignedInt = function readUnsignedInt() {
          if (this.validate(ByteArraySize.SIZE_OF_UINT32)) {
            var value = this.data.getUint32(this._position, this.$endian == ENDIAN.LITTLE_ENDIAN);
            this.position += ByteArraySize.SIZE_OF_UINT32;
            return value;
          }
        }
        /**
         * Read a 16-bit unsigned integer from the byte stream.
         * @return A 16-bit unsigned integer ranging from 0 to 65535
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取一个无符号的 16 位整数
         * @return 介于 0 和 65535 之间的 16 位无符号整数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readUnsignedShort = function readUnsignedShort() {
          if (this.validate(ByteArraySize.SIZE_OF_UINT16)) {
            var value = this.data.getUint16(this._position, this.$endian == ENDIAN.LITTLE_ENDIAN);
            this.position += ByteArraySize.SIZE_OF_UINT16;
            return value;
          }
        }
        /**
         * Read a UTF-8 character string from the byte stream Assume that the prefix of the character string is a short unsigned integer (use byte to express length)
         * @return UTF-8 character string
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取一个 UTF-8 字符串。假定字符串的前缀是无符号的短整型（以字节表示长度）
         * @return UTF-8 编码的字符串
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readUTF = function readUTF() {
          var length = this.readUnsignedShort();

          if (length > 0) {
            return this.readUTFBytes(length);
          } else {
            return "";
          }
        }
        /**
         * Read a UTF-8 byte sequence specified by the length parameter from the byte stream, and then return a character string
         * @param Specify a short unsigned integer of the UTF-8 byte length
         * @return A character string consists of UTF-8 bytes of the specified length
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 从字节流中读取一个由 length 参数指定的 UTF-8 字节序列，并返回一个字符串
         * @param length 指明 UTF-8 字节长度的无符号短整型数
         * @return 由指定长度的 UTF-8 字节组成的字符串
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readUTFBytes = function readUTFBytes(length) {
          if (!this.validate(length)) {
            return;
          }

          var data = this.data;
          var bytes = new Uint8Array(data.buffer, data.byteOffset + this._position, length);
          this.position += length;
          return this.decodeUTF8(bytes);
        }
        /**
         * Write a Boolean value. A single byte is written according to the value parameter. If the value is true, write 1; if the value is false, write 0.
         * @param value A Boolean value determining which byte is written. If the value is true, write 1; if the value is false, write 0.
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 写入布尔值。根据 value 参数写入单个字节。如果为 true，则写入 1，如果为 false，则写入 0
         * @param value 确定写入哪个字节的布尔值。如果该参数为 true，则该方法写入 1；如果该参数为 false，则该方法写入 0
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeBoolean = function writeBoolean(value) {
          this.validateBuffer(ByteArraySize.SIZE_OF_BOOLEAN);
          this._bytes[this.position++] = +value;
        }
        /**
         * Write a byte into the byte stream
         * The low 8 bits of the parameter are used. The high 24 bits are ignored.
         * @param value A 32-bit integer. The low 8 bits will be written into the byte stream
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 在字节流中写入一个字节
         * 使用参数的低 8 位。忽略高 24 位
         * @param value 一个 32 位整数。低 8 位将被写入字节流
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeByte = function writeByte(value) {
          this.validateBuffer(ByteArraySize.SIZE_OF_INT8);
          this._bytes[this.position++] = value & 0xff;
        }
        /**
         * Write the byte sequence that includes length bytes in the specified byte array, bytes, (starting at the byte specified by offset, using a zero-based index), into the byte stream
         * If the length parameter is omitted, the default length value 0 is used and the entire buffer starting at offset is written. If the offset parameter is also omitted, the entire buffer is written
         * If the offset or length parameter is out of range, they are clamped to the beginning and end of the bytes array.
         * @param bytes ByteArray Object
         * @param offset A zero-based index specifying the position into the array to begin writing
         * @param length An unsigned integer specifying how far into the buffer to write
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 将指定字节数组 bytes（起始偏移量为 offset，从零开始的索引）中包含 length 个字节的字节序列写入字节流
         * 如果省略 length 参数，则使用默认长度 0；该方法将从 offset 开始写入整个缓冲区。如果还省略了 offset 参数，则写入整个缓冲区
         * 如果 offset 或 length 超出范围，它们将被锁定到 bytes 数组的开头和结尾
         * @param bytes ByteArray 对象
         * @param offset 从 0 开始的索引，表示在数组中开始写入的位置
         * @param length 一个无符号整数，表示在缓冲区中的写入范围
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeBytes = function writeBytes(bytes, offset, length) {
          if (offset === void 0) {
            offset = 0;
          }

          if (length === void 0) {
            length = 0;
          }

          var writeLength;

          if (offset < 0) {
            return;
          }

          if (length < 0) {
            return;
          } else if (length == 0) {
            writeLength = bytes.length - offset;
          } else {
            writeLength = Math.min(bytes.length - offset, length);
          }

          if (writeLength > 0) {
            this.validateBuffer(writeLength);

            this._bytes.set(bytes._bytes.subarray(offset, offset + writeLength), this._position);

            this.position = this._position + writeLength;
          }
        }
        /**
         * Write an IEEE 754 double-precision (64 bit) floating point number into the byte stream
         * @param value Double-precision (64 bit) floating point number
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 在字节流中写入一个 IEEE 754 双精度（64 位）浮点数
         * @param value 双精度（64 位）浮点数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeDouble = function writeDouble(value) {
          this.validateBuffer(ByteArraySize.SIZE_OF_FLOAT64);
          this.data.setFloat64(this._position, value, this.$endian == ENDIAN.LITTLE_ENDIAN);
          this.position += ByteArraySize.SIZE_OF_FLOAT64;
        }
        /**
         * Write an IEEE 754 single-precision (32 bit) floating point number into the byte stream
         * @param value Single-precision (32 bit) floating point number
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 在字节流中写入一个 IEEE 754 单精度（32 位）浮点数
         * @param value 单精度（32 位）浮点数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeFloat = function writeFloat(value) {
          this.validateBuffer(ByteArraySize.SIZE_OF_FLOAT32);
          this.data.setFloat32(this._position, value, this.$endian == ENDIAN.LITTLE_ENDIAN);
          this.position += ByteArraySize.SIZE_OF_FLOAT32;
        }
        /**
         * Write a 32-bit signed integer into the byte stream
         * @param value An integer to be written into the byte stream
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 在字节流中写入一个带符号的 32 位整数
         * @param value 要写入字节流的整数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeInt = function writeInt(value) {
          this.validateBuffer(ByteArraySize.SIZE_OF_INT32);
          this.data.setInt32(this._position, value, this.$endian == ENDIAN.LITTLE_ENDIAN);
          this.position += ByteArraySize.SIZE_OF_INT32;
        }
        /**
         * Write a 16-bit integer into the byte stream. The low 16 bits of the parameter are used. The high 16 bits are ignored.
         * @param value A 32-bit integer. Its low 16 bits will be written into the byte stream
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 在字节流中写入一个 16 位整数。使用参数的低 16 位。忽略高 16 位
         * @param value 32 位整数，该整数的低 16 位将被写入字节流
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeShort = function writeShort(value) {
          this.validateBuffer(ByteArraySize.SIZE_OF_INT16);
          this.data.setInt16(this._position, value, this.$endian == ENDIAN.LITTLE_ENDIAN);
          this.position += ByteArraySize.SIZE_OF_INT16;
        }
        /**
         * Write a 32-bit unsigned integer into the byte stream
         * @param value An unsigned integer to be written into the byte stream
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 在字节流中写入一个无符号的 32 位整数
         * @param value 要写入字节流的无符号整数
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeUnsignedInt = function writeUnsignedInt(value) {
          this.validateBuffer(ByteArraySize.SIZE_OF_UINT32);
          this.data.setUint32(this._position, value, this.$endian == ENDIAN.LITTLE_ENDIAN);
          this.position += ByteArraySize.SIZE_OF_UINT32;
        }
        /**
         * Write a 16-bit unsigned integer into the byte stream
         * @param value An unsigned integer to be written into the byte stream
         * @version Egret 2.5
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 在字节流中写入一个无符号的 16 位整数
         * @param value 要写入字节流的无符号整数
         * @version Egret 2.5
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeUnsignedShort = function writeUnsignedShort(value) {
          this.validateBuffer(ByteArraySize.SIZE_OF_UINT16);
          this.data.setUint16(this._position, value, this.$endian == ENDIAN.LITTLE_ENDIAN);
          this.position += ByteArraySize.SIZE_OF_UINT16;
        }
        /**
         * Write a UTF-8 string into the byte stream. The length of the UTF-8 string in bytes is written first, as a 16-bit integer, followed by the bytes representing the characters of the string
         * @param value Character string value to be written
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 将 UTF-8 字符串写入字节流。先写入以字节表示的 UTF-8 字符串长度（作为 16 位整数），然后写入表示字符串字符的字节
         * @param value 要写入的字符串值
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeUTF = function writeUTF(value) {
          var utf8bytes = this.encodeUTF8(value);
          var length = utf8bytes.length;
          this.validateBuffer(ByteArraySize.SIZE_OF_UINT16 + length);
          this.data.setUint16(this._position, length, this.$endian == ENDIAN.LITTLE_ENDIAN);
          this.position += ByteArraySize.SIZE_OF_UINT16;

          this._writeUint8Array(utf8bytes, false);
        }
        /**
         * Write a UTF-8 string into the byte stream. Similar to the writeUTF() method, but the writeUTFBytes() method does not prefix the string with a 16-bit length word
         * @param value Character string value to be written
         * @platform Web,Native
         * @language en_US
         */

        /**
         * 将 UTF-8 字符串写入字节流。类似于 writeUTF() 方法，但 writeUTFBytes() 不使用 16 位长度的词为字符串添加前缀
         * @param value 要写入的字符串值
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.writeUTFBytes = function writeUTFBytes(value) {
          this._writeUint8Array(this.encodeUTF8(value));
        }
        /**
         *
         * @returns
         * @platform Web,Native
         */
        ;

        _proto.toString = function toString() {
          return "[ByteArray] length:" + this.length + ", bytesAvailable:" + this.bytesAvailable;
        }
        /**
         * @private
         * 将 Uint8Array 写入字节流
         * @param bytes 要写入的Uint8Array
         * @param validateBuffer
         */
        ;

        _proto._writeUint8Array = function _writeUint8Array(bytes, validateBuffer) {
          if (validateBuffer === void 0) {
            validateBuffer = true;
          }

          var pos = this._position;
          var npos = pos + bytes.length;

          if (validateBuffer) {
            this.validateBuffer(npos);
          }

          this.bytes.set(bytes, pos);
          this.position = npos;
        }
        /**
         * @param len
         * @returns
         * @platform Web,Native
         * @private
         */
        ;

        _proto.validate = function validate(len) {
          var bl = this._bytes.length;

          if (bl > 0 && this._position + len <= bl) {
            return true;
          } else {
            errorID(1025);
          }
        }
        /**********************/

        /*  PRIVATE METHODS   */

        /**********************/

        /**
         * @private
         * @param len
         * @param needReplace
         */
        ;

        _proto.validateBuffer = function validateBuffer(len) {
          this.write_position = len > this.write_position ? len : this.write_position;
          len += this._position;

          this._validateBuffer(len);
        }
        /**
         * @private
         * UTF-8 Encoding/Decoding
         */
        ;

        _proto.encodeUTF8 = function encodeUTF8(str) {
          var pos = 0;
          var codePoints = this.stringToCodePoints(str);
          var outputBytes = [];

          while (codePoints.length > pos) {
            var code_point = codePoints[pos++];

            if (this.inRange(code_point, 0xD800, 0xDFFF)) {
              this.encoderError(code_point);
            } else if (this.inRange(code_point, 0x0000, 0x007f)) {
              outputBytes.push(code_point);
            } else {
              var count = void 0,
                  offset = void 0;

              if (this.inRange(code_point, 0x0080, 0x07FF)) {
                count = 1;
                offset = 0xC0;
              } else if (this.inRange(code_point, 0x0800, 0xFFFF)) {
                count = 2;
                offset = 0xE0;
              } else if (this.inRange(code_point, 0x10000, 0x10FFFF)) {
                count = 3;
                offset = 0xF0;
              }

              outputBytes.push(this.div(code_point, Math.pow(64, count)) + offset);

              while (count > 0) {
                var temp = this.div(code_point, Math.pow(64, count - 1));
                outputBytes.push(0x80 + temp % 64);
                count -= 1;
              }
            }
          }

          return new Uint8Array(outputBytes);
        }
        /**
         * @private
         *
         * @param data
         * @returns
         */
        ;

        _proto.decodeUTF8 = function decodeUTF8(data) {
          var fatal = false;
          var pos = 0;
          var result = "";
          var code_point;
          var utf8_code_point = 0;
          var utf8_bytes_needed = 0;
          var utf8_bytes_seen = 0;
          var utf8_lower_boundary = 0;

          while (data.length > pos) {
            var _byte = data[pos++];

            if (_byte == this.EOF_byte) {
              if (utf8_bytes_needed != 0) {
                code_point = this.decoderError(fatal);
              } else {
                code_point = this.EOF_code_point;
              }
            } else {
              if (utf8_bytes_needed == 0) {
                if (this.inRange(_byte, 0x00, 0x7F)) {
                  code_point = _byte;
                } else {
                  if (this.inRange(_byte, 0xC2, 0xDF)) {
                    utf8_bytes_needed = 1;
                    utf8_lower_boundary = 0x80;
                    utf8_code_point = _byte - 0xC0;
                  } else if (this.inRange(_byte, 0xE0, 0xEF)) {
                    utf8_bytes_needed = 2;
                    utf8_lower_boundary = 0x800;
                    utf8_code_point = _byte - 0xE0;
                  } else if (this.inRange(_byte, 0xF0, 0xF4)) {
                    utf8_bytes_needed = 3;
                    utf8_lower_boundary = 0x10000;
                    utf8_code_point = _byte - 0xF0;
                  } else {
                    this.decoderError(fatal);
                  }

                  utf8_code_point = utf8_code_point * Math.pow(64, utf8_bytes_needed);
                  code_point = null;
                }
              } else if (!this.inRange(_byte, 0x80, 0xBF)) {
                utf8_code_point = 0;
                utf8_bytes_needed = 0;
                utf8_bytes_seen = 0;
                utf8_lower_boundary = 0;
                pos--;
                code_point = this.decoderError(fatal, _byte);
              } else {
                utf8_bytes_seen += 1;
                utf8_code_point = utf8_code_point + (_byte - 0x80) * Math.pow(64, utf8_bytes_needed - utf8_bytes_seen);

                if (utf8_bytes_seen !== utf8_bytes_needed) {
                  code_point = null;
                } else {
                  var cp = utf8_code_point;
                  var lower_boundary = utf8_lower_boundary;
                  utf8_code_point = 0;
                  utf8_bytes_needed = 0;
                  utf8_bytes_seen = 0;
                  utf8_lower_boundary = 0;

                  if (this.inRange(cp, lower_boundary, 0x10FFFF) && !this.inRange(cp, 0xD800, 0xDFFF)) {
                    code_point = cp;
                  } else {
                    code_point = this.decoderError(fatal, _byte);
                  }
                }
              }
            } //Decode string


            if (code_point !== null && code_point !== this.EOF_code_point) {
              if (code_point <= 0xFFFF) {
                if (code_point > 0) result += String.fromCharCode(code_point);
              } else {
                code_point -= 0x10000;
                result += String.fromCharCode(0xD800 + (code_point >> 10 & 0x3ff));
                result += String.fromCharCode(0xDC00 + (code_point & 0x3ff));
              }
            }
          }

          return result;
        }
        /**
         * @private
         *
         * @param code_point
         */
        ;

        _proto.encoderError = function encoderError(code_point) {
          errorID(1026, code_point);
        }
        /**
         * @private
         *
         * @param fatal
         * @param opt_code_point
         * @returns
         */
        ;

        _proto.decoderError = function decoderError(fatal, opt_code_point) {
          if (fatal) {
            errorID(1027);
          }

          return opt_code_point || 0xFFFD;
        };
        /**
         * @private
         *
         * @param a
         * @param min
         * @param max
         */


        _proto.inRange = function inRange(a, min, max) {
          return min <= a && a <= max;
        }
        /**
         * @private
         *
         * @param n
         * @param d
         */
        ;

        _proto.div = function div(n, d) {
          return Math.floor(n / d);
        }
        /**
         * @private
         *
         * @param string
         */
        ;

        _proto.stringToCodePoints = function stringToCodePoints(string) {
          /** @type {Array.<number>} */
          var cps = []; // Based on http://www.w3.org/TR/WebIDL/#idl-DOMString

          var i = 0,
              n = string.length;

          while (i < string.length) {
            var c = string.charCodeAt(i);

            if (!this.inRange(c, 0xD800, 0xDFFF)) {
              cps.push(c);
            } else if (this.inRange(c, 0xDC00, 0xDFFF)) {
              cps.push(0xFFFD);
            } else {
              // (inRange(c, 0xD800, 0xDBFF))
              if (i == n - 1) {
                cps.push(0xFFFD);
              } else {
                var d = string.charCodeAt(i + 1);

                if (this.inRange(d, 0xDC00, 0xDFFF)) {
                  var a = c & 0x3FF;
                  var b = d & 0x3FF;
                  i += 1;
                  cps.push(0x10000 + (a << 10) + b);
                } else {
                  cps.push(0xFFFD);
                }
              }
            }

            i += 1;
          }

          return cps;
        };

        _createClass(ByteArray, [{
          key: "endian",
          get:
          /**
           * Changes or reads the byte order; ENDIAN.BIG_ENDIAN or ENDIAN.LITTLE_ENDIAN.
           * @default ENDIAN.BIG_ENDIAN
           * @platform Web,Native
           * @language en_US
           */

          /**
           * 更改或读取数据的字节顺序；ENDIAN.BIG_ENDIAN 或 ENDIAN.LITTLE_ENDIAN。
           * @default ENDIAN.BIG_ENDIAN
           * @platform Web,Native
           * @language zh_CN
           */
          function get() {
            return this.$endian == ENDIAN.LITTLE_ENDIAN ? ENDIAN.LITTLE_ENDIAN : ENDIAN.BIG_ENDIAN;
          },
          set: function set(value) {
            this.$endian = value == ENDIAN.LITTLE_ENDIAN ? ENDIAN.LITTLE_ENDIAN : ENDIAN.BIG_ENDIAN;
          }
        }, {
          key: "readAvailable",
          get: function get() {
            return this.write_position - this._position;
          }
        }, {
          key: "buffer",
          get: function get() {
            return this.data.buffer.slice(0, this.write_position);
          },
          set:
          /**
           * @private
           */
          function set(value) {
            var wpos = value.byteLength;
            var uint8 = new Uint8Array(value);
            var bufferExtSize = this.bufferExtSize;
            var bytes;

            if (bufferExtSize == 0) {
              bytes = new Uint8Array(wpos);
            } else {
              var multi = (wpos / bufferExtSize | 0) + 1;
              bytes = new Uint8Array(multi * bufferExtSize);
            }

            bytes.set(uint8);
            this.write_position = wpos;
            this._bytes = bytes;
            this.data = new DataView(bytes.buffer);
          }
        }, {
          key: "rawBuffer",
          get: function get() {
            return this.data.buffer;
          }
        }, {
          key: "bytes",
          get: function get() {
            return this._bytes;
          }
          /**
           * @private
           * @platform Web,Native
           */

        }, {
          key: "dataView",
          get: function get() {
            return this.data;
          }
          /**
           * @private
           */
          ,
          set: function set(value) {
            this.buffer = value.buffer;
          }
          /**
           * @private
           */

        }, {
          key: "bufferOffset",
          get: function get() {
            return this.data.byteOffset;
          }
          /**
           * The current position of the file pointer (in bytes) to move or return to the ByteArray object. The next time you start reading reading method call in this position, or will start writing in this position next time call a write method.
           * @platform Web,Native
           * @language en_US
           */

          /**
           * 将文件指针的当前位置（以字节为单位）移动或返回到 ByteArray 对象中。下一次调用读取方法时将在此位置开始读取，或者下一次调用写入方法时将在此位置开始写入。
           * @platform Web,Native
           * @language zh_CN
           */

        }, {
          key: "position",
          get: function get() {
            return this._position;
          },
          set: function set(value) {
            this._position = value;

            if (value > this.write_position) {
              this.write_position = value;
            }
          }
          /**
           * The length of the ByteArray object (in bytes).
                    * If the length is set to be larger than the current length, the right-side zero padding byte array.
                       * If the length is set smaller than the current length, the byte array is truncated.
              * @platform Web,Native
              * @language en_US
              */

          /**
           * ByteArray 对象的长度（以字节为单位）。
           * 如果将长度设置为大于当前长度的值，则用零填充字节数组的右侧。
           * 如果将长度设置为小于当前长度的值，将会截断该字节数组。
           * @platform Web,Native
           * @language zh_CN
           */

        }, {
          key: "length",
          get: function get() {
            return this.write_position;
          },
          set: function set(value) {
            this.write_position = value;

            if (this.data.byteLength > value) {
              this._position = value;
            }

            this._validateBuffer(value);
          }
        }, {
          key: "bytesAvailable",
          get: function get() {
            return this.data.byteLength - this._position;
          }
        }]);

        return ByteArray;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/CDMgr.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _inheritsLoose, _createClass, cclegacy, _decorator, macro, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      macro = module.macro;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _class;

      cclegacy._RF.push({}, "cb2e28ctTBHBJ04TLKJpwHm", "CDMgr", undefined);

      var ccclass = _decorator.ccclass;
      /**
       * CD 数据结构
       */

      var CD = /*#__PURE__*/function () {
        /** 剩余时间 */

        /** CD 是否未完成 */
        function CD(
        /** 标签 */
        _tag,
        /** CD 时间 */
        _time,
        /** CD 更新回调 */
        _callback,
        /** CD 上下文或携带数据 */
        context) {
          if (context === void 0) {
            context = null;
          }

          this._current = 0;
          this._enabled = false;
          this._tag = _tag;
          this._time = _time;
          this._callback = _callback;
          this.context = context;
          this._current = _time;
        }
        /** 重置 CD */


        var _proto = CD.prototype;

        _proto.reset = function reset() {
          this._current = this.cd;
          this._enabled = true;
        }
        /** 标签 */
        ;
        /** 通知 CD 更新 */


        _proto.notify = function notify() {
          this._callback();
        };

        _createClass(CD, [{
          key: "tag",
          get: function get() {
            return this._tag;
          }
          /** 剩余时间 */

        }, {
          key: "current",
          get: function get() {
            return this._current;
          },
          set: function set(t) {
            this._current = Math.max(0, t);
          }
          /** CD 时间 */

        }, {
          key: "cd",
          get: function get() {
            return this._time;
          }
          /** 是否未完成 */

        }, {
          key: "enabled",
          get: function get() {
            return this._enabled;
          },
          set: function set(t) {
            this._enabled = t;
          }
        }]);

        return CD;
      }();
      /**
       * CD 管理器
       */


      var CDMgr = exports('CDMgr', (_dec = ccclass('CDMgr'), _dec2 = ccmodifier('CDMgr'), _dec(_class = _dec2(_class = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(CDMgr, _BaseComponent);

        function CDMgr() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          /** CD 项目列表 */

          _this._queue = [];
          return _this;
        }

        var _proto2 = CDMgr.prototype;

        _proto2.onEnable = function onEnable() {
          this.schedule(this._onTick, 1, macro.REPEAT_FOREVER);
        };

        _proto2.onDisable = function onDisable() {
          this.unschedule(this._onTick);
        }
        /**
         * 添加一份 CD 项
         * @param tag 标签
         * @param cd cd 时间
         * @param callback cd 完成回调
         * @param context cd 上下文或携带数据
         * @returns
         */
        ;

        _proto2.add = function add(tag, cd, callback, context) {
          if (context === void 0) {
            context = null;
          }

          var timer = this.get(tag);

          if (timer) {
            timer.context = context; // timer.reset();
          } else {
            timer = new CD(tag, cd, callback, context);
            timer.enabled = true;

            this._queue.push(timer);
          }

          return timer;
        }
        /**
         * 移除指定标签的 CD 项
         * @param tag 标签
         */
        ;

        _proto2.remove = function remove(tag) {
          var index = this.indexOf(tag);
          if (index > -1) this._queue.splice(index, 1);
        }
        /**
         * 获取指定 CD 项目的索引
         * @param tag
         * @returns
         */
        ;

        _proto2.indexOf = function indexOf(tag) {
          return this._queue.findIndex(function (v) {
            return v.tag === tag;
          });
        }
        /**
         * 是否包含指定标签的 CD 项目
         * @param tag 标签
         * @returns
         */
        ;

        _proto2.has = function has(tag) {
          return this.indexOf(tag) > -1;
        }
        /**
         * 获取指定标签的 CD 项目
         * @param tag
         * @returns
         */
        ;

        _proto2.get = function get(tag) {
          var index = this.indexOf(tag);

          if (index > -1) {
            return this._queue[index];
          }

          return null;
        }
        /** 更新 CD */
        ;

        _proto2._onTick = function _onTick() {
          var len = this._queue.length;
          if (len === 0) return;
          var cd;

          for (var i = len - 1; i >= 0; i--) {
            cd = this._queue[i];
            if (!cd.enabled) continue;
            cd.current -= 1;
            if (cd.current <= 0) cd.enabled = false;
            cd.notify();
          }
        };

        return CDMgr;
      }(BaseComponent)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/CircleLoader.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, cclegacy, _decorator, Sprite, Label, Animation, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Sprite = module.Sprite;
      Label = module.Label;
      Animation = module.Animation;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4;

      cclegacy._RF.push({}, "1431enOwoZDbafnKCYIDKGn", "CircleLoader", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var CircleLoader = exports('CircleLoader', (_dec = ccclass('CircleLoader'), _dec2 = ccmodifier('CircleLoader'), _dec3 = property(Sprite), _dec4 = property(Sprite), _dec5 = property(Label), _dec6 = property(Animation), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(CircleLoader, _BaseComponent);

        function CircleLoader() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "circle1", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "circle2", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "lbl", _descriptor3, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "anim", _descriptor4, _assertThisInitialized(_this));

          _this._curprogress = 0;
          _this._progress = 0;
          _this.loaded = false;
          return _this;
        }

        var _proto = CircleLoader.prototype;

        _proto.onCompleted = function onCompleted() {
          var _this2 = this;

          return new Promise(function (resolve, reject) {
            _this2.loaded = true;
            _this2.progress = 1;
            _this2._curprogress = 1;

            _this2.anim.stop();

            _this2.unscheduleAllCallbacks();

            _this2.scheduleOnce(function () {
              _this2.anim.crossFade('anim_circleloaded');
            }, 0);

            _this2.anim.once(Animation.EventType.FINISHED, function () {
              _this2.node.active = false;
              resolve();
            });
          });
        };

        _proto.load = function load() {
          var _this3 = this;

          this.loaded = false;
          this.node.active = true;
          this._curprogress = 0;
          this.progress = 0;
          this.anim.play('anim_circleloading');
          this.schedule(function () {
            if (_this3._curprogress >= _this3._progress) return;
            _this3._curprogress += 0.05;
            _this3.circle1.fillRange = _this3._curprogress;
          }, 1 / 60);
        };

        _createClass(CircleLoader, [{
          key: "progress",
          get: function get() {
            return this._progress;
          },
          set: function set(value) {
            this._progress = value;
            this.circle1.fillRange = this._progress;
          }
        }]);

        return CircleLoader;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "circle1", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "circle2", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "lbl", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "anim", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Column.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _createClass, _inheritsLoose, _asyncToGenerator, _regeneratorRuntime, cclegacy;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
      _inheritsLoose = module.inheritsLoose;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      cclegacy._RF.push({}, "2d7865TH5RKsKDTUWt/svoI", "Column", undefined);
      /** 富文本·列 */


      var Column = exports('Column', /*#__PURE__*/function () {
        /** 是否就绪 */

        /** 宽度 */

        /** 高度 */

        /** 所属行 */

        /**
         * @param container 容器
         * @param info 样式
         */
        function Column(container, info) {
          this._ready = void 0;
          this._width = void 0;
          this._height = void 0;
          this._row = void 0;
          this.container = container;
          this._height = 0;
          this._width = 0;
        }
        /** 设置所属行 */


        var _proto = Column.prototype;
        /** 测量 */

        _proto.measure = /*#__PURE__*/function () {
          var _measure = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  if (!this._ready) {
                    _context.next = 2;
                    break;
                  }

                  return _context.abrupt("return");

                case 2:
                  _context.next = 4;
                  return this.calculate();

                case 4:
                  this._ready = true;

                case 5:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));

          function measure() {
            return _measure.apply(this, arguments);
          }

          return measure;
        }()
        /** 计算 */
        ;

        _createClass(Column, [{
          key: "row",
          set: function set(row) {
            this._row = row;
          }
          /** 是否就绪 */

        }, {
          key: "ready",
          get: function get() {
            return this._ready;
          }
          /** 高度 */

        }, {
          key: "height",
          get: function get() {
            return this._height;
          }
          /** 宽度 */

        }, {
          key: "width",
          get: function get() {
            return this._width;
          }
        }]);

        return Column;
      }());
      /** 富文本·文本 */

      var TextColumn = exports('TextColumn', /*#__PURE__*/function (_Column) {
        _inheritsLoose(TextColumn, _Column);
        /** 文本测量信息 */

        /**
         * @param container 容器
         * @param info 文本样式
         */


        function TextColumn(container, info) {
          var _this;

          _this = _Column.call(this, container, info) || this;
          _this.metrics = void 0;
          _this.container = container;
          _this.info = info;
          return _this;
        }

        var _proto2 = TextColumn.prototype;

        _proto2.calculate = /*#__PURE__*/function () {
          var _calculate = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
            var ctx, metrics;
            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                  this.applyStyle();

                  if (!this._ready) {
                    ctx = this.container.ctx;
                    metrics = ctx.measureText(this.info.text);
                    this._width = Math.max(metrics.width, metrics.actualBoundingBoxLeft + metrics.actualBoundingBoxRight);
                    this._height = metrics.fontBoundingBoxAscent;
                    this.metrics = metrics;
                  }

                case 2:
                case "end":
                  return _context2.stop();
              }
            }, _callee2, this);
          }));

          function calculate() {
            return _calculate.apply(this, arguments);
          }

          return calculate;
        }();

        _proto2.applyStyle = function applyStyle() {
          var _fontSize, _fontFamily, _fontVariant, _fontWeight, _fillColor, _strokeColor, _shadowColor, _shadowBlur, _shadowOffsetX, _shadowOffsetY;

          var _this$container = this.container,
              ctx = _this$container.ctx,
              requirements = _this$container.requirements;
          var _this$info = this.info,
              fontSize = _this$info.fontSize,
              fontFamily = _this$info.fontFamily,
              fontVariant = _this$info.fontVariant,
              fontWeight = _this$info.fontWeight,
              fillColor = _this$info.fillColor,
              strokeColor = _this$info.strokeColor,
              shadowColor = _this$info.shadowColor,
              shadowBlur = _this$info.shadowBlur,
              shadowOffsetX = _this$info.shadowOffsetX,
              shadowOffsetY = _this$info.shadowOffsetY;
          fontSize = (_fontSize = fontSize) != null ? _fontSize : requirements.fontSize;
          fontFamily = (_fontFamily = fontFamily) != null ? _fontFamily : requirements.fontFamily;
          fontVariant = (_fontVariant = fontVariant) != null ? _fontVariant : requirements.fontVariant;
          fontWeight = (_fontWeight = fontWeight) != null ? _fontWeight : requirements.fontWeight;
          fillColor = (_fillColor = fillColor) != null ? _fillColor : requirements.fillColor;
          strokeColor = (_strokeColor = strokeColor) != null ? _strokeColor : requirements.strokeColor;
          shadowColor = (_shadowColor = shadowColor) != null ? _shadowColor : requirements.shadowColor;
          shadowBlur = (_shadowBlur = shadowBlur) != null ? _shadowBlur : requirements.shadowBlur;
          shadowOffsetX = (_shadowOffsetX = shadowOffsetX) != null ? _shadowOffsetX : requirements.shadowOffsetX;
          shadowOffsetY = (_shadowOffsetY = shadowOffsetY) != null ? _shadowOffsetY : requirements.shadowOffsetY;
          ctx.font = fontVariant + " " + fontWeight + " " + fontSize + "px " + fontFamily;
          ctx.fillStyle = fillColor;
          ctx.strokeStyle = strokeColor;
          ctx.shadowColor = shadowColor;
          ctx.shadowBlur = shadowBlur;
          ctx.shadowOffsetX = shadowOffsetX;
          ctx.shadowOffsetY = shadowOffsetY;
        };

        _proto2.draw = function draw(x, y) {
          this.applyStyle();
          var ctx = this.container.ctx;

          if (this.info.strokeColor) {
            ctx.strokeText(this.info.text, x, y);
          } else {
            ctx.fillText(this.info.text, x, y);
          }
        };

        return TextColumn;
      }(Column));
      /** 富文本·图片 */

      var ImageColume = exports('ImageColume', /*#__PURE__*/function (_Column2) {
        _inheritsLoose(ImageColume, _Column2);
        /** 图片元素 */

        /**
         * @param container 容器
         * @param info 图片样式
         */


        function ImageColume(container, info) {
          var _this2;

          _this2 = _Column2.call(this, container, info) || this;
          _this2._image = void 0;
          _this2.container = container;
          _this2.info = info;
          return _this2;
        }

        var _proto3 = ImageColume.prototype;

        _proto3.calculate = /*#__PURE__*/function () {
          var _calculate2 = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3() {
            var _this3 = this;

            return _regeneratorRuntime().wrap(function _callee3$(_context3) {
              while (1) switch (_context3.prev = _context3.next) {
                case 0:
                  return _context3.abrupt("return", new Promise(function (resolve) {
                    var _this3$info = _this3.info,
                        width = _this3$info.width,
                        height = _this3$info.height;
                    var img = new Image();
                    img.src = _this3.info.src;

                    img.onload = function () {
                      if (width && height === undefined) {
                        img.height = width / img.width * img.height;
                        img.width = width;
                      } else if (height && width === undefined) {
                        img.width = height / img.height * img.width;
                        img.height = height;
                      } else if (width && height) {
                        img.width = width;
                        img.height = height;
                      }

                      _this3._width = img.width;
                      _this3._height = img.height;
                      _this3._image = img;
                      resolve();
                    };
                  }));

                case 1:
                case "end":
                  return _context3.stop();
              }
            }, _callee3);
          }));

          function calculate() {
            return _calculate2.apply(this, arguments);
          }

          return calculate;
        }();

        _proto3.draw = function draw(x, y) {
          var ctx = this.container.ctx;

          if (this._image) {
            var _this$_image = this._image,
                width = _this$_image.width,
                height = _this$_image.height;
            ctx.drawImage(this._image, x, y - height, width, height);
          }
        };

        return ImageColume;
      }(Column));
      /** 富文本·多列 */

      var Columns = exports('Columns', /*#__PURE__*/function () {
        /** 富文本列·列表 */

        /**
         * @param container 容器
         * @param info 样式
         */
        function Columns(container, info) {
          this._columns = void 0;
          this.container = container;
          this._columns = [];
          this.visit(info);
        }
        /**
         * 解析富文本
         * @param info 样式
         */


        var _proto4 = Columns.prototype;

        _proto4.visit = function visit(info) {
          switch (info.type) {
            case 'text':
              this.visitText(info);
              break;

            case 'image':
              this.visitImage(info);
              break;
          }
        }
        /**
         * 解析文本
         * @param info 文本样式
         */
        ;

        _proto4.visitText = function visitText(info) {
          var _this4 = this;

          info.text.split('').forEach(function (_char) {
            var copy = JSON.parse(JSON.stringify(info));
            copy.text = _char;

            _this4._columns.push(new TextColumn(_this4.container, copy));
          });
        }
        /**
         * 解析图片
         * @param info 图片样式
         */
        ;

        _proto4.visitImage = function visitImage(info) {
          this._columns.push(new ImageColume(this.container, info));
        }
        /** 所有富文本列 */
        ;

        _createClass(Columns, [{
          key: "columns",
          get: function get() {
            return this._columns;
          }
        }]);

        return Columns;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Container.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Column.ts', './Row.ts'], function (exports) {
  var _createClass, _asyncToGenerator, _regeneratorRuntime, cclegacy, Columns, Row;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      Columns = module.Columns;
    }, function (module) {
      Row = module.Row;
    }],
    execute: function () {
      cclegacy._RF.push({}, "9df4e5++ONOp7q4OMeSYsSj", "Container", undefined);
      /** 富文本·框 */


      var Container = exports('Container', /*#__PURE__*/function () {
        /** 当前画布 */

        /** 渲染上下文 */

        /** 所有富文本行 */

        /** @param requirements 绘图基本配置 */
        function Container(requirements) {
          this.canvas = void 0;
          this.ctx = void 0;
          this._rows = void 0;
          this.requirements = requirements;
          this._rows = [];
          this.canvas = document.createElement('canvas');
          this.ctx = this.canvas.getContext('2d');
          this.init();
        }
        /** 初始化 */


        var _proto = Container.prototype;

        _proto.init = function init() {
          var _this$requirements = this.requirements,
              fontVariant = _this$requirements.fontVariant,
              fontWeight = _this$requirements.fontWeight,
              fontSize = _this$requirements.fontSize,
              fontFamily = _this$requirements.fontFamily,
              fillColor = _this$requirements.fillColor,
              strokeColor = _this$requirements.strokeColor,
              shadowColor = _this$requirements.shadowColor,
              shadowBlur = _this$requirements.shadowBlur,
              shadowOffsetX = _this$requirements.shadowOffsetX,
              shadowOffsetY = _this$requirements.shadowOffsetY,
              commentWidth = _this$requirements.commentWidth;
          this.ctx.textBaseline = 'alphabetic';
          this.ctx.lineWidth = commentWidth;
          this.ctx.font = fontVariant + " " + fontWeight + " " + fontSize + "px " + fontFamily;
          this.ctx.fillStyle = fillColor;
          this.ctx.strokeStyle = strokeColor;
          this.ctx.shadowColor = shadowColor;
          this.ctx.shadowBlur = shadowBlur;
          this.ctx.shadowOffsetX = shadowOffsetX;
          this.ctx.shadowOffsetY = shadowOffsetY;
          this.ctx.save();
        }
        /** 当前行 */
        ;
        /** 新建行 */


        _proto.appendRow = function appendRow() {
          var row = new Row(this);

          this._rows.push(row);

          return row;
        }
        /**
         * 添加富文本
         * @param info 富文本样式
         */
        ;

        _proto.append = /*#__PURE__*/function () {
          var _append = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(info) {
            var column, i;
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  column = new Columns(this, info);
                  i = 0;

                case 2:
                  if (!(i < column.columns.length)) {
                    _context.next = 8;
                    break;
                  }

                  _context.next = 5;
                  return this.appendColumn(column.columns[i]);

                case 5:
                  i++;
                  _context.next = 2;
                  break;

                case 8:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));

          function append(_x) {
            return _append.apply(this, arguments);
          }

          return append;
        }()
        /**
         * 添加富文本
         * @param columns 富文本样式列表
         */
        ;

        _proto.appendMany = /*#__PURE__*/function () {
          var _appendMany = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(columns) {
            var i;
            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                  i = 0;

                case 1:
                  if (!(i < columns.length)) {
                    _context2.next = 7;
                    break;
                  }

                  _context2.next = 4;
                  return this.append(columns[i]);

                case 4:
                  i++;
                  _context2.next = 1;
                  break;

                case 7:
                case "end":
                  return _context2.stop();
              }
            }, _callee2, this);
          }));

          function appendMany(_x2) {
            return _appendMany.apply(this, arguments);
          }

          return appendMany;
        }()
        /**
         * 添加一列富文本
         * @param column 富文本列
         */
        ;

        _proto.appendColumn = /*#__PURE__*/function () {
          var _appendColumn = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(column) {
            var row, ok;
            return _regeneratorRuntime().wrap(function _callee3$(_context3) {
              while (1) switch (_context3.prev = _context3.next) {
                case 0:
                  row = this.row;

                  if (row) {
                    _context3.next = 7;
                    break;
                  } // 找不到则新建行


                  row = this.appendRow();
                  _context3.next = 5;
                  return row.append(column, true);

                case 5:
                  _context3.next = 14;
                  break;

                case 7:
                  _context3.next = 9;
                  return row.append(column, false);

                case 9:
                  ok = _context3.sent;

                  if (ok) {
                    _context3.next = 14;
                    break;
                  } // 当前行塞不下了则新建行


                  row = this.appendRow();
                  _context3.next = 14;
                  return row.append(column, true);

                case 14:
                case "end":
                  return _context3.stop();
              }
            }, _callee3, this);
          }));

          function appendColumn(_x3) {
            return _appendColumn.apply(this, arguments);
          }

          return appendColumn;
        }()
        /** 总高度 */
        ;
        /** 绘制所有行 */


        _proto.draw = function draw() {
          var _this = this;

          this.canvas.width = this.width;
          this.canvas.height = this.height;
          this.ctx.fillStyle = '#e5b99c';
          this.ctx.fillRect(0, 0, this.width, this.height);
          this.ctx.translate(this.requirements.padding[0], this.requirements.padding[1]);
          var height = 0;

          this._rows.forEach(function (row, i) {
            height += row.height + _this.requirements.spaceY;
            row.draw(i, height);
          });
        };

        _createClass(Container, [{
          key: "row",
          get: function get() {
            return this._rows[this._rows.length - 1];
          }
        }, {
          key: "height",
          get: function get() {
            var _this2 = this;

            var heights = this._rows.map(function (row) {
              return row.height;
            });

            var height = heights.reduce(function (sum, v) {
              return sum += v + _this2.requirements.spaceY;
            }, 0);
            return height + this.requirements.padding[1] + this.requirements.padding[3];
          }
          /** 总宽度 */

        }, {
          key: "width",
          get: function get() {
            return this.requirements.maxWidth + this.requirements.padding[0] + this.requirements.padding[2];
          }
        }]);

        return Container;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Dates.ts", ['cc', './Numbers.ts'], function (exports) {
  var cclegacy, Numbers;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      Numbers = module.Numbers;
    }],
    execute: function () {
      exports('Dates', void 0);

      cclegacy._RF.push({}, "bc6522gDHBADqJJp3BcNx6K", "Dates", undefined);
      /**
       * 日期辅助方法
       */


      var Dates;

      (function (_Dates) {
        function getDateInfo() {
          var d = new Date();
          var yy = Numbers.padStart(d.getFullYear(), 4);
          var mm = Numbers.padStart(d.getMonth(), 2);
          var dd = Numbers.padStart(d.getDate(), 2);
          var h = Numbers.padStart(d.getHours(), 2);
          var m = Numbers.padStart(d.getMinutes(), 2);
          var s = Numbers.padStart(d.getSeconds(), 2);
          var ms = d.getMilliseconds();
          return {
            date: d,
            YY: yy,
            MM: mm,
            DD: dd,
            hh: h,
            mm: m,
            ss: s,
            ms: ms
          };
        }

        _Dates.getDateInfo = getDateInfo;

        function getDateString() {
          var _this$getDateInfo = this.getDateInfo(),
              YY = _this$getDateInfo.YY,
              MM = _this$getDateInfo.MM,
              DD = _this$getDateInfo.DD,
              hh = _this$getDateInfo.hh,
              mm = _this$getDateInfo.mm,
              ss = _this$getDateInfo.ss,
              ms = _this$getDateInfo.ms;

          return YY + "/" + MM + "/" + DD + " " + hh + ":" + mm + ":" + ss + "." + ms;
        }

        _Dates.getDateString = getDateString;

        function getTimeString(with_ms) {
          if (with_ms === void 0) {
            with_ms = false;
          }

          var _this$getDateInfo2 = this.getDateInfo(),
              hh = _this$getDateInfo2.hh,
              mm = _this$getDateInfo2.mm,
              ss = _this$getDateInfo2.ss,
              ms = _this$getDateInfo2.ms;

          return with_ms ? hh + ":" + mm + ":" + ss + "." + ms : hh + ":" + mm + ":" + ss;
        }

        _Dates.getTimeString = getTimeString;
      })(Dates || (Dates = exports('Dates', {})));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/debug-view-runtime-control.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Node, Color, Canvas, UITransform, instantiate, Label, RichText, Toggle, Button, director, Component;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Node = module.Node;
      Color = module.Color;
      Canvas = module.Canvas;
      UITransform = module.UITransform;
      instantiate = module.instantiate;
      Label = module.Label;
      RichText = module.RichText;
      Toggle = module.Toggle;
      Button = module.Button;
      director = module.director;
      Component = module.Component;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3;

      cclegacy._RF.push({}, "b2bd1+njXxJxaFY3ymm06WU", "debug-view-runtime-control", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var DebugViewRuntimeControl = exports('DebugViewRuntimeControl', (_dec = ccclass('internal.DebugViewRuntimeControl'), _dec2 = property(Node), _dec3 = property(Node), _dec4 = property(Node), _dec(_class = (_class2 = /*#__PURE__*/function (_Component) {
        _inheritsLoose(DebugViewRuntimeControl, _Component);

        function DebugViewRuntimeControl() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Component.call.apply(_Component, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "compositeModeToggle", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "singleModeToggle", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "EnableAllCompositeModeButton", _descriptor3, _assertThisInitialized(_this));

          _this._single = 0;
          _this.strSingle = ['No Single Debug', 'Vertex Color', 'Vertex Normal', 'Vertex Tangent', 'World Position', 'Vertex Mirror', 'Face Side', 'UV0', 'UV1', 'UV Lightmap', 'Project Depth', 'Linear Depth', 'Fragment Normal', 'Fragment Tangent', 'Fragment Binormal', 'Base Color', 'Diffuse Color', 'Specular Color', 'Transparency', 'Metallic', 'Roughness', 'Specular Intensity', 'IOR', 'Direct Diffuse', 'Direct Specular', 'Direct All', 'Env Diffuse', 'Env Specular', 'Env All', 'Emissive', 'Light Map', 'Shadow', 'AO', 'Fresnel', 'Direct Transmit Diffuse', 'Direct Transmit Specular', 'Env Transmit Diffuse', 'Env Transmit Specular', 'Transmit All', 'Direct Internal Specular', 'Env Internal Specular', 'Internal All', 'Fog'];
          _this.strComposite = ['Direct Diffuse', 'Direct Specular', 'Env Diffuse', 'Env Specular', 'Emissive', 'Light Map', 'Shadow', 'AO', 'Normal Map', 'Fog', 'Tone Mapping', 'Gamma Correction', 'Fresnel', 'Transmit Diffuse', 'Transmit Specular', 'Internal Specular', 'TT'];
          _this.strMisc = ['CSM Layer Coloration', 'Lighting With Albedo'];
          _this.compositeModeToggleList = [];
          _this.singleModeToggleList = [];
          _this.miscModeToggleList = [];
          _this.textComponentList = [];
          _this.labelComponentList = [];
          _this.textContentList = [];
          _this.hideButtonLabel = void 0;
          _this._currentColorIndex = 0;
          _this.strColor = ['<color=#ffffff>', '<color=#000000>', '<color=#ff0000>', '<color=#00ff00>', '<color=#0000ff>'];
          _this.color = [Color.WHITE, Color.BLACK, Color.RED, Color.GREEN, Color.BLUE];
          return _this;
        }

        var _proto = DebugViewRuntimeControl.prototype;

        _proto.start = function start() {
          // get canvas resolution
          var canvas = this.node.parent.getComponent(Canvas);

          if (!canvas) {
            console.error('debug-view-runtime-control should be child of Canvas');
            return;
          }

          var uiTransform = this.node.parent.getComponent(UITransform);
          var halfScreenWidth = uiTransform.width * 0.5;
          var halfScreenHeight = uiTransform.height * 0.5;
          var x = -halfScreenWidth + halfScreenWidth * 0.1,
              y = halfScreenHeight - halfScreenHeight * 0.1;
          var width = 200,
              height = 20; // new nodes

          var miscNode = this.node.getChildByName('MiscMode');
          var buttonNode = instantiate(miscNode);
          buttonNode.parent = this.node;
          buttonNode.name = 'Buttons';
          var titleNode = instantiate(miscNode);
          titleNode.parent = this.node;
          titleNode.name = 'Titles'; // title

          for (var i = 0; i < 2; i++) {
            var newLabel = instantiate(this.EnableAllCompositeModeButton.getChildByName('Label'));
            newLabel.setPosition(x + (i > 0 ? 50 + width * 2 : 150), y, 0.0);
            newLabel.setScale(0.75, 0.75, 0.75);
            newLabel.parent = titleNode;

            var _labelComponent = newLabel.getComponent(Label);

            _labelComponent.string = i ? '----------Composite Mode----------' : '----------Single Mode----------';
            _labelComponent.color = Color.WHITE;
            _labelComponent.overflow = 0;
            this.labelComponentList[this.labelComponentList.length] = _labelComponent;
          }

          y -= height; // single

          var currentRow = 0;

          for (var _i = 0; _i < this.strSingle.length; _i++, currentRow++) {
            if (_i === this.strSingle.length >> 1) {
              x += width;
              currentRow = 0;
            }

            var newNode = _i ? instantiate(this.singleModeToggle) : this.singleModeToggle;
            newNode.setPosition(x, y - height * currentRow, 0.0);
            newNode.setScale(0.5, 0.5, 0.5);
            newNode.parent = this.singleModeToggle.parent;
            var textComponent = newNode.getComponentInChildren(RichText);
            textComponent.string = this.strSingle[_i];
            this.textComponentList[this.textComponentList.length] = textComponent;
            this.textContentList[this.textContentList.length] = textComponent.string;
            newNode.on(Toggle.EventType.TOGGLE, this.toggleSingleMode, this);
            this.singleModeToggleList[_i] = newNode;
          }

          x += width; // buttons

          this.EnableAllCompositeModeButton.setPosition(x + 15, y, 0.0);
          this.EnableAllCompositeModeButton.setScale(0.5, 0.5, 0.5);
          this.EnableAllCompositeModeButton.on(Button.EventType.CLICK, this.enableAllCompositeMode, this);
          this.EnableAllCompositeModeButton.parent = buttonNode;
          var labelComponent = this.EnableAllCompositeModeButton.getComponentInChildren(Label);
          this.labelComponentList[this.labelComponentList.length] = labelComponent;
          var changeColorButton = instantiate(this.EnableAllCompositeModeButton);
          changeColorButton.setPosition(x + 90, y, 0.0);
          changeColorButton.setScale(0.5, 0.5, 0.5);
          changeColorButton.on(Button.EventType.CLICK, this.changeTextColor, this);
          changeColorButton.parent = buttonNode;
          labelComponent = changeColorButton.getComponentInChildren(Label);
          labelComponent.string = 'TextColor';
          this.labelComponentList[this.labelComponentList.length] = labelComponent;
          var HideButton = instantiate(this.EnableAllCompositeModeButton);
          HideButton.setPosition(x + 200, y, 0.0);
          HideButton.setScale(0.5, 0.5, 0.5);
          HideButton.on(Button.EventType.CLICK, this.hideUI, this);
          HideButton.parent = this.node.parent;
          labelComponent = HideButton.getComponentInChildren(Label);
          labelComponent.string = 'Hide UI';
          this.labelComponentList[this.labelComponentList.length] = labelComponent;
          this.hideButtonLabel = labelComponent; // misc

          y -= 40;

          for (var _i2 = 0; _i2 < this.strMisc.length; _i2++) {
            var _newNode = instantiate(this.compositeModeToggle);

            _newNode.setPosition(x, y - height * _i2, 0.0);

            _newNode.setScale(0.5, 0.5, 0.5);

            _newNode.parent = miscNode;

            var _textComponent = _newNode.getComponentInChildren(RichText);

            _textComponent.string = this.strMisc[_i2];
            this.textComponentList[this.textComponentList.length] = _textComponent;
            this.textContentList[this.textContentList.length] = _textComponent.string;

            var toggleComponent = _newNode.getComponent(Toggle);

            toggleComponent.isChecked = _i2 ? true : false;

            _newNode.on(Toggle.EventType.TOGGLE, _i2 ? this.toggleLightingWithAlbedo : this.toggleCSMColoration, this);

            this.miscModeToggleList[_i2] = _newNode;
          } // composite


          y -= 150;

          for (var _i3 = 0; _i3 < this.strComposite.length; _i3++) {
            var _newNode2 = _i3 ? instantiate(this.compositeModeToggle) : this.compositeModeToggle;

            _newNode2.setPosition(x, y - height * _i3, 0.0);

            _newNode2.setScale(0.5, 0.5, 0.5);

            _newNode2.parent = this.compositeModeToggle.parent;

            var _textComponent2 = _newNode2.getComponentInChildren(RichText);

            _textComponent2.string = this.strComposite[_i3];
            this.textComponentList[this.textComponentList.length] = _textComponent2;
            this.textContentList[this.textContentList.length] = _textComponent2.string;

            _newNode2.on(Toggle.EventType.TOGGLE, this.toggleCompositeMode, this);

            this.compositeModeToggleList[_i3] = _newNode2;
          }
        };

        _proto.isTextMatched = function isTextMatched(textUI, textDescription) {
          var tempText = new String(textUI);
          var findIndex = tempText.search('>');

          if (findIndex === -1) {
            return textUI === textDescription;
          } else {
            tempText = tempText.substr(findIndex + 1);
            tempText = tempText.substr(0, tempText.search('<'));
            return tempText === textDescription;
          }
        };

        _proto.toggleSingleMode = function toggleSingleMode(toggle) {
          var debugView = director.root.debugView;
          var textComponent = toggle.getComponentInChildren(RichText);

          for (var i = 0; i < this.strSingle.length; i++) {
            if (this.isTextMatched(textComponent.string, this.strSingle[i])) {
              debugView.singleMode = i;
            }
          }
        };

        _proto.toggleCompositeMode = function toggleCompositeMode(toggle) {
          var debugView = director.root.debugView;
          var textComponent = toggle.getComponentInChildren(RichText);

          for (var i = 0; i < this.strComposite.length; i++) {
            if (this.isTextMatched(textComponent.string, this.strComposite[i])) {
              debugView.enableCompositeMode(i, toggle.isChecked);
            }
          }
        };

        _proto.toggleLightingWithAlbedo = function toggleLightingWithAlbedo(toggle) {
          var debugView = director.root.debugView;
          debugView.lightingWithAlbedo = toggle.isChecked;
        };

        _proto.toggleCSMColoration = function toggleCSMColoration(toggle) {
          var debugView = director.root.debugView;
          debugView.csmLayerColoration = toggle.isChecked;
        };

        _proto.enableAllCompositeMode = function enableAllCompositeMode(button) {
          var debugView = director.root.debugView;
          debugView.enableAllCompositeMode(true);

          for (var i = 0; i < this.compositeModeToggleList.length; i++) {
            var _toggleComponent = this.compositeModeToggleList[i].getComponent(Toggle);

            _toggleComponent.isChecked = true;
          }

          var toggleComponent = this.miscModeToggleList[0].getComponent(Toggle);
          toggleComponent.isChecked = false;
          debugView.csmLayerColoration = false;
          toggleComponent = this.miscModeToggleList[1].getComponent(Toggle);
          toggleComponent.isChecked = true;
          debugView.lightingWithAlbedo = true;
        };

        _proto.hideUI = function hideUI(button) {
          var titleNode = this.node.getChildByName('Titles');
          var activeValue = !titleNode.active;
          this.singleModeToggleList[0].parent.active = activeValue;
          this.miscModeToggleList[0].parent.active = activeValue;
          this.compositeModeToggleList[0].parent.active = activeValue;
          this.EnableAllCompositeModeButton.parent.active = activeValue;
          titleNode.active = activeValue;
          this.hideButtonLabel.string = activeValue ? 'Hide UI' : 'Show UI';
        };

        _proto.changeTextColor = function changeTextColor(button) {
          this._currentColorIndex++;

          if (this._currentColorIndex >= this.strColor.length) {
            this._currentColorIndex = 0;
          }

          for (var i = 0; i < this.textComponentList.length; i++) {
            this.textComponentList[i].string = this.strColor[this._currentColorIndex] + this.textContentList[i] + '</color>';
          }

          for (var _i4 = 0; _i4 < this.labelComponentList.length; _i4++) {
            this.labelComponentList[_i4].color = this.color[this._currentColorIndex];
          }
        };

        _proto.onLoad = function onLoad() {};

        _proto.update = function update(deltaTime) {};

        return DebugViewRuntimeControl;
      }(Component), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "compositeModeToggle", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "singleModeToggle", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "EnableAllCompositeModeButton", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Declaraions.ts", ['cc'], function () {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      cclegacy._RF.push({}, "5a6d2Iuk4RAZZRt44EfjcAt", "Declaraions", undefined);

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/DLinkList.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Logger.ts'], function (exports) {
  var _createClass, cclegacy, logger;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      cclegacy._RF.push({}, "5631000EvxJzawx7bX1L4ui", "DLinkList", undefined);

      var Node = function Node(data) {
        this.data = void 0;
        this.prev = void 0;
        this.next = void 0;
        this.data = data;
        this.prev = null;
        this.next = null;
      };
      /**
       * 双向链表
       */


      var DLinkList = exports('DLinkList', /*#__PURE__*/function () {
        function DLinkList() {
          this.head = void 0;
          this.tail = void 0;
          this.len = void 0;
          this.len = 0;
        }

        var _proto = DLinkList.prototype;

        _proto.push = function push(t) {
          var node = new Node(t);
          this.len++;

          if (!this.head) {
            this.head = node;
            this.tail = node;
            return;
          }

          this.tail.next = node;
          node.prev = this.tail;
          this.tail = node;
        };

        _proto.concat = function concat() {
          for (var _len = arguments.length, ts = new Array(_len), _key = 0; _key < _len; _key++) {
            ts[_key] = arguments[_key];
          }

          for (var _i = 0, _ts = ts; _i < _ts.length; _i++) {
            var t = _ts[_i];
            this.push(t);
          }
        };

        _proto.pop = function pop() {
          var node = this.tail;

          if (this.len == 1) {
            // @ts-ignore
            this.head = null; // @ts-ignore

            this.tail = null;
            this.len = 0;
            return node.data;
          }

          this.tail = this.tail.prev;
          this.tail.next = null;
          this.len--;
          return node.data;
        };

        _proto.shift = function shift(t) {
          var node = new Node(t);
          this.len++;

          if (!this.head) {
            this.head = node;
            this.tail = node;
            return;
          }

          this.head.prev = node;
          node.next = this.head;
          this.head = node;
        };

        _proto.unshift = function unshift() {
          var node = this.head;

          if (this.len == 1) {
            this.head = null;
            this.tail = null;
            this.len = 0;
            return node.data;
          }

          this.head = this.head.next;
          this.head.prev = null;
          this.len--;
          return node.data;
        };

        _proto.at = function at(i) {
          var node;

          if (i < this.len * .5) {
            node = this.head;
            var count = 0;

            while (++count <= i) {
              node = node.next;
            }
          } else {
            node = this.tail;

            var _count = this.len - 1;

            while (--_count >= i) {
              node = node.prev;
            }
          }

          return node.data;
        };

        _proto.find = function find(t) {
          var node = this.head;

          while (node && node.data != t) {
            node = node.next;
          }

          return node;
        };

        _proto.remove = function remove(t) {
          if (!t) return;

          if (t == this.head.data) {
            this.unshift();
            return;
          }

          if (t == this.tail.data) {
            this.pop();
            return;
          }

          var node = this.find(t);

          if (node) {
            //    const prev = node.prev;
            //    const next = node.next;
            node.prev.next = node.next;
            node.next.prev = node.prev;
            this.len--;
          }
        };

        _proto.print = function print() {
          if (!this.head) return;
          var node = this.head;

          do {
            logger.log(node.data);
            node = node.next;
          } while (node != null);
        };

        _createClass(DLinkList, [{
          key: "length",
          get: function get() {
            return this.len;
          }
        }]);

        return DLinkList;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/EventDispatcher.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _createForOfIteratorHelperLoose, cclegacy, EventTarget;

  return {
    setters: [function (module) {
      _createForOfIteratorHelperLoose = module.createForOfIteratorHelperLoose;
    }, function (module) {
      cclegacy = module.cclegacy;
      EventTarget = module.EventTarget;
    }],
    execute: function () {
      cclegacy._RF.push({}, "31f99PCc5dG3LXDsRJtsqxZ", "EventDispatcher", undefined);

      var func_callback_events = exports('func_callback_events', '____callback_events____');
      var regist_event_callback = exports('regist_event_callback', function regist_event_callback(events) {
        return function (ctor, _methodName, _desc) {
          ctor.prototype[func_callback_events] = function () {
            return events;
          };
        };
      });
      /** 事件回调器 */

      var EventDispatcher = /*#__PURE__*/function () {
        function EventDispatcher() {
          this.eventTarget = new EventTarget();
        }

        var _proto = EventDispatcher.prototype;

        _proto.regist = function regist(target) {
          if (!target[func_callback_events]) return;
          var callbackEvents = target[func_callback_events]();

          for (var _iterator = _createForOfIteratorHelperLoose(callbackEvents), _step; !(_step = _iterator()).done;) {
            var eventName = _step.value;
            this.eventTarget.on(eventName, target.onEventCallback.bind(target), target);
          }
        };

        _proto.unRegist = function unRegist(target) {
          if (!target[func_callback_events]) return;
          target[func_callback_events] = null;
          this.eventTarget.targetOff(target);
        };

        _proto.emit = function emit(eventName, data) {
          this.eventTarget.emit(eventName, eventName, data);
        };

        _proto.on = function on(eventName, callback, target) {
          this.eventTarget.on(eventName, callback, target);
        };

        _proto.off = function off(eventName, callback, target) {
          this.eventTarget.off(eventName, callback, target);
        };

        return EventDispatcher;
      }();

      var dispatcher = exports('dispatcher', new EventDispatcher());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/exports.ts", ['cc'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      cclegacy._RF.push({}, "3311du3Z5RAl6db3/uoao7v", "exports", undefined);
      /**
       * 基础声明
       * @extension wlm-core
       * @author reyn
       * @date 2023.10.16
       */

      /** 语言代号 */


      var LanguageRegion = exports('LanguageRegion', /*#__PURE__*/function (LanguageRegion) {
        LanguageRegion["AR"] = "ara";
        LanguageRegion["CNS"] = "zh-Hans";
        LanguageRegion["CNT"] = "zh-Hant";
        LanguageRegion["DE"] = "deu";
        LanguageRegion["EN"] = "eng";
        LanguageRegion["ES"] = "spa";
        LanguageRegion["FR"] = "fra";
        LanguageRegion["INA"] = "ina";
        LanguageRegion["IT"] = "ita";
        LanguageRegion["JP"] = "jpn";
        LanguageRegion["KR"] = "kor";
        LanguageRegion["LA"] = "lat";
        LanguageRegion["PT"] = "por";
        LanguageRegion["RU"] = "rus";
        LanguageRegion["TH"] = "tha";
        LanguageRegion["TR"] = "tur";
        LanguageRegion["VI"] = "vie";
        return LanguageRegion;
      }({}));
      /** 基础环境配置<只读> */

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/GlobalEvent.ts", ['cc'], function (exports) {
  var cclegacy, EventTarget;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
      EventTarget = module.EventTarget;
    }],
    execute: function () {
      cclegacy._RF.push({}, "972b4ME5olPuLCmjZyEvQWR", "GlobalEvent", undefined);
      /** 全局事件派发器 */


      var GlobalEvent = exports('GlobalEvent', new EventTarget());
      /** 全局事件类型 */

      var GlobalEventType = exports('GlobalEventType', {
        WindowChanged: 'window-changed'
      });

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/GraphicsTool.ts", ['cc'], function (exports) {
  var cclegacy, Graphics;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
      Graphics = module.Graphics;
    }],
    execute: function () {
      cclegacy._RF.push({}, "ca35aBMr2tCCYPxAEUXrsfn", "GraphicsTool", undefined);

      var graphicsTool = /*#__PURE__*/function () {
        function graphicsTool() {}

        var _proto = graphicsTool.prototype;

        _proto.drawCircle = function drawCircle(node, hexColor, radius) {
          if (!node) return;
          var g = node.getComponent(Graphics) || node.addComponent(Graphics);
          g.clear();
          g.fillColor.fromHEX(hexColor);
          g.circle(0, 0, radius);
          g.fill();
          return g;
        };

        _proto.drawRect = function drawRect(node, hexColor, width, height) {
          if (!node) return;
          var g = node.getComponent(Graphics) || node.addComponent(Graphics);
          g.clear();
          g.fillColor.fromHEX(hexColor);
          g.rect(-width * .5, -height * .5, width, height);
          g.fill();
          return g;
        };

        _proto.drawLine = function drawLine(node, lineWidth, hexColor) {
          for (var _len = arguments.length, points = new Array(_len > 3 ? _len - 3 : 0), _key = 3; _key < _len; _key++) {
            points[_key - 3] = arguments[_key];
          }

          if (!node) return;
          if (!points || points.length < 2) return;
          var g = node.getComponent(Graphics) || node.addComponent(Graphics);
          g.clear();
          g.lineWidth = lineWidth;
          g.strokeColor.fromHEX(hexColor);
          g.moveTo(points[0].x, points[0].y);

          for (var i = 1; i < points.length; i++) {
            var p = points[i];
            g.lineTo(p.x, p.y);
          }

          g.stroke();
          return g;
        };

        return graphicsTool;
      }();

      var GraphicsTool = exports('GraphicsTool', new graphicsTool());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Hint.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './ViewManager.ts', './CircleLoader.ts', './LittleLoader.ts', './RollingTip.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, cclegacy, _decorator, director, view, UITransform, BlockInputEvents, Label, Animation, Sprite, UIOpacity, tween, ccmodifier, BaseComponent, vm, CircleLoader, LittleLoader, RollingTip;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      director = module.director;
      view = module.view;
      UITransform = module.UITransform;
      BlockInputEvents = module.BlockInputEvents;
      Label = module.Label;
      Animation = module.Animation;
      Sprite = module.Sprite;
      UIOpacity = module.UIOpacity;
      tween = module.tween;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      vm = module.vm;
    }, function (module) {
      CircleLoader = module.CircleLoader;
    }, function (module) {
      LittleLoader = module.LittleLoader;
    }, function (module) {
      RollingTip = module.RollingTip;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3;

      cclegacy._RF.push({}, "1b3a0MP7GRP0JJgoSXHbVkA", "Hint", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var Hint = exports('Hint', (_dec = ccclass('Hint'), _dec2 = ccmodifier('Hint'), _dec3 = property(CircleLoader), _dec4 = property(LittleLoader), _dec5 = property({
        type: RollingTip
      }), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(Hint, _BaseComponent);

        function Hint() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          _this.inited = false;
          _this.blockInput = void 0;
          _this.blockEnableCount = void 0;
          /** busyIndicator */

          _this.busyIndicator = void 0;
          _this.busyCount = void 0;
          /** AlertBox */

          _this.alertBox = void 0;
          _this.btnYes = void 0;
          _this.btnNo = void 0;
          _this.lblTitle = void 0;
          _this.lblContent = void 0;
          _this.lblYes = void 0;
          _this.lblNo = void 0;
          _this.sprYes = void 0;
          _this.sprNo = void 0;
          _this.animAlert = void 0;
          _this.alertInfos = [];
          _this.curAlertInfo = null;
          /** Tip */

          _this.tip = void 0;
          _this.lblTip = void 0;
          _this.animTip = void 0;
          /** CircleLoader */

          _initializerDefineProperty(_this, "circleLoader", _descriptor, _assertThisInitialized(_this));
          /** LittleLoader */


          _initializerDefineProperty(_this, "littleLoader", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "rollingTip", _descriptor3, _assertThisInitialized(_this));
          /** dark_mask */


          _this.darkMask = void 0; // private darkMaskCount: number = 0;

          _this.twDarkMask = void 0;
          _this.busyCountTags = {};
          return _this;
        }

        var _proto = Hint.prototype;

        _proto.start = function start() {
          var _this2 = this;

          vm.hint = this;
          director.addPersistRootNode(this.node);
          var size = view.getVisibleSize();
          this.getComponent(UITransform).setContentSize(size); // this.getComponent(UITransform).setContentSize(getWinSize());
          //控制是否阻挡下方的UI

          this.blockInput = this.node.getComponent(BlockInputEvents);
          this.blockInput.enabled = false;
          this.blockEnableCount = 0; // busyIndicator

          this.busyCount = 0;
          this.busyIndicator = this.node.getChildByName('busyIndicator'); //tip

          this.tip = this.node.getChildByName('tip');
          this.lblTip = this.tip.getChildByName('lbl').getComponent(Label);
          this.animTip = this.tip.getComponent(Animation);
          this.animTip.on(Animation.EventType.FINISHED, function (e, animState) {
            _this2.tip.active = false;
          }); // alert

          this.alertBox = this.node.getChildByName('alertBox');
          this.lblTitle = this.alertBox.getChildByPath('lblTitle').getComponent(Label);
          this.lblContent = this.alertBox.getChildByPath('lblContent').getComponent(Label);
          this.btnYes = this.alertBox.getChildByPath('btnYes');
          this.btnYes['originX'] = this.btnYes.getPosition().x;
          this.btnNo = this.alertBox.getChildByPath('btnNo'); // this.btnYes.on(Button.EventType.CLICK, this.onClick, this);
          // this.btnNo.on(Button.EventType.CLICK, this.onClick, this);

          this.lblYes = this.btnYes.getChildByName('lblYes').getComponent(Label);
          this.lblNo = this.btnNo.getChildByName('lblNo').getComponent(Label);
          this.sprYes = this.btnYes.getChildByName('sprYes').getComponent(Sprite);
          this.sprNo = this.btnNo.getChildByName('sprNo').getComponent(Sprite);
          this.animAlert = this.alertBox.getComponent(Animation);
          var vSize = view.getVisibleSize();
          this.darkMask = this.node.getChildByName('darkMask');
          this.darkMask.getComponent(UITransform).contentSize = vSize; // const graphics = this.darkMask.getComponent(Graphics);
          // graphics.fillRect(-vSize.width * .5, -vSize.height * .5, vSize.width, vSize.height);
          // graphics.fill();
          // const uitrans = this.darkMask.getComponent(UITransform);
          // uitrans.setContentSize(vSize);

          this.node.setPosition(vSize.width * 0.5, vSize.height * 0.5);
          var uiOpacity = this.darkMask.getComponent(UIOpacity);
          uiOpacity.opacity = 150;
          this.twDarkMask = tween(uiOpacity);
          this.darkMask.active = false;
          this.inited = true;
        } // BusyIndicator
        ;

        _proto.updateInputBlock = function updateInputBlock() {
          if (!this.inited) return;
          this.blockInput.enabled = this.inputDisabled;
          this.darkMask.active = this.blockInput.enabled;
        };

        _proto.addBusyCount = function addBusyCount(tag) {
          var _this3 = this;

          this.busyCount = Math.max(0, ++this.busyCount);

          if (!tag || tag.length == 0) {
            tag = '__default__';
          }

          if (!this.busyCountTags[tag]) this.busyCountTags[tag] = 1;else this.busyCountTags[tag]++;

          if (!this.busyIndicator.active) {
            setTimeout(function () {
              _this3.busyIndicator.active = _this3.busyCount > 0;
            }, 500);
          }

          this.updateInputBlock();
        };

        _proto.removeBusyCount = function removeBusyCount(tag) {
          if (!tag || tag.length == 0) {
            tag = '__default__';
          }

          if (!this.busyCountTags[tag]) this.busyCountTags[tag] = 0;
          var tagCount = this.busyCountTags[tag];

          if (tagCount > 0) {
            tagCount--;
            this.busyCountTags[tag] = tagCount;
            this.busyCount = Math.max(0, --this.busyCount);
          }

          if (this.busyIndicator.active) {
            this.busyIndicator.active = this.busyCount > 0;
          }

          this.updateInputBlock();
        };

        _proto.clearBusyCount = function clearBusyCount() {
          this.busyCount = 0;
          this.busyIndicator.active = false;
          this.updateInputBlock();
        };

        _proto.enableBlockEvent = function enableBlockEvent() {
          this.blockEnableCount = Math.max(0, ++this.blockEnableCount);
          this.updateInputBlock();
        };

        _proto.disableBlockEvent = function disableBlockEvent() {
          this.blockEnableCount = Math.max(0, --this.blockEnableCount);
          this.updateInputBlock();
        } // AlertBox
        ;

        _proto.addAlert = function addAlert(alertInfo) {
          this.alertInfos.push(alertInfo);

          if (!this.alertBox.active) {
            this.alertBox.active = true;
            this.updateInputBlock();
            this.nextAlert();
          }
        };

        _proto.nextAlert = function nextAlert() {
          if (this.alertInfos.length == 0) {
            this.alertBox.active = false;
            this.updateInputBlock();
            return;
          }

          this.animAlert.play();
          var alertInfo = this.alertInfos.shift();
          this.lblTitle.string = alertInfo.title || '提示';
          this.lblContent.string = alertInfo.content;

          if (typeof alertInfo.yesTitle == 'string') {
            this.sprYes.node.active = false;
            this.lblYes.node.active = true;
            this.lblYes.string = alertInfo.yesTitle || '确定';
          } else {
            this.sprYes.node.active = true;
            this.lblYes.node.active = false;
            this.sprYes.getComponent(Sprite).spriteFrame = alertInfo.yesTitle;
          }

          if (typeof alertInfo.noTitle == 'string') {
            this.sprNo.node.active = false;
            this.lblNo.node.active = true;
            this.lblNo.string = alertInfo.noTitle || '取消';
          } else {
            this.sprNo.node.active = true;
            this.lblNo.node.active = false;
            this.sprNo.getComponent(Sprite).spriteFrame = alertInfo.noTitle;
          }

          this.btnNo.active = !alertInfo.yesOnly;
          var pos = this.btnYes.position.clone();
          pos.x = alertInfo.yesOnly ? 0 : this.btnYes['originX'];
          this.btnYes.position.set(pos);
          this.curAlertInfo = alertInfo;
        };

        _proto.onClick = function onClick(event, data) {
          var _this$curAlertInfo, _this$curAlertInfo2;

          switch (data) {
            case 'yes':
              if ((_this$curAlertInfo = this.curAlertInfo) != null && _this$curAlertInfo.yesfunc) this.curAlertInfo.yesfunc();
              this.nextAlert();
              break;

            case 'no':
              if ((_this$curAlertInfo2 = this.curAlertInfo) != null && _this$curAlertInfo2.nofunc) this.curAlertInfo.nofunc();
              this.nextAlert();
              break;
          }
        } // tips
        ;

        _proto.addTip = function addTip(tip) {
          this.tip.active = true;
          this.lblTip.string = tip;
          this.lblTip.updateRenderData(true);
          var newWidth = this.lblTip.getComponent(UITransform).contentSize.width;
          this.tip.getComponent(UITransform).setContentSize(Math.max(250, newWidth + 60), 68);
          this.animTip.play();
        } // darkMask
        ;

        _proto.showDarkMask = function showDarkMask() {
          if (!this.inited) return;
          var uiOpacity = this.darkMask.getComponent(UIOpacity);
          this.darkMask.active = true;
          return new Promise(function (resolve) {
            tween(uiOpacity).set({
              opacity: 0
            }).to(0.25, {
              opacity: 255
            }, {
              onComplete: function onComplete() {
                resolve();
              }
            }).start();
          });
        };

        _proto.hideDarkMask = function hideDarkMask() {
          var _this4 = this;

          if (!this.inited) return;
          if (!this.darkMask.active) return;
          var uiOpacity = this.darkMask.getComponent(UIOpacity);
          return new Promise(function (resolve) {
            tween(uiOpacity).set({
              opacity: 255
            }).to(0.25, {
              opacity: 0
            }, {
              onComplete: function onComplete() {
                _this4.darkMask.active = false;
                resolve();
              }
            }).start();
          });
        }
        /** 根据不同参数返回不同的ILoader */
        ;

        _proto.getLoader = function getLoader(type) {
          switch (type) {
            case 'circleLoader':
              return this.circleLoader;

            case 'littleLoader':
              return this.littleLoader;
          }

          return this.littleLoader;
        };

        _proto.addRollingTip = function addRollingTip(text, times, speed) {
          if (times === void 0) {
            times = 1;
          }

          if (speed === void 0) {
            speed = 1;
          }

          this.removeRollingTip();
          this.rollingTip.show(text, times, speed);
        };

        _proto.removeRollingTip = function removeRollingTip() {
          this.rollingTip.hide();
        }
        /**
         * 使响应触摸事件
         * - 此接口目前仅用于相机高度的调整
         * - 谨慎使用，不要影响到其他接口的正常使用
         */
        ;

        _proto.enableInput = function enableInput() {
          this.blockInput.enabled = false;
        }
        /**
         * 使禁止触摸事件
         * - 此接口目前仅用于相机高度的调整
         * - 谨慎使用，不要影响到其他接口的正常使用
         */
        ;

        _proto.disableInput = function disableInput() {
          this.blockInput.enabled = true;
        };

        _createClass(Hint, [{
          key: "inputDisabled",
          get: function get() {
            return this.busyCount > 0 || this.blockEnableCount > 0 || this.alertBox.active || this.circleLoader.node.active || this.littleLoader.node.active;
          }
        }]);

        return Hint;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "circleLoader", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "littleLoader", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "rollingTip", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/HitColor.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _construct, cclegacy, _decorator, Camera, Node, Sprite, v3, RenderTexture, Color, Component;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _construct = module.construct;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Camera = module.Camera;
      Node = module.Node;
      Sprite = module.Sprite;
      v3 = module.v3;
      RenderTexture = module.RenderTexture;
      Color = module.Color;
      Component = module.Component;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3;

      cclegacy._RF.push({}, "0c8dfvs7TdJiYmEcwGDHtcP", "HitColor", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var HitColor = exports('HitColor', (_dec = ccclass('HitColor'), _dec2 = property(Camera), _dec3 = property(Node), _dec4 = property(Node), _dec(_class = (_class2 = /*#__PURE__*/function (_Component) {
        _inheritsLoose(HitColor, _Component);

        function HitColor() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Component.call.apply(_Component, [this].concat(args)) || this;
          _this._target = void 0;

          _initializerDefineProperty(_this, "camera", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "testTarget", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "colorBoard", _descriptor3, _assertThisInitialized(_this));

          _this._texture = void 0;
          return _this;
        }

        var _proto = HitColor.prototype;

        _proto.onLoad = function onLoad() {
          this.setTarget(this.testTarget);
        };

        _proto.setTarget = function setTarget(target) {
          if (target === null) {
            this._target = null;
            this._texture = null;
          } else {
            var sp = target.getComponent(Sprite);
            if (!sp) return;
            this._target = target;
            this._texture = sp.spriteFrame.texture;
            console.log('获得纹理', this._texture);
          }
        };

        _proto.onEnable = function onEnable() {
          this.node.on(Node.EventType.TOUCH_START, this.onTouchStart, this);
          this.node.on(Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
          this.node.on(Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
          this.node.on(Node.EventType.TOUCH_END, this.onTouchEnd, this);
        };

        _proto.onDisable = function onDisable() {
          this.node.off(Node.EventType.TOUCH_START, this.onTouchStart, this);
          this.node.off(Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);
          this.node.off(Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
          this.node.off(Node.EventType.TOUCH_END, this.onTouchEnd, this);
        };

        _proto.getHitColor = function getHitColor(loc) {
          var _console;

          if (!this._target) return;
          var sp = v3(loc.x, loc.y, 0);
          var wp = this.camera.screenToWorld(sp);
          var np = this.camera.convertToUINode(wp, this.node);
          var width = 1;
          var height = 1;
          var x = np.x + this._texture.width / 2 | 0;
          var y = this._texture.height / 2 - np.y | 0;
          var buffer = RenderTexture.prototype.readPixels.call(this._texture, x, y, width, height);

          (_console = console).log.apply(_console, [np.x | 0, np.y | 0, '-'].concat(buffer));

          return _construct(Color, buffer);
        };

        _proto.syncColor = function syncColor(color) {
          this.colorBoard.getComponent(Sprite).color = color;
        };

        _proto.onTouchStart = function onTouchStart(e) {
          e.preventSwallow = true;
          var color = this.getHitColor(e.getLocation());
          color && this.syncColor(color);
        };

        _proto.onTouchCancel = function onTouchCancel(e) {
          e.preventSwallow = true;
        };

        _proto.onTouchMove = function onTouchMove(e) {
          e.preventSwallow = true;
          var color = this.getHitColor(e.getLocation());
          color && this.syncColor(color);
        };

        _proto.onTouchEnd = function onTouchEnd(e) {
          e.preventSwallow = true;
          this.syncColor(new Color(0, 0, 0, 0));
        };

        return HitColor;
      }(Component), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "camera", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "testTarget", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "colorBoard", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/HTMLRichText.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './HTMLRichTextBuilder.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _createClass, _asyncToGenerator, _regeneratorRuntime, cclegacy, _decorator, Sprite, Component, UITransform, HTMLRichTextBuilder;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Sprite = module.Sprite;
      Component = module.Component;
      UITransform = module.UITransform;
    }, function (module) {
      HTMLRichTextBuilder = module.HTMLRichTextBuilder;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class, _class2;

      cclegacy._RF.push({}, "26654Q7PXJNHJtlQSNZRQgj", "HTMLRichText", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property,
          requireComponent = _decorator.requireComponent;
      /** HTML 富文本组件 */

      var HTMLRichText = exports('HTMLRichText', (_dec = ccclass('HTMLRichText'), _dec2 = requireComponent(Sprite), _dec3 = property({
        type: Sprite,
        readonly: true,
        visible: false
      }), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_Component) {
        _inheritsLoose(HTMLRichText, _Component);

        function HTMLRichText() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Component.call.apply(_Component, [this].concat(args)) || this;
          /** HTML 富文本渲染器 */

          _this.builder = void 0;
          return _this;
        }

        var _proto = HTMLRichText.prototype;

        _proto.onLoad = function onLoad() {
          this.builder = new HTMLRichTextBuilder();
        }
        /**
         * 创建富文本并将其应用到目标精灵上
         * @param html 富文本
         * @param width 最大宽度
         */
        ;

        _proto.applyRichText = /*#__PURE__*/function () {
          var _applyRichText = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(html, width) {
            var frame;
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  _context.next = 2;
                  return this.builder.createRichText(width, html);

                case 2:
                  frame = _context.sent;

                  if (frame) {
                    console.info('成功！', frame.width, frame.height);
                    this.sprite.spriteFrame = frame;
                    this.getComponent(UITransform).setContentSize(frame.width, frame.height);
                  } else {
                    console.error('失败！');
                  }

                case 4:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));

          function applyRichText(_x, _x2) {
            return _applyRichText.apply(this, arguments);
          }

          return applyRichText;
        }();

        _createClass(HTMLRichText, [{
          key: "sprite",
          get:
          /** 精灵 */
          function get() {
            return this.getComponent(Sprite);
          }
        }]);

        return HTMLRichText;
      }(Component), _applyDecoratedDescriptor(_class2.prototype, "sprite", [_dec3], Object.getOwnPropertyDescriptor(_class2.prototype, "sprite"), _class2.prototype), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/HTMLRichTextBuilder.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './HTMLRichTextRenderer.ts'], function (exports) {
  var _asyncToGenerator, _regeneratorRuntime, cclegacy, assetManager, ImageAsset, HTMLRichTextRenderer;

  return {
    setters: [function (module) {
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      assetManager = module.assetManager;
      ImageAsset = module.ImageAsset;
    }, function (module) {
      HTMLRichTextRenderer = module.HTMLRichTextRenderer;
    }],
    execute: function () {
      cclegacy._RF.push({}, "37c67glju5L2qAHhgyFrCGd", "HTMLRichTextBuilder", undefined);
      /** HTML 富文本渲染器单例 */


      var htmlRichTextRenderer = new HTMLRichTextRenderer();
      /** HTML 富文本构建工具 */

      var HTMLRichTextBuilder = exports('HTMLRichTextBuilder', /*#__PURE__*/function () {
        /** 已构建的元素列表 */
        function HTMLRichTextBuilder() {
          this._elements = void 0;
          this._elements = [];
        }
        /** 清空已构建的元素列表 */


        var _proto = HTMLRichTextBuilder.prototype;

        _proto.clear = function clear() {
          this._elements.length = 0;
        }
        /**
         * 构建文本元素
         * @param text 内容
         * @param style 样式
         */
        ;

        _proto.buildText = function buildText(text, style) {
          if (style) {
            this._elements.push("<span style=\"" + style + "\">" + text + "</span>");
          } else {
            this._elements.push(text);
          }
        }
        /**
         * 构建图片元素
         * @param path 本地路径
         * @param bundle 所属资源包
         * @param style 样式
         */
        ;

        _proto.buildImage = /*#__PURE__*/function () {
          var _buildImage = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(path, bundle, style) {
            var _this = this;

            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  return _context.abrupt("return", new Promise(function (resolve) {
                    assetManager.loadBundle(bundle, function (e1, bun) {
                      if (e1) {
                        console.error(e1);
                        return resolve();
                      }

                      bun.load(path, ImageAsset, function (e2, img) {
                        if (e2) {
                          console.error(e2);
                          return resolve();
                        }

                        _this.buildImageAsset(img, style);

                        resolve();
                      });
                    });
                  }));

                case 1:
                case "end":
                  return _context.stop();
              }
            }, _callee);
          }));

          function buildImage(_x, _x2, _x3) {
            return _buildImage.apply(this, arguments);
          }

          return buildImage;
        }()
        /**
         * 构建图片元素
         * @param img 图片资源
         * @param style 样式
         */
        ;

        _proto.buildImageAsset = function buildImageAsset(img, style) {
          if (img) {
            var dataURL = htmlRichTextRenderer.convertImageAsDataURL(img);

            if (style) {
              this._elements.push("<img src=\"" + dataURL + "\" style=\"" + style + "\"/>");
            } else {
              this._elements.push("<img src=\"" + dataURL + "\"/>");
            }
          }
        }
        /** 构建富文本内容 */
        ;

        _proto.build = function build() {
          var html = this._elements.join('\n');

          this.clear();
          return html;
        }
        /**
         * 创建 HTML 富文本
         * @param maxWidth 最大宽度
         * @param html 富文本内容
         * @returns
         */
        ;

        _proto.createRichText = function createRichText(maxWidth, html) {
          if (!html && this._elements.length > 0) {
            html = this.build();
          }

          if (html) {
            return htmlRichTextRenderer.createHTMLRichText(maxWidth, html);
          } else {
            return Promise.resolve(null);
          }
        };

        return HTMLRichTextBuilder;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/HTMLRichTextDemo.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './HTMLRichText.ts'], function (exports) {
  var _inheritsLoose, _asyncToGenerator, _regeneratorRuntime, cclegacy, _decorator, HTMLRichText;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
    }, function (module) {
      HTMLRichText = module.HTMLRichText;
    }],
    execute: function () {
      var _dec, _class;

      cclegacy._RF.push({}, "fb02bjvjnJDQbMq0Ei+yZMG", "HTMLRichTextDemo", undefined);

      var ccclass = _decorator.ccclass;
      /**
       * HTML 富文本 demo
       */

      var HTMLRichTextDemo = exports('HTMLRichTextDemo', (_dec = ccclass('HTMLRichTextDemo'), _dec(_class = /*#__PURE__*/function (_HTMLRichText) {
        _inheritsLoose(HTMLRichTextDemo, _HTMLRichText);

        function HTMLRichTextDemo() {
          return _HTMLRichText.apply(this, arguments) || this;
        }

        var _proto = HTMLRichTextDemo.prototype;

        _proto.start = /*#__PURE__*/function () {
          var _start = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
            var html;
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  this.builder.buildText('Show it Ben,', 'color:rgb(255,99,71); font-style: italic; text-shadow:0 0 10px blue;');
                  this.builder.buildText('<span style="text-decoration: underline blue 4px;"> 👎🏻👎🏻👎🏻 </span>');
                  _context.next = 4;
                  return this.builder.buildImage('res/big/btn_show', 'MainModule', 'width:40px;vertical-align: middle;');

                case 4:
                  this.builder.buildText('<span style="color:red; text-decoration: line-through black 2px;"> Watch out limar 🤫 </span>');
                  _context.next = 7;
                  return this.builder.buildImage('res/ui/btn_bag', 'MainModule', 'vertical-align: middle;');

                case 7:
                  html = this.builder.build();
                  _context.next = 10;
                  return this.applyRichText(html, 240);

                case 10:
                  window.twice = this.applyRichText.bind(this, html);

                case 11:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));

          function start() {
            return _start.apply(this, arguments);
          }

          return start;
        }();

        return HTMLRichTextDemo;
      }(HTMLRichText)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/HTMLRichTextRenderer.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _createClass, _asyncToGenerator, _regeneratorRuntime, cclegacy, SpriteFrame;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      SpriteFrame = module.SpriteFrame;
    }],
    execute: function () {
      cclegacy._RF.push({}, "65cffThuRhDuJijXIeeiMt3", "HTMLRichTextRenderer", undefined);
      /** HTML 富文本渲染器 */


      var HTMLRichTextRenderer = exports('HTMLRichTextRenderer', /*#__PURE__*/function () {
        function HTMLRichTextRenderer() {
          this._canvas = void 0;
          this._ctx = void 0;
          this._debug = void 0;
          this._canvas = document.createElement('canvas');
          this._ctx = this._canvas.getContext('2d', {
            willReadFrequently: true
          });
          this.debug = false;
        }
        /** 使能调试 */


        var _proto = HTMLRichTextRenderer.prototype;
        /**
         * 把图片资源转换成 DataURL
         * @param image 图片资源
         * @returns
         */

        _proto.convertImageAsDataURL = function convertImageAsDataURL(image) {
          var w = image.width;
          var h = image.height;
          this._canvas.width = w;
          this._canvas.height = h;

          this._ctx.clearRect(0, 0, w, h);

          this._ctx.drawImage(image.data, 0, 0);

          return this._canvas.toDataURL();
        }
        /**
         * 创建 HTML 富文本
         * @param maxWidth 最大宽度
         * @param html HTML 富文本
         * @returns
         */
        ;

        _proto.createHTMLRichText = /*#__PURE__*/function () {
          var _createHTMLRichText = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(maxWidth, html) {
            var _this = this;

            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  return _context.abrupt("return", new Promise(function (resolve) {
                    // 创建一个用于计算高度的元素，放在游戏容器背后，这样就看不见了
                    var text = "<span style=\"color:white; word-break: break-all; font-size: 24px; width: " + maxWidth + "px;\">" + html + "</span>";
                    var e = document.createElement('span');
                    e.style.width = maxWidth + 'px';
                    e.style.position = 'absolute';
                    e.style.display = 'block';
                    e.innerHTML = text;
                    var gameDiv = document.querySelector('#GameDiv');
                    var gameContainer = document.querySelector('.contentWrap');
                    gameContainer.insertBefore(e, gameDiv); // 下一帧才能获取到正确的高度

                    setTimeout(function () {
                      var width = e.clientWidth;
                      var height = e.clientHeight; // 记得删除这个临时的元素

                      gameContainer.removeChild(e); // 把它放在 svg 里面渲染

                      var svg = "data:image/svg+xml;charset=utf-8,\n                    <svg width=\"" + width + "\" height=\"" + height + "\" xmlns=\"http://www.w3.org/2000/svg\" xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n                    <foreignObject x=\"0\" y=\"0\" width=\"" + width + "\" height=\"" + height + "\">\n                        <span xmlns=\"http://www.w3.org/1999/xhtml\">" + text + "</span>\n                    </foreignObject>\n                    </svg>"; // 把 svg 丢给 HTMLImageElement 展示

                      var img = new Image();
                      img.crossOrigin = 'anonymous';

                      img.onload = function () {
                        // 用 Canvas 绘制 Image
                        _this._canvas.width = width;
                        _this._canvas.height = height;

                        _this._ctx.clearRect(0, 0, width, height);

                        if (_this._debug) {
                          _this._ctx.fillStyle = _this.fillColor;

                          _this._ctx.fillRect(0, 0, width, height);
                        }

                        _this._ctx.drawImage(img, 0, 0); // 将绘制结果导出为 SpriteFrame


                        resolve(SpriteFrame.createWithImage(_this._canvas));
                      };

                      img.onerror = function () {
                        return Promise.resolve(null);
                      };

                      img.src = svg;
                    }, 0);
                  }));

                case 1:
                case "end":
                  return _context.stop();
              }
            }, _callee);
          }));

          function createHTMLRichText(_x, _x2) {
            return _createHTMLRichText.apply(this, arguments);
          }

          return createHTMLRichText;
        }();

        _createClass(HTMLRichTextRenderer, [{
          key: "debug",
          set: function set(d) {
            this._debug = d;
          }
          /** 背景填充颜色（调试用） */

        }, {
          key: "fillColor",
          get: function get() {
            return this._debug ? '#f4798333' : '#000000';
          }
        }]);

        return HTMLRichTextRenderer;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/HttpClient.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Logger.ts'], function (exports) {
  var _inheritsLoose, _wrapNativeSuper, cclegacy, logger;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _wrapNativeSuper = module.wrapNativeSuper;
    }, function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      cclegacy._RF.push({}, "b2230njeERBl5KvLKZ+mts7", "HttpClient", undefined);

      var HttpRequest = /*#__PURE__*/function (_XMLHttpRequest) {
        _inheritsLoose(HttpRequest, _XMLHttpRequest);

        function HttpRequest() {
          var _this;

          _this = _XMLHttpRequest.call(this) || this;
          _this.router = void 0;
          _this.method = void 0;
          _this.data = void 0;
          _this.active = void 0;
          _this.http = void 0;
          _this.ontimeout = _this.onTimeout;
          _this.onabort = _this.onAbort;
          _this.onerror = _this.onError;
          _this.onloadstart = _this.onStart;
          _this.onprogress = _this.onProgress;
          _this.onload = _this.onLoad;
          _this.onloadend = _this.onEnd;
          _this.active = false;
          return _this;
        }

        var _proto = HttpRequest.prototype;

        _proto.initHeader = function initHeader() {
          if (this.http.requestHeaderConfig) {
            for (var _name in this.http.requestHeaderConfig) {
              var value = this.http.requestHeaderConfig[_name];
              this.setRequestHeader(_name, value);
            }
          }
        };

        _proto.request = function request(method, router, data) {
          var _this$http$funcExecut;

          (_this$http$funcExecut = this.http.funcExecuteCallback) == null ? void 0 : _this$http$funcExecut.call(this, 'request');
          this.active = true;
          this.method = method;
          this.router = router;
          this.data = data;
          this.timeout = this.http.uriCfg.timeout;
          var url = this.http.uriCfg.url + router;

          if (data == null) {
            this.open(method, url);
            this.send();
            return;
          }

          var param = '';

          switch (method) {
            case 'post':
              {
                this.open(method, url);
                this.initHeader();
                param = JSON.stringify(data);
                this.send(param);
                break;
              }

            case 'get':
              {
                for (var k in data) {
                  if (param.length > 0) param += '&';
                  param += k + '=' + data[k];
                }

                if (param.length > 0) param.substring(0, param.length - 1);
                url += '?' + param;
                this.open(method, url);
                this.initHeader();
                this.send();
                break;
              }
          } // logger.info(`[HTTP][${method}][${router}] param = ${param}`);
          // this.setRequestHeader('Content-type', 'application/json;charset=utf-8');
          // this.setRequestHeader('Authorization', '');
          // this.setRequestHeader('Content-type', 'application/x-www-form-urlencoded;charset=utf-8');
          // this.setRequestHeader('Content-type', 'multipart/form-data;boundary=----WebKitFormBoundaryVgpn2OSlfdPspczg');
          // encodeURIComponent()
          // decodeURIComponent()
          // decodeURI()
          // this.send(encodeURI(param));

        };

        _proto.onTimeout = function onTimeout(e) {
          var _this$http$funcExecut2;

          this.http.requestPools.push(this);
          (_this$http$funcExecut2 = this.http.funcExecuteCallback) == null ? void 0 : _this$http$funcExecut2.call(this, 'ontimeout');
        };

        _proto.onAbort = function onAbort(e) {
          var _this$http$funcExecut3;

          this.http.requestPools.push(this);
          (_this$http$funcExecut3 = this.http.funcExecuteCallback) == null ? void 0 : _this$http$funcExecut3.call(this, 'onabort');
        };

        _proto.onError = function onError(e) {
          var _this$http$funcExecut4;

          this.http.requestPools.push(this);
          (_this$http$funcExecut4 = this.http.funcExecuteCallback) == null ? void 0 : _this$http$funcExecut4.call(this, 'onerror');
        };

        _proto.onStart = function onStart(e) {};

        _proto.onProgress = function onProgress(e) {};

        _proto.onLoad = function onLoad(e) {//接收一次数据，有可能不完整
        };

        _proto.onEnd = function onEnd(e) {
          var _this$http$funcExecut5; //接收完整数据


          this.active = false; // TODO timeout/abort/error 都会 push，可能会存在重复问题

          this.http.requestPools.push(this);
          (_this$http$funcExecut5 = this.http.funcExecuteCallback) == null ? void 0 : _this$http$funcExecut5.call(this, 'onloadend');
          this.http.respCallback(this);
        };

        return HttpRequest;
      }( /*#__PURE__*/_wrapNativeSuper(XMLHttpRequest));
      /**
       * HTTP 请求管理
       * - 发送请求（GET/POST）
       * - XMLHttpRequest 请求资源缓存
       * - 绑定、解绑请求回调
       */


      var HttpClient = /*#__PURE__*/function () {
        /**
         * requestHeaderConfig = {
         *  'Content-type', 'application/json;charset=utf-8',
         *  ...
         * }
         */
        function HttpClient() {
          this.routerCallbacks = void 0;
          this.requestHeaderConfig = {};
          this.uriCfg = {
            url: '',
            timeout: 0
          };
          this.requestPools = null;
          this.funcExecuteCallback = void 0;
          this.requestPools = new Array();
        }

        var _proto2 = HttpClient.prototype;

        _proto2.getRequest = function getRequest() {
          var req;

          if (this.requestPools.length > 0) {
            for (var i = this.requestPools.length - 1; i >= 0; i--) {
              var r = this.requestPools[i];

              if (r.active == false) {
                req = r;
                this.requestPools.splice(i, 1);
                break;
              }
            }
          }

          if (!req) req = new HttpRequest();
          req.http = this;
          return req;
        };

        _proto2.get = function get(router, args) {
          var req = this.getRequest();
          req.request('get', router, args);
        };

        _proto2.post = function post(router, args) {
          var req = this.getRequest();
          req.request('post', router, args);
        };

        _proto2.abort = function abort() {};

        _proto2.registerRouterInCallbacks = function registerRouterInCallbacks(routerCallbacks) {
          this.routerCallbacks = this.routerCallbacks || {};

          for (var _key in routerCallbacks) {
            this.routerCallbacks[_key] = routerCallbacks[_key];
          }
        };

        _proto2.unregisterRouterInCallbacks = function unregisterRouterInCallbacks(routerCallbacks) {
          this.routerCallbacks = this.routerCallbacks || {};

          for (var _key2 in routerCallbacks) {
            delete this.routerCallbacks[_key2];
          }
        };

        _proto2.respCallback = function respCallback(req) {
          var data = null;
          var status = req.status;

          if (status >= 200 && status < 300 || status == 304) {
            if (req.responseType == 'text') data = JSON.parse(req.responseText);else data = JSON.parse(req.response);

            if (data.code !== 0) {
              var _this$funcExecuteCall;

              (_this$funcExecuteCall = this.funcExecuteCallback) == null ? void 0 : _this$funcExecuteCall.call(null, 'respCodeError', data.msg);
            }
          } else {
            data = req.statusText;
          } // logger.log(`[HTTP][Response][${status}]`, req.router, data);


          var callback = this.routerCallbacks[req.router];

          if (callback) {
            callback(data.code, data);
          } else {
            logger.warn('未处理的request: ', this.uriCfg.url + req.router, req.data, req.responseText);
          }
        };

        _proto2.addHeaderConfig = function addHeaderConfig(config) {
          for (var _key3 in config) {
            this.requestHeaderConfig[_key3] = config[_key3];
          }
        };

        return HttpClient;
      }();

      var httpClient = exports('httpClient', new HttpClient());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/IHint.ts", ['cc'], function () {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      cclegacy._RF.push({}, "592eeZS7PhJ1qBkAzWaKJd/", "IHint", undefined);

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ImageClip.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Node, Sprite, UITransform, view, director, Director, game, SpriteFrame, Component;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Node = module.Node;
      Sprite = module.Sprite;
      UITransform = module.UITransform;
      view = module.view;
      director = module.director;
      Director = module.Director;
      game = module.game;
      SpriteFrame = module.SpriteFrame;
      Component = module.Component;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2;

      cclegacy._RF.push({}, "24912uILNVEG6LiwptCdEHQ", "ImageClip", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      /**
       * 截取指定节点所在的区域
       * @description
       * 首先声明，这是一种偷懒的做法，不太建议在大范围内使用。一般来说，我们做截图是通过摄像机控制 Visibility，
       * 将需要截取的内容渲染到 RenderTexture，再从 RenderTexture 绘制到 Sprite 或者 Canvas 上。这里提供了
       * 另外一种思路，即先截取全屏，再根据目标节点的位置确定截取的区域，最终绘制到 Canvas 上。优点是简单便捷，不
       * 需要摄像机的参与；缺点是无法控制显示内容（屏幕上显示什么就截取什么内容）。
       */

      var ImageClip = exports('ImageClip', (_dec = ccclass('ImageClip'), _dec2 = property(Node), _dec3 = property(Sprite), _dec(_class = (_class2 = /*#__PURE__*/function (_Component) {
        _inheritsLoose(ImageClip, _Component);

        function ImageClip() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Component.call.apply(_Component, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "target", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "copy", _descriptor2, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = ImageClip.prototype;

        _proto.onCaptureClicked = function onCaptureClicked() {
          var _this2 = this;

          var wp = this.target.worldPosition;
          var ts = this.target.getComponent(UITransform);
          var vs = view.getVisibleSize();
          var vw = vs.width | 0;
          var vh = vs.height | 0;
          var tw = ts.width | 0;
          var th = ts.height | 0;
          director.once(Director.EVENT_AFTER_DRAW, function () {
            var url = game.canvas.toDataURL('image/jpeg');
            var img = new Image();
            img.src = url;

            img.onload = function () {
              var canvas = document.createElement('canvas');
              var ctx = canvas.getContext('2d');
              var fw = img.width / vw;
              var fh = img.height / vh;
              var w = tw * fw | 0;
              var h = th * fh | 0;
              var fx = (wp.x - ts.width / 2) * fw | 0;
              var fy = (vh - (wp.y + ts.height / 2)) * fh | 0;
              canvas.width = w;
              canvas.height = h;
              ctx.drawImage(img, fx, fy, w, h, 0, 0, w, h);
              _this2.copy.spriteFrame = SpriteFrame.createWithImage(canvas);

              _this2.copy.node.setScale(1 / fw, 1 / fh);
            };
          });
        };

        return ImageClip;
      }(Component), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "target", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "copy", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/index.ts", ['cc', './exports.ts', './base-application.ts'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      exports('LanguageRegion', module.LanguageRegion);
    }, function (module) {
      exports('BaseApplication', module.BaseApplication);
    }],
    execute: function () {
      cclegacy._RF.push({}, "8f0denHBv9F2IMhV5hcJE+U", "index", undefined);

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Interface.ts", ['cc'], function () {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      cclegacy._RF.push({}, "0424aPM+itDwoQZEYy3Pgqm", "Interface", undefined);

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Kindof.ts", ['cc'], function (exports) {
  var cclegacy, isValid, Node, Component;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
      isValid = module.isValid;
      Node = module.Node;
      Component = module.Component;
    }],
    execute: function () {
      exports('KindOf', void 0);

      cclegacy._RF.push({}, "2a108izAvpCzKrq32zXqEgG", "Kindof", undefined);
      /**
       * 类型判定辅助工具
       */


      var KindOf;

      (function (_KindOf) {
        function isFunction(item) {
          return typeof item === 'function';
        }

        _KindOf.isFunction = isFunction;

        function isNumber(item) {
          return typeof item === 'number' && !isNaN(item);
        }

        _KindOf.isNumber = isNumber;

        function isString(item) {
          return typeof item === 'string';
        }

        _KindOf.isString = isString;

        function isObject(item) {
          return Object.prototype.toString.call(item) === '[object Object]';
        }

        _KindOf.isObject = isObject;

        function isArray(item) {
          return item instanceof Array;
        }

        _KindOf.isArray = isArray;

        function isBoolean(item) {
          return typeof item === 'boolean';
        }

        _KindOf.isBoolean = isBoolean;

        function isTrue(item) {
          if (isBoolean(item)) {
            // 布尔值直接判定值
            return Boolean(item);
          } else {
            // null/undefined 判定为假，其他类型判定为真
            return !isNull(item);
          }
        }

        _KindOf.isTrue = isTrue;

        function isFalse(item) {
          return !isTrue(item);
        }

        _KindOf.isFalse = isFalse;

        function isUndefined(t) {
          return t === undefined || t === null;
        }

        _KindOf.isUndefined = isUndefined;

        function isNull(item) {
          return isUndefined(item) || !isValid(item);
        }

        _KindOf.isNull = isNull;

        function notNull(item) {
          return !isUndefined(item) && isValid(item);
        }

        _KindOf.notNull = notNull;

        function isNode(item) {
          return item instanceof Node;
        }

        _KindOf.isNode = isNode;

        function isComponent(item) {
          return item instanceof Component;
        }

        _KindOf.isComponent = isComponent;
      })(KindOf || (KindOf = exports('KindOf', {})));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/LittleLoader.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, cclegacy, _decorator, Label, ProgressBar, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Label = module.Label;
      ProgressBar = module.ProgressBar;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2;

      cclegacy._RF.push({}, "29934v5G5dPb4njoSkumYVQ", "LittleLoader", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var LittleLoader = exports('LittleLoader', (_dec = ccclass('LittleLoader'), _dec2 = ccmodifier('LittleLoader'), _dec3 = property(Label), _dec4 = property(ProgressBar), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(LittleLoader, _BaseComponent);

        function LittleLoader() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "lbl", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "progressBar", _descriptor2, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = LittleLoader.prototype;

        _proto.load = function load() {
          this.node.active = true;
          this.progress = 0;
        };

        _proto.onCompleted = function onCompleted() {
          this.progress = 1;
          this.node.active = false;
          return Promise.resolve();
        };

        _createClass(LittleLoader, [{
          key: "progress",
          get: function get() {
            return this.progressBar.progress;
          },
          set: function set(value) {
            this.progressBar.progress = value;
            this.lbl.string = (value * 100 >> 0) + '%';
          }
        }]);

        return LittleLoader;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "lbl", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "progressBar", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Logger.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Dates.ts'], function (exports) {
  var _createClass, cclegacy, Dates;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      Dates = module.Dates;
    }],
    execute: function () {
      cclegacy._RF.push({}, "a3ecbvNmhxJqLeYSrB5ZROD", "Logger", undefined);
      /**
       * 日志管理器
       */


      var Logger = /*#__PURE__*/function () {
        function Logger() {
          /**
           * 是否开启
           */
          this._enabled = true;
        }

        var _proto = Logger.prototype;
        /**
         * 输出日志
         * @param tag 日志方法类型
         * @param args 输入参数
         * @returns
         */

        _proto._print = function _print(tag) {
          var _console;

          if (!this._enabled) return;
          var date = Dates.getTimeString(true); // const stack = GetStack();
          // const fmt = `%c${date} ${stack}`;
          // const styles = [
          //     `background: ${METHOD_COLOR_MAP[tag]}`,
          //     `border-radius: 0.5em`,
          //     `color: black`,
          //     `font-weight: bold`,
          //     `padding: 2px 0.5em`,
          // ];
          // console.groupCollapsed(fmt, styles.join(';'));
          // args.length > 0 && args.forEach((v) => console[tag](v));
          // console.groupEnd();

          for (var _len = arguments.length, args = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            args[_key - 1] = arguments[_key];
          }

          (_console = console)[tag].apply(_console, [date].concat(args));
        }
        /**
         * 是否开启
         */
        ;
        /**
         * 输出调试
         * @param args 输入参数
         */


        _proto.d = function d() {
          for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            args[_key2] = arguments[_key2];
          }

          this._print.apply(this, ['debug'].concat(args));
        }
        /**
         * 输出信息
         * @param args 输入参数
         */
        ;

        _proto.i = function i() {
          for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
            args[_key3] = arguments[_key3];
          }

          this._print.apply(this, ['info'].concat(args));
        }
        /**
         * 输出警告
         * @param args 输入参数
         */
        ;

        _proto.w = function w() {
          for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
            args[_key4] = arguments[_key4];
          }

          this._print.apply(this, ['warn'].concat(args));
        }
        /**
         * 输出错误
         * @param args 输入参数
         */
        ;

        _proto.e = function e() {
          for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
            args[_key5] = arguments[_key5];
          }

          this._print.apply(this, ['error'].concat(args));
        };

        _proto.log = function log() {
          this.i.apply(this, arguments);
        };

        _proto.info = function info() {
          this.i.apply(this, arguments);
        };

        _proto.warn = function warn() {
          this.w.apply(this, arguments);
        };

        _proto.error = function error() {
          this.e.apply(this, arguments);
        };

        _createClass(Logger, [{
          key: "enabled",
          get: function get() {
            return this._enabled;
          }
          /**
           * 开启/关闭日志
           */
          ,
          set: function set(v) {
            this._enabled = v;
          }
        }]);

        return Logger;
      }();

      var logger = exports('logger', new Logger());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/main", ['./debug-view-runtime-control.ts', './BoneOfGirl.ts', './ButtonChangeAni.ts', './ButtonChangeDir.ts', './HTMLRichText.ts', './HTMLRichTextBuilder.ts', './HTMLRichTextDemo.ts', './HTMLRichTextRenderer.ts', './HitColor.ts', './ImageClip.ts', './Column.ts', './Container.ts', './Declaraions.ts', './Painter.ts', './Row.ts', './PartialSprite.ts', './RoleBone.ts', './RoleBoneExports.ts', './RoleBoneOfNvhai.ts', './RoundSprite.ts', './Web.ts', './PreloadAssets.ts', './Start.ts', './CircleLoader.ts', './Hint.ts', './LittleLoader.ts', './RollingTip.ts', './Interface.ts', './Model.ts', './Modifier.ts', './Module.ts', './View.ts', './BatchItems.ts', './Persistencee.ts', './RemoteImage.ts', './SpriteEx.ts', './Switch.ts', './ToggleContainerEx.ts', './ToggleEx.ts', './BezierCurve.ts', './BezierCurveAnimation.ts', './BezierCurveAnimationTweenUnit.ts', './MoveBase.ts', './RollingLottery.ts', './RotatingLottery.ts', './ScrollCell.ts', './ui_switcher.ts', './VirtualListItem.ts', './VirtualListView.ts', './DLinkList.ts', './AssetLocator.ts', './AudioEngine.ts', './ButtonDecorator.ts', './CDMgr.ts', './EventDispatcher.ts', './GlobalEvent.ts', './ModuleManager.ts', './QueueLoader.ts', './ResManager.ts', './StorageManager.ts', './ViewManager.ts', './ByteArray.ts', './HttpClient.ts', './SocketClient.ts', './ArrayUtils.ts', './Dates.ts', './GraphicsTool.ts', './Kindof.ts', './Logger.ts', './Numbers.ts', './Objects.ts', './Singletons.ts', './Sorter.ts', './SpineUtils.ts', './StringUtils.ts', './UtilTools.ts', './IHint.ts', './TopTouchProcessor.ts', './base-application.ts', './exports.ts', './index.ts'], function () {
  return {
    setters: [null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null],
    execute: function () {}
  };
});

System.register("chunks:///_virtual/Model.ts", ['cc'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      exports('createModel', createModel);

      cclegacy._RF.push({}, "f8f2bAFO0dIXaccWOIlcJQh", "Model", undefined);
      /**
       * 数据模型
       * - 数据托管
       * - 通知观察者
       */


      var Model = /*#__PURE__*/function () {
        function Model() {
          /** 观察者列表 */
          this._observers = [];
          /** 数据模板 */

          this._template = void 0;
        }

        var _proto = Model.prototype;
        /** 设置数据模板 */

        _proto.setTemplate = function setTemplate(template) {
          if (this._template) return;
          this._template = template;
        }
        /**
         * 添加观察者
         * @param observer 观察者
         */
        ;

        _proto.addObserver = function addObserver(observer) {
          this._observers.push(observer);
        }
        /**
         * 移除观察者
         * @param observer 观察者
         */
        ;

        _proto.removeObserver = function removeObserver(observer) {
          var i = this._observers.indexOf(observer);

          if (i > -1) {
            this._observers.splice(i, 1);
          }
        }
        /**
         * 通知（更新）观察者
         * @param evt 事件
         */
        ;

        _proto.notify = function notify(evt) {
          var value = this._template[evt];

          if (value !== undefined) {
            this._observers.forEach(function (ob) {
              return ob.onValueChange(evt, value);
            });
          }
        };

        return Model;
      }();
      /**
       * 使用数据模板创建数据模型
       * @param template 数据模板
       * @returns
       */


      function createModel(template) {
        var model = new Model();
        model.setTemplate(template);
        return new Proxy(template, {
          get: function get(target, key) {
            return key in template ? template[key] : model[key];
          },
          set: function set(target, evt, value) {
            target[evt] = value;
            model.notify(evt);
            return true;
          }
        });
      }

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Modifier.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _inheritsLoose, _createClass, cclegacy, _decorator, js, Component;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      js = module.js;
      Component = module.Component;
    }],
    execute: function () {
      exports('ccmodifier', ccmodifier);

      var _dec, _class;

      cclegacy._RF.push({}, "5ae81xGUlNJgLI6CmK4CDES", "Modifier", undefined);

      var ccclass = _decorator.ccclass;
      /** 基础组件 */

      var BaseComponent = exports('BaseComponent', (_dec = ccclass(), _dec(_class = /*#__PURE__*/function (_Component) {
        _inheritsLoose(BaseComponent, _Component);

        function BaseComponent() {
          return _Component.apply(this, arguments) || this;
        }

        _createClass(BaseComponent, [{
          key: "uname",
          get:
          /** 组件名称 */
          function get() {
            var _uname__;

            return (_uname__ = this.constructor['__uname__']) != null ? _uname__ : js.getClassName(this.constructor);
          }
        }]);

        return BaseComponent;
      }(Component)) || _class));
      /**
       * 装饰器之脚本修饰器
       * @param uname 脚本名称
       * @returns
       */

      function ccmodifier(uname) {
        return function ($target) {
          Object.defineProperty($target, '__uname__', {
            value: uname,
            writable: false
          });
        };
      }

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Module.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _inheritsLoose, cclegacy, BaseComponent;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
    }, function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      cclegacy._RF.push({}, "74dd0NEiB1HlbqtWf9bf/zZ", "Module", undefined);
      /**
       * 模块基类
       */


      var Module = exports('Module', /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(Module, _BaseComponent);

        function Module() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          /** 模块名称 */

          _this.moduleName = void 0;
          /** 归属场景 */

          _this.scene = void 0;
          /** 归属 AB */

          _this.bundle = void 0;
          /** 数据模型 */

          _this.model = void 0;
          return _this;
        }

        var _proto = Module.prototype;
        /**
         * 启动模块
         * - 开发者需要继承此接口
         * @param data 数据
         * @param fromModule 前置模块
         */

        _proto.onLaunched = function onLaunched(data, fromModule) {}
        /**
         * 模块退出回调
         * @description 这里可以做一点资源释放以及退出动画之类的操作
         */
        ;

        _proto.onExit = function onExit() {
          return Promise.resolve();
        }
        /**
         * 通知模块进入场景
         * - 模块加载完成后由模块管理器调用
         * @param sceneName 场景名称
         * @param scene 场景
         */
        ;

        _proto.onEnterScene = function onEnterScene(sceneName, scene) {}
        /**
         * 获取 HTTP 接口、回调映射
         * - 开发者需重写此方法
         * @returns
         */
        ;

        _proto.getHttpCallback = function getHttpCallback() {
          return {};
        }
        /**
         * 获取 WebSocket 接口、回调映射
         * - 开发者需重写此方法
         * @returns
         */
        ;

        _proto.getWebSocketCallback = function getWebSocketCallback() {
          return {
            mcmd: -1,
            callbacks: []
          };
        };

        return Module;
      }(BaseComponent));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ModuleManager.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Module.ts', './SocketClient.ts', './ResManager.ts', './ViewManager.ts', './Logger.ts', './HttpClient.ts'], function (exports) {
  var _asyncToGenerator, _regeneratorRuntime, _createForOfIteratorHelperLoose, cclegacy, director, SceneAsset, Module, socketClient, res, vm, logger, httpClient;

  return {
    setters: [function (module) {
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
      _createForOfIteratorHelperLoose = module.createForOfIteratorHelperLoose;
    }, function (module) {
      cclegacy = module.cclegacy;
      director = module.director;
      SceneAsset = module.SceneAsset;
    }, function (module) {
      Module = module.Module;
    }, function (module) {
      socketClient = module.socketClient;
    }, function (module) {
      res = module.res;
    }, function (module) {
      vm = module.vm;
    }, function (module) {
      logger = module.logger;
    }, function (module) {
      httpClient = module.httpClient;
    }],
    execute: function () {
      cclegacy._RF.push({}, "c32d05MdaNKjY9gzakOfcWn", "ModuleManager", undefined);
      /**
       * 模块管理器
       */


      var ModuleManager = /*#__PURE__*/function () {
        function ModuleManager() {
          // 同时只能有一个Module
          this.runningModule = void 0; // 主模块名字

          this.mainModuleName = void 0; // 主模块永驻

          this.mainModule = void 0; // 主模块需要预加载的资源

          this.preloadAssetsInfo = void 0;
        }

        var _proto = ModuleManager.prototype;
        /** 初始化预加载资源 */

        _proto.initPreloadAssets = function initPreloadAssets(assets) {
          this.preloadAssetsInfo = assets;
        }
        /** 初始化模块 */
        ;

        _proto.initModule = /*#__PURE__*/function () {
          var _initModule = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(module) {
            var bundle;
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  _context.next = 2;
                  return res.getBundle(module.moduleName);

                case 2:
                  bundle = _context.sent;
                  module.bundle = bundle;
                  httpClient.registerRouterInCallbacks(module.getHttpCallback());
                  socketClient.registerRouterInCallbacks(module.getWebSocketCallback());

                case 6:
                case "end":
                  return _context.stop();
              }
            }, _callee);
          }));

          function initModule(_x) {
            return _initModule.apply(this, arguments);
          }

          return initModule;
        }()
        /**
         * 获取模块
         * @param moduleName 模块名称
         * @returns
         */
        ;

        _proto.getModule = function getModule(moduleName) {
          if (moduleName == this.mainModuleName) {
            return this.mainModule;
          }

          if (this.runningModule && this.runningModule.moduleName == moduleName) {
            return this.runningModule;
          }

          return null;
        }
        /**
         * 获取模块的数据模型
         * @param moduleName 模块名称
         * @returns
         */
        ;

        _proto.getModuleModel = function getModuleModel(moduleName) {
          if (moduleName == this.mainModuleName) {
            var _this$mainModule;

            return (_this$mainModule = this.mainModule) == null ? void 0 : _this$mainModule.model;
          }

          if (this.runningModule && this.runningModule.moduleName == moduleName) {
            return this.runningModule.model;
          }

          return null;
        }
        /**
         * 切换场景
         * @param sceneName 场景名称
         * @param data 携带数据
         * @param loader 加载器
         * @returns
         */
        ;

        _proto.changeScene = function changeScene(sceneName, data, loader) {
          var _this = this;

          return new Promise( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(resolve, reject) {
            var bundle, runningScene;
            return _regeneratorRuntime().wrap(function _callee3$(_context3) {
              while (1) switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.next = 2;
                  return res.getBundle(_this.runningModule.moduleName);

                case 2:
                  bundle = _context3.sent;
                  runningScene = director.getScene();

                  if (!(runningScene && runningScene.name == sceneName)) {
                    _context3.next = 7;
                    break;
                  }

                  logger.w("\u573A\u666F " + sceneName + " \u5DF2\u5B58\u5728!");
                  return _context3.abrupt("return");

                case 7:
                  bundle.loadScene(sceneName, function (finished, total) {
                    loader && (loader.progress = finished / total);
                  }, /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(err, scene) {
                    var _vm$hint;

                    return _regeneratorRuntime().wrap(function _callee2$(_context2) {
                      while (1) switch (_context2.prev = _context2.next) {
                        case 0:
                          (_vm$hint = vm.hint) == null ? void 0 : _vm$hint.updateInputBlock();
                          err && reject(err);
                          director.runScene(scene, undefined, function () {
                            var runScene = director.getScene();

                            if (runScene) {
                              vm.clearAllView();
                              vm.initWithScene(runScene);

                              _this.runningModule.onEnterScene(sceneName, runScene);
                            }

                            resolve();
                          });

                        case 3:
                        case "end":
                          return _context2.stop();
                      }
                    }, _callee2);
                  })));

                case 8:
                case "end":
                  return _context3.stop();
              }
            }, _callee3);
          })));
        }
        /**
         * 加载模块内部固定场景 scene
         * @param moduleName 模块名称
         * @param loader 加载器
         * @returns
         */
        ;

        _proto.loadScene = function loadScene(moduleName, loader) {
          var _this2 = this;

          return new Promise( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(resolve, reject) {
            var bundle, sceneName;
            return _regeneratorRuntime().wrap(function _callee5$(_context5) {
              while (1) switch (_context5.prev = _context5.next) {
                case 0:
                  console.time('loadSceneInner');
                  _context5.next = 3;
                  return res.getBundle(moduleName);

                case 3:
                  bundle = _context5.sent;
                  sceneName = 'scene';
                  bundle.loadScene(sceneName, function (finished, total) {
                    // TODO 奇怪的数字 .2/.8
                    loader && (loader.progress = finished / total * 0.2 + 0.8);
                  }, /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4(err, scene) {
                    var _vm$hint2;

                    return _regeneratorRuntime().wrap(function _callee4$(_context4) {
                      while (1) switch (_context4.prev = _context4.next) {
                        case 0:
                          console.timeEnd('loadSceneInner');
                          (_vm$hint2 = vm.hint) == null ? void 0 : _vm$hint2.updateInputBlock();
                          err && reject(err);
                          director.runScene(scene, undefined, function () {
                            var runScene = director.getScene();

                            if (runScene) {
                              vm.clearAllView();
                              vm.initWithScene(runScene);
                              var comps = runScene.getChildByName(moduleName).components;
                              var module = null;

                              for (var _iterator = _createForOfIteratorHelperLoose(comps), _step; !(_step = _iterator()).done;) {
                                var _module = _step.value;

                                if (_module instanceof Module) {
                                  module = _module;
                                  break;
                                }
                              }

                              if (module == null) {
                                reject('scene 未找到组件 Module');
                                return;
                              }

                              _this2.runningModule = module;

                              _this2.runningModule.onEnterScene(sceneName, runScene);

                              resolve(_this2.runningModule);
                            }
                          });

                        case 4:
                        case "end":
                          return _context4.stop();
                      }
                    }, _callee4);
                  })));

                case 6:
                case "end":
                  return _context5.stop();
              }
            }, _callee5);
          })));
        }
        /**
         * 预加载模块内部场景
         * @param moduleName 模块名称
         * @param loader 加载器
         * @returns
         */
        ;

        _proto.preloaModuledScene = /*#__PURE__*/function () {
          var _preloaModuledScene = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee7(moduleName, loader) {
            return _regeneratorRuntime().wrap(function _callee7$(_context7) {
              while (1) switch (_context7.prev = _context7.next) {
                case 0:
                  return _context7.abrupt("return", new Promise( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee6(resolve, reject) {
                    var bundle;
                    return _regeneratorRuntime().wrap(function _callee6$(_context6) {
                      while (1) switch (_context6.prev = _context6.next) {
                        case 0:
                          _context6.next = 2;
                          return res.getBundle(moduleName);

                        case 2:
                          bundle = _context6.sent;
                          bundle.loadScene('scene', function (finished, total) {
                            if (loader) loader.progress = finished / total;
                          }, function (err, data) {
                            err && reject(err);
                            resolve();
                          });

                        case 4:
                        case "end":
                          return _context6.stop();
                      }
                    }, _callee6);
                  }))));

                case 1:
                case "end":
                  return _context7.stop();
              }
            }, _callee7);
          }));

          function preloaModuledScene(_x10, _x11) {
            return _preloaModuledScene.apply(this, arguments);
          }

          return preloaModuledScene;
        }()
        /**
         * 启动模块
         * @param name 模块名称
         * @param data 携带数据
         * @param loader 加载器
         * @returns
         */
        ;

        _proto.launchModule = /*#__PURE__*/function () {
          var _launchModule = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee8(name, data, loader) {
            var _this$runningModule, _vm$hint3, _this$runningModule2, _vm$hint4;

            var bundle, preModuleName, _loader, assets, module;

            return _regeneratorRuntime().wrap(function _callee8$(_context8) {
              while (1) switch (_context8.prev = _context8.next) {
                case 0:
                  if (data === void 0) {
                    data = null;
                  }

                  console.time("launchModule " + name);
                  _context8.next = 4;
                  return res.getBundle(name);

                case 4:
                  bundle = _context8.sent;
                  preModuleName = (_this$runningModule = this.runningModule) == null ? void 0 : _this$runningModule.moduleName;

                  if (!(name == preModuleName)) {
                    _context8.next = 9;
                    break;
                  }

                  this.runningModule.onLaunched(data, preModuleName);
                  return _context8.abrupt("return");

                case 9:
                  if (this.runningModule != this.mainModule) {
                    httpClient.unregisterRouterInCallbacks(this.runningModule.getHttpCallback());
                  }

                  _loader = loader || vm.hint.getLoader('littleLoader');
                  _loader == null ? void 0 : _loader.load();
                  (_vm$hint3 = vm.hint) == null ? void 0 : _vm$hint3.updateInputBlock();
                  assets = this.preloadAssetsInfo[name];

                  if (!(assets && assets.length > 0)) {
                    _context8.next = 17;
                    break;
                  }

                  _context8.next = 17;
                  return this.loadBundleResource(bundle, assets, loader);

                case 17:
                  console.timeEnd("launchModule " + name);
                  _context8.next = 20;
                  return (_this$runningModule2 = this.runningModule) == null ? void 0 : _this$runningModule2.onExit();

                case 20:
                  console.time("loadScene " + name);
                  _context8.next = 23;
                  return this.loadScene(name, _loader);

                case 23:
                  module = _context8.sent;
                  _context8.next = 26;
                  return loader == null ? void 0 : loader.onCompleted();

                case 26:
                  (_vm$hint4 = vm.hint) == null ? void 0 : _vm$hint4.updateInputBlock();

                  if (name == this.mainModuleName && this.mainModule == null) {
                    this.mainModule = module;
                    this.initModule(module);
                    module.onLaunched(data, preModuleName);
                  } else {
                    this.initModule(module);
                    module.onLaunched(data, preModuleName);
                  }

                  console.timeEnd("loadScene " + name);

                case 29:
                case "end":
                  return _context8.stop();
              }
            }, _callee8, this);
          }));

          function launchModule(_x14, _x15, _x16) {
            return _launchModule.apply(this, arguments);
          }

          return launchModule;
        }()
        /**
         * 加载 AB 包内资源
         * @param bundle AB 包
         * @param assets 资源列表
         * @param loader 加载器
         * @returns
         */
        ;

        _proto.loadBundleResource = function loadBundleResource(bundle, assets, loader) {
          return new Promise(function (resole, reject) {
            if (assets.length == 0) {
              resole();
              return;
            }

            var index = 0;

            function loadOne(_x17) {
              return _loadOne.apply(this, arguments);
            }

            function _loadOne() {
              _loadOne = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee9(err) {
                var asset;
                return _regeneratorRuntime().wrap(function _callee9$(_context9) {
                  while (1) switch (_context9.prev = _context9.next) {
                    case 0:
                      err && logger.e(err);

                      if (loader) {
                        // TODO 奇怪的数字 .8
                        loader.progress = 0.8 * index / assets.length;
                      }

                      if (!(index >= assets.length)) {
                        _context9.next = 5;
                        break;
                      }

                      resole();
                      return _context9.abrupt("return");

                    case 5:
                      asset = assets[index];

                      if (asset.type == SceneAsset) {
                        bundle.loadScene(asset.path, loadOne);
                      } else {
                        // @ts-ignore
                        bundle.load(asset.path, asset.type, loadOne);
                      }

                      index++;

                    case 8:
                    case "end":
                      return _context9.stop();
                  }
                }, _callee9);
              }));
              return _loadOne.apply(this, arguments);
            }

            loadOne();
          });
        };

        return ModuleManager;
      }();
      /** 模块管理器单例 */


      var moduleMgr = exports('moduleMgr', new ModuleManager());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/MoveBase.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './BezierCurveAnimation.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, cclegacy, EventHandler, _decorator, js, tween, Component, BezierCurveAnimation;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      EventHandler = module.EventHandler;
      _decorator = module._decorator;
      js = module.js;
      tween = module.tween;
      Component = module.Component;
    }, function (module) {
      BezierCurveAnimation = module.BezierCurveAnimation;
    }],
    execute: function () {
      exports('MoveBase_', void 0);

      var _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2;

      cclegacy._RF.push({}, "5e979ckhrRJ05mQXqngTVXX", "MoveBase", undefined);

      var _cc$_decorator = _decorator,
          ccclass = _cc$_decorator.ccclass,
          property = _cc$_decorator.property,
          requireComponent = _cc$_decorator.requireComponent;
      /** 运动组件基类 */

      var MoveBase = exports('MoveBase', (_dec = requireComponent(BezierCurveAnimation), _dec2 = property({
        displayName: '当前下标变更事件',
        tooltip: '(当前下标_indexN, 上个下标_indexN, 跳过状态_jumpB)',
        type: EventHandler
      }), _dec3 = property({
        displayName: '结束事件',
        type: EventHandler
      }), ccclass(_class = _dec(_class = (_class2 = /*#__PURE__*/function (_cc$Component) {
        _inheritsLoose(MoveBase, _cc$Component);

        function MoveBase() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _cc$Component.call.apply(_cc$Component, [this].concat(args)) || this;
          /* --------------- 属性 --------------- */

          /** 当前下标变更事件 */

          _initializerDefineProperty(_this, "indexChangeEvent", _descriptor, _assertThisInitialized(_this));
          /** 结束事件 */


          _initializerDefineProperty(_this, "endEvent", _descriptor2, _assertThisInitialized(_this));
          /* --------------- protected --------------- */

          /** 移动状态 */


          _this._moveB = false;
          /** 移动缓动 */

          _this._moveTween = null;
          /** 循环移动状态 */

          _this._loopRunB = false;
          /** 当前移动配置 */

          _this._moveConfig = void 0;
          /** 跳过状态 */

          _this._jumpB = false;
          /** 上次曲线 Y */

          _this._lastCurveYN = 0;
          /** 当前下标 */

          _this._currIndexN = void 0;
          /** 当前距离 */

          _this._currDistN = 0;
          /** 总距离 */

          _this._targetDistN = 0;
          /* --------------- public --------------- */

          /** 曲线组件 */

          _this.curveComp = void 0;
          return _this;
        }

        var _proto = MoveBase.prototype;
        /* ------------------------------- get/set ------------------------------- */

        _proto._setCurrIndexN = function _setCurrIndexN(valueN_) {
          if (valueN_ === this._currIndexN) {
            return;
          }

          this.indexChangeEvent.emit([valueN_, this._currIndexN, this._jumpB]);
          this._currIndexN = valueN_; // logger.log('当前选中', this._currIndexN);
        }
        /* ------------------------------- 生命周期 ------------------------------- */
        ;

        _proto.onLoad = function onLoad() {
          this._initData();
        }
        /* ------------------------------- 功能 ------------------------------- */

        /** 初始化数据 */
        ;

        _proto._initData = function _initData() {
          this.curveComp = this.getComponent(BezierCurveAnimation); // 设置更新事件

          var updateEvent = new EventHandler();
          updateEvent.component = js.getClassName(this);
          updateEvent.handler = '_eventUpdate';
          updateEvent.target = this.node;
          this.curveComp.updateEventAS.push(updateEvent); // 设置结束事件

          var endEvent = new EventHandler();
          endEvent.component = js.getClassName(this);
          endEvent.handler = '_eventEnd';
          endEvent.target = this.node;
          this.curveComp.endEventAS.push(endEvent);
        }
        /** 运动 */
        ;
        /** 停止循环 */


        _proto.stop = function stop() {
          if (!this._moveTween) {
            return;
          }

          this._moveTween.stop();

          this._moveTween = null;
          this._loopRunB = false;
          this._moveB = false;
        }
        /**
         * 循环运动
         * @param speedN_ 速度/秒
         * @param timeSN_ 时间（秒），不填则一直运动
         */
        ;

        _proto.loop = function loop(speedN_, timeSN_) {
          var _this2 = this;

          if (this._moveB) {
            return;
          }

          this._moveB = true;
          this._loopRunB = true;
          var tempN;
          var target = {
            valueN: 0,
            lastValueN: 0
          };
          this._moveTween = tween(target).repeatForever(tween().by(1, {
            valueN: 1
          }, {
            onUpdate: function onUpdate() {
              if (!_this2.isValid) {
                return;
              }

              tempN = (target.valueN - target.lastValueN) * speedN_;

              _this2._move(tempN);

              _this2._currDistN += tempN;
              target.lastValueN = target.valueN;
              _this2.currIndexN = _this2._getCurrIndex();
            }
          })).start();

          if (timeSN_ !== undefined) {
            this.scheduleOnce(function () {
              _this2.stop();
            }, timeSN_);
          }
        }
        /**
         * 跳转到指定下标
         * @param indexN_ 目标下标
         * @returns
         */
        ;

        _proto.jump = function jump(indexN_) {
          if (this._moveB && !this._loopRunB) {
            return;
          }

          this._moveB = true;
          this._jumpB = true; // 停止循环运动

          if (this._loopRunB) {
            this.stop();
          } // 更新距离


          this._targetDistN = this._currDistN = this._getMoveDist(indexN_); // 直接跳转

          this._move(this._targetDistN);

          this.currIndexN = this._getCurrIndex();
          this._moveB = false;
          this._jumpB = false;
        }
        /**
         * 移动
         * @param indexN_ 目标下标
         * @param scrollConfig_ 运动配置
         * @returns
         */
        ;

        _proto.move = function move(indexN_, scrollConfig_) {
          if (this._moveB && !this._loopRunB) {
            return;
          }

          this._moveB = true;
          this._moveConfig = new MoveBase_.MoveConfig(scrollConfig_); // 停止循环运动

          if (this._loopRunB) {
            this.stop();
          } // 更新距离


          this._lastCurveYN = 0;
          this._currDistN = 0;
          this._targetDistN = this._getMoveDist(indexN_, this._moveConfig); // 开始缓动

          this._moveTween = this.curveComp.startTween(this._moveConfig.tweenIndexNS);
        }
        /** 获取运动速度 */
        ;

        _proto.getSpeed = function getSpeed(indexN_, scrollConfig_) {
          scrollConfig_ = new MoveBase_.GetSpeedConfig(scrollConfig_);
          /** 目标距离 */

          var targetDistN = this._getMoveDist(indexN_, scrollConfig_);

          return targetDistN * this.curveComp.getCurveY(scrollConfig_.ratioN, scrollConfig_.tweenIndexNS);
        }
        /* ------------------------------- 自定义事件 ------------------------------- */
        ;

        _proto._eventUpdate = function _eventUpdate(yN_, indexN_) {
          var moveDistN = this._targetDistN * (yN_ - this._lastCurveYN);
          this._currDistN += moveDistN;

          this._move(moveDistN);

          this._lastCurveYN = yN_;
          this.currIndexN = this._getCurrIndex(); // cc.log('缓动更新', yN_, indexN_, y2N_, yN_ - this._lastCurveYN);
        };

        _proto._eventEnd = function _eventEnd() {
          var _this$_moveConfig; // 更新至终点


          var moveDistN = this._targetDistN - this._currDistN;
          this._currDistN += moveDistN;

          this._move(this._targetDistN - this._currDistN);

          this.currIndexN = this._getCurrIndex(); // 更新状态

          this._moveB = false;
          this.endEvent.emit([]);
          (_this$_moveConfig = this._moveConfig) == null ? void 0 : _this$_moveConfig.endCBF == null ? void 0 : _this$_moveConfig.endCBF(); // cc.log('缓动结束');
        };

        _createClass(MoveBase, [{
          key: "currIndexN",
          get:
          /** 当前中心下标 */
          function get() {
            return this._currIndexN;
          },
          set: function set(valueN_) {
            this._setCurrIndexN(valueN_);
          }
        }]);

        return MoveBase;
      }(Component), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "indexChangeEvent", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return new EventHandler();
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "endEvent", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return new EventHandler();
        }
      })), _class2)) || _class) || _class));
      var MoveBase_;

      (function (_MoveBase_) {
        var MoveConfig = function MoveConfig(init_) {
          /** 缓动队列 */
          this.tweenIndexNS = void 0;
          /** 结束回调 */

          this.endCBF = void 0;
          Object.assign(this, init_);
        };

        _MoveBase_.MoveConfig = MoveConfig;

        var GetSpeedConfig = /*#__PURE__*/function (_MoveConfig) {
          _inheritsLoose(GetSpeedConfig, _MoveConfig);

          function GetSpeedConfig(init_) {
            var _this3;

            _this3 = _MoveConfig.call(this, init_) || this;
            /** 进度 */

            _this3.ratioN = 0;
            Object.assign(_assertThisInitialized(_this3), init_);
            return _this3;
          }

          return GetSpeedConfig;
        }(MoveConfig);

        _MoveBase_.GetSpeedConfig = GetSpeedConfig;
      })(MoveBase_ || (MoveBase_ = exports('MoveBase_', {})));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Numbers.ts", ['cc'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      exports('Numbers', void 0);

      cclegacy._RF.push({}, "97f1dsIdetAKYXOW+0T5MnF", "Numbers", undefined);
      /**
       * 数值辅助方法
       */


      var Numbers;

      (function (_Numbers) {
        var mpow = Math.pow,
            mmax = Math.max,
            mmin = Math.min,
            mround = Math.round;
        /**
         * 保留小数点后几位
         * @param num 输入数值
         * @param reserve 保留小数点后的位数
         * @returns
         */

        function reserve(num, reserve) {
          reserve = mmax(0, reserve | 0);
          var factor = mpow(10, reserve);
          return mround(num * factor) / factor;
        }

        _Numbers.reserve = reserve;

        function clamp(val, min, max) {
          return mmin(max, mmax(val, min));
        }

        _Numbers.clamp = clamp;

        function padStart(v, digit) {
          digit = Math.max(0, digit | 0);

          if (v < Math.pow(10, digit)) {
            var prefix = digit - v.toString().length;

            if (prefix > 0) {
              return '0'.repeat(prefix) + v;
            }
          }

          return v.toString();
        }

        _Numbers.padStart = padStart;
      })(Numbers || (Numbers = exports('Numbers', {})));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Objects.ts", ['cc', './Kindof.ts'], function (exports) {
  var cclegacy, KindOf;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      KindOf = module.KindOf;
    }],
    execute: function () {
      exports('Objects', void 0);

      cclegacy._RF.push({}, "46a3dSbObFFSYcXN/597JMR", "Objects", undefined);
      /**
       * 对象辅助方法
       */


      var Objects;

      (function (_Objects) {
        function copy(data, deep) {
          if (KindOf.isObject(data)) {
            return deep ? deepCopy(data) : shallowCopy(data);
          } else {
            return data;
          }
        }

        _Objects.copy = copy;

        function lossyCopy(data) {
          return JSON.parse(JSON.stringify(data));
        }

        _Objects.lossyCopy = lossyCopy;

        function shallowCopy(data) {
          if (KindOf.isArray(data)) {
            return data.slice();
          }

          return Object.assign({}, data);
        }

        _Objects.shallowCopy = shallowCopy;
        /**
         * @zh
         * 深拷贝
         * @param data 数据
         */

        function deepCopy(data) {
          if (data instanceof Date) {
            var date = new Date();
            date.setTime(data.getTime());
            return date;
          }

          if (data instanceof Array) {
            var _ret = [];

            for (var i = 0, length = data.length; i < length; i++) {
              _ret[i] = copy(data[i], true);
            }

            return _ret;
          }

          var ret = {};

          for (var key in data) {
            ret[key] = copy(data[key], true);
          }

          return ret;
        }
        /**
         * 从原始对象上复制未定义的项到目标对象
         * @param target 目标对象
         * @param raw 原始对象
         * @returns
         */


        function assignUnsetKeys(target, raw) {
          var unset = false;

          for (var key in raw) {
            if (target[key] === undefined) {
              target[key] = copy(raw[key], true);
              unset = true;
            }
          }

          return unset;
        }

        _Objects.assignUnsetKeys = assignUnsetKeys;
      })(Objects || (Objects = exports('Objects', {})));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Painter.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Container.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, _asyncToGenerator, _regeneratorRuntime, cclegacy, _decorator, Sprite, ImageAsset, SpriteFrame, UITransform, Component, Container;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Sprite = module.Sprite;
      ImageAsset = module.ImageAsset;
      SpriteFrame = module.SpriteFrame;
      UITransform = module.UITransform;
      Component = module.Component;
    }, function (module) {
      Container = module.Container;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor;

      cclegacy._RF.push({}, "0adf05sg81NApB7/LW0tkRH", "Painter", undefined);

      var ccclass = _decorator.ccclass,
          requireComponent = _decorator.requireComponent,
          property = _decorator.property;
      var Painter = exports('Painter', (_dec = ccclass('Painter'), _dec2 = requireComponent(Sprite), _dec3 = property({
        type: Sprite,
        visible: false
      }), _dec4 = property({
        type: ImageAsset
      }), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_Component) {
        _inheritsLoose(Painter, _Component);

        function Painter() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Component.call.apply(_Component, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "img", _descriptor, _assertThisInitialized(_this));

          _this.container = void 0;
          return _this;
        }

        var _proto = Painter.prototype;

        _proto.onLoad = function onLoad() {
          this.container = new Container({
            maxWidth: 680,
            spaceX: 5,
            spaceY: 16,
            padding: [10, 4, 10, 10],
            fontFamily: 'monaco',
            fontSize: 24,
            fontVariant: '',
            fontWeight: '',
            fillColor: 'white',
            strokeColor: 'black',
            shadowColor: 'white',
            shadowBlur: 0,
            shadowOffsetX: 0,
            shadowOffsetY: 0,
            commentColor: 'red',
            commentWidth: 2
          });
          this.container.canvas.style.position = 'absolute'; // game.container.parentElement.insertBefore(this.container.canvas, game.container);
        };

        _proto.start = /*#__PURE__*/function () {
          var _start = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  console.log('---', this.container);
                  _context.next = 3;
                  return this.container.appendMany([{
                    type: 'text',
                    text: 'Hello World. This is a ',
                    fontSize: 30,
                    fillColor: 'black'
                  }, {
                    type: 'text',
                    text: 'beautiful',
                    fontSize: 34,
                    fillColor: 'blue',
                    commentColor: 'red',
                    commentStyle: 'underline'
                  }, {
                    type: 'text',
                    text: ' winter.',
                    fontSize: 30,
                    fillColor: 'white',
                    commentColor: 'red',
                    commentStyle: 'underline'
                  }, {
                    type: 'image',
                    src: this.img.nativeUrl,
                    height: 40
                  }, {
                    type: 'text',
                    fontFamily: 'STKaiti',
                    text: '你好世界，这是一个美好的冬天。',
                    fontSize: 36,
                    fillColor: 'black',
                    commentWidth: 2,
                    commentColor: '#ffdc345f',
                    commentStyle: 'highlight'
                  }]);

                case 3:
                  this.container.appendRow();
                  _context.next = 6;
                  return this.container.appendMany([{
                    type: 'text',
                    text: '换了新的一行呀。',
                    commentStyle: 'waveline',
                    commentColor: '#ff2222',
                    fontSize: 30
                  }]);

                case 6:
                  this.container.draw();
                  this.draw();

                case 8:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));

          function start() {
            return _start.apply(this, arguments);
          }

          return start;
        }();

        _proto.draw = function draw() {
          this.sprite.spriteFrame = SpriteFrame.createWithImage(this.container.canvas);
          var _this$sprite$spriteFr = this.sprite.spriteFrame,
              width = _this$sprite$spriteFr.width,
              height = _this$sprite$spriteFr.height;
          this.sprite.getComponent(UITransform).setContentSize(width, height);
        };

        _createClass(Painter, [{
          key: "sprite",
          get: function get() {
            return this.getComponent(Sprite);
          }
        }]);

        return Painter;
      }(Component), (_applyDecoratedDescriptor(_class2.prototype, "sprite", [_dec3], Object.getOwnPropertyDescriptor(_class2.prototype, "sprite"), _class2.prototype), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "img", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/PartialSprite.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, cclegacy, _decorator, Sprite, Rect, Node, UITransform, Component, rect;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Sprite = module.Sprite;
      Rect = module.Rect;
      Node = module.Node;
      UITransform = module.UITransform;
      Component = module.Component;
      rect = module.rect;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class, _class2, _descriptor;

      cclegacy._RF.push({}, "4172eLNTIpNHIisxhTsRyie", "PartialSprite", undefined);

      var ccclass = _decorator.ccclass,
          requireComponent = _decorator.requireComponent,
          property = _decorator.property;
      var PartialSprite = exports('PartialSprite', (_dec = ccclass('PartialSprite'), _dec2 = requireComponent(Sprite), _dec3 = property(Rect), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_Component) {
        _inheritsLoose(PartialSprite, _Component);

        function PartialSprite() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Component.call.apply(_Component, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "rect", _descriptor, _assertThisInitialized(_this));

          _this._rawSpriteFrame = void 0;
          return _this;
        }

        var _proto = PartialSprite.prototype;

        _proto.onLoad = function onLoad() {
          this._rawSpriteFrame = this.sprite.spriteFrame;
          console.log('初始尺寸', this.transform.width, this.transform.height);
          this.setRect(this.rect);
        };

        _proto.setRect = function setRect(rect) {
          var sp = this._rawSpriteFrame.clone();

          sp.rect = rect;
          this.sprite.spriteFrame = sp;
          console.log('修改后尺寸', this.transform.width, this.transform.height);
        };

        _proto.onEnable = function onEnable() {
          this.node.on(Node.EventType.TOUCH_START, this.onTouchStart, this);
          this.node.on(Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
          this.node.on(Node.EventType.TOUCH_END, this.onTouchEnded, this);
          this.node.on(Node.EventType.TOUCH_CANCEL, this.onTouchCanceled, this);
        };

        _proto.onDisable = function onDisable() {
          this.node.off(Node.EventType.TOUCH_START, this.onTouchStart, this);
          this.node.off(Node.EventType.TOUCH_MOVE, this.onTouchMove, this);
          this.node.off(Node.EventType.TOUCH_END, this.onTouchEnded, this);
          this.node.off(Node.EventType.TOUCH_CANCEL, this.onTouchCanceled, this);
        };

        _proto.onTouchStart = function onTouchStart(event) {
          console.log('start', event.getLocationInView());
        };

        _proto.onTouchEnded = function onTouchEnded(event) {
          console.log('ended', event.getLocationInView());
        };

        _proto.onTouchMove = function onTouchMove(event) {
          console.log('move', event.getLocationInView());
        };

        _proto.onTouchCanceled = function onTouchCanceled(event) {
          console.log('cancel', event.getLocationInView());
        };

        _createClass(PartialSprite, [{
          key: "sprite",
          get: function get() {
            return this.getComponent(Sprite);
          }
        }, {
          key: "transform",
          get: function get() {
            return this.getComponent(UITransform);
          }
        }]);

        return PartialSprite;
      }(Component), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "rect", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return rect(0, 0, 1, 1);
        }
      }), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Persistencee.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './AudioEngine.ts'], function (exports) {
  var _inheritsLoose, _createClass, cclegacy, _decorator, director, AudioSource, Node, ccmodifier, BaseComponent, audioEngine;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      director = module.director;
      AudioSource = module.AudioSource;
      Node = module.Node;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      audioEngine = module.audioEngine;
    }],
    execute: function () {
      var _dec, _dec2, _class, _class2;

      cclegacy._RF.push({}, "78185bPiTxLzYkVmVsifvxc", "Persistencee", undefined);

      var ccclass = _decorator.ccclass;
      /**
       * 常驻节点
       * - 需要设置为常驻的节点或组件应该挂载在它下面，方便管理
       */

      var Persistencee = exports('Persistencee', (_dec = ccclass('Persistencee'), _dec2 = ccmodifier('Persistencee'), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(Persistencee, _BaseComponent);

        function Persistencee() {
          return _BaseComponent.apply(this, arguments) || this;
        }

        var _proto = Persistencee.prototype;

        _proto.onLoad = function onLoad() {
          Persistencee._instance = this;
          director.addPersistRootNode(this.node);
          audioEngine.init(this.node.addComponent(AudioSource));
        };

        _proto.onEnable = function onEnable() {
          var _this$node$parent, _this$node$parent2;

          (_this$node$parent = this.node.parent) == null ? void 0 : _this$node$parent.on(Node.EventType.CHILD_ADDED, this.sortSiblingIndex, this);
          (_this$node$parent2 = this.node.parent) == null ? void 0 : _this$node$parent2.on(Node.EventType.CHILD_REMOVED, this.sortSiblingIndex, this);
          this.sortSiblingIndex();
        };

        _proto.onDestroy = function onDestroy() {
          Persistencee._instance = null;
        };

        _proto.sortSiblingIndex = function sortSiblingIndex() {
          var _this$node$parent3;

          var children = (_this$node$parent3 = this.node.parent) == null ? void 0 : _this$node$parent3.children;

          if (children) {
            var indexes = {
              MapCanvas: 0,
              ui_root: 1,
              Venue25D: 2,
              MainModule: 3,
              RVOSystem: 4,
              Hint: 5,
              Persistencee: 6
            };
            var orders = [];
            children.forEach(function (v) {
              if (v.name in indexes) orders[indexes[v.name]] = v;
            });
            orders.forEach(function (v, i) {
              v && v.setSiblingIndex(i);
            });
          }
        };

        _createClass(Persistencee, null, [{
          key: "instance",
          get: function get() {
            return Persistencee._instance;
          }
        }]);

        return Persistencee;
      }(BaseComponent), _class2._instance = null, _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/PreloadAssets.ts", ['cc', './AssetLocator.ts'], function (exports) {
  var cclegacy, AssetLocator;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      AssetLocator = module.AssetLocator;
    }],
    execute: function () {
      cclegacy._RF.push({}, "e076bXiOJxIy5wz6IV9an/f", "PreloadAssets", undefined);
      /** 模块所需预加载的资源 */


      var PreloadAssetsInfo = exports('PreloadAssetsInfo', {
        MainModule: [// UI
        AssetLocator.seekPrefab('LoginUI'), AssetLocator.seekPrefab('MainUI'), AssetLocator.seekPrefab('ChangeRoleUI'), AssetLocator.seekPrefab('MinimapUI'), AssetLocator.seekPrefab('UserInfoWindow'), AssetLocator.seekPrefab('BagUINew'), AssetLocator.seekPrefab('NoticeBoardWindow'), // AssetLocator.seekPrefab('ZoneUI'),
        // AssetLocator.seekPrefab('IntroduceUI'),
        // AssetLocator.seekPrefab('EditNicknameWindow'),
        // AssetLocator.seekPrefab('EditSignatureeWindow'),
        // AssetLocator.seekPrefab('MapStoreWindow'),
        // AssetLocator.seekPrefab('NPCInteractionUI'),
        // AssetLocator.seekPrefab('BulletinWindow'),
        // AssetLocator.seekPrefab('SelectRoleUI'),
        // NPC spine
        AssetLocator.seekSkeleton('mmo/npc/npc0/hudie'), AssetLocator.seekSkeleton('mmo/npc/npc0/jinyu'), AssetLocator.seekSkeleton('mmo/npc/npc0/fallen_leaf01'), AssetLocator.seekSkeleton('mmo/npc/npc0/pigeon'), AssetLocator.seekSkeleton('mmo/npc/npc1/role_shibei'), AssetLocator.seekSkeleton('mmo/npc/npc1/role_gaoshipai'), AssetLocator.seekSkeleton('mmo/npc/npc1/role_shitou'), AssetLocator.seekSkeleton('mmo/npc/npc2/role_shu01'), AssetLocator.seekSkeleton('mmo/npc/npc2/role_shu02'), AssetLocator.seekSkeleton('mmo/npc/npc2/role_shu03'), AssetLocator.seekSkeleton('mmo/npc/npc2/role_shu04'), AssetLocator.seekSkeleton('mmo/npc/npc2/role_shu05'), AssetLocator.seekSkeleton('mmo/npc/npc2/role_shu06'), AssetLocator.seekSkeleton('mmo/npc/npc3/role_gongnv'), AssetLocator.seekSkeleton('mmo/npc/npc3/role_qincongguan'), AssetLocator.seekSkeleton('mmo/npc/npc3/role_qinshiguan'), AssetLocator.seekSkeleton('mmo/npc/npc3/role_songxiaozong'), AssetLocator.seekSkeleton('mmo/npc/npc3/role_zhihuishi'), // 立绘 spine
        AssetLocator.seekSkeleton('spine/lihui_gongnv'), AssetLocator.seekSkeleton('spine/lihui_qincongguan'), AssetLocator.seekSkeleton('spine/lihui_qinshiguan'), AssetLocator.seekSkeleton('spine/lihui_songxiaozong'), AssetLocator.seekSkeleton('spine/lihui_zhihuishi'), // scene
        AssetLocator.seekScene('main_scene')]
      });

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/QueueLoader.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Logger.ts'], function (exports) {
  var _asyncToGenerator, _regeneratorRuntime, cclegacy, assetManager, logger;

  return {
    setters: [function (module) {
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      assetManager = module.assetManager;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      cclegacy._RF.push({}, "09083CKVJlBA4G0srjm8cgI", "QueueLoader", undefined);
      /** 任务节点 */

      /** 顺序加载器 */


      var QueueLoader = /*#__PURE__*/function () {
        function QueueLoader() {
          this.loader = null;
          this.queue = [];
          this.flag = 'idle';
        }

        var _proto = QueueLoader.prototype;

        _proto.loadQueue = function loadQueue(loader) {
          var _this$queue;

          for (var _len = arguments.length, tasks = new Array(_len > 1 ? _len - 1 : 0), _key = 1; _key < _len; _key++) {
            tasks[_key - 1] = arguments[_key];
          }

          (_this$queue = this.queue).push.apply(_this$queue, tasks);

          this.loader = loader;
          this.loader.load();
          this.loadNextOne();
        };

        _proto.loadNextOne = function loadNextOne() {
          var _this = this;

          var task = this.queue.shift();
          var bundle = assetManager.getBundle(task.bundleName);

          var progress = function progress(finished, total) {
            if (!_this.loader) return;
            _this.loader.progress += task.ratio * finished / total;
          };

          var complete = function complete(err) {
            err && logger.error(err);

            if (_this.queue.length == 0) {
              var _this$loader;

              (_this$loader = _this.loader) == null ? void 0 : _this$loader.onCompleted();
            } else {
              _this.loadNextOne();
            }
          };

          if (typeof task.paths === 'string') {
            bundle.load(task.paths, progress, complete);
          } else {
            bundle.load(task.paths, progress, complete);
          }
        };

        _proto.loadQueueAsync = function loadQueueAsync(loader) {
          var _this$queue2,
              _this2 = this;

          this.loader = loader;
          this.loader.load();

          for (var _len2 = arguments.length, tasks = new Array(_len2 > 1 ? _len2 - 1 : 0), _key2 = 1; _key2 < _len2; _key2++) {
            tasks[_key2 - 1] = arguments[_key2];
          }

          (_this$queue2 = this.queue).push.apply(_this$queue2, tasks);

          return new Promise(function (resole, reject) {
            var loadOne = function loadOne() {
              var task = _this2.queue.shift();

              var progress = function progress(finished, total) {
                if (!_this2.loader) return;
                _this2.loader.progress += task.ratio * finished / total;
              };

              var complete = /*#__PURE__*/function () {
                var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(err) {
                  var _this2$loader;

                  return _regeneratorRuntime().wrap(function _callee$(_context) {
                    while (1) switch (_context.prev = _context.next) {
                      case 0:
                        err && reject(err);

                        if (!(_this2.queue.length == 0)) {
                          _context.next = 7;
                          break;
                        }

                        _context.next = 4;
                        return (_this2$loader = _this2.loader) == null ? void 0 : _this2$loader.onCompleted();

                      case 4:
                        resole();
                        _context.next = 8;
                        break;

                      case 7:
                        loadOne();

                      case 8:
                      case "end":
                        return _context.stop();
                    }
                  }, _callee);
                }));

                return function complete(_x) {
                  return _ref.apply(this, arguments);
                };
              }();

              var bundle = assetManager.getBundle(task.bundleName);

              if (typeof task.paths === 'string') {
                bundle.load(task.paths, progress, complete);
              } else {
                bundle.load(task.paths, progress, complete);
              }
            };

            loadOne();
          });
        };

        return QueueLoader;
      }();
      /** 顺序加载器单例 */


      var queueLoader = exports('queueLoader', new QueueLoader());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/RemoteImage.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './Logger.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Sprite, assetManager, UITransform, isValid, SpriteFrame, ccmodifier, BaseComponent, logger;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Sprite = module.Sprite;
      assetManager = module.assetManager;
      UITransform = module.UITransform;
      isValid = module.isValid;
      SpriteFrame = module.SpriteFrame;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2, _descriptor3;

      cclegacy._RF.push({}, "9e211rySBZKeLg6Bvv8ulOl", "RemoteImage", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property,
          requireComponent = _decorator.requireComponent;
      /**
       * 远程图片加载器
       * > 此方法可以在一定程度上规避掉 `AssetManager.loadRemote` 存在的跨域问题，
       *   但如果图片服务器本身不支持跨域，那么也就束手无策了。归根结底，让服务器配置
       *   跨域才是解决之道。
       */

      var RemoteImage = exports('RemoteImage', (_dec = ccclass('RemoteImage'), _dec2 = ccmodifier('RemoteImage'), _dec3 = requireComponent(Sprite), _dec4 = property({
        displayName: '图片网址'
      }), _dec5 = property({
        displayName: '是否需要限定内容尺寸'
      }), _dec6 = property({
        displayName: '是否在进入时清空内容'
      }), _dec(_class = _dec2(_class = _dec3(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(RemoteImage, _BaseComponent);

        function RemoteImage() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          _this.image = void 0;

          _initializerDefineProperty(_this, "url", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "constrict", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "clearOnLoad", _descriptor3, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = RemoteImage.prototype;

        _proto.start = function start() {
          this.image = this.getComponent(Sprite);
          this.clearOnLoad && (this.image.spriteFrame = null);
          this.url && this.setUrl(this.url);
        }
        /**
         * 设置图片网址
         * @param url 图片网址
         */
        ;

        _proto.setUrl = function setUrl(url) {
          this.url = url;

          if (url) {
            this.url && this._apply();
          } else {
            if (this.image && this.image.spriteFrame) this.image.spriteFrame = null;
          }
        }
        /** 加载网络图片 */
        ;

        _proto._apply = function _apply() {
          var _this2 = this;

          this.image = this.image || this.node.getComponent(Sprite);
          var key = "@remote[" + this.url + "]";

          if (assetManager.assets.has(key)) {
            logger.d('使用缓存加载图片', key);
            this.image.spriteFrame = assetManager.assets.get(key);
            return;
          }

          var uitr = this.image.getComponent(UITransform);
          var width = 0;
          var height = 0;
          var img = new Image();
          img.src = this.url;
          img.crossOrigin = 'anonymous';

          img.onload = function () {
            if (_this2 && isValid(_this2)) {
              var canvas = document.createElement('canvas');
              var ctx = canvas.getContext('2d', {
                willReadFrequently: true
              });
              ctx.imageSmoothingEnabled = true;
              ctx.imageSmoothingQuality = 'high';
              canvas.hidden = true;

              if (_this2.constrict) {
                width = uitr.width;
                height = uitr.height;
              } else {
                width = img.width;
                height = img.height;
                uitr.setContentSize(img.width, img.height);
              }

              canvas.width = width;
              canvas.height = height;
              ctx.drawImage(img, 0, 0, width, height);
              var spriteFrame = SpriteFrame.createWithImage(canvas);
              _this2.image.spriteFrame = spriteFrame;
              assetManager.assets.add(key, spriteFrame);
              canvas = null;
              img = null;
              logger.d('加载远程图片成功，添加图片到缓存', key);
            }
          };

          img.onerror = function () {
            logger.w('加载远程图片失败', _this2.url);
          };
        };

        return RemoteImage;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "url", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return '';
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "constrict", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return true;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "clearOnLoad", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return false;
        }
      })), _class2)) || _class) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ResManager.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Logger.ts'], function (exports) {
  var _asyncToGenerator, _regeneratorRuntime, cclegacy, assetManager, logger;

  return {
    setters: [function (module) {
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      assetManager = module.assetManager;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      cclegacy._RF.push({}, "61abePYjlNKs5likFMHFGcC", "ResManager", undefined);
      /** 资源管理器 */


      var ResManager = /*#__PURE__*/function () {
        function ResManager() {}

        var _proto = ResManager.prototype;

        _proto.loadBundleAsync = /*#__PURE__*/function () {
          var _loadBundleAsync = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(name) {
            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                  return _context2.abrupt("return", new Promise( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(resolve, reject) {
                    var _bundle;

                    return _regeneratorRuntime().wrap(function _callee$(_context) {
                      while (1) switch (_context.prev = _context.next) {
                        case 0:
                          _bundle = assetManager.getBundle(name);

                          if (!_bundle) {
                            _context.next = 3;
                            break;
                          }

                          return _context.abrupt("return", resolve(_bundle));

                        case 3:
                          assetManager.loadBundle(name, function (err, bundle) {
                            err ? reject(err) : resolve(bundle);
                          });

                        case 4:
                        case "end":
                          return _context.stop();
                      }
                    }, _callee);
                  }))));

                case 1:
                case "end":
                  return _context2.stop();
              }
            }, _callee2);
          }));

          function loadBundleAsync(_x) {
            return _loadBundleAsync.apply(this, arguments);
          }

          return loadBundleAsync;
        }();

        _proto.loadBundle = function loadBundle(name, onComplete) {
          var _bundle = assetManager.getBundle(name);

          if (_bundle && onComplete) return onComplete(_bundle);
          assetManager.loadBundle(name, function (err, bundle) {
            if (err) logger.e(err);
            onComplete && onComplete(bundle);
          });
        };

        _proto.getBundle = /*#__PURE__*/function () {
          var _getBundle = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(name) {
            return _regeneratorRuntime().wrap(function _callee3$(_context3) {
              while (1) switch (_context3.prev = _context3.next) {
                case 0:
                  _context3.t0 = assetManager.getBundle(name);

                  if (_context3.t0) {
                    _context3.next = 5;
                    break;
                  }

                  _context3.next = 4;
                  return this.loadBundleAsync(name);

                case 4:
                  _context3.t0 = _context3.sent;

                case 5:
                  return _context3.abrupt("return", _context3.t0);

                case 6:
                case "end":
                  return _context3.stop();
              }
            }, _callee3, this);
          }));

          function getBundle(_x4) {
            return _getBundle.apply(this, arguments);
          }

          return getBundle;
        }();

        _proto.get = /*#__PURE__*/function () {
          var _get = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(name, path, type) {
            var _this = this;

            return _regeneratorRuntime().wrap(function _callee5$(_context5) {
              while (1) switch (_context5.prev = _context5.next) {
                case 0:
                  return _context5.abrupt("return", new Promise( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4(resolve, reject) {
                    var bundle, _asset;

                    return _regeneratorRuntime().wrap(function _callee4$(_context4) {
                      while (1) switch (_context4.prev = _context4.next) {
                        case 0:
                          _context4.next = 2;
                          return _this.loadBundleAsync(name);

                        case 2:
                          bundle = _context4.sent;

                          if (bundle) {
                            _context4.next = 5;
                            break;
                          }

                          return _context4.abrupt("return");

                        case 5:
                          _asset = bundle.get(path, type);

                          if (!_asset) {
                            _context4.next = 9;
                            break;
                          }

                          resolve(_asset);
                          return _context4.abrupt("return");

                        case 9:
                          bundle.load(path, function (err, asset) {
                            if (err) return reject(err);
                            resolve(asset);
                          });

                        case 10:
                        case "end":
                          return _context4.stop();
                      }
                    }, _callee4);
                  }))));

                case 1:
                case "end":
                  return _context5.stop();
              }
            }, _callee5);
          }));

          function get(_x5, _x6, _x7) {
            return _get.apply(this, arguments);
          }

          return get;
        }();

        _proto.load = function load(name, path, type, onComplete, onProgress) {
          this.loadBundle(name, function (bundle) {
            if (!bundle) return;

            var _asset = bundle.get(path, type);

            if (_asset) {
              if (onProgress) onProgress(1, 1);
              onComplete(_asset);
              return;
            }

            bundle.load(path, function (loaded, total) {
              if (onProgress) onProgress(loaded, total);
            }, function (err, asset) {
              if (err) logger.error(err);
              onComplete(asset);
            });
          });
        };

        _proto.loadAsync = /*#__PURE__*/function () {
          var _loadAsync = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee6(bundleName, path, type, loading) {
            var bundle, _asset;

            return _regeneratorRuntime().wrap(function _callee6$(_context6) {
              while (1) switch (_context6.prev = _context6.next) {
                case 0:
                  bundle = assetManager.getBundle(bundleName);

                  if (bundle) {
                    _context6.next = 5;
                    break;
                  }

                  _context6.next = 4;
                  return this.loadBundleAsync(bundleName);

                case 4:
                  bundle = _context6.sent;

                case 5:
                  _asset = bundle.get(path, type);

                  if (!_asset) {
                    _context6.next = 9;
                    break;
                  }

                  loading == null ? void 0 : loading.onComplete(_asset);
                  return _context6.abrupt("return");

                case 9:
                  loading == null ? void 0 : loading.show();
                  bundle.load(path, function (loaded, total) {
                    if (loading) loading == null ? void 0 : loading.onProgress(loaded, total);
                  }, function (err, asset) {
                    if (err) logger.error(err);
                    loading == null ? void 0 : loading.onComplete(asset);
                    loading == null ? void 0 : loading.hide();
                  });

                case 11:
                case "end":
                  return _context6.stop();
              }
            }, _callee6, this);
          }));

          function loadAsync(_x10, _x11, _x12, _x13) {
            return _loadAsync.apply(this, arguments);
          }

          return loadAsync;
        }();

        return ResManager;
      }();
      /** 资源管理器单例 */


      var res = exports('res', new ResManager());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/RoleBone.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './RoleBoneExports.ts'], function (exports) {
  var _inheritsLoose, _createClass, cclegacy, _decorator, sp, ccmodifier, BaseComponent, DefaultMainElements, DirectionType;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      sp = module.sp;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      DefaultMainElements = module.DefaultMainElements;
      DirectionType = module.DirectionType;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class;

      cclegacy._RF.push({}, "544b0+LGwVPDId+xDtVvs6w", "RoleBone", undefined);

      var ccclass = _decorator.ccclass,
          requireComponent = _decorator.requireComponent;
      /**
       * TODO 需要记录和同步角色主要部位上的套件信息
       */

      var RoleBone = exports('RoleBone', (_dec = ccclass('RoleBone'), _dec2 = ccmodifier('RoleBone'), _dec3 = requireComponent(sp.Skeleton), _dec(_class = _dec2(_class = _dec3(_class = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(RoleBone, _BaseComponent);

        function RoleBone() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          _this.skl = void 0;
          _this.cfg = void 0;
          _this.dir = void 0;
          _this.suits = void 0;
          _this.animate = void 0;
          return _this;
        }

        var _proto = RoleBone.prototype;

        _proto.init = function init(cfg) {
          this.skl = this.getComponent(sp.Skeleton);
          this.cfg = cfg;
          this.suits = Object.assign({}, DefaultMainElements);
          this.dir = DirectionType.Xia;
          this.animate = 'idle';
          this.changeDiretion(this.dir);
        }
        /**
         * 切换动画
         * @param ani 动画类型
         */
        ;

        _proto.changeAnimation = function changeAnimation(ani) {
          var name = this.cfg[this.dir][ani];

          if (name) {
            this.animate = ani;
            this.skl.setAnimation(0, name, true);
          } else {
            console.error("\u5207\u6362\u52A8\u753B\u5931\u8D25: " + ani);
          }
        }
        /**
         * 切换方向
         * @param dir 方向
         */
        ;

        _proto.changeDiretion = function changeDiretion(dir) {
          console.log('---准备切换方向---', dir); // 更新方向

          this.dir = dir; // 根据方向决定是否需要翻转

          this.skl.node.setScale(this.cfg[dir].flipX ? -1 : 1, 1); // 切换裸模

          this.skl.setSkin(this.cfg[dir].pifu); // 切换附件

          var data = this.skl.skeletonData.getRuntimeData();
          var elements = this.cfg[this.dir].elements;

          for (var k in elements) {
            var ele = elements[k];
            var skinName = ele.skin;
            var skin = data.findSkin(skinName);

            if (!skin) {
              this.onChangeElementError(ele, '查找皮肤');
              continue;
            }

            var slot = this.skl.findSlot(ele.slot);

            if (!slot) {
              this.onChangeElementError(ele, '查找插槽');
              continue;
            }

            var slotIndex = slot.data.index;
            var attachmentName = ele.attachment[this.suits[ele.part]];
            var attachment = skin.getAttachment(slotIndex, attachmentName);

            if (!attachment) {
              this.onChangeElementError(ele, '查找附件');
              continue;
            }

            slot.setAttachment(attachment);
            console.log('更换附件成功！', ele.slot, attachmentName, attachment);
          } // 必须先切换动作，再去更换部件


          this.changeAnimation(this.animate);
        }
        /**
         * 输出更换附件失败的信息
         * @param ele 部件及套装信息
         * @param progress 阶段
         */
        ;

        _proto.onChangeElementError = function onChangeElementError(ele, progress) {
          console.error("\u5728" + progress + "\u9636\u6BB5\u66F4\u6362\u9644\u4EF6\u5931\u8D25", this.skl.node.name, ele);
        };

        _createClass(RoleBone, [{
          key: "animation",
          get: function get() {
            return this.animate;
          }
        }, {
          key: "direction",
          get: function get() {
            return this.dir;
          }
        }]);

        return RoleBone;
      }(BaseComponent)) || _class) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/RoleBoneExports.ts", ['cc'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      cclegacy._RF.push({}, "47352lbVWFKLoseUcAhFKoX", "RoleBoneExports", undefined);
      /** 角色方向 */


      var DirectionType = exports('DirectionType', /*#__PURE__*/function (DirectionType) {
        DirectionType[DirectionType["Xia"] = 1] = "Xia";
        DirectionType[DirectionType["ZuoXia"] = 2] = "ZuoXia";
        DirectionType[DirectionType["Zuo"] = 3] = "Zuo";
        DirectionType[DirectionType["ZuoShang"] = 4] = "ZuoShang";
        DirectionType[DirectionType["Shang"] = 5] = "Shang";
        DirectionType[DirectionType["YouShang"] = 6] = "YouShang";
        DirectionType[DirectionType["You"] = 7] = "You";
        DirectionType[DirectionType["YouXia"] = 8] = "YouXia";
        return DirectionType;
      }({}));
      /** 按照方向细分套装及部件信息 */

      /** 角色身上默认的主要部件的套件 */

      var DefaultMainElements = exports('DefaultMainElements', {
        wuguan: 'default',
        toufa: 'default',
        yifu: 'default',
        kuzi: 'default',
        xiezi: 'default',
        yingzi: 'default'
      });

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/RoleBoneOfNvhai.ts", ['cc', './RoleBoneExports.ts'], function (exports) {
  var cclegacy, DirectionType;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      DirectionType = module.DirectionType;
    }],
    execute: function () {
      var _RoleBoneOfNvhai;

      cclegacy._RF.push({}, "20dcc7eR1JHY6rxSqJ4L1zJ", "RoleBoneOfNvhai", undefined);

      var RoleBoneOfNvhai = exports('RoleBoneOfNvhai', (_RoleBoneOfNvhai = {}, _RoleBoneOfNvhai[DirectionType.Xia] = {
        idle: 'idle1',
        run: 'run1',
        pifu: 'pifu/1pifu',
        flipX: false,
        elements: {
          "1-影子": {
            "slot": "1-影子",
            "skin": "yingzi/1yingzi",
            "part": "yingzi",
            "attachment": {
              "default": "1diying"
            }
          },
          "1-鞋子-左": {
            "slot": "1-鞋子-左",
            "skin": "xiezi/1xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "正面鞋子R"
            }
          },
          "1-鞋子-右": {
            "slot": "1-鞋子-右",
            "skin": "xiezi/1xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "正面鞋子L"
            }
          },
          "1-裤子": {
            "slot": "1-裤子",
            "skin": "kuzi/1kuzi",
            "part": "kuzi",
            "attachment": {
              "default": "正面裙子"
            }
          },
          "1-衣服-左胳膊": {
            "slot": "1-衣服-左胳膊",
            "skin": "yifu/1yifu",
            "part": "yifu",
            "attachment": {
              "default": "正面左胳膊"
            }
          },
          "1-衣服-右胳膊": {
            "slot": "1-衣服-右胳膊",
            "skin": "yifu/1yifu",
            "part": "yifu",
            "attachment": {
              "default": "正面右胳膊服装"
            }
          },
          "1-衣服": {
            "slot": "1-衣服",
            "skin": "yifu/1yifu",
            "part": "yifu",
            "attachment": {
              "default": "正面衣服"
            }
          },
          "1-头发-后": {
            "slot": "1-头发-后",
            "skin": "toufa/1toufa",
            "part": "toufa",
            "attachment": {
              "default": "正面后发"
            }
          },
          "1-五官-眉毛": {
            "slot": "1-五官-眉毛",
            "skin": "wuguan/1wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "正面眉毛"
            }
          },
          "1-头发-前": {
            "slot": "1-头发-前",
            "skin": "toufa/1toufa",
            "part": "toufa",
            "attachment": {
              "default": "正面头发前"
            }
          },
          "1-五官-眼睛右": {
            "slot": "1-五官-眼睛右",
            "skin": "wuguan/1wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "正面眼睛右"
            }
          },
          "1-五官-眼睛左": {
            "slot": "1-五官-眼睛左",
            "skin": "wuguan/1wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "正面眼睛左"
            }
          }
        }
      }, _RoleBoneOfNvhai[DirectionType.ZuoXia] = {
        idle: 'idle2',
        run: 'run2',
        pifu: 'pifu/2pifu',
        flipX: false,
        elements: {
          "2-影子": {
            "slot": "2-影子",
            "skin": "yingzi/2yingzi",
            "part": "yingzi",
            "attachment": {
              "default": "diying"
            }
          },
          "2-衣服-左胳膊": {
            "slot": "2-衣服-左胳膊",
            "skin": "yifu/2yifu",
            "part": "yifu",
            "attachment": {
              "default": "左胳膊衣服"
            }
          },
          "2-头发-后": {
            "slot": "2-头发-后",
            "skin": "toufa/2toufa",
            "part": "toufa",
            "attachment": {
              "default": "头发后"
            }
          },
          "2-衣服": {
            "slot": "2-衣服",
            "skin": "yifu/2yifu",
            "part": "yifu",
            "attachment": {
              "default": "衣服"
            }
          },
          "2-五官-眉毛": {
            "slot": "2-五官-眉毛",
            "skin": "wuguan/2wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "眉毛"
            }
          },
          "2-五官-眼睛右": {
            "slot": "2-五官-眼睛右",
            "skin": "wuguan/2wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "眼睛右"
            }
          },
          "2-五官-眼睛左": {
            "slot": "2-五官-眼睛左",
            "skin": "wuguan/2wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "眼睛左"
            }
          },
          "2-头发-前": {
            "slot": "2-头发-前",
            "skin": "toufa/2toufa",
            "part": "toufa",
            "attachment": {
              "default": "头发前"
            }
          },
          "2-五官-耳朵": {
            "slot": "2-五官-耳朵",
            "skin": "wuguan/2wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "耳朵"
            }
          },
          "2-鞋子-左": {
            "slot": "2-鞋子-左",
            "skin": "xiezi/2xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "左边鞋子"
            }
          },
          "2-裤子": {
            "slot": "2-裤子",
            "skin": "kuzi/2kuzi",
            "part": "kuzi",
            "attachment": {
              "default": "裙子"
            }
          },
          "2-鞋子-右": {
            "slot": "2-鞋子-右",
            "skin": "xiezi/2xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "右边鞋子"
            }
          },
          "2-衣服-右胳膊": {
            "slot": "2-衣服-右胳膊",
            "skin": "yifu/2yifu",
            "part": "yifu",
            "attachment": {
              "default": "右胳膊衣服"
            }
          }
        }
      }, _RoleBoneOfNvhai[DirectionType.Zuo] = {
        idle: 'idle3',
        run: 'run3',
        pifu: 'pifu/3pifu',
        flipX: false,
        elements: {
          "3-影子": {
            "slot": "3-影子",
            "skin": "yingzi/3yingzi",
            "part": "yingzi",
            "attachment": {
              "default": "diying"
            }
          },
          "3-衣服-左胳膊": {
            "slot": "3-衣服-左胳膊",
            "skin": "yifu/3yifu",
            "part": "yifu",
            "attachment": {
              "default": "侧左手衣服"
            }
          },
          "3-鞋子-左": {
            "slot": "3-鞋子-左",
            "skin": "xiezi/3xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "侧左边鞋子"
            }
          },
          "3-鞋子-右": {
            "slot": "3-鞋子-右",
            "skin": "xiezi/3xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "侧右边鞋子"
            }
          },
          "3-衣服": {
            "slot": "3-衣服",
            "skin": "yifu/3yifu",
            "part": "yifu",
            "attachment": {
              "default": "侧衣服"
            }
          },
          "3-裤子": {
            "slot": "3-裤子",
            "skin": "kuzi/3kuzi",
            "part": "kuzi",
            "attachment": {
              "default": "侧裙子"
            }
          },
          "3-衣服-右胳膊": {
            "slot": "3-衣服-右胳膊",
            "skin": "yifu/3yifu",
            "part": "yifu",
            "attachment": {
              "default": "侧右手衣服"
            }
          },
          "3-头发-后": {
            "slot": "3-头发-后",
            "skin": "toufa/3toufa",
            "part": "toufa",
            "attachment": {
              "default": "侧头发后"
            }
          },
          "3-五官-眼睛": {
            "slot": "3-五官-眼睛",
            "skin": "wuguan/3wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "侧五官"
            }
          },
          "3-五官-眉毛": {
            "slot": "3-五官-眉毛",
            "skin": "wuguan/3wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "侧眉毛"
            }
          },
          "3-头发-前": {
            "slot": "3-头发-前",
            "skin": "toufa/3toufa",
            "part": "toufa",
            "attachment": {
              "default": "侧头发前"
            }
          },
          "3-五官-耳朵": {
            "slot": "3-五官-耳朵",
            "skin": "wuguan/3wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "侧耳朵"
            }
          }
        }
      }, _RoleBoneOfNvhai[DirectionType.ZuoShang] = {
        idle: 'idle4',
        run: 'run4',
        pifu: 'pifu/4pifu',
        flipX: false,
        elements: {
          "4-影子": {
            "slot": "4-影子",
            "skin": "yingzi/4yingzi",
            "part": "yingzi",
            "attachment": {
              "default": "diying"
            }
          },
          "4-鞋子-右02": {
            "slot": "4-鞋子-右02",
            "skin": "xiezi/4xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "3/4鞋子右内侧"
            }
          },
          "4-鞋子-右": {
            "slot": "4-鞋子-右",
            "skin": "xiezi/4xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "3/4右鞋子"
            }
          },
          "4-鞋子-左02": {
            "slot": "4-鞋子-左02",
            "skin": "xiezi/4xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "3/4左鞋内侧"
            }
          },
          "4-鞋子-左": {
            "slot": "4-鞋子-左",
            "skin": "xiezi/4xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "3/4左鞋子"
            }
          },
          "4-衣服-右胳膊": {
            "slot": "4-衣服-右胳膊",
            "skin": "yifu/4yifu",
            "part": "yifu",
            "attachment": {
              "default": "3/4右胳膊袖子"
            }
          },
          "4-衣服": {
            "slot": "4-衣服",
            "skin": "yifu/4yifu",
            "part": "yifu",
            "attachment": {
              "default": "4衣服"
            }
          },
          "4-裤子": {
            "slot": "4-裤子",
            "skin": "kuzi/4kuzi",
            "part": "kuzi",
            "attachment": {
              "default": "4裙子"
            }
          },
          "4-衣服-左胳膊": {
            "slot": "4-衣服-左胳膊",
            "skin": "yifu/4yifu",
            "part": "yifu",
            "attachment": {
              "default": "3/4左胳膊袖子"
            }
          },
          "4-头发": {
            "slot": "4-头发",
            "skin": "toufa/4toufa",
            "part": "toufa",
            "attachment": {
              "default": "4头发"
            }
          },
          "4-五官-耳朵": {
            "slot": "4-五官-耳朵",
            "skin": "wuguan/4wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "4耳朵"
            }
          }
        }
      }, _RoleBoneOfNvhai[DirectionType.Shang] = {
        idle: 'idle5',
        run: 'run5',
        pifu: 'pifu/5pifu',
        flipX: false,
        elements: {
          "5-影子": {
            "slot": "5-影子",
            "skin": "yingzi/5yingzi",
            "part": "yingzi",
            "attachment": {
              "default": "diying"
            }
          },
          "5-衣服": {
            "slot": "5-衣服",
            "skin": "yifu/5yifu",
            "part": "yifu",
            "attachment": {
              "default": "5衣服"
            }
          },
          "5-裤子": {
            "slot": "5-裤子",
            "skin": "kuzi/5kuzi",
            "part": "kuzi",
            "attachment": {
              "default": "5裙子"
            }
          },
          "5-衣服-右胳膊": {
            "slot": "5-衣服-右胳膊",
            "skin": "yifu/5yifu",
            "part": "yifu",
            "attachment": {
              "default": "5右胳膊衣服"
            }
          },
          "5-衣服-左胳膊": {
            "slot": "5-衣服-左胳膊",
            "skin": "yifu/5yifu",
            "part": "yifu",
            "attachment": {
              "default": "5左胳膊衣服"
            }
          },
          "5-鞋子-左01": {
            "slot": "5-鞋子-左01",
            "skin": "xiezi/5xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "5左鞋子0"
            }
          },
          "5-鞋子-右01": {
            "slot": "5-鞋子-右01",
            "skin": "xiezi/5xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "5右鞋子0"
            }
          },
          "5-五官-耳朵": {
            "slot": "5-五官-耳朵",
            "skin": "wuguan/5wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "5耳朵"
            }
          },
          "5-头发": {
            "slot": "5-头发",
            "skin": "toufa/5toufa",
            "part": "toufa",
            "attachment": {
              "default": "5头发"
            }
          }
        }
      }, _RoleBoneOfNvhai[DirectionType.YouShang] = {
        idle: 'idle4',
        run: 'run4',
        pifu: 'pifu/4pifu',
        flipX: true,
        elements: {
          "4-影子": {
            "slot": "4-影子",
            "skin": "yingzi/4yingzi",
            "part": "yingzi",
            "attachment": {
              "default": "diying"
            }
          },
          "4-鞋子-右02": {
            "slot": "4-鞋子-右02",
            "skin": "xiezi/4xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "3/4鞋子右内侧"
            }
          },
          "4-鞋子-右": {
            "slot": "4-鞋子-右",
            "skin": "xiezi/4xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "3/4右鞋子"
            }
          },
          "4-鞋子-左02": {
            "slot": "4-鞋子-左02",
            "skin": "xiezi/4xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "3/4左鞋内侧"
            }
          },
          "4-鞋子-左": {
            "slot": "4-鞋子-左",
            "skin": "xiezi/4xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "3/4左鞋子"
            }
          },
          "4-衣服-右胳膊": {
            "slot": "4-衣服-右胳膊",
            "skin": "yifu/4yifu",
            "part": "yifu",
            "attachment": {
              "default": "3/4右胳膊袖子"
            }
          },
          "4-衣服": {
            "slot": "4-衣服",
            "skin": "yifu/4yifu",
            "part": "yifu",
            "attachment": {
              "default": "4衣服"
            }
          },
          "4-裤子": {
            "slot": "4-裤子",
            "skin": "kuzi/4kuzi",
            "part": "kuzi",
            "attachment": {
              "default": "4裙子"
            }
          },
          "4-衣服-左胳膊": {
            "slot": "4-衣服-左胳膊",
            "skin": "yifu/4yifu",
            "part": "yifu",
            "attachment": {
              "default": "3/4左胳膊袖子"
            }
          },
          "4-头发": {
            "slot": "4-头发",
            "skin": "toufa/4toufa",
            "part": "toufa",
            "attachment": {
              "default": "4头发"
            }
          },
          "4-五官-耳朵": {
            "slot": "4-五官-耳朵",
            "skin": "wuguan/4wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "4耳朵"
            }
          }
        }
      }, _RoleBoneOfNvhai[DirectionType.You] = {
        idle: 'idle3',
        run: 'run3',
        pifu: 'pifu/3pifu',
        flipX: true,
        elements: {
          "3-影子": {
            "slot": "3-影子",
            "skin": "yingzi/3yingzi",
            "part": "yingzi",
            "attachment": {
              "default": "diying"
            }
          },
          "3-衣服-左胳膊": {
            "slot": "3-衣服-左胳膊",
            "skin": "yifu/3yifu",
            "part": "yifu",
            "attachment": {
              "default": "侧左手衣服"
            }
          },
          "3-鞋子-左": {
            "slot": "3-鞋子-左",
            "skin": "xiezi/3xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "侧左边鞋子"
            }
          },
          "3-鞋子-右": {
            "slot": "3-鞋子-右",
            "skin": "xiezi/3xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "侧右边鞋子"
            }
          },
          "3-衣服": {
            "slot": "3-衣服",
            "skin": "yifu/3yifu",
            "part": "yifu",
            "attachment": {
              "default": "侧衣服"
            }
          },
          "3-裤子": {
            "slot": "3-裤子",
            "skin": "kuzi/3kuzi",
            "part": "kuzi",
            "attachment": {
              "default": "侧裙子"
            }
          },
          "3-衣服-右胳膊": {
            "slot": "3-衣服-右胳膊",
            "skin": "yifu/3yifu",
            "part": "yifu",
            "attachment": {
              "default": "侧右手衣服"
            }
          },
          "3-头发-后": {
            "slot": "3-头发-后",
            "skin": "toufa/3toufa",
            "part": "toufa",
            "attachment": {
              "default": "侧头发后"
            }
          },
          "3-五官-眼睛": {
            "slot": "3-五官-眼睛",
            "skin": "wuguan/3wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "侧五官"
            }
          },
          "3-五官-眉毛": {
            "slot": "3-五官-眉毛",
            "skin": "wuguan/3wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "侧眉毛"
            }
          },
          "3-头发-前": {
            "slot": "3-头发-前",
            "skin": "toufa/3toufa",
            "part": "toufa",
            "attachment": {
              "default": "侧头发前"
            }
          },
          "3-五官-耳朵": {
            "slot": "3-五官-耳朵",
            "skin": "wuguan/3wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "侧耳朵"
            }
          }
        }
      }, _RoleBoneOfNvhai[DirectionType.YouXia] = {
        idle: 'idle2',
        run: 'run2',
        pifu: 'pifu/2pifu',
        flipX: true,
        elements: {
          "2-影子": {
            "slot": "2-影子",
            "skin": "yingzi/2yingzi",
            "part": "yingzi",
            "attachment": {
              "default": "diying"
            }
          },
          "2-衣服-左胳膊": {
            "slot": "2-衣服-左胳膊",
            "skin": "yifu/2yifu",
            "part": "yifu",
            "attachment": {
              "default": "左胳膊衣服"
            }
          },
          "2-头发-后": {
            "slot": "2-头发-后",
            "skin": "toufa/2toufa",
            "part": "toufa",
            "attachment": {
              "default": "头发后"
            }
          },
          "2-衣服": {
            "slot": "2-衣服",
            "skin": "yifu/2yifu",
            "part": "yifu",
            "attachment": {
              "default": "衣服"
            }
          },
          "2-五官-眉毛": {
            "slot": "2-五官-眉毛",
            "skin": "wuguan/2wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "眉毛"
            }
          },
          "2-五官-眼睛右": {
            "slot": "2-五官-眼睛右",
            "skin": "wuguan/2wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "眼睛右"
            }
          },
          "2-五官-眼睛左": {
            "slot": "2-五官-眼睛左",
            "skin": "wuguan/2wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "眼睛左"
            }
          },
          "2-头发-前": {
            "slot": "2-头发-前",
            "skin": "toufa/2toufa",
            "part": "toufa",
            "attachment": {
              "default": "头发前"
            }
          },
          "2-五官-耳朵": {
            "slot": "2-五官-耳朵",
            "skin": "wuguan/2wuguan",
            "part": "wuguan",
            "attachment": {
              "default": "耳朵"
            }
          },
          "2-鞋子-左": {
            "slot": "2-鞋子-左",
            "skin": "xiezi/2xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "左边鞋子"
            }
          },
          "2-裤子": {
            "slot": "2-裤子",
            "skin": "kuzi/2kuzi",
            "part": "kuzi",
            "attachment": {
              "default": "裙子"
            }
          },
          "2-鞋子-右": {
            "slot": "2-鞋子-右",
            "skin": "xiezi/2xiezi",
            "part": "xiezi",
            "attachment": {
              "default": "右边鞋子"
            }
          },
          "2-衣服-右胳膊": {
            "slot": "2-衣服-右胳膊",
            "skin": "yifu/2yifu",
            "part": "yifu",
            "attachment": {
              "default": "右胳膊衣服"
            }
          }
        }
      }, _RoleBoneOfNvhai));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/RollingLottery.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './MoveBase.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, EventHandler, Enum, _decorator, rect, mat4, Node, Layout, UITransform, Mat4, v3, Rect, MoveBase_, MoveBase;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      EventHandler = module.EventHandler;
      Enum = module.Enum;
      _decorator = module._decorator;
      rect = module.rect;
      mat4 = module.mat4;
      Node = module.Node;
      Layout = module.Layout;
      UITransform = module.UITransform;
      Mat4 = module.Mat4;
      v3 = module.v3;
      Rect = module.Rect;
    }, function (module) {
      MoveBase_ = module.MoveBase_;
      MoveBase = module.MoveBase;
    }],
    execute: function () {
      exports('RollingLottery_', void 0);

      var _dec, _dec2, _class, _class2, _descriptor, _descriptor2;

      cclegacy._RF.push({}, "e08f0/RxDNHHbuoAXU4dcyg", "RollingLottery", undefined);

      var _cc$_decorator = _decorator,
          ccclass = _cc$_decorator.ccclass,
          property = _cc$_decorator.property,
          requireComponent = _cc$_decorator.requireComponent;

      var _RollingLottery;

      (function (_RollingLottery2) {
        var Direction = /*#__PURE__*/function (Direction) {
          Direction[Direction["\u7AD6"] = 0] = "\u7AD6";
          Direction[Direction["\u6A2A"] = 1] = "\u6A2A";
          return Direction;
        }({});

        _RollingLottery2.Direction = Direction;
      })(_RollingLottery || (_RollingLottery = {}));
      /** 旋转抽奖 */


      var RollingLottery = exports('RollingLottery', (_dec = property({
        displayName: '子节点刷新事件',
        tooltip: '(子节点_node, 下标_indexN)',
        type: EventHandler
      }), _dec2 = property({
        displayName: '方向',
        type: Enum(_RollingLottery.Direction)
      }), ccclass(_class = (_class2 = /*#__PURE__*/function (_MoveBase) {
        _inheritsLoose(RollingLottery, _MoveBase);

        function RollingLottery() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _MoveBase.call.apply(_MoveBase, [this].concat(args)) || this;
          /* --------------- 属性 --------------- */

          /** 子节点刷新事件 */

          _initializerDefineProperty(_this, "itemUpdateEvent", _descriptor, _assertThisInitialized(_this));
          /** 方向 */


          _initializerDefineProperty(_this, "dire", _descriptor2, _assertThisInitialized(_this));
          /* --------------- private --------------- */

          /** 周长 */


          _this._perimeterN = 0;
          /** 子节点大小 */

          _this._ItemSize = void 0;
          /** 自己矩形 */

          _this._selfRect = rect();
          /** 父节点中心点矩形 */

          _this._parentCenterRect = void 0;
          /** uiTransform 表 */

          _this._uiTransformTab = void 0;
          /* --------------- 临时变量 --------------- */

          _this._tempM4 = mat4();
          _this._temp2M4 = mat4();
          /** 滚动子节点临时变量 */

          _this._temp = new ( /*#__PURE__*/function () {
            function _class5() {
              /** 当前节点矩形 */
              this.currNodeRect = rect();
              /** 更新节点坐标 */

              this.updatePosB = false;
              /** 当前节点 UITransform */

              this.currTransform = void 0;
              /** 当前下标 */

              this.currIndexN = void 0;
              /** 超出周长倍数 */

              this.outOfRangeMultipleN = void 0;
            }

            return _class5;
          }())();
          _this._temp3Rect = rect();
          return _this;
        }

        var _proto = RollingLottery.prototype;
        /* ------------------------------- 生命周期 ------------------------------- */

        _proto.onLoad = function onLoad() {
          _MoveBase.prototype.onLoad.call(this);

          this._initView();

          this._initEvent();
        }
        /* ------------------------------- 功能 ------------------------------- */

        /** 初始化事件 */
        ;

        _proto._initEvent = function _initEvent() {
          this.node.on(Node.EventType.CHILD_ADDED, this._nodeChildAdded, this);
          this.node.on(Node.EventType.CHILD_REMOVED, this._nodeChildRemoved, this);
        }
        /** 初始化视图 */
        ;

        _proto._initView = function _initView() {
          var _this2 = this;

          var layout = this.node.getComponent(Layout);

          if (layout) {
            layout.enabled = false;
          } // 初始化子节点及选中


          if (this.node.children.length) {
            // 重置子节点
            this.node.children.forEach(function (v, kN) {
              v.name = String(kN + _this2._currIndexN);

              _this2.itemUpdateEvent.emit([v, kN + _this2._currIndexN]);
            });
            this.jump(this._currIndexN);
          }
        }
        /** 重制数据 */
        ;

        _proto._resetData = function _resetData() {
          var _this3 = this;

          this._currIndexN = 0;
          this._ItemSize = this.node.children[0].getComponent(UITransform).contentSize.clone(); // item 大小矩形，中心点在节点 (0, 0) 位置

          this._parentCenterRect = rect(this.node.worldPosition.x - this._ItemSize.width * 0.5, this.node.worldPosition.y - this._ItemSize.height * 0.5, this._ItemSize.width, this._ItemSize.height); // 重置数据

          this._uiTransformTab = Object.create(null);
          this._uiTransformTab[this.node.uuid] = this.node.getComponent(UITransform);
          this._selfRect = this._getBoundingBoxToWorld(this.node);
          this._perimeterN = 0; // 更新周长

          var itemSize;
          this.node.children.forEach(function (v) {
            _this3._uiTransformTab[v.uuid] = v.getComponent(UITransform);
            itemSize = _this3._uiTransformTab[v.uuid].contentSize;
            _this3._perimeterN += _this3.dire === _RollingLottery.Direction.横 ? itemSize.width : itemSize.height;
          });
        }
        /** 重制视图 */
        ;

        _proto._resetView = function _resetView() {
          if (this.node.children.length) {
            this.jump(this._currIndexN);
          }
        }
        /** 重置 */
        ;

        _proto._reset = function _reset() {
          this._resetData();

          this._resetView();
        }
        /**
         * 获取在世界坐标系下的节点包围盒(不包含自身激活的子节点范围)
         * @param node_ 目标节点
         * @param outRect_ 输出矩形
         * @returns 输出矩形
         */
        ;

        _proto._getBoundingBoxToWorld = function _getBoundingBoxToWorld(node_, outRect_) {
          if (outRect_ === void 0) {
            outRect_ = rect();
          }

          var uiTransform = this._uiTransformTab[node_.uuid];
          Mat4.fromRTS(this._tempM4, node_.getRotation(), node_.getPosition(), node_.getScale());
          var width = uiTransform.contentSize.width;
          var height = uiTransform.contentSize.height;
          outRect_.set(-uiTransform.anchorPoint.x * width, -uiTransform.anchorPoint.y * height, width, height);
          node_.parent.getWorldMatrix(this._temp2M4);
          Mat4.multiply(this._temp2M4, this._temp2M4, this._tempM4);
          outRect_.transformMat4(this._temp2M4);
          return outRect_;
        }
        /**
         * 更新节点下标
         * @param node_ 目标节点
         * @param indexN_ 下标
         */
        ;

        _proto._updateNodeIndex = function _updateNodeIndex(node_, indexN_) {
          node_.name = String(indexN_);
          this.itemUpdateEvent.emit([node_, indexN_]);
        }
        /**
         * 上到下移动子节点
         * @param distN_ 距离
         */
        ;

        _proto._moveNodeTopToBottom = function _moveNodeTopToBottom(distN_) {
          var _this4 = this;

          this.node.children.forEach(function (v, kN) {
            _this4._temp.currTransform = _this4._uiTransformTab[v.uuid];
            _this4._temp.updatePosB = false;

            _this4._getBoundingBoxToWorld(v, _this4._temp.currNodeRect); // 移动坐标


            _this4._temp.currNodeRect.y += distN_; // 相交则更新节点坐标

            if (_this4._temp.currNodeRect.intersects(_this4._selfRect)) {
              _this4._temp.updatePosB = true;
            } // 若不相交则超出范围
            else {
                // 若节点在上方则跳过更新
                if (_this4._temp.currNodeRect.yMin > _this4._selfRect.yMax) {
                  _this4._temp.updatePosB = true;
                } else {
                  // (超出范围 / 周长) + 超出视图区域的 1
                  _this4._temp.outOfRangeMultipleN = Math.floor((_this4._selfRect.yMin - _this4._temp.currNodeRect.yMax) / _this4._perimeterN) + 1; // 更新坐标

                  _this4._temp.currNodeRect.y += _this4._temp.outOfRangeMultipleN * _this4._perimeterN;
                  v.worldPosition = v3(v.worldPosition.x, _this4._temp.currNodeRect.y + _this4._ItemSize.height * _this4._temp.currTransform.anchorY); // 更新 item 下标

                  _this4._updateNodeIndex(v, Number(v.name) - _this4._temp.outOfRangeMultipleN * _this4.node.children.length);
                }
              } // 更新节点坐标


            if (_this4._temp.updatePosB) {
              v.worldPosition = v3(v.worldPosition.x, _this4._temp.currNodeRect.y + _this4._temp.currNodeRect.height * _this4._temp.currTransform.anchorY);
            } // 更新当前下标


            _this4._temp.currIndexN = Number(v.name);

            if (_this4._temp.currIndexN < _this4._currIndexN && Rect.intersection(_this4._temp3Rect, _this4._temp.currNodeRect, _this4._parentCenterRect).height >= _this4._parentCenterRect.height * 0.5) {
              _this4.currIndexN = _this4._temp.currIndexN;
            }
          });
        }
        /**
         * 下到上移动子节点
         * @param distN_ 距离
         */
        ;

        _proto._moveNodeBottomToTop = function _moveNodeBottomToTop(distN_) {
          var _this5 = this;

          this.node.children.forEach(function (v, kN) {
            _this5._temp.currTransform = _this5._uiTransformTab[v.uuid];
            _this5._temp.updatePosB = false;

            _this5._getBoundingBoxToWorld(v, _this5._temp.currNodeRect); // 移动坐标


            _this5._temp.currNodeRect.y += distN_; // 相交则更新节点坐标

            if (_this5._temp.currNodeRect.intersects(_this5._selfRect)) {
              _this5._temp.updatePosB = true;
            } // 若不相交则超出范围
            else {
                // 若节点在下方则跳过更新
                if (_this5._selfRect.yMin > _this5._temp.currNodeRect.yMax) {
                  _this5._temp.updatePosB = true;
                } else {
                  // (超出范围 / 周长) + 超出视图区域的 1
                  _this5._temp.outOfRangeMultipleN = Math.floor((_this5._temp.currNodeRect.yMin - _this5._selfRect.yMax) / _this5._perimeterN) + 1; // 更新坐标

                  _this5._temp.currNodeRect.y -= _this5._temp.outOfRangeMultipleN * _this5._perimeterN;
                  v.worldPosition = v3(v.worldPosition.x, _this5._temp.currNodeRect.y + _this5._ItemSize.height * _this5._temp.currTransform.anchorY); // 更新 item 下标

                  _this5._updateNodeIndex(v, Number(v.name) + _this5._temp.outOfRangeMultipleN * _this5.node.children.length);
                }
              } // 更新节点坐标


            if (_this5._temp.updatePosB) {
              v.worldPosition = v3(v.worldPosition.x, _this5._temp.currNodeRect.y + _this5._temp.currNodeRect.height * _this5._temp.currTransform.anchorY);
            } // 更新当前下标


            _this5._temp.currIndexN = Number(v.name);

            if (_this5._temp.currIndexN > _this5._currIndexN && Rect.intersection(_this5._temp3Rect, _this5._temp.currNodeRect, _this5._parentCenterRect).height >= _this5._parentCenterRect.height * 0.5) {
              _this5.currIndexN = _this5._temp.currIndexN;
            }
          });
        }
        /**
         * 左到右移动子节
         * @param distN_ 距离
         */
        ;

        _proto._moveNodeLeftToRight = function _moveNodeLeftToRight(distN_) {
          var _this6 = this;

          this.node.children.forEach(function (v, kN) {
            _this6._temp.currTransform = _this6._uiTransformTab[v.uuid];
            _this6._temp.updatePosB = false;

            _this6._getBoundingBoxToWorld(v, _this6._temp.currNodeRect); // 移动坐标


            _this6._temp.currNodeRect.x += distN_; // 相交则更新节点坐标

            if (_this6._temp.currNodeRect.intersects(_this6._selfRect)) {
              _this6._temp.updatePosB = true;
            } // 若不相交则超出范围
            else {
                // 若节点在左方则跳过更新
                if (_this6._temp.currNodeRect.xMax < _this6._selfRect.xMin) {
                  _this6._temp.updatePosB = true;
                } else {
                  // (超出范围 / 周长) + 超出视图区域的 1
                  _this6._temp.outOfRangeMultipleN = Math.floor((_this6._temp.currNodeRect.xMin - _this6._selfRect.xMax) / _this6._perimeterN) + 1; // 更新坐标

                  _this6._temp.currNodeRect.x -= _this6._temp.outOfRangeMultipleN * _this6._perimeterN;
                  v.worldPosition = v3(_this6._temp.currNodeRect.x + _this6._ItemSize.width * _this6._temp.currTransform.anchorX, v.worldPosition.y); // 更新 item 下标

                  _this6._updateNodeIndex(v, Number(v.name) - _this6._temp.outOfRangeMultipleN * _this6.node.children.length);
                }
              } // 更新节点坐标


            if (_this6._temp.updatePosB) {
              v.worldPosition = v3(_this6._temp.currNodeRect.x + _this6._temp.currNodeRect.width * _this6._temp.currTransform.anchorX, v.worldPosition.y);
            } // 更新当前下标


            _this6._temp.currIndexN = Number(v.name);

            if (_this6._temp.currIndexN < _this6._currIndexN && Rect.intersection(_this6._temp3Rect, _this6._temp.currNodeRect, _this6._parentCenterRect).width >= _this6._parentCenterRect.width * 0.5) {
              _this6.currIndexN = _this6._temp.currIndexN;
            }
          });
        }
        /**
         * 右到左移动子节
         * @param distN_ 距离
         */
        ;

        _proto._moveNodeRightToLeft = function _moveNodeRightToLeft(distN_) {
          var _this7 = this;

          this.node.children.forEach(function (v, kN) {
            _this7._temp.currTransform = _this7._uiTransformTab[v.uuid];
            _this7._temp.updatePosB = false;

            _this7._getBoundingBoxToWorld(v, _this7._temp.currNodeRect); // 移动坐标


            _this7._temp.currNodeRect.x += distN_; // 相交则更新节点坐标

            if (_this7._temp.currNodeRect.intersects(_this7._selfRect)) {
              _this7._temp.updatePosB = true;
            } // 若不相交则超出范围
            else {
                // 若节点在右方则跳过更新
                if (_this7._temp.currNodeRect.xMin > _this7._selfRect.xMax) {
                  _this7._temp.updatePosB = true;
                } else {
                  // (超出范围 / 周长) + 超出视图区域的 1
                  _this7._temp.outOfRangeMultipleN = Math.floor((_this7._selfRect.xMin - _this7._temp.currNodeRect.xMax) / _this7._perimeterN) + 1; // 更新坐标

                  _this7._temp.currNodeRect.x += _this7._temp.outOfRangeMultipleN * _this7._perimeterN;
                  v.worldPosition = v3(_this7._temp.currNodeRect.x + _this7._ItemSize.width * _this7._temp.currTransform.anchorX, v.worldPosition.y); // 更新 item 下标

                  _this7._updateNodeIndex(v, Number(v.name) + _this7._temp.outOfRangeMultipleN * _this7.node.children.length);
                }
              } // 更新节点坐标


            if (_this7._temp.updatePosB) {
              v.worldPosition = v3(_this7._temp.currNodeRect.x + _this7._temp.currNodeRect.width * _this7._temp.currTransform.anchorX, v.worldPosition.y);
            } // 更新当前下标


            _this7._temp.currIndexN = Number(v.name);

            if (_this7._temp.currIndexN > _this7._currIndexN && Rect.intersection(_this7._temp3Rect, _this7._temp.currNodeRect, _this7._parentCenterRect).width >= _this7._parentCenterRect.width * 0.5) {
              _this7.currIndexN = _this7._temp.currIndexN;
            }
          });
        }
        /** 初始化数据 */
        ;

        _proto._initData = function _initData() {
          _MoveBase.prototype._initData.call(this);

          this._resetData();
        }
        /** 运动 */
        ;

        _proto._move = function _move(valueN_) {
          if (!valueN_) {
            return;
          } // 左右滚动


          if (this.dire === _RollingLottery.Direction.横) {
            // 从左往右
            if (valueN_ > 0) {
              this._moveNodeLeftToRight(valueN_);
            } // 从右往左
            else if (valueN_ < 0) {
                this._moveNodeRightToLeft(valueN_);
              }
          } // 上下滚动
          else {
              // 从上往下
              if (valueN_ < 0) {
                this._moveNodeTopToBottom(valueN_);
              } // 从下往上
              else if (valueN_ > 0) {
                  this._moveNodeBottomToTop(valueN_);
                }
            }
        }
        /** 获取当前下标 */
        ;

        _proto._getCurrIndex = function _getCurrIndex() {
          return this.currIndexN;
        };

        _proto._getMoveDist = function _getMoveDist(indexN_, scrollConfig_) {
          /** 当前节点 */
          var currNode = this.node.getChildByName(String(this._currIndexN));
          /** 间隔格子 */

          var intervalN = indexN_ - this._currIndexN;
          /** 格子距离 */

          var boxDistN = this.dire === _RollingLottery.Direction.横 ? this._ItemSize.width : this._ItemSize.height;
          /** 当前格子距父节点(0, 0)的偏移坐标 */

          var offsetDistV3 = this.node.worldPosition.clone().subtract(currNode.worldPosition); // 设置总距离

          if (this.dire === _RollingLottery.Direction.横) {
            return -intervalN * boxDistN + offsetDistV3.x;
          } else {
            return intervalN * boxDistN + offsetDistV3.y;
          }
        };

        _proto.loop = function loop(speedN_, timeSN_) {
          if (this.dire === _RollingLottery.Direction.竖) {
            speedN_ = -speedN_;
          }

          _MoveBase.prototype.loop.call(this, speedN_, timeSN_);
        };

        _proto.move = function move(indexN_, scrollConfig_) {
          _MoveBase.prototype.move.call(this, indexN_, new RollingLottery_.ScrollConfig(scrollConfig_));
        };

        _proto.jump = function jump(indexN_) {
          _MoveBase.prototype.jump.call(this, indexN_);

          this.currIndexN = indexN_;
        };

        _proto.getSpeed = function getSpeed(indexN_, scrollConfig_) {
          return _MoveBase.prototype.getSpeed.call(this, indexN_, new RollingLottery_.GetSpeedConfig(scrollConfig_));
        }
        /* ------------------------------- 节点事件 ------------------------------- */
        ;

        _proto._nodeChildAdded = function _nodeChildAdded() {
          this._reset();
        };

        _proto._nodeChildRemoved = function _nodeChildRemoved() {
          this._reset();
        };

        return RollingLottery;
      }(MoveBase), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "itemUpdateEvent", [_dec], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return new EventHandler();
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "dire", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return _RollingLottery.Direction.竖;
        }
      })), _class2)) || _class));
      var RollingLottery_;

      (function (_RollingLottery_) {
        var Direction = _RollingLottery_.Direction = _RollingLottery.Direction;

        var ScrollConfig = /*#__PURE__*/function (_MoveBase_$MoveConfig) {
          _inheritsLoose(ScrollConfig, _MoveBase_$MoveConfig);

          function ScrollConfig(init_) {
            return _MoveBase_$MoveConfig.call(this, init_) || this;
          }

          return ScrollConfig;
        }(MoveBase_.MoveConfig);

        _RollingLottery_.ScrollConfig = ScrollConfig;

        var GetSpeedConfig = /*#__PURE__*/function (_ScrollConfig) {
          _inheritsLoose(GetSpeedConfig, _ScrollConfig);

          function GetSpeedConfig(init_) {
            var _this8;

            _this8 = _ScrollConfig.call(this, init_) || this;
            /** 进度 */

            _this8.ratioN = 0;
            Object.assign(_assertThisInitialized(_this8), init_);
            return _this8;
          }

          return GetSpeedConfig;
        }(ScrollConfig);

        _RollingLottery_.GetSpeedConfig = GetSpeedConfig;
      })(RollingLottery_ || (RollingLottery_ = exports('RollingLottery_', {})));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/RollingTip.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, UITransform, Label, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      UITransform = module.UITransform;
      Label = module.Label;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3;

      cclegacy._RF.push({}, "f561dfweQFOmaF8ChuQFJUO", "RollingTip", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      /**
       * 跑马灯功能
       * - 当前用于全服滚动公告
       */

      var RollingTip = exports('RollingTip', (_dec = ccclass('RollingTip'), _dec2 = ccmodifier('RollingTip'), _dec3 = property({
        type: UITransform
      }), _dec4 = property({
        type: UITransform
      }), _dec5 = property({
        type: Label
      }), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(RollingTip, _BaseComponent);

        function RollingTip() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "bkgTranform", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "labTransform", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "lab", _descriptor3, _assertThisInitialized(_this));
          /** 当前文本 */


          _this.__text = '';
          /** 滚动次数 */

          _this.__times = 3;
          /** 滚动速度*/

          _this.__speed = 1;
          return _this;
        }

        var _proto = RollingTip.prototype;
        /**
         * 显示内容
         * @param text 文本内容
         * @param times 滚动次数
         * @param speed 滚动速度
         */

        _proto.show = function show(text, times, speed) {
          if (times === void 0) {
            times = 1;
          }

          if (speed === void 0) {
            speed = 2;
          }

          this.__text = text;
          this.__times = times;
          this.__speed = speed;
          this.lab.string = text;

          this._resetPosition();

          this.node.active = true;
        }
        /**
         * 隐藏（关闭）
         */
        ;

        _proto.hide = function hide() {
          this._resetPosition();

          this.node.active = false;
        }
        /**
         * 重置初始位置
         */
        ;

        _proto._resetPosition = function _resetPosition() {
          this.lab.node.setPosition(this.bkgTranform.width / 2, 0);
        }
        /**
         * 获取滚动的终点位置（X轴）
         * @returns
         */
        ;

        _proto._getTargetPosition = function _getTargetPosition() {
          return -this.bkgTranform.width / 2 - this.labTransform.width;
        }
        /**
         * 更新滚动位置
         */
        ;

        _proto.update = function update() {
          var _this$lab$node$positi = this.lab.node.position,
              x = _this$lab$node$positi.x,
              y = _this$lab$node$positi.y;
          var nx = x - this.__speed;
          this.lab.node.setPosition(nx, y);

          if (nx <= this._getTargetPosition()) {
            this.__times--; // logger.log(`剩余 ${this._times} 遍`);

            this._resetPosition();

            if (this.__times <= 0) {
              this.hide();
            }
          }
        };

        return RollingTip;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "bkgTranform", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "labTransform", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "lab", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/RotatingLottery.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './MoveBase.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, Enum, Node, _decorator, MoveBase_, MoveBase;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      Enum = module.Enum;
      Node = module.Node;
      _decorator = module._decorator;
    }, function (module) {
      MoveBase_ = module.MoveBase_;
      MoveBase = module.MoveBase;
    }],
    execute: function () {
      exports('RotatingLottery_', void 0);

      var _dec, _dec2, _dec3, _dec4, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4;

      cclegacy._RF.push({}, "d0fb97GSclCwrpLHOR8yP7o", "RotatingLottery", undefined);

      var _cc$_decorator = _decorator,
          ccclass = _cc$_decorator.ccclass,
          property = _cc$_decorator.property,
          requireComponent = _cc$_decorator.requireComponent;

      var _RotatingLottery;

      (function (_RotatingLottery2) {
        var Direction = /*#__PURE__*/function (Direction) {
          Direction[Direction["\u987A\u65F6\u9488"] = 0] = "\u987A\u65F6\u9488";
          Direction[Direction["\u9006\u65F6\u9488"] = 1] = "\u9006\u65F6\u9488";
          return Direction;
        }({});

        _RotatingLottery2.Direction = Direction;
      })(_RotatingLottery || (_RotatingLottery = {}));
      /** 旋转抽奖 */


      var RotatingLottery = exports('RotatingLottery', (_dec = property({
        displayName: '方向',
        type: Enum(_RotatingLottery.Direction)
      }), _dec2 = property({
        displayName: '旋转指针'
      }), _dec3 = property({
        displayName: '旋转对象',
        type: Node
      }), _dec4 = property({
        displayName: '内容容器',
        type: Node
      }), ccclass(_class = (_class2 = /*#__PURE__*/function (_MoveBase) {
        _inheritsLoose(RotatingLottery, _MoveBase);

        function RotatingLottery() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _MoveBase.call.apply(_MoveBase, [this].concat(args)) || this;
          /* --------------- 属性 --------------- */

          /** 方向 */

          _initializerDefineProperty(_this, "dire", _descriptor, _assertThisInitialized(_this));
          /** 旋转指针 */


          _initializerDefineProperty(_this, "rotateArrowB", _descriptor2, _assertThisInitialized(_this));
          /** 旋转对象 */


          _initializerDefineProperty(_this, "rotateNode", _descriptor3, _assertThisInitialized(_this));
          /** 内容容器 */


          _initializerDefineProperty(_this, "contentNode", _descriptor4, _assertThisInitialized(_this));
          /* --------------- private --------------- */

          /** 内容角度区间 */


          _this._contentAngleNs = [];
          /** 特殊角度下标 */

          _this._specialAngleIndexN = void 0;
          return _this;
        }

        var _proto = RotatingLottery.prototype;
        /* ------------------------------- 生命周期 ------------------------------- */

        _proto.onLoad = function onLoad() {
          _MoveBase.prototype.onLoad.call(this);

          this._initEvent();
        }
        /* ------------------------------- 功能 ------------------------------- */

        /** 初始化事件 */
        ;

        _proto._initEvent = function _initEvent() {
          this.node.on(Node.EventType.CHILD_ADDED, this._nodeChildAdded, this);
          this.node.on(Node.EventType.CHILD_REMOVED, this._nodeChildRemoved, this);
        }
        /** 更新角度区间 */
        ;

        _proto._updateAngleRange = function _updateAngleRange() {
          var _this2 = this;

          var leftNode;
          var rightNode;
          this.contentNode.children.forEach(function (v, kN) {
            // 获取左右节点
            leftNode = _this2.contentNode.children[kN + 1 === _this2.contentNode.children.length ? 0 : kN + 1];
            rightNode = _this2.contentNode.children[kN - 1 < 0 ? _this2.contentNode.children.length - 1 : kN - 1]; // 获取当前节点最大角度

            if (leftNode.angle < v.angle) {
              _this2._contentAngleNs[kN] = v.angle + Math.min((360 + leftNode.angle - v.angle) * 0.5, (v.angle - rightNode.angle) * 0.5);
            } else if (v.angle > rightNode.angle) {
              _this2._contentAngleNs[kN] = v.angle + Math.min((leftNode.angle - v.angle) * 0.5, (v.angle - rightNode.angle) * 0.5);
            } else {
              _this2._specialAngleIndexN = kN;
              _this2._contentAngleNs[kN] = v.angle + Math.min((leftNode.angle - v.angle) * 0.5, (v.angle + (360 - rightNode.angle)) * 0.5);
            }
          });
        }
        /** 重置 */
        ;

        _proto._reset = function _reset() {
          this._updateAngleRange();
        }
        /** 初始化数据 */
        ;

        _proto._initData = function _initData() {
          _MoveBase.prototype._initData.call(this);

          this._updateAngleRange();
        }
        /** 运动 */
        ;

        _proto._move = function _move(valueN_) {
          this.rotateNode.angle += valueN_;
        }
        /** 获取当前下标 */
        ;

        _proto._getCurrIndex = function _getCurrIndex() {
          var angleN = this.rotateNode.angle % 360;

          if (angleN < 0) {
            angleN += 360;
          }

          var resultN;

          for (var kN = 0, lenN = this._contentAngleNs.length; kN < lenN; ++kN) {
            if (angleN < this._contentAngleNs[kN]) {
              resultN = kN;
              break;
            }
          }

          if (resultN === undefined) {
            resultN = this._specialAngleIndexN;
          }

          if (!this.rotateArrowB) {
            resultN = this._contentAngleNs.length - 1 - resultN;
          }

          return resultN;
        };

        _proto._getMoveDist = function _getMoveDist(indexN_, scrollConfig_) {
          /** 目标节点角度 */
          var targetNodeAngleN = this.contentNode.children[indexN_].angle;
          /** 旋转节点角度 */

          var rotateNodeAngleN = this.rotateNode.angle %= 360;
          /** 目标角度 */

          var targetAngleN; // 计算最终角度

          if (this.dire === _RotatingLottery.Direction.顺时针) {
            // 旋转指针
            if (this.rotateArrowB) {
              targetAngleN = -(360 - targetNodeAngleN) - rotateNodeAngleN;

              if (targetAngleN > rotateNodeAngleN) {
                targetAngleN -= 360;
              }
            } // 旋转转盘
            else {
                targetAngleN = -targetNodeAngleN - rotateNodeAngleN;

                if (targetAngleN > rotateNodeAngleN) {
                  targetAngleN -= 360;
                }
              }

            targetAngleN %= 360; // 添加圈数

            if (!this._jumpB && scrollConfig_) {
              targetAngleN -= scrollConfig_.turnN * 360;
              targetAngleN += scrollConfig_.offsetAngleN;
            }
          } else {
            // 旋转指针
            if (this.rotateArrowB) {
              targetAngleN = targetNodeAngleN - rotateNodeAngleN;

              if (targetAngleN < rotateNodeAngleN) {
                targetAngleN += 360;
              }
            } // 旋转转盘
            else {
                targetAngleN = 360 - targetNodeAngleN - rotateNodeAngleN;

                if (targetAngleN < rotateNodeAngleN) {
                  targetAngleN += 360;
                }
              }

            targetAngleN %= 360; // 添加圈数

            if (!this._jumpB && scrollConfig_) {
              targetAngleN += scrollConfig_.turnN * 360;
              targetAngleN += scrollConfig_.offsetAngleN;
            }
          }

          return targetAngleN;
        };

        _proto.loop = function loop(speedN_, timeSN_) {
          if (this.dire === _RotatingLottery.Direction.顺时针) {
            speedN_ = -speedN_;
          }

          _MoveBase.prototype.loop.call(this, speedN_, timeSN_);
        };

        _proto.move = function move(indexN_, scrollConfig_) {
          _MoveBase.prototype.move.call(this, indexN_, new RotatingLottery_.ScrollConfig(scrollConfig_));
        };

        _proto.getSpeed = function getSpeed(indexN_, scrollConfig_) {
          return _MoveBase.prototype.getSpeed.call(this, indexN_, new RotatingLottery_.GetSpeedConfig(scrollConfig_));
        }
        /* ------------------------------- 节点事件 ------------------------------- */
        ;

        _proto._nodeChildAdded = function _nodeChildAdded() {
          this._reset();
        };

        _proto._nodeChildRemoved = function _nodeChildRemoved() {
          this._reset();
        };

        return RotatingLottery;
      }(MoveBase), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "dire", [_dec], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return _RotatingLottery.Direction.顺时针;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "rotateArrowB", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return false;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "rotateNode", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "contentNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      })), _class2)) || _class));
      var RotatingLottery_;

      (function (_RotatingLottery_) {
        var Direction = _RotatingLottery_.Direction = _RotatingLottery.Direction;

        var ScrollConfig = /*#__PURE__*/function (_MoveBase_$MoveConfig) {
          _inheritsLoose(ScrollConfig, _MoveBase_$MoveConfig);

          function ScrollConfig(init_) {
            var _this3;

            _this3 = _MoveBase_$MoveConfig.call(this, init_) || this;
            /** 圈数 */

            _this3.turnN = 1;
            /** 偏移角度 */

            _this3.offsetAngleN = 0;
            Object.assign(_assertThisInitialized(_this3), init_);
            return _this3;
          }

          return ScrollConfig;
        }(MoveBase_.MoveConfig);

        _RotatingLottery_.ScrollConfig = ScrollConfig;

        var GetSpeedConfig = /*#__PURE__*/function (_ScrollConfig) {
          _inheritsLoose(GetSpeedConfig, _ScrollConfig);

          function GetSpeedConfig(init_) {
            var _this4;

            _this4 = _ScrollConfig.call(this, init_) || this;
            /** 进度 */

            _this4.ratioN = 0;
            Object.assign(_assertThisInitialized(_this4), init_);
            return _this4;
          }

          return GetSpeedConfig;
        }(ScrollConfig);

        _RotatingLottery_.GetSpeedConfig = GetSpeedConfig;
      })(RotatingLottery_ || (RotatingLottery_ = exports('RotatingLottery_', {})));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/RoundSprite.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, _asyncToGenerator, _regeneratorRuntime, cclegacy, _decorator, Sprite, CCInteger, DynamicAtlasManager, Color, ImageAsset, Texture2D, SpriteFrame, director, Director, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Sprite = module.Sprite;
      CCInteger = module.CCInteger;
      DynamicAtlasManager = module.DynamicAtlasManager;
      Color = module.Color;
      ImageAsset = module.ImageAsset;
      Texture2D = module.Texture2D;
      SpriteFrame = module.SpriteFrame;
      director = module.director;
      Director = module.Director;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor;

      cclegacy._RF.push({}, "b7700egZu1HArxllkOYN7Yo", "RoundSprite", undefined);

      var ccclass = _decorator.ccclass,
          requireComponent = _decorator.requireComponent,
          property = _decorator.property;
      /** 隐藏的 HTMLCanvasElement */

      var HiddenCanvas = /*#__PURE__*/function () {
        /** HTMLCanvasElement */

        /** CanvasRenderingContext2D */
        function HiddenCanvas() {
          this.canvas = void 0;
          this.ctx = void 0;
          this.canvas = document.createElement('canvas');
          this.ctx = this.canvas.getContext('2d', {
            willReadFrequently: true
          });
        }
        /**
         * 调整画布尺寸
         * @param width 宽度
         * @param height 长度
         */


        var _proto = HiddenCanvas.prototype;

        _proto.resize = function resize(width, height) {
          this.canvas.width = width;
          this.canvas.height = height;
          this.clear();
        }
        /** 清空画布 */
        ;

        _proto.clear = function clear() {
          this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        }
        /** 转换为 DataURL */
        ;

        _proto.toDataURL = function toDataURL() {
          return this.canvas.toDataURL();
        }
        /** 转换为 HTMLImageElement */
        ;

        _proto.toImageElement = /*#__PURE__*/function () {
          var _toImageElement = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
            var _this = this;

            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  return _context.abrupt("return", new Promise(function (resolve) {
                    var img = new Image();
                    img.src = _this.canvas.toDataURL();

                    img.onload = function () {
                      return resolve(img);
                    };
                  }));

                case 1:
                case "end":
                  return _context.stop();
              }
            }, _callee);
          }));

          function toImageElement() {
            return _toImageElement.apply(this, arguments);
          }

          return toImageElement;
        }()
        /** 转换为 ImageAsset */
        ;

        _proto.toImageAsset = function toImageAsset() {
          return new ImageAsset(this.canvas);
        }
        /** 转换为 Texture2D */
        ;

        _proto.toTexture2D = function toTexture2D() {
          var img = this.toImageAsset();
          var tex = new Texture2D();
          tex.image = img;
          return tex;
        }
        /** 转换为 SpriteFrame */
        ;

        _proto.toSpriteFrame = function toSpriteFrame() {
          var img = this.toImageAsset();
          return SpriteFrame.createWithImage(img);
        }
        /**
         * 将精灵帧切成圆形
         * @param spriteFrame 精灵帧
         * @param radius 半径
         * @param seperately 独立绘制（选择否的话会影响全部引用此精灵帧的精灵）
         * @returns
         */
        ;

        _proto.toRoundSpriteFrame = function toRoundSpriteFrame(spriteFrame, radius, seperately) {
          var _this2 = this;

          if (!spriteFrame || !spriteFrame.isValid || spriteFrame.__converted__) return;
          var tex = spriteFrame.texture;
          var img = tex == null ? void 0 : tex.image.data;
          var tw = tex.width;
          var th = tex.height;
          var center = {
            x: tw / 2 | 0,
            y: th / 2 | 0
          };
          this.resize(tex.width, tex.height);
          this.ctx.save();
          this.ctx.beginPath();
          this.ctx.arc(center.x, center.y, radius, 0, Math.PI * 2);
          this.ctx.closePath();
          this.ctx.clip();
          this.ctx.drawImage(img, center.x - radius, center.y - radius, radius * 2, radius * 2, center.x - radius, center.y - radius, radius * 2, radius * 2);
          this.ctx.restore();

          if (seperately) {
            spriteFrame = this.toSpriteFrame();
          } else {
            spriteFrame.texture = this.toTexture2D();
          }

          spriteFrame.packable = true;
          spriteFrame.__converted__ = true;
          director.once(Director.EVENT_AFTER_DRAW, function () {
            _this2.clear();
          });
          return spriteFrame;
        };

        return HiddenCanvas;
      }();

      var RoundSprite = exports('RoundSprite', (_dec = ccclass('RoundSprite'), _dec2 = ccmodifier('RoundSprite'), _dec3 = requireComponent(Sprite), _dec4 = property({
        type: CCInteger,
        displayName: '半径'
      }), _dec5 = property({
        type: Sprite,
        displayName: '圆形图片',
        readonly: true
      }), _dec(_class = _dec2(_class = _dec3(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(RoundSprite, _BaseComponent);

        function RoundSprite() {
          var _this3;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this3 = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;

          _initializerDefineProperty(_this3, "radius", _descriptor, _assertThisInitialized(_this3));

          _this3._canvas = void 0;
          return _this3;
        }

        var _proto2 = RoundSprite.prototype;

        _proto2.onLoad = function onLoad() {
          DynamicAtlasManager.instance.enabled = false;
          this.sprite.color = new Color(0, 0, 0, 0);
          this._canvas = new HiddenCanvas();
          console.log(DynamicAtlasManager.instance);
        };

        _proto2.start = /*#__PURE__*/function () {
          var _start = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2() {
            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                  this.sprite.spriteFrame = this._canvas.toRoundSpriteFrame(this.sprite.spriteFrame, this.radius, false);
                  this.sprite.color = new Color(255, 255, 255, 255);

                case 2:
                case "end":
                  return _context2.stop();
              }
            }, _callee2, this);
          }));

          function start() {
            return _start.apply(this, arguments);
          }

          return start;
        }();

        _createClass(RoundSprite, [{
          key: "sprite",
          get: function get() {
            return this.getComponent(Sprite);
          }
        }]);

        return RoundSprite;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "radius", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _applyDecoratedDescriptor(_class2.prototype, "sprite", [_dec5], Object.getOwnPropertyDescriptor(_class2.prototype, "sprite"), _class2.prototype)), _class2)) || _class) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Row.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Column.ts'], function (exports) {
  var _createClass, _asyncToGenerator, _regeneratorRuntime, cclegacy, TextColumn;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
    }, function (module) {
      TextColumn = module.TextColumn;
    }],
    execute: function () {
      cclegacy._RF.push({}, "1b6da8/2rFKw4gwE7eeVcXe", "Row", undefined);
      /** 富文本·行 */


      var Row = exports('Row', /*#__PURE__*/function () {
        /** 列数组 */

        /** 当前宽度 */

        /** @param container 容器 */
        function Row(container) {
          this._columns = void 0;
          this._width = void 0;
          this.container = container;
          this._columns = [];
          this._width = 0;
        }
        /**
         * 添加列
         * @param column 列
         * @param force 是否强制
         * @returns
         */


        var _proto = Row.prototype;

        _proto.append = /*#__PURE__*/function () {
          var _append = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(column, force) {
            var width;
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  _context.next = 2;
                  return column.measure();

                case 2:
                  if (!force) {
                    _context.next = 9;
                    break;
                  } // 强制塞进去


                  this._width = column.width;

                  this._columns.push(column);

                  column.row = this;
                  return _context.abrupt("return", true);

                case 9:
                  // 先看看能不能塞进去
                  width = this._width + column.width + this.container.requirements.spaceX;

                  if (!(width <= this.container.requirements.maxWidth)) {
                    _context.next = 15;
                    break;
                  }

                  this._width = width;

                  this._columns.push(column);

                  column.row = this;
                  return _context.abrupt("return", true);

                case 15:
                  return _context.abrupt("return", false);

                case 16:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));

          function append(_x, _x2) {
            return _append.apply(this, arguments);
          }

          return append;
        }()
        /** 行高 */
        ;
        /**
         * 绘制
         * @param row 当前行
         * @param startY Y 轴起始点
         */


        _proto.draw = function draw(row, startY) {
          var _commentInfo$commentW, _commentInfo$commentC, _commentInfo$commentC2, _commentInfo$commentW2, _commentInfo$commentC3;

          var x = 0;
          var y = startY;
          var offset = this.textOffset;
          var requirements = this.container.requirements;
          var commentStart = undefined;
          var commentInfo = undefined;
          var commentEnded = false;
          var ctx = this.container.ctx;
          var height = this.height;
          var isTailing = false;

          for (var i = 0; i < this._columns.length; i++) {
            var ele = this._columns[i];
            var endY = y - (ele instanceof TextColumn ? offset : 0);
            ele.draw(x, endY);

            if (ele instanceof TextColumn) {
              if (ele.info.commentStyle) {
                if (commentStart === undefined) {
                  commentStart = x;
                  commentInfo = ele.info;
                }

                if (commentStart !== undefined && i === this._columns.length - 1) {
                  commentEnded = true;
                  isTailing = true;
                }
              } else {
                if (commentStart !== undefined && commentStart >= 0) {
                  commentEnded = true;
                }
              }
            } else {
              if (commentStart !== undefined && commentStart >= 0) {
                commentEnded = true;
              }
            }

            if (commentEnded && commentStart !== undefined && commentStart >= 0) {
              var endX = x + (isTailing ? ele.width : 0);

              switch (commentInfo.commentStyle) {
                case 'underline':
                  ctx.beginPath();
                  ctx.lineWidth = (_commentInfo$commentW = commentInfo.commentWidth) != null ? _commentInfo$commentW : requirements.commentWidth;
                  ctx.strokeStyle = (_commentInfo$commentC = commentInfo.commentColor) != null ? _commentInfo$commentC : requirements.commentColor;
                  ctx.moveTo(commentStart, y);
                  ctx.lineTo(endX, y);
                  ctx.stroke();
                  break;

                case 'highlight':
                  ctx.beginPath();
                  ctx.fillStyle = (_commentInfo$commentC2 = commentInfo.commentColor) != null ? _commentInfo$commentC2 : requirements.commentColor;
                  ctx.roundRect(commentStart, endY - height, endX - commentStart, height + requirements.spaceY / 2, 8);
                  ctx.fill();
                  break;

                case 'waveline':
                  ctx.beginPath();
                  ctx.lineWidth = (_commentInfo$commentW2 = commentInfo.commentWidth) != null ? _commentInfo$commentW2 : requirements.commentWidth;
                  ctx.strokeStyle = (_commentInfo$commentC3 = commentInfo.commentColor) != null ? _commentInfo$commentC3 : requirements.commentColor;
                  var piece = 16;
                  var width = endX - commentStart;
                  var segement = width / piece | 0;
                  var _x3 = commentStart;
                  ctx.moveTo(_x3, y + 4);

                  for (var _i = 0; _i < segement; _i++) {
                    _x3 += piece;
                    ctx.quadraticCurveTo(_x3 - piece / 2, y - 6, _x3, y + 4);
                  }

                  ctx.stroke();
                  break;
              }

              commentStart = undefined;
              commentEnded = false;
              commentInfo = undefined;
              isTailing = false;
            }

            x += ele.width + this.container.requirements.spaceX;
          }
        }
        /** 文本偏移量 */
        ;

        _createClass(Row, [{
          key: "height",
          get: function get() {
            if (this._columns.length === 0) {
              return this.container.requirements.fontSize;
            }

            var heights = this._columns.map(function (ele) {
              return ele.height;
            });

            return Math.max.apply(null, heights);
          }
          /** 行宽 */

        }, {
          key: "width",
          get: function get() {
            return this.container.requirements.maxWidth;
          }
        }, {
          key: "textOffset",
          get: function get() {
            var offset = 0;

            for (var i = 0; i < this._columns.length; i++) {
              var ele = this._columns[i];

              if (ele instanceof TextColumn) {
                offset = Math.max(offset, ele.metrics.actualBoundingBoxDescent);
              }
            }

            return (offset + this.container.requirements.spaceY) / 2;
          }
        }]);

        return Row;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ScrollCell.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './MoveBase.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, Enum, Node, _decorator, MoveBase_, MoveBase;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      Enum = module.Enum;
      Node = module.Node;
      _decorator = module._decorator;
    }, function (module) {
      MoveBase_ = module.MoveBase_;
      MoveBase = module.MoveBase;
    }],
    execute: function () {
      exports('ScrollCell_', void 0);

      var _dec, _dec2, _dec3, _class, _class2, _descriptor, _descriptor2, _descriptor3;

      cclegacy._RF.push({}, "b016d+IKFRD9biwPyc0d92Q", "ScrollCell", undefined);

      var _cc$_decorator = _decorator,
          ccclass = _cc$_decorator.ccclass,
          property = _cc$_decorator.property,
          requireComponent = _cc$_decorator.requireComponent;

      var _ScrollCell;

      (function (_ScrollCell2) {
        var Direction = /*#__PURE__*/function (Direction) {
          Direction[Direction["\u987A\u5E8F"] = 0] = "\u987A\u5E8F";
          Direction[Direction["\u5012\u5E8F"] = 1] = "\u5012\u5E8F";
          return Direction;
        }({});

        _ScrollCell2.Direction = Direction;
      })(_ScrollCell || (_ScrollCell = {}));
      /** 滚动单格 */


      var ScrollCell = exports('ScrollCell', (_dec = property({
        displayName: '方向',
        type: Enum(_ScrollCell.Direction)
      }), _dec2 = property({
        displayName: '内容容器',
        type: Node
      }), _dec3 = property({
        displayName: '初始下标'
      }), ccclass(_class = (_class2 = /*#__PURE__*/function (_MoveBase) {
        _inheritsLoose(ScrollCell, _MoveBase);

        function ScrollCell() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _MoveBase.call.apply(_MoveBase, [this].concat(args)) || this;
          /* --------------- 属性 --------------- */

          /** 方向 */

          _initializerDefineProperty(_this, "dire", _descriptor, _assertThisInitialized(_this));
          /** 内容容器 */


          _initializerDefineProperty(_this, "contentNode", _descriptor2, _assertThisInitialized(_this));
          /** 初始下标 */


          _initializerDefineProperty(_this, "startIndexN", _descriptor3, _assertThisInitialized(_this));
          /* --------------- private --------------- */

          /** 总距离 */


          _this._totalDistN = 0;
          return _this;
        }

        var _proto = ScrollCell.prototype;
        /* ------------------------------- 生命周期 ------------------------------- */

        _proto.onLoad = function onLoad() {
          _MoveBase.prototype.onLoad.call(this);

          this._initView();
        }
        /* ------------------------------- 功能 ------------------------------- */

        /** 初始化视图 */
        ;

        _proto._initView = function _initView() {
          this.jump(this.startIndexN);
          this._totalDistN = 0;
        }
        /** 初始化数据 */
        ;

        _proto._initData = function _initData() {
          _MoveBase.prototype._initData.call(this);
        }
        /** 运动 */
        ;

        _proto._move = function _move(valueN_) {}
        /** 获取当前下标 */
        ;

        _proto._getCurrIndex = function _getCurrIndex() {
          return Math.floor((this._totalDistN + this._currDistN) / (1 / this.contentNode.children.length));
        };

        _proto._getMoveDist = function _getMoveDist(indexN_, scrollConfig_) {
          /** 上次距离 */
          var lastDistN = this._totalDistN % 1;
          /** 目标距离 */

          var targetDist = 1 / this.contentNode.children.length * indexN_;

          if (this.dire === _ScrollCell.Direction.顺序) {
            if (targetDist > lastDistN) {
              targetDist = targetDist - lastDistN;
            } else if (targetDist < lastDistN) {
              targetDist = 1 - lastDistN + targetDist;
            } // 圈数


            return targetDist + (scrollConfig_ ? scrollConfig_.turnN : 0);
          } else {
            if (targetDist > lastDistN) {
              targetDist = targetDist - lastDistN;
            } else if (targetDist < lastDistN) {
              targetDist = 1 - lastDistN + targetDist;
            } // 圈数


            return -(1 - targetDist) - (scrollConfig_ ? scrollConfig_.turnN : 0);
          }
        };

        _proto.stop = function stop() {
          if (this._moveB) {
            this._totalDistN += this._currDistN;
          }

          _MoveBase.prototype.stop.call(this);
        };

        _proto.loop = function loop(speedN_, timeSN_) {
          if (this.dire === _ScrollCell.Direction.倒序) {
            speedN_ = -speedN_;
          }

          _MoveBase.prototype.loop.call(this, speedN_, timeSN_);
        };

        _proto.jump = function jump(indexN_) {
          this._totalDistN = 0;

          _MoveBase.prototype.jump.call(this, indexN_);

          this._totalDistN = this._targetDistN;
        };

        _proto.move = function move(indexN_, scrollConfig_) {
          _MoveBase.prototype.move.call(this, indexN_, new ScrollCell_.ScrollConfig(scrollConfig_));
        };

        _proto.getSpeed = function getSpeed(indexN_, scrollConfig_) {
          return _MoveBase.prototype.getSpeed.call(this, indexN_, new ScrollCell_.GetSpeedConfig(scrollConfig_));
        }
        /* ------------------------------- 自定义事件 ------------------------------- */
        ;

        _proto._eventEnd = function _eventEnd() {
          _MoveBase.prototype._eventEnd.call(this);

          this._totalDistN += this._targetDistN;
        };

        return ScrollCell;
      }(MoveBase), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "dire", [_dec], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return _ScrollCell.Direction.顺序;
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "contentNode", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "startIndexN", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 0;
        }
      })), _class2)) || _class));
      var ScrollCell_;

      (function (_ScrollCell_) {
        var Direction = _ScrollCell_.Direction = _ScrollCell.Direction;

        var ScrollConfig = /*#__PURE__*/function (_MoveBase_$MoveConfig) {
          _inheritsLoose(ScrollConfig, _MoveBase_$MoveConfig);

          function ScrollConfig(init_) {
            var _this2;

            _this2 = _MoveBase_$MoveConfig.call(this, init_) || this;
            /** 圈数 */

            _this2.turnN = 1;
            Object.assign(_assertThisInitialized(_this2), init_);
            return _this2;
          }

          return ScrollConfig;
        }(MoveBase_.MoveConfig);

        _ScrollCell_.ScrollConfig = ScrollConfig;

        var GetSpeedConfig = /*#__PURE__*/function (_ScrollConfig) {
          _inheritsLoose(GetSpeedConfig, _ScrollConfig);

          function GetSpeedConfig(init_) {
            var _this3;

            _this3 = _ScrollConfig.call(this, init_) || this;
            /** 进度 */

            _this3.ratioN = 0;
            Object.assign(_assertThisInitialized(_this3), init_);
            return _this3;
          }

          return GetSpeedConfig;
        }(ScrollConfig);

        _ScrollCell_.GetSpeedConfig = GetSpeedConfig;
      })(ScrollCell_ || (ScrollCell_ = exports('ScrollCell_', {})));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Singletons.ts", ['cc'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      exports({
        delInstance: delInstance,
        getInstance: getInstance
      });

      cclegacy._RF.push({}, "12b27Gp0DBN87wSphDAe/Fn", "Singletons", undefined);
      /** 存储所有单例的记录表 */


      var __INSTANCES__ = Object.create(null);
      /**
       * 获取单例
       * @param name 单例类名称
       */


      function get(name) {
        return __INSTANCES__[name];
      }
      /**
       * 设置单例
       * @param name 单例类名称
       * @param instance 单例实例
       * @returns
       */


      function set(name, instance) {
        __INSTANCES__[name] = instance;
        return instance;
      }
      /**
       * 销毁单例
       * @param name 单例类名称
       */


      function del(name) {
        var instance = __INSTANCES__[name];
        'deinit' in instance && instance.deinit();
        'dispose' in instance && instance.dispose();
        'destroy' in instance && instance.destroy();
        delete __INSTANCES__[name];
      }
      /**
       * 获取单例
       * @param cls 单例类
       * @returns
       */


      function getInstance(cls) {
        return get(this.name) || set(this.name, new cls());
      }
      /**
       * 销毁单例
       * @param cls 单例类
       * @returns
       */


      function delInstance(cls) {
        del(cls.name);
      }

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/SocketClient.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './ByteArray.ts'], function (exports) {
  var _inheritsLoose, _createClass, cclegacy, errorID, warnID, EventTarget, ByteArray;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      errorID = module.errorID;
      warnID = module.warnID;
      EventTarget = module.EventTarget;
    }, function (module) {
      ByteArray = module.ByteArray;
    }],
    execute: function () {
      cclegacy._RF.push({}, "a91ebo51yFMdoayAfCKAm5c", "SocketClient", undefined);
      /**
       * TYPE_STRING 以字符串格式发送和接收数据
       * TYPE_BINARY 以二进制格式发送和接收数据
       * @platform Web,Native
       * @language zh_CN
       */


      var WS_DATA_TYPE = exports('WS_DATA_TYPE', /*#__PURE__*/function (WS_DATA_TYPE) {
        WS_DATA_TYPE[WS_DATA_TYPE["TYPE_STRING"] = 1] = "TYPE_STRING";
        WS_DATA_TYPE[WS_DATA_TYPE["TYPE_BINARY"] = 2] = "TYPE_BINARY";
        return WS_DATA_TYPE;
      }({}));
      /**
       * WebSocket 事件
       */

      var SOCKET_EVENT = exports('SOCKET_EVENT', /*#__PURE__*/function (SOCKET_EVENT) {
        SOCKET_EVENT["CONNECTED"] = "connected";
        SOCKET_EVENT["CLOSE"] = "close";
        SOCKET_EVENT["ERROR"] = "io_error";
        SOCKET_EVENT["DATA"] = "data";
        return SOCKET_EVENT;
      }({}));

      var SocketClient = /*#__PURE__*/function (_EventTarget) {
        _inheritsLoose(SocketClient, _EventTarget);
        /**
         * context 在 SocketClient 没有任何用处，
         * 设计初衷是用于不同模块间交互，请自行灵活使用
         */

        /**
         * @private
         */

        /**
         * @private
         */

        /**
         * @private
         */

        /**
         * @private
         */

        /**
         * @private
         */

        /**
         * 创建一个 WebSocket 对象
         * @platform Web,Native
         * @language zh_CN
         */


        function SocketClient() {
          var _this;

          _this = _EventTarget.call(this) || this;
          _this.context = {};
          _this.socket = void 0;
          _this._writeMessage = '';
          _this._readMessage = '';
          _this._connected = false;
          _this._connecting = false;
          /**
           * @private
           */

          _this._isReadySend = false;
          /**
           * @private
           */

          _this._readByte = void 0;
          /**
           * @private
           */

          _this._writeByte = void 0;
          /**
           * @private
           */

          _this._bytesWrite = false;
          /**
           * @private
           */

          _this._type = WS_DATA_TYPE.TYPE_STRING;

          if (!window['WebSocket']) {
            errorID(3100);
          }

          _this._connected = false;
          _this._writeMessage = '';
          _this._readMessage = ''; // this.socket = new Socket();
          // this.socket.addCallBacks(this.onConnect, this.onClose, this.onSocketData, this.onError, this);

          return _this;
        }
        /**
         *
         * @param url ws://xxx.xxx.xxx.xxx:port
         */


        var _proto = SocketClient.prototype;

        _proto.initWebSocket = function initWebSocket(url, callback) {
          var _this2 = this;

          this.socket = new WebSocket(url);
          this.socket.binaryType = 'arraybuffer';

          this.socket.onopen = function () {
            _this2.onConnect();

            if (callback) callback();
          };

          this.socket.onclose = function (e) {
            _this2.onClose();
          };

          this.socket.onerror = function (e) {
            _this2.onError();
          };

          this.socket.onmessage = function (e) {
            if (e.data) {
              _this2.onSocketData(e.data);
            } else {
              //for mygame
              _this2.onSocketData.call(_this2, e);
            }
          };
        }
        /**
         * 将套接字连接到指定的主机和端口
         * @param host 要连接到的主机的名称或 IP 地址
         * @param port 要连接到的端口号
         * @platform Web,Native
         * @language zh_CN
         */

        /*
        public connect(host: string, port: number): void {
            if (!this._connecting && !this._connected) {
                this._connecting = true;
                this.socket.connect(host, port);
            }
        }
        */

        /**
         * 根据提供的url连接
         * @param url 全地址。如ws://echo.socketClient.org:80
         */
        ;

        _proto.connectByUrl = function connectByUrl(url, callback) {
          if (!this._connecting && !this._connected) {
            this._connecting = true; // this.socket.connectByUrl(url);
            // this._bindEvent();

            this.initWebSocket(url, callback);
          }
        }
        /**
         * 关闭套接字
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.close = function close() {
          if (this._connected) {
            this.socket.close();
          }
        }
        /**
         * @private
         *
         */
        ;

        _proto.onConnect = function onConnect() {
          this._connected = true;
          this._connecting = false;
          this.emit(SOCKET_EVENT.CONNECTED);
        }
        /**
         * @private
         *
         */
        ;

        _proto.onClose = function onClose() {
          this._connected = false;
          this.emit(SOCKET_EVENT.CLOSE);
        }
        /**
         * @private
         *
         */
        ;

        _proto.onError = function onError() {
          if (this._connecting) {
            this._connecting = false;
          }

          this.emit(SOCKET_EVENT.ERROR);
        }
        /**
         * @private
         *
         * @param message
         */
        ;

        _proto.onSocketData = function onSocketData(message) {
          if (typeof message == 'string') {
            this.emit(SOCKET_EVENT.DATA, message);
          } else {
            // this._readByte._writeUint8Array(new Uint8Array(message));
            this.emit(SOCKET_EVENT.DATA, new Int8Array(message));
          }
        }
        /**
         * 对套接字输出缓冲区中积累的所有数据进行刷新
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.flush = function flush() {
          if (!this._connected) {
            warnID(3101);
            return;
          }

          if (this._writeMessage) {
            this.socket.send(this._writeMessage);
            this._writeMessage = '';
          }

          if (this._bytesWrite) {
            this.socket.send(this._writeByte.buffer);
            this._bytesWrite = false;

            this._writeByte.clear();
          }

          this._isReadySend = false;
        };
        /**
         * 将字符串数据写入套接字
         * @param message 要写入套接字的字符串
         * @platform Web,Native
         * @language zh_CN
         */


        _proto.writeUTF = function writeUTF(message) {
          if (!this._connected) {
            warnID(3101);
            return;
          }

          if (this._type == WS_DATA_TYPE.TYPE_BINARY) {
            this._bytesWrite = true;

            this._writeByte.writeUTF(message);
          } else {
            this._writeMessage += message;
          }

          this.flush(); // return;
          // if (this._isReadySend) {
          //     return;
          // }
          // this._isReadySend = true;
          // callLater(this.flush, this);
        }
        /**
         * 从套接字读取一个 UTF-8 字符串
         * @returns {string}
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readUTF = function readUTF() {
          var message;

          if (this._type == WS_DATA_TYPE.TYPE_BINARY) {
            this._readByte.position = 0;
            message = this._readByte.readUTF();

            this._readByte.clear();
          } else {
            message = this._readMessage;
            this._readMessage = '';
          }

          return message;
        };
        /**
         * 从指定的字节数组写入一系列字节。写入操作从 offset 指定的位置开始。
         * 如果省略了 length 参数，则默认长度 0 将导致该方法从 offset 开始写入整个缓冲区。
         * 如果还省略了 offset 参数，则写入整个缓冲区。
         * @param bytes 要从中读取数据的 ByteArray 对象
         * @param offset ByteArray 对象中从零开始的偏移量，应由此开始执行数据写入
         * @param length 要写入的字节数。默认值 0 导致从 offset 参数指定的值开始写入整个缓冲区
         * @platform Web,Native
         * @language zh_CN
         */


        _proto.writeByteArray = function writeByteArray(bytes, offset, length) {
          if (offset === void 0) {
            offset = 0;
          }

          if (length === void 0) {
            length = 0;
          }

          if (!this._connected) {
            warnID(3101);
            return;
          }

          if (!this._writeByte) {
            warnID(3102);
            return;
          }

          this._bytesWrite = true;

          this._writeByte.writeBytes(bytes, offset, length);

          this.flush();
        };

        _proto.writeBytes = function writeBytes(bytes) {
          if (!this._writeByte) {
            warnID(3102);
            return;
          }

          this._bytesWrite = true;

          this._writeByte._writeUint8Array(bytes);

          this.flush();
        }
        /*
        public send(data: any) {
            if (!this._connected) {
                warnID(3101);
                return;
            }
            this.socket.send(data.buffer);
            this._isReadySend = false;
        }
        */

        /**
         * 从套接字读取 length 参数指定的数据字节数。从 offset 所表示的位置开始，将这些字节读入指定的字节数组
         * @param bytes 要将数据读入的 ByteArray 对象
         * @param offset 数据读取的偏移量应从该字节数组中开始
         * @param length 要读取的字节数。默认值 0 导致读取所有可用的数据
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.readBytes = function readBytes(bytes, offset, length) {
          if (offset === void 0) {
            offset = 0;
          }

          if (length === void 0) {
            length = 0;
          }

          if (!this._readByte) {
            warnID(3102);
            return;
          }

          this._readByte.position = 0;

          this._readByte.readBytes(bytes, offset, length);

          this._readByte.clear();
        }
        /**
         * 表示此 Socket 对象目前是否已连接
         * @platform Web,Native
         * @language zh_CN
         */
        ;

        _proto.registerRouterInCallbacks = function registerRouterInCallbacks(callbacks) {
          if (callbacks) {
            if (callbacks.mcmd <= 0) return;
            this.context[callbacks.mcmd] = callbacks.callbacks;
          }
        };

        _createClass(SocketClient, [{
          key: "connected",
          get: function get() {
            return this._connected;
          }
        }, {
          key: "type",
          get:
          /**
           * 发送和接收数据的格式，默认是字符串格式
           * @platform Web,Native
           * @language zh_CN
           */
          function get() {
            return this._type;
          },
          set: function set(value) {
            this._type = value; // this.socket.binaryType = value == WS_DATA_TYPE.TYPE_BINARY ? 'arraybuffer' : 'blob';

            if (value == WS_DATA_TYPE.TYPE_BINARY && !this._writeByte) {
              this._readByte = new ByteArray();
              this._writeByte = new ByteArray();
            }
          }
        }]);

        return SocketClient;
      }(EventTarget);

      SocketClient.URI = 'ws://';
      var socketClient = exports('socketClient', new SocketClient());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Sorter.ts", ['cc', './Logger.ts'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }, null],
    execute: function () {
      cclegacy._RF.push({}, "e12d2PpnlNId4WTW7TwOvGl", "Sorter", undefined);

      var sorter = /*#__PURE__*/function () {
        function sorter() {}

        var _proto = sorter.prototype;

        _proto.swap = function swap(datas, index1, index2) {
          var t = datas[index1];
          datas[index1] = datas[index2];
          datas[index2] = t;
        };

        _proto.defaultSortFunc = function defaultSortFunc(left, right) {
          return left < right;
        };

        _proto.normalSort = function normalSort(datas, sortFunc) {
          sortFunc = sortFunc || this.defaultSortFunc;
          datas.sort(function (left, right) {
            return sortFunc(left, right) ? -1 : 1;
          });
        };

        _proto.bubbleSort = function bubbleSort(datas, sortFunc) {
          sortFunc = sortFunc || this.defaultSortFunc;

          for (var i = datas.length; i > 1; i--) {
            for (var j = 0; j < i; j++) {
              // if (datas[j] > datas[j+1]) {
              if (sortFunc(datas[j + 1], datas[j])) {
                this.swap(datas, j, j + 1);
              }
            }
          }
        };

        _proto.selectionSort = function selectionSort(datas, sortFunc) {
          sortFunc = sortFunc || this.defaultSortFunc;

          for (var i = 0; i < datas.length - 1; i++) {
            var min = i;

            for (var j = i + 1; j < datas.length; j++) {
              // if (datas[min] > datas[j]) {
              if (sortFunc(datas[j], datas[min])) {
                min = j;
              }
            }

            if (min != i) {
              this.swap(datas, i, min);
            }
          }
        };

        _proto.insertSort = function insertSort(datas, sortFunc) {
          sortFunc = sortFunc || this.defaultSortFunc;

          for (var i = 1; i < datas.length; i++) {
            var tem = datas[i];
            var j = i; // while (datas[j - 1] > tem && j > 0) {

            while (sortFunc(tem, datas[j - 1]) && j > 0) {
              datas[j] = datas[j - 1];
              j--;
            }

            datas[j] = tem;
          }
        };

        return sorter;
      }();

      var Sorter = exports('Sorter', new sorter());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/SpineUtils.ts", ['cc', './Logger.ts'], function (exports) {
  var cclegacy, sp, logger;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
      sp = module.sp;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      cclegacy._RF.push({}, "70fdb0lCoBMlIJX2Zpt9BQc", "SpineUtils", undefined);
      /**
       * Spine 工具
       */


      var SpineUtils = exports('SpineUtils', /*#__PURE__*/function () {
        function SpineUtils() {}
        /** Spine 事件 */

        /**
         * 获取当前皮肤名称
         * @param skl 骨骼组件
         * @returns
         */


        SpineUtils.getCurrentSkin = function getCurrentSkin(skl) {
          var _skl$_skeleton;

          return (_skl$_skeleton = skl._skeleton) == null ? void 0 : _skl$_skeleton.skin.name;
        }
        /**
         * 整体换装
         * @param skl 骨骼组件
         */
        ;

        SpineUtils.changeSkin = function changeSkin(skl, skinName) {
          var data = skl.skeletonData.getRuntimeData();
          if (!data) return logger.e(skl.name + " \u8D44\u6E90\u672A\u6784\u5EFA");
          var skin = data.findSkin(skinName);
          if (!skin) return logger.e("\u672A\u627E\u5230\u76AE\u80A4 " + skinName);
          skl.setSkin(skin.name);
          skl.node.emit(SpineUtils.EventType.SkinChanged, {
            skin: skin.name
          });
        }
        /**
         * 局部换装
         * - 需要清楚知道部件所在的位置（要求美术给出）
         * @param skl 骨骼组件
         * @param skinName 皮肤名称
         * @param slotName 插槽名称
         * @param attachmentName 部件名称
         */
        ;

        SpineUtils.changeSlotAttachment = function changeSlotAttachment(skl, skinName, slotName, attachmentName) {
          var data = skl.skeletonData.getRuntimeData();
          if (!data) return logger.e(skl.name + " \u8D44\u6E90\u672A\u6784\u5EFA");
          var skin = data.findSkin(skinName);
          if (!skin) return logger.e("\u672A\u627E\u5230\u76AE\u80A4 " + skinName);
          var slotIndex = data.findSlotIndex(slotName);
          if (slotIndex === -1) return logger.e("\u672A\u627E\u5230\u76AE\u80A4 " + skinName + " \u7684\u63D2\u69FD " + slotName);
          console.log(skin, slotName, skin.attachments);
          var attachment = skin.getAttachment(slotIndex, attachmentName);
          if (!attachment) return logger.e("\u672A\u627E\u5230\u76AE\u80A4 " + skinName + " \u7684\u90E8\u4EF6 " + attachmentName);
          skl.findSlot(slotName).setAttachment(attachment);
          skl.node.emit(SpineUtils.EventType.AttachmentChanged, {
            skin: skinName,
            slot: slotName,
            attachment: attachmentName
          }); // 如果使用 PRIVATE_CACHE/SHARED_CACHE 缓存模式，则需要更新缓存

          var cacheMode = skl['_cacheMode'];

          if (cacheMode === sp.AnimationCacheMode.PRIVATE_CACHE || cacheMode === sp.AnimationCacheMode.SHARED_CACHE) {
            skl.invalidAnimationCache();
          }
        };

        return SpineUtils;
      }());
      SpineUtils.EventType = {
        SkinChanged: 'skin-changed',
        AttachmentChanged: 'attachment-changed'
      };

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/SpriteEx.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './Logger.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, _asyncToGenerator, _regeneratorRuntime, cclegacy, _decorator, Sprite, UITransform, Size, assetManager, SpriteFrame, resources, ccmodifier, BaseComponent, logger;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Sprite = module.Sprite;
      UITransform = module.UITransform;
      Size = module.Size;
      assetManager = module.assetManager;
      SpriteFrame = module.SpriteFrame;
      resources = module.resources;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5;

      cclegacy._RF.push({}, "9f7e9/Szo9HnJ+35CRRXymC", "SpriteEx", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property,
          menu = _decorator.menu,
          requireComponent = _decorator.requireComponent;
      var SpriteEx = exports('SpriteEx', (_dec = ccclass('SpriteEx'), _dec2 = ccmodifier('SpriteEx'), _dec3 = requireComponent(Sprite), _dec4 = menu('UIEx/SpriteEx'), _dec5 = property({
        tooltip: 'Sprite图片地址'
      }), _dec6 = property({
        tooltip: '是否远程链接'
      }), _dec7 = property({
        tooltip: '是否在载入的时候影藏'
      }), _dec8 = property({
        tooltip: '是否在Destroy时释放资源'
      }), _dec9 = property({
        tooltip: '是否在载入新资源时释放旧的资源'
      }), _dec(_class = _dec2(_class = _dec3(_class = _dec4(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(SpriteEx, _BaseComponent);

        function SpriteEx() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "texturePath", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "isRemote", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "hideWhenLoad", _descriptor3, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "releaseWhenDestroy", _descriptor4, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "releaseWhenLoadNew", _descriptor5, _assertThisInitialized(_this));

          _this.size = void 0;
          _this.sprite = void 0;
          _this.isLoading = false;
          _this.willLoadPath = '';
          return _this;
        }

        var _proto = SpriteEx.prototype;

        _proto.onLoad = function onLoad() {
          var _this$texturePath;

          if (!this.sprite) this.sprite = this.getComponent(Sprite);
          if (!this.sprite) logger.error('SpriteEx必须挂在Sprite组件上'); // this.sprite.material.passes[0].blendState.targets[0].

          var uitrans = this.sprite.node.getComponent(UITransform);
          this.size = new Size(uitrans.width, uitrans.height);
          if (((_this$texturePath = this.texturePath) == null ? void 0 : _this$texturePath.length) == 0) return;
          this.setTexture(this.texturePath);
        };

        _proto.onDestroy = function onDestroy() {
          if (this.isRemote) {
            if (this.sprite && this.sprite.isValid) {
              if (this.sprite.spriteFrame && this.sprite.spriteFrame.isValid) {
                this.sprite.spriteFrame.destroy();
                this.sprite.spriteFrame = null;
              }
            }
          }

          if (!this.releaseWhenDestroy) return; // resources.release(this.texturePath);
        };

        _proto._load = function _load(_path) {
          var _this2 = this;

          return new Promise(function (resolve, reject) {
            var path = _path;

            if (_this2.isRemote) {
              assetManager.loadRemote(path, function (err, asset) {
                logger.log('加载远程图片资源', path, err, asset);

                if (err) {
                  reject(err);
                  return;
                }

                if (path === asset.nativeUrl) {
                  resolve([path, SpriteFrame.createWithImage(asset)]);
                }
              });
            } else {
              // TODO 可以改进下，支持从 bundle 加载
              var sprFrame = resources.get(path, SpriteFrame);

              if (sprFrame) {
                resolve([path, sprFrame]);
                return;
              }

              resources.load(path, function (err, asset) {
                if (err) {
                  reject(err);
                  return;
                }

                resolve([path, asset]);
              });
            }
          });
        }
        /**
         * 不要在父组件的 `start`, `onData`, `onLoad` 里调用setTexture，因为SpriteEx本身可能还未执行完onLoad，还未初始化
         * 可以在父组件里使用setTimeout(SpriteEx.setTexture, 0) 在下一帧 执行setTexture
         * @param texturePath
         * @returns
         */
        ;

        _proto.setTexture = /*#__PURE__*/function () {
          var _setTexture = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(texturePath) {
            var _yield$this$_load, path, spriteFrame;

            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  if (!(this.texturePath === texturePath)) {
                    _context.next = 2;
                    break;
                  }

                  return _context.abrupt("return");

                case 2:
                  if (this.releaseWhenLoadNew && !this.isRemote && this.texturePath !== '' && this.texturePath !== texturePath) resources.release(this.texturePath);

                  if (!(texturePath === '' || texturePath == null || texturePath == undefined)) {
                    _context.next = 7;
                    break;
                  }

                  this.sprite.spriteFrame = null;
                  this.texturePath = '';
                  return _context.abrupt("return");

                case 7:
                  if (!this.isLoading) {
                    _context.next = 10;
                    break;
                  }

                  this.willLoadPath = texturePath;
                  return _context.abrupt("return");

                case 10:
                  this.isLoading = true;
                  this.willLoadPath = texturePath;
                  _context.next = 14;
                  return this._load(this.willLoadPath);

                case 14:
                  _yield$this$_load = _context.sent;
                  path = _yield$this$_load[0];
                  spriteFrame = _yield$this$_load[1];
                  this.isLoading = false;
                  this.texturePath = path;

                  if (this.willLoadPath === path) {
                    if (this.isValid) {
                      if (this.sprite) this.sprite.spriteFrame = spriteFrame; // 只显示最后加载的一个

                      /*
                      const sprWidth = spriteFrame.width;
                      const sprHeight = spriteFrame.height;
                      let scale = 1;
                      logger.log(this, this.size);
                      
                      const wh = this.size.width <= this.size.height ? this.size.width : this.size.height;
                      if (sprWidth > sprHeight) {
                          // 宽度顶到边
                          scale = this.size.width / sprWidth;
                      } else {
                          // 高度顶到边
                          scale = this.size.height / sprHeight;
                      }
                      
                      this.spriteFrame = spriteFrame;
                      const uitrans = this.sprite.node.getComponent(UITransform);
                      uitrans.setContentSize(sprWidth, sprHeight);
                      this.sprite.node.setScale(v3(scale, scale, 1));
                      */
                    }
                  } else {
                    this.setTexture(this.willLoadPath); // 继续下一个
                  }

                case 20:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));

          function setTexture(_x) {
            return _setTexture.apply(this, arguments);
          }

          return setTexture;
        }();

        _proto.setBase64Code = /*#__PURE__*/function () {
          var _setBase64Code = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(base64) {
            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                case "end":
                  return _context2.stop();
              }
            }, _callee2);
          }));

          function setBase64Code(_x2) {
            return _setBase64Code.apply(this, arguments);
          }

          return setBase64Code;
        }();

        _createClass(SpriteEx, [{
          key: "spriteFrame",
          get: function get() {
            return this.sprite.spriteFrame;
          },
          set: function set(sprframe) {
            this.sprite.spriteFrame = sprframe;
          }
        }]);

        return SpriteEx;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "texturePath", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return '';
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "isRemote", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return false;
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "hideWhenLoad", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return false;
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "releaseWhenDestroy", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return false;
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "releaseWhenLoadNew", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return false;
        }
      })), _class2)) || _class) || _class) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Start.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './ModuleManager.ts', './PreloadAssets.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, _asyncToGenerator, _regeneratorRuntime, cclegacy, _decorator, ProgressBar, Node, Label, macro, ccmodifier, BaseComponent, moduleMgr, PreloadAssetsInfo;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      ProgressBar = module.ProgressBar;
      Node = module.Node;
      Label = module.Label;
      macro = module.macro;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      moduleMgr = module.moduleMgr;
    }, function (module) {
      PreloadAssetsInfo = module.PreloadAssetsInfo;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor, _descriptor2, _descriptor3;

      cclegacy._RF.push({}, "f2110UxikJC04mSX6TAAjTQ", "Start", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      /**
       * 初始场景
       */

      var Start = exports('Start', (_dec = ccclass('Start'), _dec2 = ccmodifier('Start'), _dec3 = property(ProgressBar), _dec4 = property(Node), _dec5 = property(Label), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(Start, _BaseComponent);

        function Start() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "progressBar", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "progressHeaderNode", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "lblProgress", _descriptor3, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = Start.prototype;

        _proto.load = function load() {
          this.progress = 0;
        };

        _proto.onCompleted = function onCompleted() {
          return Promise.resolve();
        };

        _proto.start = /*#__PURE__*/function () {
          var _start = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee() {
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  macro.ENABLE_WEBGL_ANTIALIAS = true;
                  moduleMgr.initPreloadAssets(PreloadAssetsInfo);
                  moduleMgr.mainModuleName = 'MainModule';
                  moduleMgr.launchModule('MainModule', null, this);

                case 4:
                case "end":
                  return _context.stop();
              }
            }, _callee, this);
          }));

          function start() {
            return _start.apply(this, arguments);
          }

          return start;
        }();

        _createClass(Start, [{
          key: "progress",
          get: function get() {
            var _this$progressBar;

            return (_this$progressBar = this.progressBar) == null ? void 0 : _this$progressBar.progress;
          },
          set: function set(value) {
            if (!this.progressBar) return;
            this.progressBar.progress = value;
            this.progressHeaderNode.setPosition(this.progressBar.node.position.x + 1004 * value, this.progressHeaderNode.position.y);
            this.lblProgress.string = "\u6B63\u5728\u52AA\u529B\u52A0\u8F7D\u8D44\u6E90 " + (value * 100 >> 0) + "%";
          }
        }]);

        return Start;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "progressBar", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "progressHeaderNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "lblProgress", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StorageManager.ts", ['cc'], function (exports) {
  var cclegacy, sys;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
      sys = module.sys;
    }],
    execute: function () {
      cclegacy._RF.push({}, "877d3nNz5NJ25fqniPSgmg+", "StorageManager", undefined);
      /**
       * 本地持久化存储管理器
       */


      var StorageManager = exports('StorageManager', /*#__PURE__*/function () {
        function StorageManager() {
          /** 项目名称 */
          this._project = void 0;
          /** 基础数据 */

          this._data = void 0;
          /** 模板数据 */

          this._copy = void 0;
        }

        StorageManager.getInstance = function getInstance() {
          // @ts-ignore
          this._instance = this._instance || new StorageManager();
          return this._instance;
        };

        var _proto = StorageManager.prototype;
        /**
         * 初始化
         * @param project 项目名称
         * @param data 模板数据
         */

        _proto.init = function init(project, data) {
          this._project = project;
          this._copy = Object.assign({}, data);
          this._data = data;
          this.read();
        }
        /** 读取项目 */
        ;

        _proto.read = function read() {
          try {
            var data = sys.localStorage.getItem(this._project);

            if (data) {
              var _data = JSON.parse(data); // 抛弃过时存储项


              for (var _k in _data) {
                if (this._data[_k] !== undefined) {
                  this._data[_k] = _data[_k];
                }
              } // 读取完成后进行一次保存


              this.save();
            } else {
              throw new Error('项目未初始化');
            }
          } catch (e) {
            this.reset();
          }
        }
        /**
         * 读取存储项
         * @param key 存储项键名
         * @returns
         */
        ;

        _proto.get = function get(key) {
          return this._data[key];
        }
        /**
         * 获取存储项值
         * @param key 存储项键名
         * @param value 存储项值
         */
        ;

        _proto.set = function set(key, value) {
          this._data[key] = value;
          this.save();
        }
        /**
         * 重置项目
         */
        ;

        _proto.reset = function reset() {
          this._data = Object.assign({}, this._copy);
          this.save();
        }
        /** 保存项目 */
        ;

        _proto.save = function save() {
          sys.localStorage.setItem(this._project, JSON.stringify(this._data));
        }
        /** 遍历 */
        ;

        _proto.each = function each(fn) {
          for (var _k2 in this._data) {
            if (!fn(_k2, this._data[_k2])) break;
          }
        };

        return StorageManager;
      }());
      StorageManager._instance = void 0;

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StringUtils.ts", ['cc'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      cclegacy._RF.push({}, "fd5f4lHFMVJ+r6/3t/WzKwL", "StringUtils", undefined);

      var StringUtils = exports('StringUtils', /*#__PURE__*/function () {
        function StringUtils() {}
        /**
         * 计算字符串所占的内存字节数，默认使用UTF-8的编码方式计算，也可制定为UTF-16
         * UTF-8 是一种可变长度的 Unicode 编码格式，使用一至四个字节为每个字符编码
         * 
         * 000000 - 00007F(128个代码)  0zzzzzzz(00-7F)        一个字节
         * 000080 - 0007FF(1920个代码)  110yyyyy(C0-DF) 10zzzzzz(80-BF)    两个字节
         * 000800 - 00D7FF 
         * 00E000 - 00FFFF(61440个代码) 1110xxxx(E0-EF) 10yyyyyy 10zzzzzz   三个字节
         * 010000 - 10FFFF(1048576个代码) 11110www(F0-F7) 10xxxxxx 10yyyyyy 10zzzzzz 四个字节
         * 
         * 注: Unicode在范围 D800-DFFF 中不存在任何字符
         * {@link http://zh.wikipedia.org/wiki/UTF-8}
         * 
         * UTF-16 大部分使用两个字节编码，编码超出 65535 的使用四个字节
         * 000000 - 00FFFF 两个字节
         * 010000 - 10FFFF 四个字节
         * 
         * {@link http://zh.wikipedia.org/wiki/UTF-16}
         * @param {String} str 
         * @param {String} charset utf-8, utf-16
         * @return {Number}
         */


        StringUtils.sizeof = function sizeof(str, charset) {
          if (charset === void 0) {
            charset = 'utf-8';
          }

          var total = 0,
              charCode = 0;

          if (charset === 'utf-16') {
            for (var i = 0, len = str.length; i < len; i++) {
              charCode = str.charCodeAt(i);

              if (charCode <= 0xffff) {
                total += 2;
              } else {
                total += 4;
              }
            }
          } else {
            for (var _i = 0, _len = str.length; _i < _len; _i++) {
              charCode = str.charCodeAt(_i);

              if (charCode <= 0x007f) {
                total += 1;
              } else if (charCode <= 0x07ff) {
                total += 2;
              } else if (charCode <= 0xffff) {
                total += 3;
              } else {
                total += 4;
              }
            }
          }

          return total;
        }
        /** 
         * 超过长度（...）处理
         * @str 需要进行处理的字符串，可含汉字
         * @len 需要显示多少个汉字，两个英文字母相当于一个汉字
        */
        ;

        StringUtils.ellipsis = function ellipsis(str, len) {
          var reg = /[\u4e00-\u9fa5]/g;
          var slice = str.substring(0, len);
          var cCharNum = ~~(slice.match(reg) && slice.match(reg).length);

          if (cCharNum == 0) {
            var _realen = slice.length * 2 - cCharNum - 1;

            return str.substring(0, _realen) + (_realen < str.length ? "..." : "");
          }

          if (cCharNum < len) return str;
          var realen = slice.length * 2 - cCharNum - 1;
          return str.substring(0, realen) + (realen < str.length ? "..." : "");
        }
        /**
         * 截断字符串，未超出限制则不作处理
         * @str 需要进行处理的字符串，可含汉字
         * @param start 起始下标
         * @param threshold 最大字符数（中文字符算 2 个字符）
         * @param suffix 截断后缀
         */
        ;

        StringUtils.clamp = function clamp(str, start, threshold, suffix) {
          if (suffix === void 0) {
            suffix = '...';
          }

          if (str.replace(/[^\x00-\xff]/g, 'xx').length <= threshold) return str;
          var charCount = 0;
          var result = '';

          for (var i = start; i < str.length; i++) {
            charCount += /[^\x00-\xff]/.test(str[i]) ? 2 : 1;
            if (charCount > threshold) return result += suffix;
            result += str[i];
          }

          return result;
        }; // 截取前len个长度的字符


        StringUtils.truncateChineseString = function truncateChineseString(str, len) {
          var length = str.replace(/[^\u0000-\u00ff]/g, 'xx').length; // 把中文字符计算为两个字符

          if (length <= 100) return str;
          var count = 0;
          var result = '';

          for (var i = 0; i < str.length; i++) {
            if (str.charCodeAt(i) > 255) {
              count += 2; // 中文字符占两个字节
            } else {
              count += 1; // 英文字符占一个字节
            }

            if (count > len) break;
            result += str[i];
          }

          return result;
        };

        return StringUtils;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Switch.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, cclegacy, _decorator, Node, CCBoolean, Button, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Node = module.Node;
      CCBoolean = module.CCBoolean;
      Button = module.Button;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2;

      cclegacy._RF.push({}, "99f41asxB9J/6emAn/mL/PD", "Switch", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property,
          menu = _decorator.menu;
      var Switch = exports('Switch', (_dec = ccclass('Switch'), _dec2 = ccmodifier('Switch'), _dec3 = menu('UIEx/Switch'), _dec4 = property(Node), _dec5 = property(Node), _dec6 = property({
        type: CCBoolean
      }), _dec(_class = _dec2(_class = _dec3(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(Switch, _BaseComponent);

        function Switch() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "onSwitchNode", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "offSwitchNode", _descriptor2, _assertThisInitialized(_this));

          _this._switch = false;
          return _this;
        }

        var _proto = Switch.prototype;

        _proto.start = function start() {
          var _this2 = this;

          var btn = this.getComponent(Button) || this.addComponent(Button);
          btn == null ? void 0 : btn.node.on(Button.EventType.CLICK, function () {
            _this2["switch"] = !_this2["switch"];
          });
        };

        _createClass(Switch, [{
          key: "switch",
          get: function get() {
            return this._switch;
          },
          set: function set(value) {
            var needEmit = this._switch != value;
            this._switch = value;
            this.onSwitchNode.active = value;
            this.offSwitchNode.active = !value;
            needEmit && this.node.emit('SwitchChanged');
          }
        }]);

        return Switch;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "onSwitchNode", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "offSwitchNode", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _applyDecoratedDescriptor(_class2.prototype, "switch", [_dec6], Object.getOwnPropertyDescriptor(_class2.prototype, "switch"), _class2.prototype)), _class2)) || _class) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ToggleContainerEx.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, cclegacy, _decorator, CCBoolean, EventHandler, NodeEventType, Component, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      CCBoolean = module.CCBoolean;
      EventHandler = module.EventHandler;
      NodeEventType = module.NodeEventType;
      Component = module.Component;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _class, _class2, _descriptor;

      cclegacy._RF.push({}, "af093VESMRNdqeFZsLC863+", "ToggleContainerEx", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property,
          executeInEditMode = _decorator.executeInEditMode,
          menu = _decorator.menu;
      /**
       * @en
       * ToggleContainer is not a visible UI component but a way to modify the behavior of a set of Toggles. <br/>
       * Toggles that belong to the same group could only have one of them to be switched on at a time.<br/>
       * Note: All the first layer child node containing the toggle component will auto be added to the container.
       *
       * @zh
       * ToggleGroup 不是一个可见的 UI 组件，它可以用来修改一组 Toggle  组件的行为。当一组 Toggle 属于同一个 ToggleGroup 的时候，<br/>
       * 任何时候只能有一个 Toggle 处于选中状态。
       */

      var ToggleContainerEx = exports('ToggleContainerEx', (_dec = ccclass('ToggleContainerEx'), _dec2 = ccmodifier('ToggleContainerEx'), _dec3 = menu('UIEx/ToggleContainerEx'), _dec4 = property({
        serializable: true,
        type: CCBoolean
      }), _dec5 = property({
        serializable: true,
        type: [EventHandler]
      }), _dec(_class = _dec2(_class = _dec3(_class = executeInEditMode(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(ToggleContainerEx, _BaseComponent);

        function ToggleContainerEx() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          _this._allowSwitchOff = false;
          /**
           * @en
           * If Toggle is clicked, it will trigger event's handler.
           *
           * @zh
           * Toggle 按钮的点击事件列表。
           */

          _initializerDefineProperty(_this, "checkEvents", _descriptor, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = ToggleContainerEx.prototype;

        _proto.onEnable = function onEnable() {
          this.ensureValidState();
          this.node.on(NodeEventType.CHILD_ADDED, this.ensureValidState, this);
          this.node.on(NodeEventType.CHILD_REMOVED, this.ensureValidState, this);
        };

        _proto.onDisable = function onDisable() {
          this.node.off(NodeEventType.CHILD_ADDED, this.ensureValidState, this);
          this.node.off(NodeEventType.CHILD_REMOVED, this.ensureValidState, this);
        };

        _proto.pickOneActiveToggle = function pickOneActiveToggle() {
          return this.toggleItems.find(function (value) {
            return value.isChecked;
          });
        };

        _proto.activeToggles = function activeToggles() {
          return this.toggleItems.filter(function (x) {
            return x.isChecked;
          });
        };

        _proto.anyTogglesChecked = function anyTogglesChecked() {
          return !!this.toggleItems.find(function (x) {
            return x.isChecked;
          });
        }
        /**
         * @en
         * Refresh the state of the managed toggles.
         *
         * @zh
         * 刷新管理的 toggle 状态。
         *
         * @param toggle @en The toggle to be updated @zh 需要被更新的切换键
         * @param emitEvent @en Whether events are needed to be emitted @zh 是否需要触发事件
         */
        ;

        _proto.notifyToggleCheck = function notifyToggleCheck(toggle, emitEvent) {
          if (emitEvent === void 0) {
            emitEvent = true;
          }

          if (!this.enabledInHierarchy) {
            return;
          }

          for (var i = 0; i < this.toggleItems.length; i++) {
            var item = this.toggleItems[i];

            if (item === toggle) {
              continue;
            }

            if (emitEvent) {
              item.isChecked = false;
            } else {
              item.setIsCheckedWithoutNotify(false);
            }
          }

          if (this.checkEvents) {
            Component.EventHandler.emitEvents(this.checkEvents, toggle);
          }
        };

        _proto.ensureValidState = function ensureValidState() {
          var toggles = this.toggleItems;

          if (!this._allowSwitchOff && !this.anyTogglesChecked() && toggles.length !== 0) {
            var toggle = toggles[0];
            toggle.isChecked = true;
            this.notifyToggleCheck(toggle);
          }

          var activeToggles = this.activeToggles();

          if (activeToggles.length > 1) {
            var firstToggle = activeToggles[0];

            for (var i = 0; i < activeToggles.length; ++i) {
              var _toggle = activeToggles[i];

              if (_toggle === firstToggle) {
                continue;
              }

              _toggle.isChecked = false;
            }
          }
        };

        _createClass(ToggleContainerEx, [{
          key: "allowSwitchOff",
          get:
          /**
           * @en
           * If this setting is true, a toggle could be switched off and on when pressed.
           * If it is false, it will make sure there is always only one toggle could be switched on
           * and the already switched on toggle can't be switched off.
           *
           * @zh
           * 如果这个设置为 true，那么 toggle 按钮在被点击的时候可以反复地被选中和未选中。
           */
          function get() {
            return this._allowSwitchOff;
          },
          set: function set(value) {
            this._allowSwitchOff = value;
          }
        }, {
          key: "toggleItems",
          get:
          /**
           * @en
           * Read only property, return the toggle items array reference managed by ToggleContainer.
           *
           * @zh
           * 只读属性，返回 toggleContainer 管理的 toggle 数组引用。
           */
          function get() {
            return this.node.children.map(function (item) {
              var toggle = item.getComponent('ToggleEx');

              if (toggle && toggle.enabled) {
                return toggle;
              }

              return null;
            }).filter(Boolean);
          }
        }]);

        return ToggleContainerEx;
      }(BaseComponent), (_applyDecoratedDescriptor(_class2.prototype, "allowSwitchOff", [_dec4], Object.getOwnPropertyDescriptor(_class2.prototype, "allowSwitchOff"), _class2.prototype), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "checkEvents", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      })), _class2)) || _class) || _class) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ToggleEx.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './ToggleContainerEx.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createForOfIteratorHelperLoose, _createClass, cclegacy, _decorator, Sprite, EventHandler, SpriteFrame, Node, Toggle, Button, ToggleContainerEx;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createForOfIteratorHelperLoose = module.createForOfIteratorHelperLoose;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Sprite = module.Sprite;
      EventHandler = module.EventHandler;
      SpriteFrame = module.SpriteFrame;
      Node = module.Node;
      Toggle = module.Toggle;
      Button = module.Button;
    }, function (module) {
      ToggleContainerEx = module.ToggleContainerEx;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _dec11, _dec12, _dec13, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _class3;

      cclegacy._RF.push({}, "a7761WLTj1NcLIF6qKkmFSw", "ToggleEx", undefined);

      var ccclass = _decorator.ccclass,
          help = _decorator.help,
          requireComponent = _decorator.requireComponent,
          executionOrder = _decorator.executionOrder,
          menu = _decorator.menu,
          type = _decorator.type,
          property = _decorator.property;
      /**
       * @zh
       * ToggleEx 是一个 增强版的 Toggle，与 ToggleGroupEx 一起使用；
       * 由 Toggle + checkMask 优化为单 ToggleEx 状态切换
       * 添加 checkActiveNodes:Node[] 节点列表，用于控制状态选中/未选中时，checkActiveNodes 节点的激活与否
       */

      var ToggleEx = exports('ToggleEx', (_dec = ccclass('ToggleEx'), _dec2 = help('i18n:cc.Toggle'), _dec3 = executionOrder(110), _dec4 = menu('UIEx/ToggleEx'), _dec5 = requireComponent(Sprite), _dec6 = property({
        serializable: true
      }), _dec7 = property({
        displayOrder: 1
      }), _dec8 = type([EventHandler]), _dec9 = property({
        type: [EventHandler],
        serializable: true,
        tooltip: '状态切换事件，建议在 ToggleContainerEx.checkEvents 里处理'
      }), _dec10 = property({
        type: SpriteFrame,
        serializable: true,
        displayOrder: 0,
        tooltip: '未激活状态下的SpriteFrame'
      }), _dec11 = property({
        type: SpriteFrame,
        serializable: true,
        displayOrder: 0,
        tooltip: '激活状态下的SpriteFrame'
      }), _dec12 = property({
        type: [Node],
        serializable: true,
        displayOrder: 0,
        tooltip: 'toggle 激活后出现的节点列表'
      }), _dec13 = property({
        serializable: true,
        displayOrder: 0,
        tooltip: '自定义参数，便于checkEvents回调里做处理'
      }), _dec(_class = _dec2(_class = _dec3(_class = _dec4(_class = _dec5(_class = (_class2 = (_class3 = /*#__PURE__*/function (_Button) {
        _inheritsLoose(ToggleEx, _Button);

        function ToggleEx() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Button.call.apply(_Button, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "_isChecked", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "checkEvents", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "uncheckSpriteFrame", _descriptor3, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "checkedSpriteFrame", _descriptor4, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "checkActiveNodes", _descriptor5, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "customData", _descriptor6, _assertThisInitialized(_this));

          _this._sprite = null;
          return _this;
        }

        var _proto = ToggleEx.prototype;

        _proto._internalToggle = function _internalToggle() {
          this.isChecked = !this.isChecked;
        };

        _proto._set = function _set(value, emitEvent) {
          if (emitEvent === void 0) {
            emitEvent = true;
          }

          if (this._isChecked == value) return;
          this._isChecked = value;
          var group = this._toggleContainer;

          if (group && group.enabled && this.enabled) {
            if (value || !group.anyTogglesChecked() && !group.allowSwitchOff) {
              this._isChecked = true;
              group.notifyToggleCheck(this, emitEvent);
            }
          }

          this.updateStatus();

          if (emitEvent) {
            this._emitToggleEvents();
          }
        } // 更新状态
        ;

        _proto.updateStatus = function updateStatus() {
          if (this.sprite) {
            this.sprite.spriteFrame = this._isChecked ? this.checkedSpriteFrame : this.uncheckSpriteFrame;
          }

          if (!this.checkActiveNodes) return;

          for (var _iterator = _createForOfIteratorHelperLoose(this.checkActiveNodes), _step; !(_step = _iterator()).done;) {
            var node = _step.value;
            node.active = this._isChecked;
          }
        }
        /**
         * @en
         * Set isChecked without invoking checkEvents.
         *
         * @zh
         * 设置 isChecked 而不调用 checkEvents 回调。
         *
         * @param value @en Whether this toggle is pressed @zh 是否被按下
         */
        ;

        _proto.setIsCheckedWithoutNotify = function setIsCheckedWithoutNotify(value) {
          this._set(value, false);
        };

        _proto.onEnable = function onEnable() {
          _Button.prototype.onEnable.call(this);

          this.updateStatus();
          {
            this.node.on(ToggleEx.EventType.CLICK, this._internalToggle, this);
          }
        };

        _proto.onDisable = function onDisable() {
          _Button.prototype.onDisable.call(this);

          {
            this.node.off(ToggleEx.EventType.CLICK, this._internalToggle, this);
          }
        };

        _proto._emitToggleEvents = function _emitToggleEvents() {
          this.node.emit(ToggleEx.EventType.TOGGLE, this);

          if (this.checkEvents) {
            EventHandler.emitEvents(this.checkEvents, this);
          }
        };

        _createClass(ToggleEx, [{
          key: "isChecked",
          get:
          /**
           * @en
           * When this value is true, the check mark component will be enabled,
           * otherwise the check mark component will be disabled.
           *
           * @zh
           * 如果这个设置为 true，则 check mark 组件会处于 enabled 状态，否则处于 disabled 状态。
           */
          function get() {
            return this._isChecked;
          },
          set: function set(value) {
            this._set(value);
          }
        }, {
          key: "sprite",
          get: function get() {
            if (!this._sprite) {
              this._sprite = this.getComponent(Sprite);
            }

            return this._sprite;
          }
        }, {
          key: "_resizeToTarget",
          set: function set(value) {
            if (value) {
              this._resizeNodeToTargetNode();
            }
          }
        }, {
          key: "_toggleContainer",
          get: function get() {
            var parent = this.node.parent;

            if (Node.isNode(parent)) {
              return parent.getComponent(ToggleContainerEx);
            }

            return null;
          }
        }]);

        return ToggleEx;
      }(Button), _class3.EventType = Toggle.EventType, _class3), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "_isChecked", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return true;
        }
      }), _applyDecoratedDescriptor(_class2.prototype, "isChecked", [_dec7], Object.getOwnPropertyDescriptor(_class2.prototype, "isChecked"), _class2.prototype), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "checkEvents", [_dec8, _dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "uncheckSpriteFrame", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "checkedSpriteFrame", [_dec11], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "checkActiveNodes", [_dec12], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return [];
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "customData", [_dec13], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return '';
        }
      })), _class2)) || _class) || _class) || _class) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/TopTouchProcessor.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './Logger.ts', './UtilTools.ts'], function (exports) {
  var _inheritsLoose, _createForOfIteratorHelperLoose, cclegacy, _decorator, Node, Component, ccmodifier, BaseComponent, logger, UtilTools;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _createForOfIteratorHelperLoose = module.createForOfIteratorHelperLoose;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Node = module.Node;
      Component = module.Component;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      logger = module.logger;
    }, function (module) {
      UtilTools = module.UtilTools;
    }],
    execute: function () {
      var _dec, _dec2, _class, _class2;

      cclegacy._RF.push({}, "8d0cdeYX/RBW6DqPqvgZcUV", "TopTouchProcessor", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property; // 非入侵性

      var InteractionIdleTimeCounter = /*#__PURE__*/function (_Component) {
        _inheritsLoose(InteractionIdleTimeCounter, _Component);

        function InteractionIdleTimeCounter() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Component.call.apply(_Component, [this].concat(args)) || this; // 是否闲置

          _this.inIdle = false; // 交互闲置时间，过了这个时间，触发 onInteractionIdle

          _this.leftTime = 0; // 闲置时长

          _this.idleTriggerTime = 0;
          return _this;
        }

        return InteractionIdleTimeCounter;
      }(Component);

      var TopTouchProcessor = exports('TopTouchProcessor', (_dec = ccclass('TopTouchProcessor'), _dec2 = ccmodifier('TopTouchProcessor'), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(TopTouchProcessor, _BaseComponent);

        function TopTouchProcessor() {
          var _this2;

          for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            args[_key2] = arguments[_key2];
          }

          _this2 = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          _this2.isStart = false;
          _this2.idlers = [];
          _this2.clickers = [];
          return _this2;
        }

        var _proto = TopTouchProcessor.prototype;

        _proto.onLoad = function onLoad() {
          TopTouchProcessor.instance = this;
        };

        _proto.start = function start() {
          this.node.on(Node.EventType.TOUCH_START, this._onTouchBegan, this, false);
          this.node.on(Node.EventType.TOUCH_MOVE, this._onTouchMoved, this, false);
          this.node.on(Node.EventType.TOUCH_END, this._onTouchEnded, this, false);
          this.node.on(Node.EventType.TOUCH_CANCEL, this._onTouchCancelled, this, false);
        };

        _proto.addClicker = function addClicker(clicker) {
          if (!(clicker instanceof Component)) {
            logger.warn('clicker 必须是一个Component');
            return;
          }

          if (this.clickers.indexOf(clicker) != -1) return;
          this.clickers.push(clicker);
        };

        _proto.removeClicker = function removeClicker(clicker) {
          var index = this.clickers.indexOf(clicker);
          if (index == -1) return;
          this.clickers.splice(index, 1);
        };

        _proto.addIdler = function addIdler(idler, idleTriggerTime) {
          var _idler = idler;

          if (!(_idler instanceof Component)) {
            logger.warn('idler 必须是一个Component');
            return;
          }

          var timeCounter = UtilTools.setupComponent(_idler, InteractionIdleTimeCounter);
          idler.timeCounter = timeCounter;
          timeCounter.leftTime = idleTriggerTime;
          timeCounter.idleTriggerTime = idleTriggerTime;
          timeCounter.inIdle = false;
          timeCounter.node.on(Node.EventType.NODE_DESTROYED, function () {
            TopTouchProcessor.instance.removeIdler(idler);
          });
          timeCounter.node.on('force_cancle_idle', function () {
            if (timeCounter.inIdle) idler.onInteractionIdleCancel();
            timeCounter.inIdle = false;
            timeCounter.leftTime = timeCounter.idleTriggerTime;
          });
          this.idlers.push(idler);
        };

        _proto.removeIdler = function removeIdler(idler) {
          var idx = this.idlers.indexOf(idler);

          if (idx != -1) {
            this.idlers.splice(idx, 1);
          }
        };

        _proto.update = function update(dt) {
          if (!this.isStart) return;

          for (var _iterator = _createForOfIteratorHelperLoose(this.idlers), _step; !(_step = _iterator()).done;) {
            var idler = _step.value;
            var timeCounter = idler.timeCounter;
            if (!idler.canIdleTimeCountdown() || !timeCounter || timeCounter.inIdle) continue;
            timeCounter.leftTime -= dt;

            if (timeCounter.leftTime <= 0) {
              timeCounter.inIdle = true;
              idler.onInteractionIdle();
            }
          }
        };

        _proto._onTouchBegan = function _onTouchBegan(event, captureListeners) {
          event.preventSwallow = true;

          for (var _iterator2 = _createForOfIteratorHelperLoose(this.clickers), _step2; !(_step2 = _iterator2()).done;) {
            var clicker = _step2.value;
            clicker == null ? void 0 : clicker.onClick();
          }
        };

        _proto._onTouchMoved = function _onTouchMoved(event, captureListeners) {
          event.preventSwallow = true;
        };

        _proto._onTouchEnded = function _onTouchEnded(event, captureListeners) {
          event.preventSwallow = true;
          this.scheduleOnce(this.cancelAllIdler.bind(this));
        };

        _proto._onTouchCancelled = function _onTouchCancelled(event, captureListeners) {
          event.preventSwallow = true;
        };

        _proto.cancelAllIdler = function cancelAllIdler() {
          for (var _iterator3 = _createForOfIteratorHelperLoose(this.idlers), _step3; !(_step3 = _iterator3()).done;) {
            var idler = _step3.value;
            var timeCounter = idler['timeCounter'];
            if (timeCounter.inIdle) idler.onInteractionIdleCancel();
            timeCounter.inIdle = false;
            timeCounter.leftTime = timeCounter.idleTriggerTime;
          }
        };

        return TopTouchProcessor;
      }(BaseComponent), _class2.instance = void 0, _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ui_switcher.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Logger.ts'], function (exports) {
  var _applyDecoratedDescriptor, _initializerDefineProperty, _inheritsLoose, _assertThisInitialized, _createClass, cclegacy, _decorator, CCString, CCBoolean, Vec3, Vec2, CCInteger, UITransform, Label, Sprite, Color, UIOpacity, Component, logger;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _initializerDefineProperty = module.initializerDefineProperty;
      _inheritsLoose = module.inheritsLoose;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      CCString = module.CCString;
      CCBoolean = module.CCBoolean;
      Vec3 = module.Vec3;
      Vec2 = module.Vec2;
      CCInteger = module.CCInteger;
      UITransform = module.UITransform;
      Label = module.Label;
      Sprite = module.Sprite;
      Color = module.Color;
      UIOpacity = module.UIOpacity;
      Component = module.Component;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _dec11, _dec12, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _descriptor10, _descriptor11, _dec13, _dec14, _dec15, _class4, _class5, _descriptor12, _descriptor13, _dec16, _dec17, _dec18, _class7, _class8, _descriptor14, _descriptor15, _dec19, _dec20, _dec21, _dec22, _dec23, _dec24, _dec25, _dec26, _class10, _class11, _descriptor16, _descriptor17, _descriptor18, _descriptor19;

      cclegacy._RF.push({}, "7723dwkUABCa6fh2jdKkcKg", "ui_switcher", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property,
          inspector = _decorator.inspector,
          executeInEditMode = _decorator.executeInEditMode,
          menu = _decorator.menu;
      var CCTransform = exports('CCTransform', (_dec = ccclass('CCTransform'), _dec2 = property(CCString), _dec3 = property(CCString), _dec4 = property(CCString), _dec5 = property(CCBoolean), _dec6 = property(Vec3), _dec7 = property(Vec3), _dec8 = property(Vec3), _dec9 = property(Vec2), _dec10 = property(Vec2), _dec11 = property(CCInteger), _dec12 = property(Vec3), _dec(_class = (_class2 = function CCTransform() {
        _initializerDefineProperty(this, "nodeUuid", _descriptor, this);

        _initializerDefineProperty(this, "guid", _descriptor2, this);

        _initializerDefineProperty(this, "stateUuid", _descriptor3, this);

        _initializerDefineProperty(this, "active", _descriptor4, this);

        _initializerDefineProperty(this, "position", _descriptor5, this);

        _initializerDefineProperty(this, "rotation", _descriptor6, this);

        _initializerDefineProperty(this, "scale", _descriptor7, this);

        _initializerDefineProperty(this, "anchor", _descriptor8, this);

        _initializerDefineProperty(this, "size", _descriptor9, this);

        _initializerDefineProperty(this, "opacity", _descriptor10, this);

        _initializerDefineProperty(this, "color", _descriptor11, this);
      }, (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "nodeUuid", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "";
        }
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "guid", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "";
        }
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "stateUuid", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "";
        }
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "active", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return true;
        }
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "position", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return new Vec3(0, 0, 0);
        }
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "rotation", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return new Vec3(0, 0, 0);
        }
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "scale", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return new Vec3(0, 0, 0);
        }
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "anchor", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return new Vec2(0.5, 0.5);
        }
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "size", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return new Vec2(0, 0);
        }
      }), _descriptor10 = _applyDecoratedDescriptor(_class2.prototype, "opacity", [_dec11], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 255;
        }
      }), _descriptor11 = _applyDecoratedDescriptor(_class2.prototype, "color", [_dec12], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return new Vec3(255, 255, 255);
        }
      })), _class2)) || _class));
      var SwitchState = exports('SwitchState', (_dec13 = ccclass('SwitchState'), _dec14 = property(CCString), _dec15 = property(CCString), _dec13(_class4 = (_class5 = function SwitchState() {
        _initializerDefineProperty(this, "uuid", _descriptor12, this);

        _initializerDefineProperty(this, "name", _descriptor13, this);
      }, (_descriptor12 = _applyDecoratedDescriptor(_class5.prototype, "uuid", [_dec14], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "";
        }
      }), _descriptor13 = _applyDecoratedDescriptor(_class5.prototype, "name", [_dec15], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "";
        }
      })), _class5)) || _class4));
      var TargetNode = exports('TargetNode', (_dec16 = ccclass('TargetNode'), _dec17 = property(CCString), _dec18 = property(CCString), _dec16(_class7 = (_class8 = function TargetNode() {
        _initializerDefineProperty(this, "guid", _descriptor14, this);

        _initializerDefineProperty(this, "uuid", _descriptor15, this);
      }, (_descriptor14 = _applyDecoratedDescriptor(_class8.prototype, "guid", [_dec17], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "";
        }
      }), _descriptor15 = _applyDecoratedDescriptor(_class8.prototype, "uuid", [_dec18], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "";
        }
      })), _class8)) || _class7));
      var UISwitcher = exports('UISwitcher', (_dec19 = ccclass('UISwitcher'), _dec20 = inspector('packages://ui_switcher/inspector/switcher/switcher.js'), _dec21 = executeInEditMode(), _dec22 = menu('Custom/UI状态切换组件'), _dec23 = property({
        visible: false
      }), _dec24 = property({
        visible: false
      }), _dec25 = property({
        visible: false
      }), _dec26 = property({
        visible: false
      }), _dec19(_class10 = _dec20(_class10 = _dec21(_class10 = _dec22(_class10 = (_class11 = /*#__PURE__*/function (_Component) {
        _inheritsLoose(UISwitcher, _Component);

        function UISwitcher() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Component.call.apply(_Component, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "targetNodes", _descriptor16, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "switchStates", _descriptor17, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "ccTransforms", _descriptor18, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "curState", _descriptor19, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = UISwitcher.prototype;

        _proto.changeStateByIndex = function changeStateByIndex(index) {
          this.curState = index;

          this._onCurStateChange(index);
        };

        _proto.changeStateByName = function changeStateByName(name) {
          var states = JSON.parse(this.switchStates);

          for (var i = 0; i < states.length; i++) {
            if (name == states[i].name) {
              this.changeStateByIndex(i);
              break;
            }
          }
        };

        _proto._onStatesChange = function _onStatesChange() {
          var transforms = [];
          var states = [];
          var switchStates = JSON.parse(this.switchStates);

          for (var i = 0; i < switchStates.length; i++) {
            states.push(switchStates[i].uuid);
          }

          var ccTransforms = JSON.parse(this.ccTransforms);

          for (var _i = 0; _i < ccTransforms.length; _i++) {
            if (states.indexOf(ccTransforms[_i].stateUuid) >= 0) {
              transforms.push(ccTransforms[_i]);
            }
          }

          this.ccTransforms = JSON.stringify(transforms);
        };

        _proto._onCurStateChange = function _onCurStateChange(index) {
          var states = JSON.parse(this.switchStates);
          var curState = states[index];
          var transforms = JSON.parse(this.ccTransforms);

          for (var i = 0; i < transforms.length; i++) {
            var trans = transforms[i];

            if (trans.stateUuid != curState.uuid) {
              continue;
            }

            var child = this._getNodeByUuid(this._getNodeByGuid(trans.guid));

            if (child) {
              child.active = trans.active;
              child.setPosition(trans.position);
              child.eulerAngles = trans.rotation;
              child.setScale(trans.scale);
              var uiTrans = child.getComponent(UITransform);

              if (uiTrans) {
                uiTrans.setAnchorPoint(trans.anchor.x, trans.anchor.y);
                uiTrans.setContentSize(trans.size.x, trans.size.y);
              }

              var render = null;
              render = child.getComponent(Label);

              if (!render) {
                render = child.getComponent(Sprite);
              }

              if (render) {
                render.color = new Color(trans.color.x, trans.color.y, trans.color.z, trans.opacity);
              } else {
                var opacity = child.getComponent(UIOpacity);

                if (opacity) {
                  opacity.opacity = trans.opacity;
                }
              }
            } else {
              logger.warn("没有找到此节点：" + trans.guid);
            }
          }
        };

        _proto._getNodeByGuid = function _getNodeByGuid(guid) {
          var targetNodes = JSON.parse(this.targetNodes);

          for (var i = 0; i < targetNodes.length; i++) {
            if (targetNodes[i].guid == guid) {
              return targetNodes[i].uuid;
            }
          }

          return null;
        };

        _proto._changeTargetByGuid = function _changeTargetByGuid(newUuid, guid) {
          this._removeTargetTransByUuid(guid);

          this._changeTargetTransByUuid(newUuid, guid);
        };

        _proto._removeTargetTransByUuid = function _removeTargetTransByUuid(guid) {
          var transforms = [];
          var ccTransforms = JSON.parse(this.ccTransforms);

          for (var i = 0; i < ccTransforms.length; i++) {
            if (ccTransforms[i].guid != guid) {
              transforms.push(ccTransforms[i]);
            }
          }

          this.ccTransforms = JSON.stringify(transforms);
        };

        _proto._changeTargetTransByUuid = function _changeTargetTransByUuid(uuid, guid) {
          var node = this._getNodeByUuid(uuid);

          if (!node && uuid != "") {
            logger.warn("未找到此node:" + uuid);
            return;
          }

          var targetNodes = JSON.parse(this.targetNodes);
          var has = false;

          for (var i = 0; i < targetNodes.length; i++) {
            if (targetNodes[i].guid == guid) {
              targetNodes[i].uuid = uuid;
              has = true;
            }
          }

          if (!has) {
            var tNode = new TargetNode();
            tNode.guid = guid;

            if (node) {
              tNode.uuid = node.uuid;
            } else {
              tNode.uuid = "";
            }

            targetNodes.push(tNode);
          }

          this.targetNodes = JSON.stringify(targetNodes);
          var ccTransforms = JSON.parse(this.ccTransforms);
          var switchStates = JSON.parse(this.switchStates);

          for (var _i2 = 0; _i2 < switchStates.length; _i2++) {
            var trans = new CCTransform();
            trans.nodeUuid = uuid;
            trans.guid = guid;
            trans.stateUuid = switchStates[_i2].uuid;

            if (node) {
              trans.active = node.active;
              trans.position = new Vec3(node.position.x, node.position.y, node.position.z);
              trans.rotation = new Vec3(node.eulerAngles.x, node.eulerAngles.y, node.eulerAngles.z);
              trans.scale = new Vec3(node.scale.x, node.scale.y, node.scale.z);
              var render = null;
              render = node.getComponent(Label);

              if (!render) {
                render = node.getComponent(Sprite);
              }

              if (render) {
                trans.color = new Vec3(render.color.r, render.color.g, render.color.b);
                trans.opacity = render.color.a;
              } else {
                trans.color = new Vec3(255, 255, 255);
                trans.opacity = 255;
              }

              var opacity = node.getComponent(UIOpacity);

              if (opacity) {
                trans.opacity = opacity.opacity;
              }

              var uiTrans = node.getComponent(UITransform);

              if (uiTrans) {
                trans.anchor = new Vec2(uiTrans.anchorX, uiTrans.anchorY);
                trans.size = new Vec2(uiTrans.width, uiTrans.height);
              } else {
                trans.anchor = new Vec2(0.5, 0.5);
                trans.size = new Vec2(100, 100);
              }
            }

            ccTransforms.push(trans);
          }

          this.ccTransforms = JSON.stringify(ccTransforms);
        };

        _proto._removeStateByUuid = function _removeStateByUuid(uuid) {
          var transforms = [];
          var ccTransforms = JSON.parse(this.ccTransforms);

          for (var i = 0; i < ccTransforms.length; i++) {
            if (ccTransforms[i].stateUuid != uuid) {
              transforms.push(ccTransforms[i]);
            }
          }

          this.ccTransforms = JSON.stringify(transforms);
        };

        _proto._addStateByUuid = function _addStateByUuid(uuid) {
          var targetNodes = JSON.parse(this.targetNodes);
          var ccTransforms = JSON.parse(this.ccTransforms);

          for (var i = 0; i < targetNodes.length; i++) {
            var nodeUuid = targetNodes[i].uuid;

            var node = this._getNodeByUuid(nodeUuid);

            if (!node) {
              continue;
            }

            var trans = new CCTransform();
            trans.nodeUuid = nodeUuid;
            trans.stateUuid = uuid;
            trans.guid = targetNodes[i].guid;
            trans.active = node.active;
            trans.position = new Vec3(node.position.x, node.position.y, node.position.z);
            trans.rotation = new Vec3(node.eulerAngles.x, node.eulerAngles.y, node.eulerAngles.z);
            var render = null;
            render = node.getComponent(Label);

            if (!render) {
              render = node.getComponent(Sprite);
            }

            if (render) {
              trans.color = new Vec3(render.color.r, render.color.g, render.color.b);
              trans.opacity = render.color.a;
            } else {
              trans.color = new Vec3(255, 255, 255);
              trans.opacity = 255;
            }

            var opacity = node.getComponent(UIOpacity);

            if (opacity) {
              trans.opacity = opacity.opacity;
            }

            var uiTrans = node.getComponent(UITransform);

            if (uiTrans) {
              trans.anchor = new Vec2(uiTrans.anchorX, uiTrans.anchorY);
              trans.size = new Vec2(uiTrans.width, uiTrans.height);
            } else {
              trans.anchor = new Vec2(0.5, 0.5);
              trans.size = new Vec2(100, 100);
            }

            ccTransforms.push(trans);
          }

          this.ccTransforms = JSON.stringify(ccTransforms);
        };

        _proto._findNodeFromChildByUuid = function _findNodeFromChildByUuid(uuid, parent) {
          if (parent.uuid == uuid) {
            return parent;
          }

          for (var i = 0; i < parent.children.length; i++) {
            var child = parent.children[i];

            var result = this._findNodeFromChildByUuid(uuid, child);

            if (result) {
              return result;
            }
          }

          return null;
        };

        _proto._getNodeByUuid = function _getNodeByUuid(uuid) {
          {
            return this._findNodeFromChildByUuid(uuid, this.node.parent || this.node);
          }
        };

        _createClass(UISwitcher, [{
          key: "onSwitchStatesChange",
          set: function set(value) {
            if (value) {
              var s = value.states;
              var states = [];

              for (var i = 0; i < s.length; i++) {
                var item = s[i];
                var state = new SwitchState();
                state.uuid = item.uuid;
                state.name = item.name;
                states.push(state);
              }

              this.switchStates = JSON.stringify(states);
            }
          }
        }, {
          key: "onRemoveSwitchState",
          set: function set(value) {
            if (value != null) {
              var uuid = value.uuid;
              var states = [];
              var switchStates = JSON.parse(this.switchStates);

              for (var i = 0; i < switchStates.length; i++) {
                if (uuid != switchStates[i].uuid) {
                  states.push(switchStates[i]);
                }
              }

              this.switchStates = JSON.stringify(states);
              var transforms = [];
              var ccTransforms = JSON.parse(this.ccTransforms);

              for (var _i3 = 0; _i3 < ccTransforms.length; _i3++) {
                if (ccTransforms[_i3].stateUuid != uuid) {
                  transforms.push(ccTransforms[_i3]);
                }
              }

              this.ccTransforms = JSON.stringify(transforms);
            }
          }
        }, {
          key: "onAddSwitchState",
          set: function set(value) {
            if (value != null) {
              var state = new SwitchState();
              state.uuid = value.uuid;
              state.name = "";
              var states = JSON.parse(this.switchStates);
              states.push(state);
              this.switchStates = JSON.stringify(states);

              this._removeStateByUuid(value);

              this._addStateByUuid(value);
            }
          }
        }, {
          key: "onCurStateChange",
          set: function set(value) {
            if (value) {
              var index = value.index;
              this.curState = index;

              this._onCurStateChange(index);
            }
          }
        }, {
          key: "onTransChange",
          set: function set(value) {
            if (value && value != "") {
              var transforms = [];
              var t = JSON.parse(value.trans);
              var stateUuid = t[0].stateUuid;
              var ccTransforms = JSON.parse(this.ccTransforms);

              for (var i = 0; i < ccTransforms.length; i++) {
                if (ccTransforms[i].stateUuid != stateUuid) {
                  transforms.push(ccTransforms[i]);
                }
              }

              for (var _i4 = 0; _i4 < t.length; _i4++) {
                var item = t[_i4];
                var trans = new CCTransform();
                trans.nodeUuid = item.nodeUuid;
                trans.guid = item.guid;
                trans.stateUuid = item.stateUuid;
                trans.active = item.active;
                trans.position = item.position;
                trans.rotation = item.rotation;
                trans.scale = item.scale;
                trans.anchor = item.anchor;
                trans.size = item.size;
                trans.color = item.color;
                trans.opacity = item.opacity;
                transforms.push(trans);
              }

              this.ccTransforms = JSON.stringify(transforms);
            }
          }
        }, {
          key: "onRemoveTarget",
          set: function set(value) {
            if (value != null) {
              var nodes = [];
              var targetNodes = JSON.parse(this.targetNodes);

              for (var i = 0; i < targetNodes.length; i++) {
                if (targetNodes[i].guid != value.guid) {
                  nodes.push(targetNodes[i]);
                }
              }

              this.targetNodes = JSON.stringify(nodes);
              var transforms = [];
              var ccTransforms = JSON.parse(this.ccTransforms);

              for (var _i5 = 0; _i5 < ccTransforms.length; _i5++) {
                if (ccTransforms[_i5].guid != value.guid) {
                  transforms.push(ccTransforms[_i5]);
                }
              }

              this.ccTransforms = JSON.stringify(transforms);
            }
          }
        }, {
          key: "onAddTarget",
          set: function set(value) {
            if (value) {
              this._changeTargetByGuid(value.uuid, value.guid);
            } else {
              logger.error("添加目标值错误");
            }
          }
        }, {
          key: "onChangeTarget",
          set: function set(value) {
            if (value) {
              this._changeTargetByGuid(value.uuid, value.guid);
            } else {
              logger.error("修改目标值错误");
            }
          }
        }]);

        return UISwitcher;
      }(Component), (_descriptor16 = _applyDecoratedDescriptor(_class11.prototype, "targetNodes", [_dec23], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "[]";
        }
      }), _descriptor17 = _applyDecoratedDescriptor(_class11.prototype, "switchStates", [_dec24], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "[]";
        }
      }), _descriptor18 = _applyDecoratedDescriptor(_class11.prototype, "ccTransforms", [_dec25], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return "[]";
        }
      }), _descriptor19 = _applyDecoratedDescriptor(_class11.prototype, "curState", [_dec26], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return 0;
        }
      })), _class11)) || _class10) || _class10) || _class10) || _class10));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/UtilTools.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './env', './index.js', './Logger.ts'], function (exports) {
  var _createClass, _asyncToGenerator, _regeneratorRuntime, cclegacy, Node, UITransform, view, sys, screen, game, RenderTexture, director, Director, DEBUG, Long, logger;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      Node = module.Node;
      UITransform = module.UITransform;
      view = module.view;
      sys = module.sys;
      screen = module.screen;
      game = module.game;
      RenderTexture = module.RenderTexture;
      director = module.director;
      Director = module.Director;
    }, function (module) {
      DEBUG = module.DEBUG;
    }, function (module) {
      Long = module.default;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      cclegacy._RF.push({}, "b11ea5JYTpD07Aa5O6G9iMz", "UtilTools", undefined);
      /** 用于本地测试的 IP 白名单 */


      var IP_WHITE_LIST = ['127.0.0.1', 'localhost', '128.0.3.193'];
      var UtilTools = exports('UtilTools', /*#__PURE__*/function () {
        function UtilTools() {}
        /**
         * 在节点上部署组件
         * @param source 源组件
         * @param target 目标组件
         * @returns
         */


        UtilTools.setupComponent = function setupComponent(source, target) {
          if (source instanceof Node) {
            return source.getComponent(target) || source.addComponent(target);
          }

          return source.getComponent(target) || source.addComponent(target);
        }
        /**
         * 适配屏幕
         * @param node 节点
         */
        ;

        UtilTools.fitScreen = function fitScreen(node) {
          this.setupComponent(node, UITransform).setContentSize(view.getVisibleSize());
        }
        /** 是否本地开发环境 */
        ;

        UtilTools.isDev = function isDev() {
          return IP_WHITE_LIST.indexOf(location.hostname) > -1 || DEBUG;
        }
        /** 获取窗口尺寸 */
        ;

        UtilTools.getWinSize = function getWinSize() {
          return sys.isBrowser ? screen.windowSize : view.getVisibleSize();
        };
        /**
         * 复制文本到剪贴板
         * @param text 文本
         * @param onOk 成功回调
         * @param onBad 失败回调
         */


        UtilTools.copyToClipboard = function copyToClipboard(text, onOk, onBad) {
          return new Promise(function (resolve, reject) {
            // @ts-ignore
            navigator.permissions.query({
              name: 'clipboard-write',
              allowWithoutGesture: false
            }).then(function (result) {
              UtilTools.__ClipboardWritable__ = result.state == 'granted' || result.state == 'prompt';

              if (UtilTools.__ClipboardWritable__) {
                navigator.clipboard.writeText(text).then(function () {
                  onOk && onOk();
                  resolve();
                }, function () {
                  onBad && onBad();
                  reject('写入失败');
                });
              } else {
                onBad && onBad();
                reject('剪切板写入权限不足');
              }
            });
          });
        }
        /**
         * 从剪贴板读取文本
         * @returns
         */
        ;

        UtilTools.readFromClipboard = function readFromClipboard() {
          return new Promise(function (resolve, reject) {
            // @ts-ignore
            navigator.permissions.query({
              name: 'clipboard-read',
              allowWithoutGesture: false
            }).then(function (result) {
              UtilTools.__ClipboardReadable__ = result.state == 'granted' || result.state == 'prompt';

              if (UtilTools.__ClipboardReadable__) {
                resolve(navigator.clipboard.readText() || '');
              } else {
                reject('剪切板读取权限不足');
              }
            });
          });
        }
        /**
         * 获取网址中的参数部分
         * @returns
         */
        ;

        UtilTools.queryURLParams = function queryURLParams() {
          if (!sys.isBrowser) return null;
          var obj = {}; // 声明参数对象

          var url = window.location.search.split('?')[1];
          if (!url) return obj;
          var arr = url.split('&'); // 以&符号分割为数组

          for (var i = 0; i < arr.length; i++) {
            var arrNew = arr[i].split('='); // 以"="分割为数组

            obj[arrNew[0]] = arrNew[1];
          }

          return obj;
        }
        /**
         * 刷新页面
         */
        ;

        UtilTools.refreshUrl = function refreshUrl() {
          if (!sys.isBrowser) return;
          window.location.reload();
        }
        /**
         * 时间格式转Date
         * @param dateStr 2023-06-27
         * @returns
         */
        ;

        UtilTools.toDate = function toDate(dateFmt) {
          return new Date(dateFmt);
        }
        /**
         * 2023-06-27
         * @param date
         * @returns
         */
        ;

        UtilTools.toDateString = function toDateString(date, sep) {
          if (sep) return date.toLocaleDateString().replace(/\//g, sep);
          return date.toLocaleDateString();
        };

        UtilTools.toMS = function toMS(n) {
          var m = Math.floor(n % 3600 / 60);
          m = m < 10 ? '0' + m : m;
          var s = Math.floor(n % 60);
          s = s < 10 ? '0' + s : s;
          return m + ":" + s;
        };

        UtilTools.toHMS = function toHMS(n) {
          var h = Math.floor(n / 3600);
          h = h < 10 ? '0' + h : h;
          var m = Math.floor(n % 3600 / 60);
          m = m < 10 ? '0' + m : m;
          var s = Math.floor(n % 60);
          s = s < 10 ? '0' + s : s;
          return h + ":" + m + ":" + s;
        };

        UtilTools.capture = /*#__PURE__*/function () {
          var _capture = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(camera, captureInfo) {
            var texture, width, height, region, createInfo;
            return _regeneratorRuntime().wrap(function _callee$(_context) {
              while (1) switch (_context.prev = _context.next) {
                case 0:
                  texture = new RenderTexture();
                  width = captureInfo.width, height = captureInfo.height, region = captureInfo.region;
                  createInfo = {
                    width: width,
                    height: height,
                    name: "capture_" + Date.now()
                  };
                  texture.reset(createInfo);
                  camera.targetTexture = texture;
                  logger.i('截图信息', captureInfo);
                  return _context.abrupt("return", new Promise(function (resolve) {
                    director.once(Director.EVENT_AFTER_DRAW, function () {
                      var imagePixels = texture.readPixels();
                      imagePixels = UtilTools.flipImageX(imagePixels, width, height);
                      {
                        logger.i('Web 截屏');
                        var canvas = document.createElement('canvas'); // canvas.width = screen.windowSize.width;
                        // canvas.height = screen.windowSize.height;

                        canvas.width = region.width;
                        canvas.height = region.height;
                        var context = canvas.getContext('2d');
                        var imageData = context.createImageData(region.width, region.height);
                        imageData.data.set(imagePixels);
                        context.putImageData(imageData, 0, 0);
                        canvas.toBlob(function (blob) {
                          var newImage = document.createElement('img');
                          var url = URL.createObjectURL(blob);

                          newImage.onload = function () {
                            var urlObject = window.URL || window.webkitURL || window;
                            URL.revokeObjectURL(url);
                            var save_link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a'); // @ts-ignore

                            save_link.href = urlObject.createObjectURL(blob); // @ts-ignore

                            save_link.download = "capture_" + Date.now() + ".png";
                            save_link.click();
                            resolve(imagePixels);
                          };

                          newImage.src = url; // document.body.appendChild(newImage);
                        });
                      }
                      camera.targetTexture = null;
                      texture.destroy();
                    });
                  }));

                case 7:
                case "end":
                  return _context.stop();
              }
            }, _callee);
          }));

          function capture(_x, _x2) {
            return _capture.apply(this, arguments);
          }

          return capture;
        }();

        UtilTools.flipImageX = function flipImageX(data, width, height) {
          //图片数组纵向翻转
          var newData = new Uint8Array(data.length);

          for (var i = 0; i < height; i++) {
            for (var j = 0; j < width; j++) {
              var index = (width * i + j) * 4;
              var newIndex = (width * (height - i - 1) + j) * 4;
              newData[newIndex] = data[index];
              newData[newIndex + 1] = data[index + 1];
              newData[newIndex + 2] = data[index + 2];
              newData[newIndex + 3] = data[index + 3];
            }
          }

          return newData;
        };

        UtilTools.flipImageY = function flipImageY(data, width, height) {
          //图片数组横向翻转
          var newData = new Uint8Array(data.length);

          for (var i = 0; i < height; i++) {
            for (var j = 0; j < width; j++) {
              var newIndex = (width * i + j) * 4;
              var index = (width - j + i * width) * 4;
              newData[newIndex] = data[index];
              newData[newIndex + 1] = data[index + 1];
              newData[newIndex + 2] = data[index + 2];
              newData[newIndex + 3] = data[index + 3];
            }
          }

          return newData;
        };

        UtilTools.canvas2Image = function canvas2Image() {
          game.canvas.toBlob(function (blob) {
            var newImage = document.createElement('img');
            var url = URL.createObjectURL(blob);

            newImage.onload = function () {
              var urlObject = window.URL || window.webkitURL || window;
              URL.revokeObjectURL(url);
              var save_link = document.createElementNS('http://www.w3.org/1999/xhtml', 'a'); // @ts-ignore

              save_link.href = urlObject.createObjectURL(blob); // @ts-ignore

              save_link.download = 'capture.png';
              save_link.click();
            };

            newImage.src = url; // document.body.appendChild(newImage);
          });
        };

        UtilTools.parseInt64 = function parseInt64(int64) {
          return Long.isLong(int64) ? Long.fromValue(int64).toNumber() : 1;
        };

        _createClass(UtilTools, null, [{
          key: "clipboardReadable",
          get:
          /**
           * 剪切板是否可读
           */
          function get() {
            return UtilTools.__ClipboardReadable__;
          }
          /**
           * 剪切板是否可写
           */

        }, {
          key: "clipboardWritable",
          get: function get() {
            return UtilTools.__ClipboardWritable__;
          }
        }]);

        return UtilTools;
      }());
      UtilTools.__ClipboardReadable__ = false;
      UtilTools.__ClipboardWritable__ = false;
      UtilTools.isDev() && (window.utils = UtilTools);

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/View.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './AudioEngine.ts', './ButtonDecorator.ts', './EventDispatcher.ts', './Logger.ts', './Modifier.ts'], function (exports) {
  var _inheritsLoose, _createClass, cclegacy, UITransform, view, Graphics, BlockInputEvents, tween, UIOpacity, v3, js, Button, audioEngine, buttonDecorator, dispatcher, logger, BaseComponent;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      UITransform = module.UITransform;
      view = module.view;
      Graphics = module.Graphics;
      BlockInputEvents = module.BlockInputEvents;
      tween = module.tween;
      UIOpacity = module.UIOpacity;
      v3 = module.v3;
      js = module.js;
      Button = module.Button;
    }, function (module) {
      audioEngine = module.audioEngine;
    }, function (module) {
      buttonDecorator = module.buttonDecorator;
    }, function (module) {
      dispatcher = module.dispatcher;
    }, function (module) {
      logger = module.logger;
    }, function (module) {
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      cclegacy._RF.push({}, "92be0ij7AhIlpYExYwQTvhD", "View", undefined);
      /** 默认点击间隔 */


      var DefaultClickDelay = exports('DefaultClickDelay', 1000);
      /** 用做弹窗黑色半透背景和触摸传递隔断层 */

      /**
       * 视图-节点（骨架）
       */

      var DgflyNode = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(DgflyNode, _BaseComponent);

        function DgflyNode() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          /** 预制体路径 */

          _this.prefabPath = void 0;
          /**归属模块 */

          _this.underModuleName = void 0;
          return _this;
        }

        var _proto = DgflyNode.prototype;
        /**
         * 将操作应用到所有按钮上
         * @param operation 操作（注册/注销）
         */

        _proto._applyOperationOnButtons = function _applyOperationOnButtons(operation) {
          var btn;
          var listners = buttonDecorator.get(js.getClassName(this)); // logger.d(this, js.getClassName(this), listners);

          for (var name in listners) {
            btn = this[name];

            if (!btn) {
              logger.warn("\u8BF7\u68C0\u67E5 " + name + " \u662F\u5426\u914D\u7F6E\u5230\u7F16\u8F91\u9762\u677F\u4E86");
              continue;
            }

            operation.call(this, btn, listners[name]);
          }
        }
        /**
         * 注册按钮点击事件
         * @param btn 按钮
         * @param info 按钮注册信息
         */
        ;

        _proto._registerButton = function _registerButton(btn, info) {
          var _this2 = this;

          info.lastClickTime = 0;
          btn.node.on(Button.EventType.CLICK, function () {
            var nowTime = Date.now();
            if (info.lastClickTime + info.clickDelay > nowTime) return;
            info.lastClickTime = nowTime;

            if (info.sound) {
              if (typeof info.sound === 'string') {
                audioEngine.playOneShot(info.sound, 1);
              } else {
                audioEngine.playOneShot(info.sound.path, 1, info.sound.bundle);
              }
            } // @ts-ignore


            typeof info.func === 'string' ? _this2[info.func]() : info.func(_this2);
          });
        }
        /**
         * 注销按钮点击事件
         * @param btn 按钮
         * @param info 按钮注册信息（可不传）
         **/
        ;

        _proto._unregisterButtton = function _unregisterButtton(btn, info) {
          btn.node.off(Button.EventType.CLICK);
        };

        _proto.init = function init() {
          this._applyOperationOnButtons(this._registerButton);
        };

        _proto.deinit = function deinit() {
          this._applyOperationOnButtons(this._unregisterButtton);
        };

        _proto.onData = function onData(data) {};

        _proto.onWillAddToParent = function onWillAddToParent() {};

        _proto.onAddedToParent = function onAddedToParent() {};

        _proto.onWillRemoveFromParent = function onWillRemoveFromParent() {};

        _proto.onRemovedFromParent = function onRemovedFromParent() {};

        _proto.onValueChange = function onValueChange(name, value) {};

        _proto.onEventCallback = function onEventCallback(eventType, data) {};

        return DgflyNode;
      }(BaseComponent);
      /**
       * 视图-通用
       */


      var DgflyUI = exports('DgflyUI', /*#__PURE__*/function (_DgflyNode) {
        _inheritsLoose(DgflyUI, _DgflyNode);

        function DgflyUI() {
          return _DgflyNode.apply(this, arguments) || this;
        }

        var _proto2 = DgflyUI.prototype;

        _proto2.init = function init() {
          this.getComponent(UITransform).setContentSize(view.getVisibleSize());
          dispatcher.regist(this);

          this._initBackGround();

          _DgflyNode.prototype.init.call(this);
        }
        /**
         * 是否显示黑色半透背景
         */
        ;
        /** 初始化黑色半透背景 */


        _proto2._initBackGround = function _initBackGround() {
          if (!this.showBackground) return;
          var vsize = view.getVisibleSize();
          var g = this.node.getComponent(Graphics) || this.node.addComponent(Graphics);
          g.fillColor = g.fillColor.fromHEX(this.backgroundInfo.bgHexColor);
          g.fillRect(-vsize.width, -vsize.height, vsize.width * 2, vsize.height * 2);
          g.fill();
          this.node.getComponent(UITransform).setContentSize(vsize);
          this.backgroundInfo.swallow && this.node.addComponent(BlockInputEvents);
        };

        _proto2.deinit = function deinit() {
          _DgflyNode.prototype.deinit.call(this);

          dispatcher.unRegist(this);
        };

        _createClass(DgflyUI, [{
          key: "showBackground",
          get: function get() {
            return false;
          }
          /**
           * 获取黑色半透背景信息
           * 需要更改背景颜色时可以重写此方法
           * @returns
           */

        }, {
          key: "backgroundInfo",
          get: function get() {
            return {
              bgHexColor: '#00000054',
              swallow: true
            };
          }
        }]);

        return DgflyUI;
      }(DgflyNode));
      /**
       * 视图-窗口（弹窗）
       */

      var DgflyWindow = exports('DgflyWindow', /*#__PURE__*/function (_DgflyUI) {
        _inheritsLoose(DgflyWindow, _DgflyUI);

        function DgflyWindow() {
          return _DgflyUI.apply(this, arguments) || this;
        }

        var _proto3 = DgflyWindow.prototype;
        /** 执行弹窗进入动画 */

        _proto3._playAnimationOnPopup = function _playAnimationOnPopup() {
          tween(this.node).set({
            position: DgflyWindow.SHOW_ANIMATION_START_AT
          }).to(DgflyWindow.SHOW_ANIMATION_DURATION, {
            position: DgflyWindow.SHOW_ANIMATION_STOP_AT
          }).start();
          tween(this.node.getComponent(UIOpacity)).set({
            opacity: DgflyWindow.SHOW_ANIMATION_OPACITY_START_AT
          }).to(DgflyWindow.SHOW_ANIMATION_DURATION, {
            opacity: DgflyWindow.SHOW_ANIMATION_OPACITY_STOP_AT
          }).start();
        };

        _createClass(DgflyWindow, [{
          key: "showBackground",
          get:
          /** 默认进入动画持续时间 */

          /** 默认进入动画起始透明度 */

          /** 默认进入动画结束透明度 */

          /** 默认进入动画起始位置 */

          /** 默认进入动画结束位置 */

          /**
           * 是否显示黑色半透背景
           */
          function get() {
            return true;
          }
        }]);

        return DgflyWindow;
      }(DgflyUI));
      DgflyWindow.SHOW_ANIMATION_DURATION = 0.2;
      DgflyWindow.SHOW_ANIMATION_OPACITY_START_AT = 0;
      DgflyWindow.SHOW_ANIMATION_OPACITY_STOP_AT = 255;
      DgflyWindow.SHOW_ANIMATION_START_AT = v3(0, 50, 0);
      DgflyWindow.SHOW_ANIMATION_STOP_AT = v3(0, 0, 0);

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/ViewManager.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './View.ts', './QueueLoader.ts', './Logger.ts', './GlobalEvent.ts', './ResManager.ts', './ModuleManager.ts'], function (exports) {
  var _createForOfIteratorHelperLoose, _asyncToGenerator, _regeneratorRuntime, cclegacy, view, Camera, Layers, Node, UITransform, Widget, instantiate, Prefab, assetManager, UIOpacity, tween, easing, DgflyUI, DgflyWindow, queueLoader, logger, GlobalEvent, GlobalEventType, res, moduleMgr;

  return {
    setters: [function (module) {
      _createForOfIteratorHelperLoose = module.createForOfIteratorHelperLoose;
      _asyncToGenerator = module.asyncToGenerator;
      _regeneratorRuntime = module.regeneratorRuntime;
    }, function (module) {
      cclegacy = module.cclegacy;
      view = module.view;
      Camera = module.Camera;
      Layers = module.Layers;
      Node = module.Node;
      UITransform = module.UITransform;
      Widget = module.Widget;
      instantiate = module.instantiate;
      Prefab = module.Prefab;
      assetManager = module.assetManager;
      UIOpacity = module.UIOpacity;
      tween = module.tween;
      easing = module.easing;
    }, function (module) {
      DgflyUI = module.DgflyUI;
      DgflyWindow = module.DgflyWindow;
    }, function (module) {
      queueLoader = module.queueLoader;
    }, function (module) {
      logger = module.logger;
    }, function (module) {
      GlobalEvent = module.GlobalEvent;
      GlobalEventType = module.GlobalEventType;
    }, function (module) {
      res = module.res;
    }, function (module) {
      moduleMgr = module.moduleMgr;
    }],
    execute: function () {
      cclegacy._RF.push({}, "3693fXXvUhP0K1SIQJw/uKi", "ViewManager", undefined);
      /** 视图管理器 */


      var ViewManager = /*#__PURE__*/function () {
        function ViewManager() {
          this._views = [];
          this._windows = [];
          this.camera = void 0;
          this.root = void 0;
          this.layer_bottom = void 0;
          this.layer_ui = void 0;
          this.layer_window = void 0;
          this.layer_top = void 0;
          this.hint = void 0;
        }

        var _proto = ViewManager.prototype;

        _proto.isLastViewOf = function isLastViewOf(view) {
          var last = this._views[this._views.length - 1];
          return last === view || last.prefabPath === view;
        };

        _proto.initWithScene = function initWithScene(scene) {
          var _this = this;

          view.getDesignResolutionSize();
          var vSize = view.getVisibleSize(); // logger.log('VM.init.vSize(w,h)', vSize.width, vSize.height);
          // logger.log('VM.init.screenSize(w,h)', screen.width, screen.height);
          // if (vSize.width / vSize.height <= 1334 / 750)
          //     view.setResolutionPolicy(ResolutionPolicy.FIXED_WIDTH);
          // else
          //     view.setResolutionPolicy(ResolutionPolicy.FIXED_HEIGHT);
          // view.setDesignResolutionSize();

          var root = scene.getChildByName('ui_root');
          if (!root) throw "Scene\u5FC5\u987B\u8981\u6709\u4E2A\u5305\u542BCamera\u7684\u8282\u70B9root";
          this.root = root;
          this.camera = this.root.getChildByName('Camera').getComponent(Camera);
          /*
          this.captureRoot = this.scene.getChildByName('captureRoot');
          this.captureCamera = this.captureRoot.getChildByPath("CaptureCamera")?.getComponent(Camera);
          const captureTexture = new RenderTexture();
          captureTexture.reset({ width: vSize.width, height: vSize.height });
          this.captureCamera.targetTexture = captureTexture;
          */

          var dSize = view.getDesignResolutionSize();

          var createLayer = function createLayer(name, layerEnum, z) {
            var layer = _this.root.getChildByName(name);

            if (layer) return layer;
            layer = new Node(name);
            layer.layer = layerEnum;
            var uitrans = layer.addComponent(UITransform);
            uitrans.setContentSize(view.getVisibleSize());

            if (z != undefined) {
              _this.root.insertChild(layer, z);
            } else {
              _this.root.addChild(layer);
            }

            var widget = layer.addComponent(Widget);
            widget.alignMode = Widget.AlignMode.ALWAYS;
            widget.isAlignTop = true;
            widget.isAlignBottom = true;
            widget.isAlignLeft = true;
            widget.isAlignRight = true;
            widget.top = widget.bottom = widget.left = widget.right = 0;
            widget.target = _this.root;
            return layer;
          };

          this.layer_bottom = createLayer('layer_bottom', Layers.Enum.UI_2D, 2);
          this.layer_ui = createLayer('layer_ui', Layers.Enum.UI_2D, 3);
          this.layer_window = createLayer('layer_window', Layers.Enum.UI_2D, 10);
          this.layer_top = createLayer('layer_top', Layers.Enum.UI_2D, 20);
          var pos = this.layer_bottom.getPosition();
          pos.x = -dSize.width * 0.5;
          pos.y = -dSize.height * 0.5;
          this.layer_bottom.setPosition(pos);
          console.log('initWithScene', scene.name);
          this.layer_window.targetOff(this);

          if (scene.name === 'main_scene') {
            this.layer_ui.on(Node.EventType.CHILD_ADDED, this._onWindowChildrenChanged, this);
            this.layer_ui.on(Node.EventType.CHILD_REMOVED, this._onWindowChildrenChanged, this);
            this.layer_window.on(Node.EventType.CHILD_ADDED, this._onWindowChildrenChanged, this);
            this.layer_window.on(Node.EventType.CHILD_REMOVED, this._onWindowChildrenChanged, this);
          }
        };

        _proto._onWindowChildrenChanged = function _onWindowChildrenChanged() {
          var num1 = this.layer_ui.children.length;
          var num2 = this.layer_window.children.length;
          GlobalEvent.emit(GlobalEventType.WindowChanged, num1 + num2 - 1);
        };

        _proto.getDgflyUI = function getDgflyUI(node) {
          for (var _iterator = _createForOfIteratorHelperLoose(node.components), _step; !(_step = _iterator()).done;) {
            var comp = _step.value;

            if (comp instanceof DgflyUI) {
              return comp;
            }
          }

          return null;
        };

        _proto.changeUI = /*#__PURE__*/function () {
          var _changeUI = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee2(prefabPath, data, bundleName) {
            var _this2 = this;

            return _regeneratorRuntime().wrap(function _callee2$(_context2) {
              while (1) switch (_context2.prev = _context2.next) {
                case 0:
                  return _context2.abrupt("return", new Promise( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee(resolve, rej) {
                    var _this2$hint, _this2$hint2;

                    var lastView, bundlename, bundle, prefab, prefabNode, view, _iterator2, _step2, _moduleMgr$runningMod, _view, model;

                    return _regeneratorRuntime().wrap(function _callee$(_context) {
                      while (1) switch (_context.prev = _context.next) {
                        case 0:
                          lastView = _this2._views[_this2._views.length - 1];

                          if (!(lastView && lastView.prefabPath == prefabPath)) {
                            _context.next = 5;
                            break;
                          }

                          logger.log('同一个View');
                          resolve(lastView);
                          return _context.abrupt("return");

                        case 5:
                          (_this2$hint = _this2.hint) == null ? void 0 : _this2$hint.enableBlockEvent(); // const p = res.get<Prefab>(bundleName || moduleMgr.runningModule.moduleName, prefabPath, Prefab).catch((err: Error) => {
                          //     throw err;
                          // });

                          bundlename = bundleName || moduleMgr.runningModule.moduleName;
                          bundle = assetManager.getBundle(bundlename);
                          prefab = bundle.get(prefabPath, Prefab);

                          if (prefab) {
                            _context.next = 13;
                            break;
                          }

                          _context.next = 12;
                          return queueLoader.loadQueueAsync(_this2.hint.getLoader('littleLoader'), {
                            bundleName: bundlename,
                            paths: prefabPath,
                            ratio: 1
                          });

                        case 12:
                          prefab = bundle.get(prefabPath, Prefab);

                        case 13:
                          prefabNode = instantiate(prefab);
                          view = _this2.getDgflyUI(prefabNode);
                          view.prefabPath = prefabPath;
                          view.underModuleName = bundlename;

                          for (_iterator2 = _createForOfIteratorHelperLoose(_this2._views); !(_step2 = _iterator2()).done;) {
                            _view = _step2.value;
                            (_moduleMgr$runningMod = moduleMgr.runningModule.model) == null ? void 0 : _moduleMgr$runningMod.removeObserver(_view);

                            _view.onWillRemoveFromParent();

                            _view.node.removeFromParent();

                            _view.onRemovedFromParent();

                            _view.deinit();

                            _view.node.destroy();
                          }

                          _this2._views.length = 0;

                          _this2._views.push(view);

                          view.onWillAddToParent();

                          _this2.layer_ui.addChild(view.node);

                          view.onAddedToParent();
                          view.init();
                          model = moduleMgr.getModuleModel(bundlename);
                          model == null ? void 0 : model.addObserver(view);
                          data && view.onData(data);
                          (_this2$hint2 = _this2.hint) == null ? void 0 : _this2$hint2.disableBlockEvent(); // useTrans && this.hint?.hideDarkMask();

                          resolve(view);

                        case 29:
                        case "end":
                          return _context.stop();
                      }
                    }, _callee);
                  }))));

                case 1:
                case "end":
                  return _context2.stop();
              }
            }, _callee2);
          }));

          function changeUI(_x, _x2, _x3) {
            return _changeUI.apply(this, arguments);
          }

          return changeUI;
        }();

        _proto.pushUI = /*#__PURE__*/function () {
          var _pushUI = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee4(prefabPath, data, bundleName) {
            var _this3 = this;

            return _regeneratorRuntime().wrap(function _callee4$(_context4) {
              while (1) switch (_context4.prev = _context4.next) {
                case 0:
                  return _context4.abrupt("return", new Promise( /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee3(resolve) {
                    var _this3$hint, _this3$hint2, _this3$hint3;

                    var lastView, bundlename, bundle, prefab, prefabNode, view, model, widget;
                    return _regeneratorRuntime().wrap(function _callee3$(_context3) {
                      while (1) switch (_context3.prev = _context3.next) {
                        case 0:
                          lastView = _this3._views[_this3._views.length - 1];

                          if (!(lastView && lastView.prefabPath == prefabPath)) {
                            _context3.next = 5;
                            break;
                          }

                          logger.log('同一个View');
                          resolve(lastView);
                          return _context3.abrupt("return");

                        case 5:
                          (_this3$hint = _this3.hint) == null ? void 0 : _this3$hint.enableBlockEvent();
                          /*
                          const p1 = this.hint?.showDarkMask();
                          const p2 = res.get<Prefab>(bundleName || moduleMgr.runningModule.moduleName, prefabPath, Prefab).catch((err: Error) => {
                              throw err;
                          });
                          const p = await Promise.all([p1, p2]);
                           const prefab = p[1] as Prefab;
                          const prefabNode = instantiate(prefab);
                          */

                          bundlename = bundleName || moduleMgr.runningModule.moduleName;
                          _context3.next = 9;
                          return res.loadBundleAsync(bundlename);

                        case 9:
                          bundle = _context3.sent;
                          prefab = bundle.get(prefabPath, Prefab);

                          if (prefab) {
                            _context3.next = 15;
                            break;
                          }

                          _context3.next = 14;
                          return queueLoader.loadQueueAsync(_this3.hint.getLoader('littleLoader'), {
                            bundleName: bundlename,
                            paths: prefabPath,
                            ratio: 1
                          });

                        case 14:
                          prefab = bundle.get(prefabPath, Prefab);

                        case 15:
                          prefabNode = instantiate(prefab);
                          view = _this3.getDgflyUI(prefabNode);
                          view.prefabPath = prefabPath;
                          view.underModuleName = bundlename;

                          _this3._views.push(view);

                          view.onWillAddToParent();

                          _this3.layer_ui.addChild(view.node); // view.getComponent(UITransform).setContentSize(view.getVisibleSize());


                          view.onAddedToParent();
                          view.init();
                          model = moduleMgr.getModuleModel(bundlename);
                          model == null ? void 0 : model.addObserver(view);
                          data && view.onData(data);
                          if (lastView) lastView.node.active = false;
                          widget = view.node.getComponent(Widget);

                          if (widget) {
                            widget.target = _this3.layer_ui;
                          }

                          (_this3$hint2 = _this3.hint) == null ? void 0 : _this3$hint2.disableBlockEvent();
                          (_this3$hint3 = _this3.hint) == null ? void 0 : _this3$hint3.hideDarkMask();
                          resolve(view);

                        case 33:
                        case "end":
                          return _context3.stop();
                      }
                    }, _callee3);
                  }))));

                case 1:
                case "end":
                  return _context4.stop();
              }
            }, _callee4);
          }));

          function pushUI(_x6, _x7, _x8) {
            return _pushUI.apply(this, arguments);
          }

          return pushUI;
        }();

        _proto.backToPath = function backToPath(prefabPath) {
          var index = -1;

          for (index = this._views.length - 1; index >= 0; index--) {
            var _view2 = this._views[index];

            if (_view2.prefabPath && _view2.prefabPath === prefabPath) {
              break;
            }
          }

          if (index == -1) return;

          for (var i = index + 1; i < this._views.length; i++) {
            this.goBackView();
          }
        };

        _proto.goBackView = function goBackView(step) {
          if (step === void 0) {
            step = 1;
          }

          logger.log('backPreview', step);

          for (var i = 0; i < step; i++) {
            if (this._views.length == 0) break;
            var lastView = this._views[this._views.length - 1];
            var model = moduleMgr.getModuleModel(lastView.underModuleName);
            model == null ? void 0 : model.removeObserver(lastView);
            lastView.onWillRemoveFromParent();
            lastView.node.removeFromParent();
            lastView.onRemovedFromParent();
            lastView.deinit();
            lastView.node.destroy();
            this._views.length--;
            lastView = this._views[this._views.length - 1];
            lastView && (lastView.node.active = true);
          }
        };

        _proto.addWindow = /*#__PURE__*/function () {
          var _addWindow = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime().mark(function _callee5(prefabPath, data, bundleName, useTrans) {
            var _iterator3, _step3, win, bundlename, bundle, prefab, prefabNode, window0, model, uiOpacity, twObj, tw;

            return _regeneratorRuntime().wrap(function _callee5$(_context5) {
              while (1) switch (_context5.prev = _context5.next) {
                case 0:
                  if (useTrans === void 0) {
                    useTrans = true;
                  }

                  _iterator3 = _createForOfIteratorHelperLoose(this._windows);

                case 2:
                  if ((_step3 = _iterator3()).done) {
                    _context5.next = 9;
                    break;
                  }

                  win = _step3.value;

                  if (!(win.prefabPath == prefabPath)) {
                    _context5.next = 7;
                    break;
                  }

                  if (data) {
                    win.onData(data);
                  }

                  return _context5.abrupt("return");

                case 7:
                  _context5.next = 2;
                  break;

                case 9:
                  bundlename = bundleName || moduleMgr.runningModule.moduleName;
                  bundle = assetManager.getBundle(bundlename);
                  prefab = bundle.get(prefabPath, Prefab);

                  if (prefab) {
                    _context5.next = 16;
                    break;
                  }

                  _context5.next = 15;
                  return queueLoader.loadQueueAsync(this.hint.getLoader('littleLoader'), {
                    bundleName: bundlename,
                    paths: prefabPath,
                    ratio: 1
                  });

                case 15:
                  prefab = bundle.get(prefabPath, Prefab);

                case 16:
                  prefabNode = instantiate(prefab);
                  window0 = this.getDgflyUI(prefabNode);
                  window0.prefabPath = prefabPath;
                  window0.underModuleName = bundlename;

                  this._windows.push(window0);

                  window0.onWillAddToParent();
                  this.layer_window.addChild(window0.node);
                  window0.onAddedToParent();
                  window0.init();
                  model = moduleMgr.getModuleModel(bundlename);
                  model == null ? void 0 : model.addObserver(window0);
                  data && window0.onData(data);

                  if (useTrans) {
                    _context5.next = 30;
                    break;
                  }

                  return _context5.abrupt("return");

                case 30:
                  uiOpacity = window0.node.getComponent(UIOpacity) || window0.node.addComponent(UIOpacity);
                  uiOpacity.opacity = 50;
                  window0.node.setPosition(0, -200, 0);
                  twObj = {
                    y: -200,
                    opacity: 100
                  };
                  tw = tween(twObj);
                  tw.to(0.5, {
                    y: 0,
                    opacity: 255
                  }, {
                    easing: easing.sineInOut,
                    onUpdate: function onUpdate(target) {
                      if (!uiOpacity || !uiOpacity.isValid || !uiOpacity.node || !uiOpacity.node.isValid) {
                        tw.stop();
                        return;
                      }

                      uiOpacity.opacity = target.opacity;
                      window0.node.setPosition(0, target.y, 0);
                    }
                  }).start();

                case 36:
                case "end":
                  return _context5.stop();
              }
            }, _callee5, this);
          }));

          function addWindow(_x10, _x11, _x12, _x13) {
            return _addWindow.apply(this, arguments);
          }

          return addWindow;
        }()
        /*
        // 暂时不需要
        public async addWindowToCapture(Ctor: DgflyWindowCtor, data?: any, callback?: (window: IDgflyUI) => void) {
            const window: DgflyWindow = new Ctor();
             await window[func_load_prefab](window, null).catch(err => {
                throw err;
            });
             this.captureRoot.addChild(window.node);
             if (data) {
                window.data = data;
                window.onData(data);
            }
             if (callback) callback(window);
        }
        */
        ;

        _proto.removeWindow = function removeWindow(window0) {
          for (var i = this._windows.length - 1; i >= 0; i++) {
            if (this._windows[i] == window0) {
              this._windows.splice(i, 1);

              break;
            }
          }

          var model = moduleMgr.getModuleModel(window0.underModuleName);
          model == null ? void 0 : model.removeObserver(window0);
          window0.onWillRemoveFromParent();
          window0.node.removeFromParent();
          window0.onRemovedFromParent();
          window0.deinit();
          window0.node.destroy();
        };

        _proto.removeWindowByPath = function removeWindowByPath(prefabPath) {
          var windows = this.layer_window.getComponentsInChildren(DgflyWindow);

          for (var _iterator4 = _createForOfIteratorHelperLoose(windows), _step4; !(_step4 = _iterator4()).done;) {
            var win = _step4.value;

            if (win.prefabPath == prefabPath) {
              this.removeWindow(win);
              return;
            }
          }
        };

        _proto.removeAllWindow = function removeAllWindow() {
          for (var _iterator5 = _createForOfIteratorHelperLoose(this.layer_window.children), _step5; !(_step5 = _iterator5()).done;) {
            var win = _step5.value;
            var dgWin = this.getDgflyUI(win);
            var model = moduleMgr.getModuleModel(dgWin.underModuleName);
            model == null ? void 0 : model.removeObserver(dgWin);
            dgWin.onWillRemoveFromParent();
            dgWin.node.removeFromParent();
            dgWin.onRemovedFromParent();
            dgWin.deinit();
            win.destroy();
          }
          /*
          // 暂时不需要
          this.captureRoot.removeAllChildren();
          */


          this._windows.length = 0;
        };

        _proto.clearAllView = function clearAllView() {
          this._views.length = 0;
          this._windows.length = 0;
        };

        return ViewManager;
      }();

      var vm = exports('vm', new ViewManager());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/VirtualListItem.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, cclegacy, _decorator, Node, ccmodifier, BaseComponent;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Node = module.Node;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class, _class2, _descriptor;

      cclegacy._RF.push({}, "ed8bdWZRYhHrJ+Jjv+iA/h+", "VirtualListItem", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      /**
       * 单项渲染基类 T数据结构
       * @author slf
       *  */

      var VirtualListItem = exports('VirtualListItem', (_dec = ccclass('VirtualListItem'), _dec2 = ccmodifier('VirtualListItem'), _dec3 = property({
        displayName: '是否添加点击事件'
      }), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(VirtualListItem, _BaseComponent);

        function VirtualListItem() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "isClick", _descriptor, _assertThisInitialized(_this));
          /** 回调函数 */


          _this.callback = void 0;
          /** 回调作用域 */

          _this.cbThis = void 0;
          /** 数据结构 */

          _this._data = void 0;
          return _this;
        }

        var _proto = VirtualListItem.prototype;
        /**
         * 数据发生变化回调
         **/

        _proto.dataChanged = function dataChanged() {}
        /** 刷新数据 */
        ;

        _proto.refreshData = function refreshData() {
          this.dataChanged();
        }
        /** 销毁 */
        ;

        _proto.onDestroy = function onDestroy() {
          this._data = null;
        }
        /**
         * 设置点击回调
         * @param cb 回调函数
         * @param cbT 回调作用域
         */
        ;

        _proto.setTouchCallback = function setTouchCallback(cb, cbT) {
          this.callback = cb;
          this.cbThis = cbT;

          if (this.node) {
            if (this.node.hasEventListener(Node.EventType.TOUCH_END)) {
              this.node.off(Node.EventType.TOUCH_END, this.onClickCallback, this);
            }

            this.node.on(Node.EventType.TOUCH_END, this.onClickCallback, this);
          }
        }
        /**
         * 预制体点击回调
         * - 会携带数据
         * @param e
         */
        ;

        _proto.onClickCallback = function onClickCallback(e) {
          this.callback && this.callback.call(this.cbThis, this.data);
        };

        _createClass(VirtualListItem, [{
          key: "data",
          get: function get() {
            return this._data;
          },
          set: function set(v) {
            this._data = v;
            this.dataChanged();
          }
        }]);

        return VirtualListItem;
      }(BaseComponent), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "isClick", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return false;
        }
      }), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/VirtualListView.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './VirtualListItem.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Prefab, Layout, UITransform, Vec2, macro, instantiate, ScrollView, VirtualListItem;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Prefab = module.Prefab;
      Layout = module.Layout;
      UITransform = module.UITransform;
      Vec2 = module.Vec2;
      macro = module.macro;
      instantiate = module.instantiate;
      ScrollView = module.ScrollView;
    }, function (module) {
      VirtualListItem = module.VirtualListItem;
    }],
    execute: function () {
      var _dec, _dec2, _class, _class2, _descriptor;

      cclegacy._RF.push({}, "5fa94OoavRHDalt3efv99H5", "VirtualListView", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      /**
       * 虚拟滚动视图
       * - 扩展自 `ScrollView`
       * 渲染预制体必需挂载 `VirtualListItem` 子类
       * @author slf
       */

      var VirtualListView = exports('VirtualListView', (_dec = ccclass('VirtualListView'), _dec2 = property({
        type: Prefab,
        serializable: true,
        displayName: '渲染预制体'
      }), _dec(_class = (_class2 = /*#__PURE__*/function (_ScrollView) {
        _inheritsLoose(VirtualListView, _ScrollView);

        function VirtualListView() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _ScrollView.call.apply(_ScrollView, [this].concat(args)) || this;
          /** 渲染预制体必需挂载 `VirtualListItem` 组件 */

          _initializerDefineProperty(_this, "itemRenderer", _descriptor, _assertThisInitialized(_this));
          /** 子项点击回调函数 */


          _this.callback = void 0;
          /** 子项点击回调作用域 */

          _this.cbThis = void 0;
          /** 最大渲染预制体 垂直数量 */

          _this._verticalCount = void 0;
          /** 最大渲染预制体 水平数量 */

          _this._horizontalCount = void 0;
          /** 预制体宽 */

          _this._itemW = void 0;
          /** 预制体高 */

          _this._itemH = void 0;
          /** 预制体池 */

          _this._itemPool = void 0;
          /** 预制体列表 */

          _this._itemList = void 0;
          /** 预制体渲染类列表 */

          _this._itemRendererList = void 0;
          /** 数据列表 */

          _this._dataList = void 0;
          /** 开始坐标 */

          _this._startPos = void 0;
          /** 布局*/

          _this._contentLayout = void 0;
          /** 强制刷新 */

          _this._forcedRefresh = void 0;
          /** 刷新 */

          _this._refresh = void 0;
          /** UITransform */

          _this._uiTransform = void 0;
          return _this;
        }

        var _proto = VirtualListView.prototype;

        _proto.onLoad = function onLoad() {
          this._itemList = [];
          this._itemPool = [];
          this._itemRendererList = [];
          this._contentLayout = this.content.getComponent(Layout);
          this._contentLayout.enabled = false;
          this._uiTransform = this.node.getComponent(UITransform); // 起始位置

          var itemNode = this.itemRenderer.data.getComponent(UITransform);
          this._startPos = new Vec2(itemNode.width * itemNode.anchorX + this._contentLayout.paddingLeft, -(itemNode.height * itemNode.anchorY + this._contentLayout.paddingTop)); // 预制体宽高

          this._itemW = itemNode.width + this._contentLayout.spacingX;
          this._itemH = itemNode.height + this._contentLayout.spacingY; // 垂直、水平最大预制体数量

          this._horizontalCount = Math.ceil(this._uiTransform.width / this._itemW) + 1;
          this._verticalCount = Math.ceil(this._uiTransform.height / this._itemH) + 1;

          if (this._contentLayout.type == Layout.Type.GRID) {
            if (this._contentLayout.startAxis == Layout.AxisDirection.HORIZONTAL) {
              this._horizontalCount = Math.floor(this._uiTransform.width / this._itemW);
            } else {
              this._verticalCount = Math.floor(this._uiTransform.height / this._itemH);
            }
          }
        };

        _proto.onDestroy = function onDestroy() {
          this._dataList = null;
          this._itemList = null;
          this._itemRendererList = null;
          this.unschedule(this._refreshItem);
        }
        /** 利用 ScrollView 本身方法来标记滑动中 */
        ;

        _proto._setContentPosition = function _setContentPosition(position) {
          // @ts-ignore
          _ScrollView.prototype['_setContentPosition'].call(this, position);

          this._refresh = true;
        }
        /**
         * 设置列表子项点击回调
         * 回调会携带当前子项的 data
         * @param cb 回调
         * @param cbT 作用域
         */
        ;

        _proto.setTouchItemCallback = function setTouchItemCallback(cb, cbT) {
          this.callback = cb;
          this.cbThis = cbT;
        }
        /** 选中数据  */
        ;

        _proto._onItemTap = function _onItemTap(data) {
          this.callback && this.callback.call(this.cbThis, data);
        }
        /**
         * 刷新数据
         * @param data 数据源 单项|队列
         */
        ;

        _proto.refreshData = function refreshData(data) {
          if (Array.isArray(data)) {
            this._dataList = data;
          } else {
            this._dataList = [data];
          }

          this._addItem();

          this._refreshContentSize();

          this._forcedRefresh = true;
          this._refresh = true;
          this.unschedule(this._refreshItem);
          this.schedule(this._refreshItem, 0.1, macro.REPEAT_FOREVER);
        }
        /** 添加预制体 */
        ;

        _proto._addItem = function _addItem() {
          var len = 0;

          switch (this._contentLayout.type) {
            case Layout.Type.HORIZONTAL:
              len = this._horizontalCount;
              break;

            case Layout.Type.VERTICAL:
              len = this._verticalCount;
              break;

            case Layout.Type.GRID:
              len = this._horizontalCount * this._verticalCount;
              break;
          }

          len = Math.min(len, this._dataList.length);
          var itemListLen = this._itemList.length;

          if (itemListLen < len) {
            var itemRenderer = null;

            for (var i = itemListLen; i < len; i++) {
              var child = this._itemPool.length > 0 ? this._itemPool.shift() : instantiate(this.itemRenderer);
              this.content.addChild(child);

              this._itemList.push(child);

              itemRenderer = child.getComponent(VirtualListItem);

              this._itemRendererList.push(itemRenderer);

              if (itemRenderer.isClick) {
                itemRenderer.setTouchCallback(this._onItemTap, this);
              }
            }
          } else {
            var cL = this.content.children.length;
            var item;

            while (cL > len) {
              item = this._itemList[cL - 1];
              this.content.removeChild(item);

              this._itemList.splice(cL - 1, 1);

              this._itemRendererList.splice(cL - 1, 1);

              this._itemPool.push(item);

              cL = this.content.children.length;
            }
          }
        }
        /** 根据数据数量改变 content 宽高 */
        ;

        _proto._refreshContentSize = function _refreshContentSize() {
          var layout = this._contentLayout;
          var dataListLen = this._dataList.length;

          switch (this._contentLayout.type) {
            case Layout.Type.VERTICAL:
              this.content.getComponent(UITransform).height = layout.paddingTop + dataListLen * this._itemH + layout.paddingBottom;
              break;

            case Layout.Type.HORIZONTAL:
              this.content.getComponent(UITransform).width = layout.paddingLeft + dataListLen * this._itemW + layout.paddingRight;
              break;

            case Layout.Type.GRID:
              if (this._contentLayout.startAxis == Layout.AxisDirection.HORIZONTAL) {
                this.content.getComponent(UITransform).height = layout.paddingTop + Math.ceil(dataListLen / this._horizontalCount) * this._itemH + layout.paddingBottom;
              } else if (this._contentLayout.startAxis == Layout.AxisDirection.VERTICAL) {
                this.content.getComponent(UITransform).width = layout.paddingLeft + Math.ceil(dataListLen / this._verticalCount) * this._itemW + layout.paddingRight;
              }

              break;
          }
        }
        /** 刷新预制体位置和数据填充 */
        ;

        _proto._refreshItem = function _refreshItem() {
          if (!this._refresh) {
            return;
          }

          switch (this._contentLayout.type) {
            case Layout.Type.HORIZONTAL:
              this._refreshHorizontal();

              break;

            case Layout.Type.VERTICAL:
              this._refreshVertical();

              break;

            case Layout.Type.GRID:
              this._refreshGrid();

              break;
          }

          this._refresh = false;
          this._forcedRefresh = false;
        }
        /** 刷新水平 */
        ;

        _proto._refreshHorizontal = function _refreshHorizontal() {
          var start = Math.floor(Math.abs(this.content.position.x) / this._itemW);

          if (start < 0 || this.content.position.x > 0) {
            //超出边界处理
            start = 0;
          }

          var end = start + this._horizontalCount;

          if (end > this._dataList.length) {
            //超出边界处理
            end = this._dataList.length;
            start = Math.max(end - this._horizontalCount, 0);
          }

          var tempV = 0;
          var itemListLen = this._itemList.length;
          var item, pos, idx;

          for (var i = 0; i < itemListLen; i++) {
            idx = (start + i) % itemListLen;
            item = this._itemList[idx];
            pos = item.getPosition();
            tempV = this._startPos.x + (start + i) * this._itemW;

            if (pos.x != tempV || this._forcedRefresh) {
              console.log('修改的数据=' + (start + i));
              pos.x = tempV;
              item.position = pos;
              this._itemRendererList[idx].data = this._dataList[start + i];
            }
          }
        }
        /** 刷新垂直 */
        ;

        _proto._refreshVertical = function _refreshVertical() {
          var start = Math.floor(Math.abs(this.content.position.y) / this._itemH);

          if (start < 0 || this.content.position.y < 0) {
            start = 0;
          }

          var end = start + this._verticalCount;

          if (end > this._dataList.length) {
            end = this._dataList.length;
            start = Math.max(end - this._verticalCount, 0);
          }

          var tempV = 0;
          var itemListLen = this._itemList.length;
          var item, pos, idx;

          for (var i = 0; i < itemListLen; i++) {
            idx = (start + i) % itemListLen;
            item = this._itemList[idx];
            pos = item.getPosition();
            tempV = this._startPos.y + -(start + i) * this._itemH;

            if (pos.y != tempV || this._forcedRefresh) {
              console.log('修改的数据=' + (start + i));
              pos.y = tempV;
              item.position = pos;
              this._itemRendererList[idx].data = this._dataList[start + i];
            }
          }
        }
        /** 刷新网格 */
        ;

        _proto._refreshGrid = function _refreshGrid() {
          //是否垂直方向 添加网格
          var isVDirection = this._contentLayout.startAxis == Layout.AxisDirection.VERTICAL;

          var start = Math.floor(Math.abs(this.content.position.y) / this._itemH) * this._horizontalCount;

          if (isVDirection) {
            start = Math.floor(Math.abs(this.content.position.x) / this._itemW) * this._verticalCount;

            if (this.content.position.x > 0) {
              start = 0;
            }
          } else if (this.content.position.y < 0) {
            start = 0;
          }

          if (start < 0) {
            start = 0;
          }

          var end = start + this._horizontalCount * this._verticalCount;

          if (end > this._dataList.length) {
            end = this._dataList.length;
            start = Math.max(end - this._horizontalCount * this._verticalCount, 0);
          }

          var tempX = 0;
          var tempY = 0;
          var itemListLen = this._itemList.length;
          var item, pos, idx;

          for (var i = 0; i < itemListLen; i++) {
            idx = (start + i) % itemListLen;
            item = this._itemList[idx];
            pos = item.getPosition();

            if (isVDirection) {
              tempX = this._startPos.x + Math.floor((start + i) / this._verticalCount) * this._itemW;
              tempY = this._startPos.y + -((start + i) % this._verticalCount) * this._itemH;
            } else {
              tempX = this._startPos.x + (start + i) % this._horizontalCount * this._itemW;
              tempY = this._startPos.y + -Math.floor((start + i) / this._horizontalCount) * this._itemH;
            }

            if (pos.y != tempY || pos.x != tempX || this._forcedRefresh) {
              console.log('修改的数据=' + (start + i));
              pos.x = tempX;
              pos.y = tempY;
              item.position = pos;
              this._itemRendererList[idx].data = this._dataList[start + i];
            }
          }
        };

        return VirtualListView;
      }(ScrollView), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "itemRenderer", [_dec2], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: function initializer() {
          return null;
        }
      }), _class2)) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/Web.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Module.ts', './Logger.ts', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, WebView, Module, logger, ccmodifier;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      WebView = module.WebView;
    }, function (module) {
      Module = module.Module;
    }, function (module) {
      logger = module.logger;
    }, function (module) {
      ccmodifier = module.ccmodifier;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _class, _class2, _descriptor;

      cclegacy._RF.push({}, "98817enDoBCuZBJaOaphYA5", "Web", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var Web = exports('Web', (_dec = ccclass('Web'), _dec2 = ccmodifier('Web'), _dec3 = property(WebView), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_Module) {
        _inheritsLoose(Web, _Module);

        function Web() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _Module.call.apply(_Module, [this].concat(args)) || this;
          _this.moduleName = 'Web';

          _initializerDefineProperty(_this, "webView", _descriptor, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = Web.prototype;

        _proto.onLoad = function onLoad() {};

        _proto.getHttpCallback = function getHttpCallback() {
          return null;
        };

        _proto.onLaunched = function onLaunched(data, originModuleName) {
          logger.log('onLaunched', this.moduleName, originModuleName);
          this.webView.url = data.url;
        };

        return Web;
      }(Module), _descriptor = _applyDecoratedDescriptor(_class2.prototype, "webView", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

(function(r) {
  r('virtual:///prerequisite-imports/main', 'chunks:///_virtual/main'); 
})(function(mid, cid) {
    System.register(mid, [cid], function (_export, _context) {
    return {
        setters: [function(_m) {
            var _exportObj = {};

            for (var _key in _m) {
              if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _m[_key];
            }
      
            _export(_exportObj);
        }],
        execute: function () { }
    };
    });
});