System.register("chunks:///_virtual/Egg.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './GlobalEvent.ts', './UtilTools.ts', './StrikesExports.ts'], function (exports) {
  var _inheritsLoose, cclegacy, _decorator, tween, v3, instantiate, ccmodifier, BaseComponent, GlobalEvent, UtilTools, EggPosition, MoveSteps, StrikesEventType, BeltOffset, WhichBelt;

  return {
    setters: [function (module) {
      _inheritsLoose = module.inheritsLoose;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      tween = module.tween;
      v3 = module.v3;
      instantiate = module.instantiate;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      GlobalEvent = module.GlobalEvent;
    }, function (module) {
      UtilTools = module.UtilTools;
    }, function (module) {
      EggPosition = module.EggPosition;
      MoveSteps = module.MoveSteps;
      StrikesEventType = module.StrikesEventType;
      BeltOffset = module.BeltOffset;
      WhichBelt = module.WhichBelt;
    }],
    execute: function () {
      var _dec, _dec2, _class;

      cclegacy._RF.push({}, "739d4aYudxJaK7nykW0/VlX", "Egg", undefined);

      var ccclass = _decorator.ccclass;
      /**
       * 金色蛋蛋
       */

      var Egg = exports('Egg', (_dec = ccclass('Egg'), _dec2 = ccmodifier('Egg'), _dec(_class = _dec2(_class = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(Egg, _BaseComponent);

        function Egg() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;
          _this.pool = void 0;
          _this.belt = WhichBelt.Top;
          _this.moves = 0;
          return _this;
        }

        var _proto = Egg.prototype;
        /**
         * 初始化
         * @param belt 位于哪条传送带
         * @param pool 蛋蛋对象池
         */

        _proto.init = function init(belt, pool) {
          this.pool = pool;
          this.belt = belt;
          this.moves = 0;
          var _EggPosition$belt = EggPosition[belt],
              x = _EggPosition$belt[0],
              y = _EggPosition$belt[1];
          this.node.setPosition(x, y);
        }
        /**
         * 移动
         * @param duration 移动时间
         */
        ;

        _proto.move = function move(duration) {
          this.moves++;
          var steps = MoveSteps[this.belt];

          if (this.moves > steps) {
            this.pool.put(this);
            this.moves = 0;
            GlobalEvent.emit(StrikesEventType.GameOver);
          }

          var _BeltOffset$this$belt = BeltOffset[this.belt],
              xf = _BeltOffset$this$belt[0],
              yf = _BeltOffset$this$belt[1];
          tween(this.node).by(duration, {
            position: v3(xf, yf)
          }).start();
        };

        _proto.reset = function reset() {
          this.moves = 0;
        };

        return Egg;
      }(BaseComponent)) || _class) || _class));
      /**
       * 蛋蛋对象池
       */

      var EggPool = exports('EggPool', /*#__PURE__*/function () {
        function EggPool() {
          this._eggs = void 0;
          this._template = void 0;
        }

        var _proto2 = EggPool.prototype;
        /**
         * 初始化
         * @param node 蛋蛋节点
         */

        _proto2.init = function init(node) {
          this._template = node;
          this._eggs = [];

          for (var i = 0; i < 10; i++) {
            this._eggs[i] = this._new();
          }
        }
        /** 创建一个全新的蛋蛋 */
        ;

        _proto2._new = function _new() {
          var node = instantiate(this._template);
          node.active = true;
          return UtilTools.setupComponent(node, Egg);
        }
        /** 从对象池中拿走一个蛋蛋 */
        ;

        _proto2.get = function get() {
          if (this._eggs.length > 0) {
            return this._eggs.pop();
          }

          return this._new();
        }
        /**
         * 把蛋蛋放回对象池
         * @param egg 蛋蛋
         */
        ;

        _proto2.put = function put(egg) {
          egg.node.removeFromParent();

          this._eggs.push(egg);
        }
        /** 销毁对象池 */
        ;

        _proto2.destroy = function destroy() {
          this._eggs.forEach(function (egg) {
            return egg.node.destroy();
          });

          this._eggs.length = 0;
        };

        return EggPool;
      }());

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/SG_Strikes", ['./StrikesExports.ts', './Egg.ts', './StrikesGameUI.ts', './StrikesHelpWindow.ts', './StrikesHomeUI.ts', './StrikesMissWindow.ts', './StrikesRankItem.ts', './StrikesRankWindow.ts', './StrikesResultWindow.ts'], function () {
  return {
    setters: [null, null, null, null, null, null, null, null, null],
    execute: function () {}
  };
});

System.register("chunks:///_virtual/StrikesExports.ts", ['cc'], function (exports) {
  var cclegacy;
  return {
    setters: [function (module) {
      cclegacy = module.cclegacy;
    }],
    execute: function () {
      var _MoveSteps, _EggPosition, _BeltDirection, _BeltOffset, _HammerDirection;

      cclegacy._RF.push({}, "7ba47c0dCtGeLDD/28uMYdS", "StrikesExports", undefined);
      /** 传送带类型 */


      var WhichBelt = exports('WhichBelt', /*#__PURE__*/function (WhichBelt) {
        WhichBelt[WhichBelt["Top"] = 0] = "Top";
        WhichBelt[WhichBelt["Left"] = 1] = "Left";
        WhichBelt[WhichBelt["Bottom"] = 2] = "Bottom";
        WhichBelt[WhichBelt["Right"] = 3] = "Right";
        return WhichBelt;
      }({}));
      /** 传送带配置 */

      var TimeSteps = exports('TimeSteps', [// 从 0 秒开始，每隔 1 秒出现一个金蛋，传送带前进步幅为 1
      [0, 1, 1], // 从 30 秒开始，每隔 0.7 秒出现一个金蛋，传送带前进步幅为 2
      [30, 0.7, 2], // 从 30 秒开始，每隔 0.4 秒出现一个金蛋，传送带前进步幅为 3
      [60, 0.4, 3]]);
      /** 金蛋可移动的步数 */

      var MoveSteps = exports('MoveSteps', (_MoveSteps = {}, _MoveSteps[WhichBelt.Top] = 3, _MoveSteps[WhichBelt.Left] = 6, _MoveSteps[WhichBelt.Bottom] = 3, _MoveSteps[WhichBelt.Right] = 6, _MoveSteps));
      /** 金蛋初始位置 */

      var EggPosition = exports('EggPosition', (_EggPosition = {}, _EggPosition[WhichBelt.Top] = [0, 410], _EggPosition[WhichBelt.Left] = [-644, 54], _EggPosition[WhichBelt.Bottom] = [0, -360], _EggPosition[WhichBelt.Right] = [644, 54], _EggPosition));
      /** 传送带方向 */

      var BeltDirection = exports('BeltDirection', (_BeltDirection = {}, _BeltDirection[WhichBelt.Top] = '↑', _BeltDirection[WhichBelt.Left] = '←', _BeltDirection[WhichBelt.Bottom] = '↓', _BeltDirection[WhichBelt.Right] = '→', _BeltDirection));
      /** 传送带偏移 */

      var BeltOffset = exports('BeltOffset', (_BeltOffset = {}, _BeltOffset[WhichBelt.Top] = [0, -97], _BeltOffset[WhichBelt.Left] = [93, 0], _BeltOffset[WhichBelt.Bottom] = [0, 97], _BeltOffset[WhichBelt.Right] = [-93, 0], _BeltOffset));
      /** 锤子方向 */

      var HammerDirection = exports('HammerDirection', (_HammerDirection = {}, _HammerDirection[WhichBelt.Top] = 1, _HammerDirection[WhichBelt.Left] = 4, _HammerDirection[WhichBelt.Bottom] = 3, _HammerDirection[WhichBelt.Right] = 2, _HammerDirection));
      /** 敲金蛋事件 */

      var StrikesEventType = exports('StrikesEventType', /*#__PURE__*/function (StrikesEventType) {
        StrikesEventType["GameOver"] = "game-over";
        StrikesEventType["GameRelive"] = "game-relive";
        StrikesEventType["GameQuit"] = "game-quit";
        StrikesEventType["GameRestart"] = "game-restart";
        return StrikesEventType;
      }({}));
      /** 每条传送带上同时最多允许存在的金蛋个数 */

      var MaxAllowNum = exports('MaxAllowNum', 2);
      /** 游戏倒计时 */

      var StrikesCountdown = exports('StrikesCountdown', 3);
      /** 排行榜数据-一条 */

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StrikesGameUI.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './View.ts', './ButtonDecorator.ts', './GlobalEvent.ts', './ViewManager.ts', './ArrayUtils.ts', './Logger.ts', './StorageData.ts', './StrikesExports.ts', './Egg.ts', './Modifier.ts'], function (exports) {
  var _createClass, _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Button, Node, Label, sp, input, Input, tween, UIOpacity, KeyCode, v3, DgflyUI, buttonDecorator, GlobalEvent, vm, pickElementFrom, logger, storageData, WhichBelt, StrikesEventType, MaxAllowNum, HammerDirection, BeltOffset, TimeSteps, StrikesCountdown, EggPool, Egg, ccmodifier;

  return {
    setters: [function (module) {
      _createClass = module.createClass;
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Button = module.Button;
      Node = module.Node;
      Label = module.Label;
      sp = module.sp;
      input = module.input;
      Input = module.Input;
      tween = module.tween;
      UIOpacity = module.UIOpacity;
      KeyCode = module.KeyCode;
      v3 = module.v3;
    }, function (module) {
      DgflyUI = module.DgflyUI;
    }, function (module) {
      buttonDecorator = module.buttonDecorator;
    }, function (module) {
      GlobalEvent = module.GlobalEvent;
    }, function (module) {
      vm = module.vm;
    }, function (module) {
      pickElementFrom = module.pickElementFrom;
    }, function (module) {
      logger = module.logger;
    }, function (module) {
      storageData = module.storageData;
    }, function (module) {
      WhichBelt = module.WhichBelt;
      StrikesEventType = module.StrikesEventType;
      MaxAllowNum = module.MaxAllowNum;
      HammerDirection = module.HammerDirection;
      BeltOffset = module.BeltOffset;
      TimeSteps = module.TimeSteps;
      StrikesCountdown = module.StrikesCountdown;
    }, function (module) {
      EggPool = module.EggPool;
      Egg = module.Egg;
    }, function (module) {
      ccmodifier = module.ccmodifier;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _dec11, _dec12, _dec13, _dec14, _dec15, _dec16, _dec17, _dec18, _dec19, _dec20, _dec21, _dec22, _dec23, _dec24, _dec25, _dec26, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6, _descriptor7, _descriptor8, _descriptor9, _descriptor10, _descriptor11, _descriptor12, _descriptor13, _descriptor14, _descriptor15, _descriptor16, _descriptor17, _descriptor18, _descriptor19, _descriptor20, _descriptor21;

      cclegacy._RF.push({}, "d0904b68ilAqrzCkJQNLxaw", "StrikesGameUI", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      /**
       * 传送带
       */

      var Belt = exports('Belt', /*#__PURE__*/function () {
        /** 传送带类型 */

        /** 距离上次放置金蛋后移动的格数 */
        function Belt(which) {
          this._which = WhichBelt.Top;
          this._moves = 0;
          this._which = which;
          this._moves = 0;
        }
        /** 传送带类型 */


        var _proto = Belt.prototype;
        /** 移动1格 */

        _proto.move = function move() {
          --this._moves;
        }
        /** 放置金蛋 */
        ;

        _proto.put = function put() {
          this._moves = 3;
        };

        _createClass(Belt, [{
          key: "which",
          get: function get() {
            return this._which;
          }
          /** 是否可以产生金蛋 */

        }, {
          key: "can",
          get: function get() {
            return this._moves <= 0;
          }
          /** 移动距离 */

        }, {
          key: "moves",
          get: function get() {
            return this._moves;
          }
        }]);

        return Belt;
      }());
      /**
       * 游戏逻辑控制
       */

      var GameLogic = /*#__PURE__*/function () {
        /** 观察者 */

        /** 传送带们 */

        /** 累计时间 */

        /** 时间片 */

        /** 时间进度 */

        /** 得分 */

        /** 暂停状态 */
        function GameLogic() {
          this._observer = void 0;
          this._belts = void 0;
          this._time = 0;
          this._piece = 0;
          this._step = 0;
          this._score = 0;
          this._paused = false;
          this._belts = [new Belt(WhichBelt.Top), new Belt(WhichBelt.Left), new Belt(WhichBelt.Bottom), new Belt(WhichBelt.Right)];
        }
        /**
         * 设置目标
         * @param ob 目标
         */


        var _proto2 = GameLogic.prototype;

        _proto2.setTarget = function setTarget(ob) {
          this._observer = ob;
        }
        /** 重置数据 */
        ;

        _proto2.reset = function reset() {
          this._time = 0;
          this._score = 0;
          this._piece = 0;
          this._step = 0;
          this._paused = true;

          this._observer.refreshData();

          this._observer.onReset();
        }
        /** 开始 */
        ;

        _proto2.start = function start() {
          this._paused = false;
        }
        /** 暂停 */
        ;

        _proto2.pause = function pause() {
          this._paused = true;
        }
        /** 当前运行时间 */
        ;
        /** 增加得分 */


        _proto2.addScore = function addScore() {
          ++this._score;
        }
        /** 当前运行状态-暂停否 */
        ;
        /** 移动一格回调 */


        _proto2.onTriggered = function onTriggered() {
          this._observer.nextEggAt();

          this._observer.move();
        }
        /** 帧刷新 */
        ;

        _proto2.onStep = function onStep() {
          this._observer.refreshData();
        }
        /** 加速 */
        ;

        _proto2.onSpeedUp = function onSpeedUp() {
          this._observer.onSpeedUp();
        }
        /** 帧刷新 */
        ;

        _proto2.update = function update(dt) {
          if (this._paused || !this._observer) return;
          this._time += dt;
          var from = 0;
          var interval = 0;

          for (var i = TimeSteps.length - 1; i >= 0; i--) {
            var _TimeSteps$i = TimeSteps[i];
            from = _TimeSteps$i[0];
            interval = _TimeSteps$i[1];

            if (this._time >= from) {
              if (i > this._step) {
                this.onSpeedUp();
              }

              this._step = i;
              break;
            }
          }

          this.onStep();

          if (this._time - this._piece >= interval) {
            this._piece = this._time;
            this.onTriggered();
          }
        };

        _createClass(GameLogic, [{
          key: "time",
          get: function get() {
            return this._time >> 0;
          }
          /** 当前得分 */

        }, {
          key: "score",
          get: function get() {
            return this._score;
          }
        }, {
          key: "paused",
          get: function get() {
            return this._paused;
          }
          /** 当前运行状态-正在运行否 */

        }, {
          key: "running",
          get: function get() {
            return !this._paused;
          }
          /** 当前运行速度 */

        }, {
          key: "interval",
          get: function get() {
            return TimeSteps[this._step][1];
          }
        }]);

        return GameLogic;
      }();
      /**
       * 敲金蛋游戏页面
       */


      var StrikesGameUI = exports('StrikesGameUI', (_dec = ccclass('StrikesGameUI'), _dec2 = ccmodifier('StrikesGameUI'), _dec3 = buttonDecorator.inject('onBack'), _dec4 = property(Button), _dec5 = buttonDecorator.inject('onResume'), _dec6 = property(Button), _dec7 = buttonDecorator.inject('onStop'), _dec8 = property(Button), _dec9 = property(Node), _dec10 = property(Label), _dec11 = property(Label), _dec12 = property(Node), _dec13 = property(Node), _dec14 = property(Node), _dec15 = property(Node), _dec16 = property(Node), _dec17 = property(Node), _dec18 = property(Node), _dec19 = property(Node), _dec20 = property(Node), _dec21 = property({
        type: sp.Skeleton,
        displayName: '锤子待机动画'
      }), _dec22 = property({
        type: sp.Skeleton,
        displayName: '锤子捶下动画'
      }), _dec23 = property({
        type: sp.Skeleton,
        displayName: '金蛋破碎动画-上'
      }), _dec24 = property({
        type: sp.Skeleton,
        displayName: '金蛋破碎动画-下'
      }), _dec25 = property({
        type: sp.Skeleton,
        displayName: '金蛋破碎动画-左'
      }), _dec26 = property({
        type: sp.Skeleton,
        displayName: '金蛋破碎动画-右'
      }), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_DgflyUI) {
        _inheritsLoose(StrikesGameUI, _DgflyUI);

        function StrikesGameUI() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _DgflyUI.call.apply(_DgflyUI, [this].concat(args)) || this;
          _this.gameLogic = new GameLogic();

          _initializerDefineProperty(_this, "btnClose", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnResume", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnStop", _descriptor3, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "touchNode", _descriptor4, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "labScore", _descriptor5, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "labTime", _descriptor6, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "countdownNode", _descriptor7, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "speedUpNode", _descriptor8, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "pauseNode", _descriptor9, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "beltTop", _descriptor10, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "beltBottom", _descriptor11, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "beltLeft", _descriptor12, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "beltRight", _descriptor13, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "eggTemplate", _descriptor14, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "eggsNode", _descriptor15, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "aniHammerIdle", _descriptor16, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "aniHammerDrop", _descriptor17, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "aniEggStrikedTop", _descriptor18, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "aniEggStrikedBottom", _descriptor19, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "aniEggStrikedLeft", _descriptor20, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "aniEggStrikedRight", _descriptor21, _assertThisInitialized(_this));
          /** 金蛋对象池 */


          _this._eggPool = void 0;
          /** 上次敲击时间记录 */

          _this._lastStrike = void 0;
          /** 上次按步调试时间记录 */

          _this._lastDebugClick = void 0;
          /** 是否开启按步调试 */

          _this._debug = false;
          /** 是否正在移动 */

          _this._moving = false;
          /** 是否开启 AI */

          _this._aiEnabled = false;
          /** 是否可以复活 */

          _this._canRelive = true;
          return _this;
        }

        var _proto3 = StrikesGameUI.prototype;

        _proto3.start = function start() {
          this.gameLogic.setTarget(this);
          this.gameLogic.reset();
          this.startGame();
        };

        _proto3.onEnable = function onEnable() {
          this._eggPool = new EggPool();

          this._eggPool.init(this.eggTemplate);

          this.touchNode.on(Node.EventType.TOUCH_END, this._onSlide, this);
          input.on(Input.EventType.KEY_UP, this._onKeyPressed, this);
          GlobalEvent.on(StrikesEventType.GameOver, this._onGameOver, this);
          GlobalEvent.on(StrikesEventType.GameRelive, this._onGameRelive, this);
          GlobalEvent.on(StrikesEventType.GameQuit, this._onGameQuit, this);
          GlobalEvent.on(StrikesEventType.GameRestart, this._onGameRestart, this);
        };

        _proto3.onDisable = function onDisable() {
          this._eggPool.destroy();

          this.touchNode.off(Node.EventType.TOUCH_END, this._onSlide, this);
          input.off(Input.EventType.KEY_UP, this._onKeyPressed, this);
          GlobalEvent.off(StrikesEventType.GameOver, this._onGameOver, this);
          GlobalEvent.off(StrikesEventType.GameRelive, this._onGameRelive, this);
          GlobalEvent.off(StrikesEventType.GameRestart, this._onGameRestart, this);
        }
        /** 开始游戏 */
        ;

        _proto3.startGame = function startGame() {
          var _this2 = this;

          var data = {
            times: StrikesCountdown
          };

          var onStart = function onStart() {
            if (!_this2 || !_this2.isValid) return;

            _this2.countdownNode.children.forEach(function (v, i) {
              v.active = i + 1 == data.times;
            });
          };

          var onComplete = function onComplete() {
            if (!_this2 || !_this2.isValid) return;

            _this2.countdownNode.children.forEach(function (v, i) {
              v.active = i + 1 == data.times;
            });

            if (data.times <= 0) {
              _this2.countdownNode.children.forEach(function (v, i) {
                v.active = false;
              });

              _this2.gameLogic.start();
            }
          };

          tween(data).by(1, {
            times: -1
          }, {
            onStart: onStart,
            onComplete: onComplete
          }).repeat(data.times).start();
        }
        /** 正在移动否 */
        ;
        /** 游戏加速回调 */


        _proto3.onSpeedUp = function onSpeedUp() {
          this.speedUpNode.active = true;
          var comp = this.speedUpNode.getComponent(UIOpacity);
          comp.opacity = 0;
          tween(comp).to(0.5, {
            opacity: 255
          }).delay(1).to(0.5, {
            opacity: 0
          }).start();
        }
        /** 游戏重置 */
        ;

        _proto3.onReset = function onReset() {
          this._lastStrike = 0;
          this._lastDebugClick = 0;
          this._moving = false;
          this._canRelive = true;
          this.aniHammerIdle.node.active = true;
          this.aniEggStrikedTop.node.active = false;
          this.aniEggStrikedBottom.node.active = false;
          this.aniEggStrikedLeft.node.active = false;
          this.aniEggStrikedRight.node.active = false;
          this.aniHammerDrop.node.active = false;
          this.speedUpNode.active = false;
          this.collectEggs();
        }
        /** 游戏暂停 */
        ;

        _proto3.onPause = function onPause() {
          this.pauseNode.active = true;
        }
        /** 游戏恢复 */
        ;

        _proto3.onResume = function onResume() {
          this.pauseNode.active = false;
          this.startGame();
        }
        /** 游戏结束-直接结算 */
        ;

        _proto3.onStop = function onStop() {
          this.pauseNode.active = false;
          storageData.set('strikes_score', this.gameLogic.score);
          vm.addWindow('prefab/StrikesResultWindow', null, this.underModuleName);
        }
        /** 返回 */
        ;

        _proto3.onBack = function onBack() {
          if (this.gameLogic.running) {
            this.gameLogic.pause();
            this.onPause();
          } else {
            vm.goBackView();
          }
        }
        /** 回收金蛋 */
        ;

        _proto3.collectEggs = function collectEggs() {
          var children = this.eggsNode.children;

          for (var i = children.length - 1; i >= 0; i--) {
            this._eggPool.put(children[i].getComponent(Egg));
          }
        }
        /** 生成金蛋 */
        ;

        _proto3.nextEggAt = function nextEggAt() {
          var _eggNums;
          /** 用于统计每条带上的金蛋数量 */


          var eggNums = (_eggNums = {}, _eggNums[WhichBelt.Top] = 0, _eggNums[WhichBelt.Bottom] = 0, _eggNums[WhichBelt.Left] = 0, _eggNums[WhichBelt.Right] = 0, _eggNums);
          var eggs = this.eggsNode.children.slice();
          var canTB = true;
          var egg;

          for (var i = 0; i < eggs.length; i++) {
            egg = eggs[i].getComponent(Egg);
            eggNums[egg.belt]++;

            if ((egg.belt === WhichBelt.Left || egg.belt === WhichBelt.Right) && egg.moves === 3) {
              // 这种情况下，是不允许在上下传送带生成金蛋的
              canTB = false;
            }
          }

          var canLeft = eggNums[WhichBelt.Left] < MaxAllowNum;
          var canRight = eggNums[WhichBelt.Right] < MaxAllowNum;
          var canTop = canTB && eggNums[WhichBelt.Top] < MaxAllowNum;
          var canBottom = canTB && eggNums[WhichBelt.Bottom] < MaxAllowNum;
          var directions = [];
          canLeft && directions.push(WhichBelt.Left);
          canRight && directions.push(WhichBelt.Right);
          canTop && directions.push(WhichBelt.Top);
          canBottom && directions.push(WhichBelt.Bottom);

          if (directions.length > 0) {
            var which = pickElementFrom(directions); // logger.d(eggNums);
            // logger.d(`可以传送带生成金蛋吗？`);
            // logger.d(`左：${canLeft ? '可以' : '不可以'} 右：${canRight ? '可以' : '不可以'}`);
            // logger.d(`上：${canTop ? '可以' : '不可以'} 下：${canBottom ? '可以' : '不可以'}`);
            // logger.d(`在传送带 ${BeltDirection[which]} 上生成了金蛋`);

            this.newEggAt(which);
          }
        }
        /** 游戏结束回调 */
        ;

        _proto3._onGameOver = function _onGameOver() {
          logger.d('游戏结束！');
          this.gameLogic.pause();
          storageData.set('strikes_score', this.gameLogic.score);

          if (this._canRelive) {
            vm.addWindow('prefab/StrikesMissWindow', null, this.underModuleName);
          } else {
            vm.addWindow('prefab/StrikesResultWindow', null, this.underModuleName);
          }
        }
        /** 游戏复活回调 */
        ;

        _proto3._onGameRelive = function _onGameRelive() {
          this._canRelive = false;
          this.collectEggs();
          this.startGame();
        }
        /** 游戏退出回调 */
        ;

        _proto3._onGameQuit = function _onGameQuit() {
          vm.goBackView();
        }
        /** 游戏重开回调 */
        ;

        _proto3._onGameRestart = function _onGameRestart() {
          this.gameLogic.reset();
          this.startGame();
        }
        /** 键盘监听回调 */
        ;

        _proto3._onKeyPressed = function _onKeyPressed(e) {
          var code = e.keyCode;

          switch (code) {
            case KeyCode.ARROW_LEFT:
              this._slideTo(WhichBelt.Left);

              break;

            case KeyCode.ARROW_RIGHT:
              this._slideTo(WhichBelt.Right);

              break;

            case KeyCode.ARROW_DOWN:
              this._slideTo(WhichBelt.Bottom);

              break;

            case KeyCode.ARROW_UP:
              this._slideTo(WhichBelt.Top);

              break;

            case KeyCode.BACKSPACE:
              this._debug = false;
              vm.hint.addTip('单步调试已关闭');
              break;

            case KeyCode.SPACE:
              if (!this._debug) vm.hint.addTip('单步调试已开启');
              this._debug = true;
              this.debugNextStep();
              break;

            case KeyCode.KEY_A:
              this._aiEnabled = !this._aiEnabled;
              vm.hint.addTip('AI 锤子已' + (this._aiEnabled ? '开启' : '关闭'));
              break;
          }
        }
        /** 刷新数据 */
        ;

        _proto3.refreshData = function refreshData() {
          this.labTime.string = (this.gameLogic.time >> 0) + "s";
          this.labScore.string = "" + this.gameLogic.score;
        }
        /** 锤子滑动监听回调 */
        ;

        _proto3._onSlide = function _onSlide(touch) {
          if (this.gameLogic.paused) return;
          var start = touch.getStartLocation();
          var ended = touch.getLocation();
          var _x$y = {
            x: ended.x - start.x,
            y: ended.y - start.y
          },
              x = _x$y.x,
              y = _x$y.y;
          var offset = 30;

          if (x > offset && y <= offset) {
            this._slideTo(WhichBelt.Right);
          } else if (x < -offset && y <= offset) {
            this._slideTo(WhichBelt.Left);
          } else if (x <= offset && y >= offset) {
            this._slideTo(WhichBelt.Top);
          } else if (x <= offset && y <= -offset) {
            this._slideTo(WhichBelt.Bottom);
          }
        }
        /** 挥动锤子 */
        ;

        _proto3._slideTo = function _slideTo(which) {
          var _this3 = this;

          var now = Date.now();
          /** 用于限制锤子挥动次数 */

          var diff = now - this._lastStrike >= this.gameLogic.interval * 1000 * 0.5;

          if (diff) {
            this._lastStrike = now;
            this.aniHammerIdle.node.active = false;
            this.aniHammerDrop.node.active = true;
            this.aniHammerDrop.clearAnimation();
            var ani = "attack" + HammerDirection[which];
            var track = this.aniHammerDrop.setAnimation(0, ani, false);
            this.aniHammerDrop.setTrackCompleteListener(track, function () {
              _this3.aniHammerDrop.clearTracks();

              _this3.aniHammerDrop.node.active = false;
              _this3.aniHammerIdle.node.active = true;
            });
            track.timeScale = Math.ceil(1 / this.gameLogic.interval * 10) / 10; // logger.d(`锤子挥向 ${BeltDirection[which]} 速度：${track.timeScale}`);
            // 敲金蛋目标检测

            this.scheduleOnce(function () {
              if (_this3.gameLogic.paused) return; // 检查该方向的传送带上有没有金蛋

              _this3._checkTargeted(which);
            }, this.gameLogic.interval * 0.5);
          } else {
            logger.w('锤子动作还没播放完呢！', this.gameLogic.interval);
          }
        }
        /** 检测敲击目标 */
        ;

        _proto3._checkTargeted = function _checkTargeted(which) {
          var eggs = this.eggsNode.children.slice();
          var egg;
          var target;

          for (var i = 0; i < eggs.length; i++) {
            egg = eggs[i].getComponent(Egg);

            if (egg.belt === which) {
              if ((egg.belt === WhichBelt.Bottom || egg.belt === WhichBelt.Top) && egg.moves >= 3) {
                target = egg;
                break;
              }

              if ((egg.belt === WhichBelt.Left || egg.belt === WhichBelt.Right) && egg.moves >= 6) {
                target = egg;
                break;
              }
            }
          }

          if (target) {
            this._eggPool.put(target);

            var animations = [this.aniEggStrikedTop, this.aniEggStrikedLeft, this.aniEggStrikedBottom, this.aniEggStrikedRight];
            var animation = animations[which];
            animation.node.active = true;
            this.gameLogic.addScore();
            var track = animation.setAnimation(0, 'attack', false);
            animation.setTrackCompleteListener(track, function () {
              animation.clearTracks();
              animation.node.active = false;
            });
          }
        }
        /**
         * 在指定传送带上生成金蛋
         * @param which 传送带
         */
        ;

        _proto3.newEggAt = function newEggAt(which) {
          var egg = this._eggPool.get();

          egg.init(which, this._eggPool);
          this.eggsNode.addChild(egg.node); // 在上方传送带的金蛋要调整一下层级

          egg.belt === WhichBelt.Top && egg.node.setSiblingIndex(0);
        }
        /**
         * 移动传送带
         * @param belt 传送带
         * @param which 传送带索引
         */
        ;

        _proto3.moveBelt = function moveBelt(belt, which) {
          var _this4 = this;

          var _BeltOffset$which = BeltOffset[which],
              xf = _BeltOffset$which[0],
              yf = _BeltOffset$which[1];
          var duration = this.gameLogic.interval * 0.5;
          belt.children.forEach(function (v) {
            tween(v).by(duration, {
              position: v3(xf, yf, 0)
            }).call(function () {
              if (!_this4 || !_this4.isValid) return;

              if (which === WhichBelt.Top && v.position.y < 37) {
                v.setPosition(v.position.x, 328.5);
              } else if (which === WhichBelt.Bottom && v.position.y > -48) {
                v.setPosition(v.position.x, -339.5);
              } else if (which === WhichBelt.Left && v.position.x > -46.5) {
                v.setPosition(-604.5, v.position.y);
              } else if (which === WhichBelt.Right && v.position.x < 46.5) {
                v.setPosition(604.5, v.position.y);
              }

              _this4._moving = false;
            }).start();
          });
        }
        /** 移动一格 */
        ;

        _proto3.move = function move() {
          this._moving = true;
          this.moveBelt(this.beltTop, WhichBelt.Top);
          this.moveBelt(this.beltBottom, WhichBelt.Bottom);
          this.moveBelt(this.beltLeft, WhichBelt.Left);
          this.moveBelt(this.beltRight, WhichBelt.Right);
          var duration = this.gameLogic.interval * 0.5;
          this.eggsNode.children.slice().forEach(function (v) {
            v.getComponent(Egg).move(duration);
          }); // AI 自动敲

          if (this._aiEnabled) {
            var which;
            var eggs = this.eggsNode.children.slice();
            var egg;

            for (var i = 0; i < eggs.length; i++) {
              egg = eggs[i].getComponent(Egg);

              if ((egg.belt === WhichBelt.Bottom || egg.belt === WhichBelt.Top) && egg.moves >= 3) {
                which = egg.belt;
                break;
              }

              if ((egg.belt === WhichBelt.Left || egg.belt === WhichBelt.Right) && egg.moves >= 6) {
                which = egg.belt;
                break;
              }
            }

            if (which !== undefined) {
              // logger.d('敲中了', egg.belt, egg.moves, egg);
              this._slideTo(which);
            }
          }
        }
        /** 按步调试 */
        ;

        _proto3.debugNextStep = function debugNextStep() {
          if (this._moving) return;
          var now = Date.now();
          var diff = now - this._lastDebugClick;

          if (diff >= this.gameLogic.interval * 1000) {
            this._lastDebugClick = now;
            this.gameLogic.update(this.gameLogic.interval);
          }
        }
        /** 帧刷新 */
        ;

        _proto3.update = function update(dt) {
          if (!this._debug) {
            this.gameLogic.update(dt);
          }
        };

        _createClass(StrikesGameUI, [{
          key: "showBackground",
          get: function get() {
            return true;
          }
        }, {
          key: "moving",
          get: function get() {
            return this._moving;
          }
        }]);

        return StrikesGameUI;
      }(DgflyUI), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "btnClose", [_dec3, _dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "btnResume", [_dec5, _dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "btnStop", [_dec7, _dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "touchNode", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "labScore", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "labTime", [_dec11], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor7 = _applyDecoratedDescriptor(_class2.prototype, "countdownNode", [_dec12], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor8 = _applyDecoratedDescriptor(_class2.prototype, "speedUpNode", [_dec13], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor9 = _applyDecoratedDescriptor(_class2.prototype, "pauseNode", [_dec14], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor10 = _applyDecoratedDescriptor(_class2.prototype, "beltTop", [_dec15], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor11 = _applyDecoratedDescriptor(_class2.prototype, "beltBottom", [_dec16], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor12 = _applyDecoratedDescriptor(_class2.prototype, "beltLeft", [_dec17], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor13 = _applyDecoratedDescriptor(_class2.prototype, "beltRight", [_dec18], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor14 = _applyDecoratedDescriptor(_class2.prototype, "eggTemplate", [_dec19], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor15 = _applyDecoratedDescriptor(_class2.prototype, "eggsNode", [_dec20], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor16 = _applyDecoratedDescriptor(_class2.prototype, "aniHammerIdle", [_dec21], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor17 = _applyDecoratedDescriptor(_class2.prototype, "aniHammerDrop", [_dec22], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor18 = _applyDecoratedDescriptor(_class2.prototype, "aniEggStrikedTop", [_dec23], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor19 = _applyDecoratedDescriptor(_class2.prototype, "aniEggStrikedBottom", [_dec24], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor20 = _applyDecoratedDescriptor(_class2.prototype, "aniEggStrikedLeft", [_dec25], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor21 = _applyDecoratedDescriptor(_class2.prototype, "aniEggStrikedRight", [_dec26], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StrikesHelpWindow.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './View.ts', './ButtonDecorator.ts', './ViewManager.ts', './StorageData.ts', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Button, PageView, DgflyWindow, buttonDecorator, vm, storageData, ccmodifier;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Button = module.Button;
      PageView = module.PageView;
    }, function (module) {
      DgflyWindow = module.DgflyWindow;
    }, function (module) {
      buttonDecorator = module.buttonDecorator;
    }, function (module) {
      vm = module.vm;
    }, function (module) {
      storageData = module.storageData;
    }, function (module) {
      ccmodifier = module.ccmodifier;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4;

      cclegacy._RF.push({}, "59c17P77xlBprYwNguz6OXG", "StrikesHelpWindow", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var StrikesHelpWindow = exports('StrikesHelpWindow', (_dec = ccclass('StrikesHelpWindow'), _dec2 = ccmodifier('StrikesHelpWindow'), _dec3 = buttonDecorator.inject('onCloseView'), _dec4 = property(Button), _dec5 = buttonDecorator.inject('onNextPage'), _dec6 = property(Button), _dec7 = buttonDecorator.inject('onPrevPage'), _dec8 = property(Button), _dec9 = property(PageView), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_DgflyWindow) {
        _inheritsLoose(StrikesHelpWindow, _DgflyWindow);

        function StrikesHelpWindow() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _DgflyWindow.call.apply(_DgflyWindow, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "btnClose", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnRight", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnLeft", _descriptor3, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "pageView", _descriptor4, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = StrikesHelpWindow.prototype;

        _proto.start = function start() {
          storageData.set('strikes_help', true);
          this.gotoPage(0);
        }
        /** 关闭页面 */
        ;

        _proto.onCloseView = function onCloseView() {
          vm.removeWindow(this);
        }
        /**
         * 前往第n页（从0开始）
         * @param page 页面
         * @returns 
         */
        ;

        _proto.gotoPage = function gotoPage(page) {
          page = page >> 0;
          var max = this.pageView.getPages().length;
          var cur = this.pageView.getCurrentPageIndex();
          if (cur < 0 || cur >= max) return;
          this.pageView.setCurrentPageIndex(page);
          this.btnLeft.node.active = page > 0;
          this.btnRight.node.active = page < max - 1;
        }
        /** 前往上一页 */
        ;

        _proto.onPrevPage = function onPrevPage() {
          this.gotoPage(this.pageView.getCurrentPageIndex() - 1);
        }
        /** 前往下一页 */
        ;

        _proto.onNextPage = function onNextPage() {
          this.gotoPage(this.pageView.getCurrentPageIndex() + 1);
        };

        return StrikesHelpWindow;
      }(DgflyWindow), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "btnClose", [_dec3, _dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "btnRight", [_dec5, _dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "btnLeft", [_dec7, _dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "pageView", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StrikesHomeUI.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './View.ts', './ButtonDecorator.ts', './ViewManager.ts', './StorageData.ts', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, _createClass, cclegacy, _decorator, Button, DgflyUI, buttonDecorator, vm, storageData, ccmodifier;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
      _createClass = module.createClass;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Button = module.Button;
    }, function (module) {
      DgflyUI = module.DgflyUI;
    }, function (module) {
      buttonDecorator = module.buttonDecorator;
    }, function (module) {
      vm = module.vm;
    }, function (module) {
      storageData = module.storageData;
    }, function (module) {
      ccmodifier = module.ccmodifier;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4;

      cclegacy._RF.push({}, "b4a94ssF2NJaLhlNEjoUqsB", "StrikesHomeUI", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var StrikesHomeUI = exports('StrikesHomeUI', (_dec = ccclass('StrikesHomeUI'), _dec2 = ccmodifier('StrikesHomeUI'), _dec3 = buttonDecorator.inject(function () {
        return vm.goBackView();
      }), _dec4 = property(Button), _dec5 = buttonDecorator.inject('_onStartGame'), _dec6 = property(Button), _dec7 = buttonDecorator.inject('_onOpenRank'), _dec8 = property(Button), _dec9 = buttonDecorator.inject('_onOpenHelp'), _dec10 = property(Button), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_DgflyUI) {
        _inheritsLoose(StrikesHomeUI, _DgflyUI);

        function StrikesHomeUI() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _DgflyUI.call.apply(_DgflyUI, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "btnClose", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnStart", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnRank", _descriptor3, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnHelp", _descriptor4, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = StrikesHomeUI.prototype;

        _proto.start = function start() {
          if (!storageData.get('strikes_help')) {
            this._onOpenHelp();
          }
        }
        /** 点击开始游戏 */
        ;

        _proto._onStartGame = function _onStartGame() {
          vm.pushUI('prefab/StrikesGameUI', null, this.underModuleName);
        }
        /** 打开排行榜 */
        ;

        _proto._onOpenRank = function _onOpenRank() {
          vm.addWindow('prefab/StrikesRankWindow', true, this.underModuleName);
        }
        /** 打开帮助 */
        ;

        _proto._onOpenHelp = function _onOpenHelp() {
          vm.addWindow('prefab/StrikesHelpWindow', null, this.underModuleName);
        };

        _createClass(StrikesHomeUI, [{
          key: "showBackground",
          get: function get() {
            return true;
          }
        }]);

        return StrikesHomeUI;
      }(DgflyUI), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "btnClose", [_dec3, _dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "btnStart", [_dec5, _dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "btnRank", [_dec7, _dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "btnHelp", [_dec9, _dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StrikesMissWindow.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './View.ts', './ButtonDecorator.ts', './GlobalEvent.ts', './ViewManager.ts', './StrikesExports.ts', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Button, DgflyWindow, buttonDecorator, GlobalEvent, vm, StrikesEventType, ccmodifier;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Button = module.Button;
    }, function (module) {
      DgflyWindow = module.DgflyWindow;
    }, function (module) {
      buttonDecorator = module.buttonDecorator;
    }, function (module) {
      GlobalEvent = module.GlobalEvent;
    }, function (module) {
      vm = module.vm;
    }, function (module) {
      StrikesEventType = module.StrikesEventType;
    }, function (module) {
      ccmodifier = module.ccmodifier;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _class, _class2, _descriptor, _descriptor2;

      cclegacy._RF.push({}, "d52carvgApMcbHHA3IhaCxL", "StrikesMissWindow", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var StrikesMissWindow = exports('StrikesMissWindow', (_dec = ccclass('StrikesMissWindow'), _dec2 = ccmodifier('StrikesMissWindow'), _dec3 = buttonDecorator.inject('onConfirm'), _dec4 = property(Button), _dec5 = buttonDecorator.inject('onRelive'), _dec6 = property(Button), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_DgflyWindow) {
        _inheritsLoose(StrikesMissWindow, _DgflyWindow);

        function StrikesMissWindow() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _DgflyWindow.call.apply(_DgflyWindow, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "btnOK", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnAgain", _descriptor2, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = StrikesMissWindow.prototype;
        /** 确认-前往结算 */

        _proto.onConfirm = function onConfirm() {
          var moduleName = this.underModuleName;
          vm.removeWindow(this);
          vm.addWindow('prefab/StrikesResultWindow', null, moduleName);
        }
        /** 分享-复活机会 */
        ;

        _proto.onRelive = function onRelive() {
          GlobalEvent.emit(StrikesEventType.GameRelive);
          vm.removeWindow(this);
        };

        return StrikesMissWindow;
      }(DgflyWindow), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "btnOK", [_dec3, _dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "btnAgain", [_dec5, _dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StrikesRankItem.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './Modifier.ts', './RemoteImage.ts', './ResManager.ts', './Logger.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Sprite, Label, SpriteFrame, ccmodifier, BaseComponent, RemoteImage, res, logger;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Sprite = module.Sprite;
      Label = module.Label;
      SpriteFrame = module.SpriteFrame;
    }, function (module) {
      ccmodifier = module.ccmodifier;
      BaseComponent = module.BaseComponent;
    }, function (module) {
      RemoteImage = module.RemoteImage;
    }, function (module) {
      res = module.res;
    }, function (module) {
      logger = module.logger;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5;

      cclegacy._RF.push({}, "bc1c3XxNuhGQJdWuyEO6TR7", "StrikesRankItem", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var StrikesRankItem = exports('StrikesRankItem', (_dec = ccclass('StrikesRankItem'), _dec2 = ccmodifier('StrikesRankItem'), _dec3 = property(Sprite), _dec4 = property(Sprite), _dec5 = property(Label), _dec6 = property(Label), _dec7 = property(Label), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_BaseComponent) {
        _inheritsLoose(StrikesRankItem, _BaseComponent);

        function StrikesRankItem() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _BaseComponent.call.apply(_BaseComponent, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "rankSp", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "headSp", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "nameLab", _descriptor3, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "rankLab", _descriptor4, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "scoreLab", _descriptor5, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = StrikesRankItem.prototype;
        /**
         * 更新数据
         * @param data 排行榜数据-一条
         */

        _proto.setData = function setData(data) {
          var _this2 = this;

          var isTop3 = data.rank <= 3;
          this.rankSp.node.active = isTop3;
          this.rankLab.node.active = !isTop3;
          this.nameLab.string = data.name.toString();
          this.scoreLab.string = data.score.toString();

          if (isTop3) {
            var path = "image/countdown_" + data.rank + "/spriteFrame";
            res.get('SG_Strikes', path, SpriteFrame).then(function (frame) {
              logger.d(frame);
              if (frame) _this2.rankSp.spriteFrame = frame;
            });
            this.rankSp.node.active = true;
            this.rankLab.node.active = false;
          } else {
            this.rankLab.string = data.rank.toString();
            this.rankSp.node.active = false;
            this.rankLab.node.active = true;
          }

          this.headSp.getComponent(RemoteImage).setUrl(data.avatar);
        };

        return StrikesRankItem;
      }(BaseComponent), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "rankSp", [_dec3], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "headSp", [_dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "nameLab", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "rankLab", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "scoreLab", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StrikesRankWindow.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './View.ts', './ButtonDecorator.ts', './ViewManager.ts', './Logger.ts', './StrikesRankItem.ts', './StorageData.ts', './RemoteImage.ts', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Button, ScrollView, Node, Label, instantiate, DgflyWindow, buttonDecorator, vm, logger, StrikesRankItem, storageData, RemoteImage, ccmodifier;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Button = module.Button;
      ScrollView = module.ScrollView;
      Node = module.Node;
      Label = module.Label;
      instantiate = module.instantiate;
    }, function (module) {
      DgflyWindow = module.DgflyWindow;
    }, function (module) {
      buttonDecorator = module.buttonDecorator;
    }, function (module) {
      vm = module.vm;
    }, function (module) {
      logger = module.logger;
    }, function (module) {
      StrikesRankItem = module.StrikesRankItem;
    }, function (module) {
      storageData = module.storageData;
    }, function (module) {
      RemoteImage = module.RemoteImage;
    }, function (module) {
      ccmodifier = module.ccmodifier;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5, _descriptor6;

      cclegacy._RF.push({}, "4bc2bQ2riFNR79uKPQmvjEX", "StrikesRankWindow", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var StrikesRankWindow = exports('StrikesRankWindow', (_dec = ccclass('StrikesRankWindow'), _dec2 = ccmodifier('StrikesRankWindow'), _dec3 = buttonDecorator.inject('_onCloseView'), _dec4 = property(Button), _dec5 = property(ScrollView), _dec6 = property(Node), _dec7 = property(Node), _dec8 = property(Label), _dec9 = property(Label), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_DgflyWindow) {
        _inheritsLoose(StrikesRankWindow, _DgflyWindow);

        function StrikesRankWindow() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _DgflyWindow.call.apply(_DgflyWindow, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "btnClose", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "rankView", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "rankItem", _descriptor3, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "rankInfoNode", _descriptor4, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "labMyRank", _descriptor5, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "labMyScore", _descriptor6, _assertThisInitialized(_this));
          /** 排行榜数据 */


          _this._data = void 0;
          /** 当前已加载的数量 */

          _this._currentIdx = 0;
          return _this;
        }

        var _proto = StrikesRankWindow.prototype;
        /** 关闭窗口 */

        _proto._onCloseView = function _onCloseView() {
          vm.removeWindow(this);
        }
        /** 收到排行榜数据 */
        ;

        _proto.onData = function onData(data) {
          logger.d('排行榜数据', data);
          data = {
            myRank: (Math.random() * 100 >> 0) + 100,
            myScore: storageData.get('strikes_high_score'),
            list: [{
              name: '一拳超人',
              rank: 1,
              score: 1018
            }, {
              name: '琦玉老师',
              rank: 2,
              score: 998
            }, {
              name: '光头强者',
              rank: 3,
              score: 995
            }, {
              name: '烈焰红唇',
              rank: 4,
              score: 992
            }, {
              name: '蓝色生死簿',
              rank: 5,
              score: 990
            }, {
              name: '繁·爱',
              rank: 6,
              score: 988
            }, {
              name: '虎啸山庄',
              rank: 7,
              score: 980
            }, {
              name: '大帅逼兜不住',
              rank: 8,
              score: 970
            }, {
              name: '江山美如画',
              rank: 9,
              score: 966
            }, {
              name: '爱我有几分',
              rank: 10,
              score: 888
            }, {
              name: '蛋蛋达人',
              rank: 11,
              score: 812
            }, {
              name: '无限禁制~咻咻咻',
              rank: 12,
              score: 792
            }, {
              name: '请问？我不想靠',
              rank: 13,
              score: 702
            }, {
              name: '鲜花10元',
              rank: 14,
              score: 613
            }, {
              name: '蛋糕没有蛋',
              rank: 15,
              score: 523
            }, {
              name: '小镇姑娘',
              rank: 16,
              score: 448
            }, {
              name: '城市做题家',
              rank: 17,
              score: 390
            }, {
              name: '好先森不做家务事',
              rank: 18,
              score: 348
            }, {
              name: '招摇的小男孩',
              rank: 19,
              score: 288
            }, {
              name: '纯洁的人不加班',
              rank: 20,
              score: 219
            }, {
              name: '不知道叫什么',
              rank: 21,
              score: 191
            }, {
              name: '不管你是不是',
              rank: 22,
              score: 110
            }, {
              name: '反正你是xx',
              rank: 23,
              score: 98
            }]
          };
          this._data = data;
          this.reload();
          this.rankView.content;
          this.rankView.content;
        }
        /** 加载排行榜-一条 */
        ;

        _proto._loadRankItem = function _loadRankItem() {
          var count = this._data.list.length;

          if (this._currentIdx >= count) {
            this.unschedule(this._loadRankItem);
            logger.d('已全部加载完成', count);
            return;
          }

          var data = this._data.list[this._currentIdx];
          if (!data) return;
          var item = instantiate(this.rankItem);
          item.getComponent(StrikesRankItem).setData(data);
          this.rankView.content.addChild(item);
          this._currentIdx++;
        }
        /**
         * 加载荣誉榜排名
         * @param rank 排名
         */
        ;

        _proto._loadRankInfo = function _loadRankInfo(rank) {
          var data = this._data.list[rank - 1];
          var labName = this.rankInfoNode.getChildByName('LabName' + rank);
          var rankNode = this.rankInfoNode.getChildByName('countdown' + rank);
          var headNode = this.rankInfoNode.getChildByName('head' + rank);

          if (data) {
            rankNode.active = true;
            headNode.active = true;
            labName.active = true;
            labName.getComponent(Label).string = data.name;
            headNode.getComponent(RemoteImage).setUrl(data.avatar);
          } else {
            rankNode.active = false;
            headNode.active = false;
            labName.active = false;
          }
        }
        /** 刷新信息 */
        ;

        _proto.reload = function reload() {
          // 更新荣誉榜
          this._loadRankInfo(1);

          this._loadRankInfo(2);

          this._loadRankInfo(3); // 更新个人记录


          this.labMyRank.string = "\u6211\u7684\u540D\u6B21: " + this._data.myRank;
          this.labMyScore.string = "\u5386\u53F2\u6700\u9AD8\u5F97\u5206: " + this._data.myScore; // 加载排行榜列表

          this.unschedule(this._loadRankItem);
          this._currentIdx = 0;
          this.rankView.content.removeAllChildren();
          this.rankView.scrollToTop();
          this.schedule(this._loadRankItem, 0);
        };

        return StrikesRankWindow;
      }(DgflyWindow), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "btnClose", [_dec3, _dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "rankView", [_dec5], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "rankItem", [_dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "rankInfoNode", [_dec7], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "labMyRank", [_dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor6 = _applyDecoratedDescriptor(_class2.prototype, "labMyScore", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

System.register("chunks:///_virtual/StrikesResultWindow.ts", ['./rollupPluginModLoBabelHelpers.js', 'cc', './View.ts', './ButtonDecorator.ts', './GlobalEvent.ts', './ViewManager.ts', './StorageData.ts', './StrikesExports.ts', './Modifier.ts'], function (exports) {
  var _applyDecoratedDescriptor, _inheritsLoose, _initializerDefineProperty, _assertThisInitialized, cclegacy, _decorator, Button, Label, Node, DgflyWindow, buttonDecorator, GlobalEvent, vm, storageData, StrikesEventType, ccmodifier;

  return {
    setters: [function (module) {
      _applyDecoratedDescriptor = module.applyDecoratedDescriptor;
      _inheritsLoose = module.inheritsLoose;
      _initializerDefineProperty = module.initializerDefineProperty;
      _assertThisInitialized = module.assertThisInitialized;
    }, function (module) {
      cclegacy = module.cclegacy;
      _decorator = module._decorator;
      Button = module.Button;
      Label = module.Label;
      Node = module.Node;
    }, function (module) {
      DgflyWindow = module.DgflyWindow;
    }, function (module) {
      buttonDecorator = module.buttonDecorator;
    }, function (module) {
      GlobalEvent = module.GlobalEvent;
    }, function (module) {
      vm = module.vm;
    }, function (module) {
      storageData = module.storageData;
    }, function (module) {
      StrikesEventType = module.StrikesEventType;
    }, function (module) {
      ccmodifier = module.ccmodifier;
    }],
    execute: function () {
      var _dec, _dec2, _dec3, _dec4, _dec5, _dec6, _dec7, _dec8, _dec9, _dec10, _class, _class2, _descriptor, _descriptor2, _descriptor3, _descriptor4, _descriptor5;

      cclegacy._RF.push({}, "3a053xkyklPr5h/Xnf4xePL", "StrikesResultWindow", undefined);

      var ccclass = _decorator.ccclass,
          property = _decorator.property;
      var StrikesResultWindow = exports('StrikesResultWindow', (_dec = ccclass('StrikesResultWindow'), _dec2 = ccmodifier('StrikesResultWindow'), _dec3 = buttonDecorator.inject('onOpenHelp'), _dec4 = property(Button), _dec5 = buttonDecorator.inject('onGameQuit'), _dec6 = property(Button), _dec7 = buttonDecorator.inject('onGameRestart'), _dec8 = property(Button), _dec9 = property(Label), _dec10 = property(Node), _dec(_class = _dec2(_class = (_class2 = /*#__PURE__*/function (_DgflyWindow) {
        _inheritsLoose(StrikesResultWindow, _DgflyWindow);

        function StrikesResultWindow() {
          var _this;

          for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
          }

          _this = _DgflyWindow.call.apply(_DgflyWindow, [this].concat(args)) || this;

          _initializerDefineProperty(_this, "btnHelp", _descriptor, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnOK", _descriptor2, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "btnAgain", _descriptor3, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "labScore", _descriptor4, _assertThisInitialized(_this));

          _initializerDefineProperty(_this, "highScoreNode", _descriptor5, _assertThisInitialized(_this));

          return _this;
        }

        var _proto = StrikesResultWindow.prototype;

        _proto.onAddedToParent = function onAddedToParent() {
          var score = storageData.get('strikes_score');
          var high = storageData.get('strikes_high_score');
          this.labScore.string = score.toString();

          if (score > high) {
            storageData.set('strikes_high_score', score);
            this.highScoreNode.active = true;
          } else {
            this.highScoreNode.active = false;
          }
        }
        /** 打开帮助 */
        ;

        _proto.onOpenHelp = function onOpenHelp() {
          vm.addWindow('prefab/StrikesHelpWindow', null, this.underModuleName);
        }
        /** 退出游戏 */
        ;

        _proto.onGameQuit = function onGameQuit() {
          vm.removeWindow(this);
          GlobalEvent.emit(StrikesEventType.GameQuit);
        }
        /** 重开游戏 */
        ;

        _proto.onGameRestart = function onGameRestart() {
          vm.removeWindow(this);
          GlobalEvent.emit(StrikesEventType.GameRestart);
        };

        return StrikesResultWindow;
      }(DgflyWindow), (_descriptor = _applyDecoratedDescriptor(_class2.prototype, "btnHelp", [_dec3, _dec4], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor2 = _applyDecoratedDescriptor(_class2.prototype, "btnOK", [_dec5, _dec6], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor3 = _applyDecoratedDescriptor(_class2.prototype, "btnAgain", [_dec7, _dec8], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor4 = _applyDecoratedDescriptor(_class2.prototype, "labScore", [_dec9], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      }), _descriptor5 = _applyDecoratedDescriptor(_class2.prototype, "highScoreNode", [_dec10], {
        configurable: true,
        enumerable: true,
        writable: true,
        initializer: null
      })), _class2)) || _class) || _class));

      cclegacy._RF.pop();
    }
  };
});

(function(r) {
  r('virtual:///prerequisite-imports/SG_Strikes', 'chunks:///_virtual/SG_Strikes'); 
})(function(mid, cid) {
    System.register(mid, [cid], function (_export, _context) {
    return {
        setters: [function(_m) {
            var _exportObj = {};

            for (var _key in _m) {
              if (_key !== "default" && _key !== "__esModule") _exportObj[_key] = _m[_key];
            }
      
            _export(_exportObj);
        }],
        execute: function () { }
    };
    });
});