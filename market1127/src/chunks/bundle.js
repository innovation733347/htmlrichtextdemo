System.register([], function(_export, _context) { return { execute: function () {
System.register("chunks:///_virtual/env", [], function (exports) {
  return {
    execute: function () {
      var DEBUG = exports('DEBUG', true);
    }
  };
});

System.register("chunks:///_virtual/index.js", [], function (exports) {
  return {
    execute: function () {
      exports('default', Long);
      /**
       * @license
       * Copyright 2009 The Closure Library Authors
       * Copyright 2020 Daniel Wirtz / The long.js Authors.
       *
       * Licensed under the Apache License, Version 2.0 (the "License");
       * you may not use this file except in compliance with the License.
       * You may obtain a copy of the License at
       *
       *     http://www.apache.org/licenses/LICENSE-2.0
       *
       * Unless required by applicable law or agreed to in writing, software
       * distributed under the License is distributed on an "AS IS" BASIS,
       * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
       * See the License for the specific language governing permissions and
       * limitations under the License.
       *
       * SPDX-License-Identifier: Apache-2.0
       */
      // WebAssembly optimizations to do native i64 multiplication and divide

      var wasm = null;

      try {
        wasm = new WebAssembly.Instance(new WebAssembly.Module(new Uint8Array([0, 97, 115, 109, 1, 0, 0, 0, 1, 13, 2, 96, 0, 1, 127, 96, 4, 127, 127, 127, 127, 1, 127, 3, 7, 6, 0, 1, 1, 1, 1, 1, 6, 6, 1, 127, 1, 65, 0, 11, 7, 50, 6, 3, 109, 117, 108, 0, 1, 5, 100, 105, 118, 95, 115, 0, 2, 5, 100, 105, 118, 95, 117, 0, 3, 5, 114, 101, 109, 95, 115, 0, 4, 5, 114, 101, 109, 95, 117, 0, 5, 8, 103, 101, 116, 95, 104, 105, 103, 104, 0, 0, 10, 191, 1, 6, 4, 0, 35, 0, 11, 36, 1, 1, 126, 32, 0, 173, 32, 1, 173, 66, 32, 134, 132, 32, 2, 173, 32, 3, 173, 66, 32, 134, 132, 126, 34, 4, 66, 32, 135, 167, 36, 0, 32, 4, 167, 11, 36, 1, 1, 126, 32, 0, 173, 32, 1, 173, 66, 32, 134, 132, 32, 2, 173, 32, 3, 173, 66, 32, 134, 132, 127, 34, 4, 66, 32, 135, 167, 36, 0, 32, 4, 167, 11, 36, 1, 1, 126, 32, 0, 173, 32, 1, 173, 66, 32, 134, 132, 32, 2, 173, 32, 3, 173, 66, 32, 134, 132, 128, 34, 4, 66, 32, 135, 167, 36, 0, 32, 4, 167, 11, 36, 1, 1, 126, 32, 0, 173, 32, 1, 173, 66, 32, 134, 132, 32, 2, 173, 32, 3, 173, 66, 32, 134, 132, 129, 34, 4, 66, 32, 135, 167, 36, 0, 32, 4, 167, 11, 36, 1, 1, 126, 32, 0, 173, 32, 1, 173, 66, 32, 134, 132, 32, 2, 173, 32, 3, 173, 66, 32, 134, 132, 130, 34, 4, 66, 32, 135, 167, 36, 0, 32, 4, 167, 11])), {}).exports;
      } catch (e) {// no wasm support :(
      }
      /**
       * Constructs a 64 bit two's-complement integer, given its low and high 32 bit values as *signed* integers.
       *  See the from* functions below for more convenient ways of constructing Longs.
       * @exports Long
       * @class A Long class for representing a 64 bit two's-complement integer value.
       * @param {number} low The low (signed) 32 bits of the long
       * @param {number} high The high (signed) 32 bits of the long
       * @param {boolean=} unsigned Whether unsigned or not, defaults to signed
       * @constructor
       */


      function Long(low, high, unsigned) {
        /**
         * The low 32 bits as a signed value.
         * @type {number}
         */
        this.low = low | 0;
        /**
         * The high 32 bits as a signed value.
         * @type {number}
         */

        this.high = high | 0;
        /**
         * Whether unsigned or not.
         * @type {boolean}
         */

        this.unsigned = !!unsigned;
      } // The internal representation of a long is the two given signed, 32-bit values.
      // We use 32-bit pieces because these are the size of integers on which
      // Javascript performs bit-operations.  For operations like addition and
      // multiplication, we split each number into 16 bit pieces, which can easily be
      // multiplied within Javascript's floating-point representation without overflow
      // or change in sign.
      //
      // In the algorithms below, we frequently reduce the negative case to the
      // positive case by negating the input(s) and then post-processing the result.
      // Note that we must ALWAYS check specially whether those values are MIN_VALUE
      // (-2^63) because -MIN_VALUE == MIN_VALUE (since 2^63 cannot be represented as
      // a positive number, it overflows back into a negative).  Not handling this
      // case would often result in infinite recursion.
      //
      // Common constant values ZERO, ONE, NEG_ONE, etc. are defined below the from*
      // methods on which they depend.

      /**
       * An indicator used to reliably determine if an object is a Long or not.
       * @type {boolean}
       * @const
       * @private
       */


      Long.prototype.__isLong__;
      Object.defineProperty(Long.prototype, "__isLong__", {
        value: true
      });
      /**
       * @function
       * @param {*} obj Object
       * @returns {boolean}
       * @inner
       */

      function isLong(obj) {
        return (obj && obj["__isLong__"]) === true;
      }
      /**
       * @function
       * @param {*} value number
       * @returns {number}
       * @inner
       */


      function ctz32(value) {
        var c = Math.clz32(value & -value);
        return value ? 31 - c : c;
      }
      /**
       * Tests if the specified object is a Long.
       * @function
       * @param {*} obj Object
       * @returns {boolean}
       */


      Long.isLong = isLong;
      /**
       * A cache of the Long representations of small integer values.
       * @type {!Object}
       * @inner
       */

      var INT_CACHE = {};
      /**
       * A cache of the Long representations of small unsigned integer values.
       * @type {!Object}
       * @inner
       */

      var UINT_CACHE = {};
      /**
       * @param {number} value
       * @param {boolean=} unsigned
       * @returns {!Long}
       * @inner
       */

      function fromInt(value, unsigned) {
        var obj, cachedObj, cache;

        if (unsigned) {
          value >>>= 0;

          if (cache = 0 <= value && value < 256) {
            cachedObj = UINT_CACHE[value];
            if (cachedObj) return cachedObj;
          }

          obj = fromBits(value, 0, true);
          if (cache) UINT_CACHE[value] = obj;
          return obj;
        } else {
          value |= 0;

          if (cache = -128 <= value && value < 128) {
            cachedObj = INT_CACHE[value];
            if (cachedObj) return cachedObj;
          }

          obj = fromBits(value, value < 0 ? -1 : 0, false);
          if (cache) INT_CACHE[value] = obj;
          return obj;
        }
      }
      /**
       * Returns a Long representing the given 32 bit integer value.
       * @function
       * @param {number} value The 32 bit integer in question
       * @param {boolean=} unsigned Whether unsigned or not, defaults to signed
       * @returns {!Long} The corresponding Long value
       */


      Long.fromInt = fromInt;
      /**
       * @param {number} value
       * @param {boolean=} unsigned
       * @returns {!Long}
       * @inner
       */

      function fromNumber(value, unsigned) {
        if (isNaN(value)) return unsigned ? UZERO : ZERO;

        if (unsigned) {
          if (value < 0) return UZERO;
          if (value >= TWO_PWR_64_DBL) return MAX_UNSIGNED_VALUE;
        } else {
          if (value <= -TWO_PWR_63_DBL) return MIN_VALUE;
          if (value + 1 >= TWO_PWR_63_DBL) return MAX_VALUE;
        }

        if (value < 0) return fromNumber(-value, unsigned).neg();
        return fromBits(value % TWO_PWR_32_DBL | 0, value / TWO_PWR_32_DBL | 0, unsigned);
      }
      /**
       * Returns a Long representing the given value, provided that it is a finite number. Otherwise, zero is returned.
       * @function
       * @param {number} value The number in question
       * @param {boolean=} unsigned Whether unsigned or not, defaults to signed
       * @returns {!Long} The corresponding Long value
       */


      Long.fromNumber = fromNumber;
      /**
       * @param {number} lowBits
       * @param {number} highBits
       * @param {boolean=} unsigned
       * @returns {!Long}
       * @inner
       */

      function fromBits(lowBits, highBits, unsigned) {
        return new Long(lowBits, highBits, unsigned);
      }
      /**
       * Returns a Long representing the 64 bit integer that comes by concatenating the given low and high bits. Each is
       *  assumed to use 32 bits.
       * @function
       * @param {number} lowBits The low 32 bits
       * @param {number} highBits The high 32 bits
       * @param {boolean=} unsigned Whether unsigned or not, defaults to signed
       * @returns {!Long} The corresponding Long value
       */


      Long.fromBits = fromBits;
      /**
       * @function
       * @param {number} base
       * @param {number} exponent
       * @returns {number}
       * @inner
       */

      var pow_dbl = Math.pow; // Used 4 times (4*8 to 15+4)

      /**
       * @param {string} str
       * @param {(boolean|number)=} unsigned
       * @param {number=} radix
       * @returns {!Long}
       * @inner
       */

      function fromString(str, unsigned, radix) {
        if (str.length === 0) throw Error('empty string');

        if (typeof unsigned === 'number') {
          // For goog.math.long compatibility
          radix = unsigned;
          unsigned = false;
        } else {
          unsigned = !!unsigned;
        }

        if (str === "NaN" || str === "Infinity" || str === "+Infinity" || str === "-Infinity") return unsigned ? UZERO : ZERO;
        radix = radix || 10;
        if (radix < 2 || 36 < radix) throw RangeError('radix');
        var p;
        if ((p = str.indexOf('-')) > 0) throw Error('interior hyphen');else if (p === 0) {
          return fromString(str.substring(1), unsigned, radix).neg();
        } // Do several (8) digits each time through the loop, so as to
        // minimize the calls to the very expensive emulated div.

        var radixToPower = fromNumber(pow_dbl(radix, 8));
        var result = ZERO;

        for (var i = 0; i < str.length; i += 8) {
          var size = Math.min(8, str.length - i),
              value = parseInt(str.substring(i, i + size), radix);

          if (size < 8) {
            var power = fromNumber(pow_dbl(radix, size));
            result = result.mul(power).add(fromNumber(value));
          } else {
            result = result.mul(radixToPower);
            result = result.add(fromNumber(value));
          }
        }

        result.unsigned = unsigned;
        return result;
      }
      /**
       * Returns a Long representation of the given string, written using the specified radix.
       * @function
       * @param {string} str The textual representation of the Long
       * @param {(boolean|number)=} unsigned Whether unsigned or not, defaults to signed
       * @param {number=} radix The radix in which the text is written (2-36), defaults to 10
       * @returns {!Long} The corresponding Long value
       */


      Long.fromString = fromString;
      /**
       * @function
       * @param {!Long|number|string|!{low: number, high: number, unsigned: boolean}} val
       * @param {boolean=} unsigned
       * @returns {!Long}
       * @inner
       */

      function fromValue(val, unsigned) {
        if (typeof val === 'number') return fromNumber(val, unsigned);
        if (typeof val === 'string') return fromString(val, unsigned); // Throws for non-objects, converts non-instanceof Long:

        return fromBits(val.low, val.high, typeof unsigned === 'boolean' ? unsigned : val.unsigned);
      }
      /**
       * Converts the specified value to a Long using the appropriate from* function for its type.
       * @function
       * @param {!Long|number|string|!{low: number, high: number, unsigned: boolean}} val Value
       * @param {boolean=} unsigned Whether unsigned or not, defaults to signed
       * @returns {!Long}
       */


      Long.fromValue = fromValue; // NOTE: the compiler should inline these constant values below and then remove these variables, so there should be
      // no runtime penalty for these.

      /**
       * @type {number}
       * @const
       * @inner
       */

      var TWO_PWR_16_DBL = 1 << 16;
      /**
       * @type {number}
       * @const
       * @inner
       */

      var TWO_PWR_24_DBL = 1 << 24;
      /**
       * @type {number}
       * @const
       * @inner
       */

      var TWO_PWR_32_DBL = TWO_PWR_16_DBL * TWO_PWR_16_DBL;
      /**
       * @type {number}
       * @const
       * @inner
       */

      var TWO_PWR_64_DBL = TWO_PWR_32_DBL * TWO_PWR_32_DBL;
      /**
       * @type {number}
       * @const
       * @inner
       */

      var TWO_PWR_63_DBL = TWO_PWR_64_DBL / 2;
      /**
       * @type {!Long}
       * @const
       * @inner
       */

      var TWO_PWR_24 = fromInt(TWO_PWR_24_DBL);
      /**
       * @type {!Long}
       * @inner
       */

      var ZERO = fromInt(0);
      /**
       * Signed zero.
       * @type {!Long}
       */

      Long.ZERO = ZERO;
      /**
       * @type {!Long}
       * @inner
       */

      var UZERO = fromInt(0, true);
      /**
       * Unsigned zero.
       * @type {!Long}
       */

      Long.UZERO = UZERO;
      /**
       * @type {!Long}
       * @inner
       */

      var ONE = fromInt(1);
      /**
       * Signed one.
       * @type {!Long}
       */

      Long.ONE = ONE;
      /**
       * @type {!Long}
       * @inner
       */

      var UONE = fromInt(1, true);
      /**
       * Unsigned one.
       * @type {!Long}
       */

      Long.UONE = UONE;
      /**
       * @type {!Long}
       * @inner
       */

      var NEG_ONE = fromInt(-1);
      /**
       * Signed negative one.
       * @type {!Long}
       */

      Long.NEG_ONE = NEG_ONE;
      /**
       * @type {!Long}
       * @inner
       */

      var MAX_VALUE = fromBits(0xFFFFFFFF | 0, 0x7FFFFFFF | 0, false);
      /**
       * Maximum signed value.
       * @type {!Long}
       */

      Long.MAX_VALUE = MAX_VALUE;
      /**
       * @type {!Long}
       * @inner
       */

      var MAX_UNSIGNED_VALUE = fromBits(0xFFFFFFFF | 0, 0xFFFFFFFF | 0, true);
      /**
       * Maximum unsigned value.
       * @type {!Long}
       */

      Long.MAX_UNSIGNED_VALUE = MAX_UNSIGNED_VALUE;
      /**
       * @type {!Long}
       * @inner
       */

      var MIN_VALUE = fromBits(0, 0x80000000 | 0, false);
      /**
       * Minimum signed value.
       * @type {!Long}
       */

      Long.MIN_VALUE = MIN_VALUE;
      /**
       * @alias Long.prototype
       * @inner
       */

      var LongPrototype = Long.prototype;
      /**
       * Converts the Long to a 32 bit integer, assuming it is a 32 bit integer.
       * @this {!Long}
       * @returns {number}
       */

      LongPrototype.toInt = function toInt() {
        return this.unsigned ? this.low >>> 0 : this.low;
      };
      /**
       * Converts the Long to a the nearest floating-point representation of this value (double, 53 bit mantissa).
       * @this {!Long}
       * @returns {number}
       */


      LongPrototype.toNumber = function toNumber() {
        if (this.unsigned) return (this.high >>> 0) * TWO_PWR_32_DBL + (this.low >>> 0);
        return this.high * TWO_PWR_32_DBL + (this.low >>> 0);
      };
      /**
       * Converts the Long to a string written in the specified radix.
       * @this {!Long}
       * @param {number=} radix Radix (2-36), defaults to 10
       * @returns {string}
       * @override
       * @throws {RangeError} If `radix` is out of range
       */


      LongPrototype.toString = function toString(radix) {
        radix = radix || 10;
        if (radix < 2 || 36 < radix) throw RangeError('radix');
        if (this.isZero()) return '0';

        if (this.isNegative()) {
          // Unsigned Longs are never negative
          if (this.eq(MIN_VALUE)) {
            // We need to change the Long value before it can be negated, so we remove
            // the bottom-most digit in this base and then recurse to do the rest.
            var radixLong = fromNumber(radix),
                div = this.div(radixLong),
                rem1 = div.mul(radixLong).sub(this);
            return div.toString(radix) + rem1.toInt().toString(radix);
          } else return '-' + this.neg().toString(radix);
        } // Do several (6) digits each time through the loop, so as to
        // minimize the calls to the very expensive emulated div.


        var radixToPower = fromNumber(pow_dbl(radix, 6), this.unsigned),
            rem = this;
        var result = '';

        while (true) {
          var remDiv = rem.div(radixToPower),
              intval = rem.sub(remDiv.mul(radixToPower)).toInt() >>> 0,
              digits = intval.toString(radix);
          rem = remDiv;
          if (rem.isZero()) return digits + result;else {
            while (digits.length < 6) digits = '0' + digits;

            result = '' + digits + result;
          }
        }
      };
      /**
       * Gets the high 32 bits as a signed integer.
       * @this {!Long}
       * @returns {number} Signed high bits
       */


      LongPrototype.getHighBits = function getHighBits() {
        return this.high;
      };
      /**
       * Gets the high 32 bits as an unsigned integer.
       * @this {!Long}
       * @returns {number} Unsigned high bits
       */


      LongPrototype.getHighBitsUnsigned = function getHighBitsUnsigned() {
        return this.high >>> 0;
      };
      /**
       * Gets the low 32 bits as a signed integer.
       * @this {!Long}
       * @returns {number} Signed low bits
       */


      LongPrototype.getLowBits = function getLowBits() {
        return this.low;
      };
      /**
       * Gets the low 32 bits as an unsigned integer.
       * @this {!Long}
       * @returns {number} Unsigned low bits
       */


      LongPrototype.getLowBitsUnsigned = function getLowBitsUnsigned() {
        return this.low >>> 0;
      };
      /**
       * Gets the number of bits needed to represent the absolute value of this Long.
       * @this {!Long}
       * @returns {number}
       */


      LongPrototype.getNumBitsAbs = function getNumBitsAbs() {
        if (this.isNegative()) // Unsigned Longs are never negative
          return this.eq(MIN_VALUE) ? 64 : this.neg().getNumBitsAbs();
        var val = this.high != 0 ? this.high : this.low;

        for (var bit = 31; bit > 0; bit--) if ((val & 1 << bit) != 0) break;

        return this.high != 0 ? bit + 33 : bit + 1;
      };
      /**
       * Tests if this Long's value equals zero.
       * @this {!Long}
       * @returns {boolean}
       */


      LongPrototype.isZero = function isZero() {
        return this.high === 0 && this.low === 0;
      };
      /**
       * Tests if this Long's value equals zero. This is an alias of {@link Long#isZero}.
       * @returns {boolean}
       */


      LongPrototype.eqz = LongPrototype.isZero;
      /**
       * Tests if this Long's value is negative.
       * @this {!Long}
       * @returns {boolean}
       */

      LongPrototype.isNegative = function isNegative() {
        return !this.unsigned && this.high < 0;
      };
      /**
       * Tests if this Long's value is positive or zero.
       * @this {!Long}
       * @returns {boolean}
       */


      LongPrototype.isPositive = function isPositive() {
        return this.unsigned || this.high >= 0;
      };
      /**
       * Tests if this Long's value is odd.
       * @this {!Long}
       * @returns {boolean}
       */


      LongPrototype.isOdd = function isOdd() {
        return (this.low & 1) === 1;
      };
      /**
       * Tests if this Long's value is even.
       * @this {!Long}
       * @returns {boolean}
       */


      LongPrototype.isEven = function isEven() {
        return (this.low & 1) === 0;
      };
      /**
       * Tests if this Long's value equals the specified's.
       * @this {!Long}
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */


      LongPrototype.equals = function equals(other) {
        if (!isLong(other)) other = fromValue(other);
        if (this.unsigned !== other.unsigned && this.high >>> 31 === 1 && other.high >>> 31 === 1) return false;
        return this.high === other.high && this.low === other.low;
      };
      /**
       * Tests if this Long's value equals the specified's. This is an alias of {@link Long#equals}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */


      LongPrototype.eq = LongPrototype.equals;
      /**
       * Tests if this Long's value differs from the specified's.
       * @this {!Long}
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */

      LongPrototype.notEquals = function notEquals(other) {
        return !this.eq(
        /* validates */
        other);
      };
      /**
       * Tests if this Long's value differs from the specified's. This is an alias of {@link Long#notEquals}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */


      LongPrototype.neq = LongPrototype.notEquals;
      /**
       * Tests if this Long's value differs from the specified's. This is an alias of {@link Long#notEquals}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */

      LongPrototype.ne = LongPrototype.notEquals;
      /**
       * Tests if this Long's value is less than the specified's.
       * @this {!Long}
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */

      LongPrototype.lessThan = function lessThan(other) {
        return this.comp(
        /* validates */
        other) < 0;
      };
      /**
       * Tests if this Long's value is less than the specified's. This is an alias of {@link Long#lessThan}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */


      LongPrototype.lt = LongPrototype.lessThan;
      /**
       * Tests if this Long's value is less than or equal the specified's.
       * @this {!Long}
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */

      LongPrototype.lessThanOrEqual = function lessThanOrEqual(other) {
        return this.comp(
        /* validates */
        other) <= 0;
      };
      /**
       * Tests if this Long's value is less than or equal the specified's. This is an alias of {@link Long#lessThanOrEqual}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */


      LongPrototype.lte = LongPrototype.lessThanOrEqual;
      /**
       * Tests if this Long's value is less than or equal the specified's. This is an alias of {@link Long#lessThanOrEqual}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */

      LongPrototype.le = LongPrototype.lessThanOrEqual;
      /**
       * Tests if this Long's value is greater than the specified's.
       * @this {!Long}
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */

      LongPrototype.greaterThan = function greaterThan(other) {
        return this.comp(
        /* validates */
        other) > 0;
      };
      /**
       * Tests if this Long's value is greater than the specified's. This is an alias of {@link Long#greaterThan}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */


      LongPrototype.gt = LongPrototype.greaterThan;
      /**
       * Tests if this Long's value is greater than or equal the specified's.
       * @this {!Long}
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */

      LongPrototype.greaterThanOrEqual = function greaterThanOrEqual(other) {
        return this.comp(
        /* validates */
        other) >= 0;
      };
      /**
       * Tests if this Long's value is greater than or equal the specified's. This is an alias of {@link Long#greaterThanOrEqual}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */


      LongPrototype.gte = LongPrototype.greaterThanOrEqual;
      /**
       * Tests if this Long's value is greater than or equal the specified's. This is an alias of {@link Long#greaterThanOrEqual}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {boolean}
       */

      LongPrototype.ge = LongPrototype.greaterThanOrEqual;
      /**
       * Compares this Long's value with the specified's.
       * @this {!Long}
       * @param {!Long|number|string} other Other value
       * @returns {number} 0 if they are the same, 1 if the this is greater and -1
       *  if the given one is greater
       */

      LongPrototype.compare = function compare(other) {
        if (!isLong(other)) other = fromValue(other);
        if (this.eq(other)) return 0;
        var thisNeg = this.isNegative(),
            otherNeg = other.isNegative();
        if (thisNeg && !otherNeg) return -1;
        if (!thisNeg && otherNeg) return 1; // At this point the sign bits are the same

        if (!this.unsigned) return this.sub(other).isNegative() ? -1 : 1; // Both are positive if at least one is unsigned

        return other.high >>> 0 > this.high >>> 0 || other.high === this.high && other.low >>> 0 > this.low >>> 0 ? -1 : 1;
      };
      /**
       * Compares this Long's value with the specified's. This is an alias of {@link Long#compare}.
       * @function
       * @param {!Long|number|string} other Other value
       * @returns {number} 0 if they are the same, 1 if the this is greater and -1
       *  if the given one is greater
       */


      LongPrototype.comp = LongPrototype.compare;
      /**
       * Negates this Long's value.
       * @this {!Long}
       * @returns {!Long} Negated Long
       */

      LongPrototype.negate = function negate() {
        if (!this.unsigned && this.eq(MIN_VALUE)) return MIN_VALUE;
        return this.not().add(ONE);
      };
      /**
       * Negates this Long's value. This is an alias of {@link Long#negate}.
       * @function
       * @returns {!Long} Negated Long
       */


      LongPrototype.neg = LongPrototype.negate;
      /**
       * Returns the sum of this and the specified Long.
       * @this {!Long}
       * @param {!Long|number|string} addend Addend
       * @returns {!Long} Sum
       */

      LongPrototype.add = function add(addend) {
        if (!isLong(addend)) addend = fromValue(addend); // Divide each number into 4 chunks of 16 bits, and then sum the chunks.

        var a48 = this.high >>> 16;
        var a32 = this.high & 0xFFFF;
        var a16 = this.low >>> 16;
        var a00 = this.low & 0xFFFF;
        var b48 = addend.high >>> 16;
        var b32 = addend.high & 0xFFFF;
        var b16 = addend.low >>> 16;
        var b00 = addend.low & 0xFFFF;
        var c48 = 0,
            c32 = 0,
            c16 = 0,
            c00 = 0;
        c00 += a00 + b00;
        c16 += c00 >>> 16;
        c00 &= 0xFFFF;
        c16 += a16 + b16;
        c32 += c16 >>> 16;
        c16 &= 0xFFFF;
        c32 += a32 + b32;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c48 += a48 + b48;
        c48 &= 0xFFFF;
        return fromBits(c16 << 16 | c00, c48 << 16 | c32, this.unsigned);
      };
      /**
       * Returns the difference of this and the specified Long.
       * @this {!Long}
       * @param {!Long|number|string} subtrahend Subtrahend
       * @returns {!Long} Difference
       */


      LongPrototype.subtract = function subtract(subtrahend) {
        if (!isLong(subtrahend)) subtrahend = fromValue(subtrahend);
        return this.add(subtrahend.neg());
      };
      /**
       * Returns the difference of this and the specified Long. This is an alias of {@link Long#subtract}.
       * @function
       * @param {!Long|number|string} subtrahend Subtrahend
       * @returns {!Long} Difference
       */


      LongPrototype.sub = LongPrototype.subtract;
      /**
       * Returns the product of this and the specified Long.
       * @this {!Long}
       * @param {!Long|number|string} multiplier Multiplier
       * @returns {!Long} Product
       */

      LongPrototype.multiply = function multiply(multiplier) {
        if (this.isZero()) return this;
        if (!isLong(multiplier)) multiplier = fromValue(multiplier); // use wasm support if present

        if (wasm) {
          var low = wasm["mul"](this.low, this.high, multiplier.low, multiplier.high);
          return fromBits(low, wasm["get_high"](), this.unsigned);
        }

        if (multiplier.isZero()) return this.unsigned ? UZERO : ZERO;
        if (this.eq(MIN_VALUE)) return multiplier.isOdd() ? MIN_VALUE : ZERO;
        if (multiplier.eq(MIN_VALUE)) return this.isOdd() ? MIN_VALUE : ZERO;

        if (this.isNegative()) {
          if (multiplier.isNegative()) return this.neg().mul(multiplier.neg());else return this.neg().mul(multiplier).neg();
        } else if (multiplier.isNegative()) return this.mul(multiplier.neg()).neg(); // If both longs are small, use float multiplication


        if (this.lt(TWO_PWR_24) && multiplier.lt(TWO_PWR_24)) return fromNumber(this.toNumber() * multiplier.toNumber(), this.unsigned); // Divide each long into 4 chunks of 16 bits, and then add up 4x4 products.
        // We can skip products that would overflow.

        var a48 = this.high >>> 16;
        var a32 = this.high & 0xFFFF;
        var a16 = this.low >>> 16;
        var a00 = this.low & 0xFFFF;
        var b48 = multiplier.high >>> 16;
        var b32 = multiplier.high & 0xFFFF;
        var b16 = multiplier.low >>> 16;
        var b00 = multiplier.low & 0xFFFF;
        var c48 = 0,
            c32 = 0,
            c16 = 0,
            c00 = 0;
        c00 += a00 * b00;
        c16 += c00 >>> 16;
        c00 &= 0xFFFF;
        c16 += a16 * b00;
        c32 += c16 >>> 16;
        c16 &= 0xFFFF;
        c16 += a00 * b16;
        c32 += c16 >>> 16;
        c16 &= 0xFFFF;
        c32 += a32 * b00;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c32 += a16 * b16;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c32 += a00 * b32;
        c48 += c32 >>> 16;
        c32 &= 0xFFFF;
        c48 += a48 * b00 + a32 * b16 + a16 * b32 + a00 * b48;
        c48 &= 0xFFFF;
        return fromBits(c16 << 16 | c00, c48 << 16 | c32, this.unsigned);
      };
      /**
       * Returns the product of this and the specified Long. This is an alias of {@link Long#multiply}.
       * @function
       * @param {!Long|number|string} multiplier Multiplier
       * @returns {!Long} Product
       */


      LongPrototype.mul = LongPrototype.multiply;
      /**
       * Returns this Long divided by the specified. The result is signed if this Long is signed or
       *  unsigned if this Long is unsigned.
       * @this {!Long}
       * @param {!Long|number|string} divisor Divisor
       * @returns {!Long} Quotient
       */

      LongPrototype.divide = function divide(divisor) {
        if (!isLong(divisor)) divisor = fromValue(divisor);
        if (divisor.isZero()) throw Error('division by zero'); // use wasm support if present

        if (wasm) {
          // guard against signed division overflow: the largest
          // negative number / -1 would be 1 larger than the largest
          // positive number, due to two's complement.
          if (!this.unsigned && this.high === -0x80000000 && divisor.low === -1 && divisor.high === -1) {
            // be consistent with non-wasm code path
            return this;
          }

          var low = (this.unsigned ? wasm["div_u"] : wasm["div_s"])(this.low, this.high, divisor.low, divisor.high);
          return fromBits(low, wasm["get_high"](), this.unsigned);
        }

        if (this.isZero()) return this.unsigned ? UZERO : ZERO;
        var approx, rem, res;

        if (!this.unsigned) {
          // This section is only relevant for signed longs and is derived from the
          // closure library as a whole.
          if (this.eq(MIN_VALUE)) {
            if (divisor.eq(ONE) || divisor.eq(NEG_ONE)) return MIN_VALUE; // recall that -MIN_VALUE == MIN_VALUE
            else if (divisor.eq(MIN_VALUE)) return ONE;else {
                // At this point, we have |other| >= 2, so |this/other| < |MIN_VALUE|.
                var halfThis = this.shr(1);
                approx = halfThis.div(divisor).shl(1);

                if (approx.eq(ZERO)) {
                  return divisor.isNegative() ? ONE : NEG_ONE;
                } else {
                  rem = this.sub(divisor.mul(approx));
                  res = approx.add(rem.div(divisor));
                  return res;
                }
              }
          } else if (divisor.eq(MIN_VALUE)) return this.unsigned ? UZERO : ZERO;

          if (this.isNegative()) {
            if (divisor.isNegative()) return this.neg().div(divisor.neg());
            return this.neg().div(divisor).neg();
          } else if (divisor.isNegative()) return this.div(divisor.neg()).neg();

          res = ZERO;
        } else {
          // The algorithm below has not been made for unsigned longs. It's therefore
          // required to take special care of the MSB prior to running it.
          if (!divisor.unsigned) divisor = divisor.toUnsigned();
          if (divisor.gt(this)) return UZERO;
          if (divisor.gt(this.shru(1))) // 15 >>> 1 = 7 ; with divisor = 8 ; true
            return UONE;
          res = UZERO;
        } // Repeat the following until the remainder is less than other:  find a
        // floating-point that approximates remainder / other *from below*, add this
        // into the result, and subtract it from the remainder.  It is critical that
        // the approximate value is less than or equal to the real value so that the
        // remainder never becomes negative.


        rem = this;

        while (rem.gte(divisor)) {
          // Approximate the result of division. This may be a little greater or
          // smaller than the actual value.
          approx = Math.max(1, Math.floor(rem.toNumber() / divisor.toNumber())); // We will tweak the approximate result by changing it in the 48-th digit or
          // the smallest non-fractional digit, whichever is larger.

          var log2 = Math.ceil(Math.log(approx) / Math.LN2),
              delta = log2 <= 48 ? 1 : pow_dbl(2, log2 - 48),
              // Decrease the approximation until it is smaller than the remainder.  Note
          // that if it is too large, the product overflows and is negative.
          approxRes = fromNumber(approx),
              approxRem = approxRes.mul(divisor);

          while (approxRem.isNegative() || approxRem.gt(rem)) {
            approx -= delta;
            approxRes = fromNumber(approx, this.unsigned);
            approxRem = approxRes.mul(divisor);
          } // We know the answer can't be zero... and actually, zero would cause
          // infinite recursion since we would make no progress.


          if (approxRes.isZero()) approxRes = ONE;
          res = res.add(approxRes);
          rem = rem.sub(approxRem);
        }

        return res;
      };
      /**
       * Returns this Long divided by the specified. This is an alias of {@link Long#divide}.
       * @function
       * @param {!Long|number|string} divisor Divisor
       * @returns {!Long} Quotient
       */


      LongPrototype.div = LongPrototype.divide;
      /**
       * Returns this Long modulo the specified.
       * @this {!Long}
       * @param {!Long|number|string} divisor Divisor
       * @returns {!Long} Remainder
       */

      LongPrototype.modulo = function modulo(divisor) {
        if (!isLong(divisor)) divisor = fromValue(divisor); // use wasm support if present

        if (wasm) {
          var low = (this.unsigned ? wasm["rem_u"] : wasm["rem_s"])(this.low, this.high, divisor.low, divisor.high);
          return fromBits(low, wasm["get_high"](), this.unsigned);
        }

        return this.sub(this.div(divisor).mul(divisor));
      };
      /**
       * Returns this Long modulo the specified. This is an alias of {@link Long#modulo}.
       * @function
       * @param {!Long|number|string} divisor Divisor
       * @returns {!Long} Remainder
       */


      LongPrototype.mod = LongPrototype.modulo;
      /**
       * Returns this Long modulo the specified. This is an alias of {@link Long#modulo}.
       * @function
       * @param {!Long|number|string} divisor Divisor
       * @returns {!Long} Remainder
       */

      LongPrototype.rem = LongPrototype.modulo;
      /**
       * Returns the bitwise NOT of this Long.
       * @this {!Long}
       * @returns {!Long}
       */

      LongPrototype.not = function not() {
        return fromBits(~this.low, ~this.high, this.unsigned);
      };
      /**
       * Returns count leading zeros of this Long.
       * @this {!Long}
       * @returns {!number}
       */


      LongPrototype.countLeadingZeros = function countLeadingZeros() {
        return this.high ? Math.clz32(this.high) : Math.clz32(this.low) + 32;
      };
      /**
       * Returns count leading zeros. This is an alias of {@link Long#countLeadingZeros}.
       * @function
       * @param {!Long}
       * @returns {!number}
       */


      LongPrototype.clz = LongPrototype.countLeadingZeros;
      /**
       * Returns count trailing zeros of this Long.
       * @this {!Long}
       * @returns {!number}
       */

      LongPrototype.countTrailingZeros = function countTrailingZeros() {
        return this.low ? ctz32(this.low) : ctz32(this.high) + 32;
      };
      /**
       * Returns count trailing zeros. This is an alias of {@link Long#countTrailingZeros}.
       * @function
       * @param {!Long}
       * @returns {!number}
       */


      LongPrototype.ctz = LongPrototype.countTrailingZeros;
      /**
       * Returns the bitwise AND of this Long and the specified.
       * @this {!Long}
       * @param {!Long|number|string} other Other Long
       * @returns {!Long}
       */

      LongPrototype.and = function and(other) {
        if (!isLong(other)) other = fromValue(other);
        return fromBits(this.low & other.low, this.high & other.high, this.unsigned);
      };
      /**
       * Returns the bitwise OR of this Long and the specified.
       * @this {!Long}
       * @param {!Long|number|string} other Other Long
       * @returns {!Long}
       */


      LongPrototype.or = function or(other) {
        if (!isLong(other)) other = fromValue(other);
        return fromBits(this.low | other.low, this.high | other.high, this.unsigned);
      };
      /**
       * Returns the bitwise XOR of this Long and the given one.
       * @this {!Long}
       * @param {!Long|number|string} other Other Long
       * @returns {!Long}
       */


      LongPrototype.xor = function xor(other) {
        if (!isLong(other)) other = fromValue(other);
        return fromBits(this.low ^ other.low, this.high ^ other.high, this.unsigned);
      };
      /**
       * Returns this Long with bits shifted to the left by the given amount.
       * @this {!Long}
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Shifted Long
       */


      LongPrototype.shiftLeft = function shiftLeft(numBits) {
        if (isLong(numBits)) numBits = numBits.toInt();
        if ((numBits &= 63) === 0) return this;else if (numBits < 32) return fromBits(this.low << numBits, this.high << numBits | this.low >>> 32 - numBits, this.unsigned);else return fromBits(0, this.low << numBits - 32, this.unsigned);
      };
      /**
       * Returns this Long with bits shifted to the left by the given amount. This is an alias of {@link Long#shiftLeft}.
       * @function
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Shifted Long
       */


      LongPrototype.shl = LongPrototype.shiftLeft;
      /**
       * Returns this Long with bits arithmetically shifted to the right by the given amount.
       * @this {!Long}
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Shifted Long
       */

      LongPrototype.shiftRight = function shiftRight(numBits) {
        if (isLong(numBits)) numBits = numBits.toInt();
        if ((numBits &= 63) === 0) return this;else if (numBits < 32) return fromBits(this.low >>> numBits | this.high << 32 - numBits, this.high >> numBits, this.unsigned);else return fromBits(this.high >> numBits - 32, this.high >= 0 ? 0 : -1, this.unsigned);
      };
      /**
       * Returns this Long with bits arithmetically shifted to the right by the given amount. This is an alias of {@link Long#shiftRight}.
       * @function
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Shifted Long
       */


      LongPrototype.shr = LongPrototype.shiftRight;
      /**
       * Returns this Long with bits logically shifted to the right by the given amount.
       * @this {!Long}
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Shifted Long
       */

      LongPrototype.shiftRightUnsigned = function shiftRightUnsigned(numBits) {
        if (isLong(numBits)) numBits = numBits.toInt();
        if ((numBits &= 63) === 0) return this;
        if (numBits < 32) return fromBits(this.low >>> numBits | this.high << 32 - numBits, this.high >>> numBits, this.unsigned);
        if (numBits === 32) return fromBits(this.high, 0, this.unsigned);
        return fromBits(this.high >>> numBits - 32, 0, this.unsigned);
      };
      /**
       * Returns this Long with bits logically shifted to the right by the given amount. This is an alias of {@link Long#shiftRightUnsigned}.
       * @function
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Shifted Long
       */


      LongPrototype.shru = LongPrototype.shiftRightUnsigned;
      /**
       * Returns this Long with bits logically shifted to the right by the given amount. This is an alias of {@link Long#shiftRightUnsigned}.
       * @function
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Shifted Long
       */

      LongPrototype.shr_u = LongPrototype.shiftRightUnsigned;
      /**
       * Returns this Long with bits rotated to the left by the given amount.
       * @this {!Long}
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Rotated Long
       */

      LongPrototype.rotateLeft = function rotateLeft(numBits) {
        var b;
        if (isLong(numBits)) numBits = numBits.toInt();
        if ((numBits &= 63) === 0) return this;
        if (numBits === 32) return fromBits(this.high, this.low, this.unsigned);

        if (numBits < 32) {
          b = 32 - numBits;
          return fromBits(this.low << numBits | this.high >>> b, this.high << numBits | this.low >>> b, this.unsigned);
        }

        numBits -= 32;
        b = 32 - numBits;
        return fromBits(this.high << numBits | this.low >>> b, this.low << numBits | this.high >>> b, this.unsigned);
      };
      /**
       * Returns this Long with bits rotated to the left by the given amount. This is an alias of {@link Long#rotateLeft}.
       * @function
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Rotated Long
       */


      LongPrototype.rotl = LongPrototype.rotateLeft;
      /**
       * Returns this Long with bits rotated to the right by the given amount.
       * @this {!Long}
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Rotated Long
       */

      LongPrototype.rotateRight = function rotateRight(numBits) {
        var b;
        if (isLong(numBits)) numBits = numBits.toInt();
        if ((numBits &= 63) === 0) return this;
        if (numBits === 32) return fromBits(this.high, this.low, this.unsigned);

        if (numBits < 32) {
          b = 32 - numBits;
          return fromBits(this.high << b | this.low >>> numBits, this.low << b | this.high >>> numBits, this.unsigned);
        }

        numBits -= 32;
        b = 32 - numBits;
        return fromBits(this.low << b | this.high >>> numBits, this.high << b | this.low >>> numBits, this.unsigned);
      };
      /**
       * Returns this Long with bits rotated to the right by the given amount. This is an alias of {@link Long#rotateRight}.
       * @function
       * @param {number|!Long} numBits Number of bits
       * @returns {!Long} Rotated Long
       */


      LongPrototype.rotr = LongPrototype.rotateRight;
      /**
       * Converts this Long to signed.
       * @this {!Long}
       * @returns {!Long} Signed long
       */

      LongPrototype.toSigned = function toSigned() {
        if (!this.unsigned) return this;
        return fromBits(this.low, this.high, false);
      };
      /**
       * Converts this Long to unsigned.
       * @this {!Long}
       * @returns {!Long} Unsigned long
       */


      LongPrototype.toUnsigned = function toUnsigned() {
        if (this.unsigned) return this;
        return fromBits(this.low, this.high, true);
      };
      /**
       * Converts this Long to its byte representation.
       * @param {boolean=} le Whether little or big endian, defaults to big endian
       * @this {!Long}
       * @returns {!Array.<number>} Byte representation
       */


      LongPrototype.toBytes = function toBytes(le) {
        return le ? this.toBytesLE() : this.toBytesBE();
      };
      /**
       * Converts this Long to its little endian byte representation.
       * @this {!Long}
       * @returns {!Array.<number>} Little endian byte representation
       */


      LongPrototype.toBytesLE = function toBytesLE() {
        var hi = this.high,
            lo = this.low;
        return [lo & 0xff, lo >>> 8 & 0xff, lo >>> 16 & 0xff, lo >>> 24, hi & 0xff, hi >>> 8 & 0xff, hi >>> 16 & 0xff, hi >>> 24];
      };
      /**
       * Converts this Long to its big endian byte representation.
       * @this {!Long}
       * @returns {!Array.<number>} Big endian byte representation
       */


      LongPrototype.toBytesBE = function toBytesBE() {
        var hi = this.high,
            lo = this.low;
        return [hi >>> 24, hi >>> 16 & 0xff, hi >>> 8 & 0xff, hi & 0xff, lo >>> 24, lo >>> 16 & 0xff, lo >>> 8 & 0xff, lo & 0xff];
      };
      /**
       * Creates a Long from its byte representation.
       * @param {!Array.<number>} bytes Byte representation
       * @param {boolean=} unsigned Whether unsigned or not, defaults to signed
       * @param {boolean=} le Whether little or big endian, defaults to big endian
       * @returns {Long} The corresponding Long value
       */


      Long.fromBytes = function fromBytes(bytes, unsigned, le) {
        return le ? Long.fromBytesLE(bytes, unsigned) : Long.fromBytesBE(bytes, unsigned);
      };
      /**
       * Creates a Long from its little endian byte representation.
       * @param {!Array.<number>} bytes Little endian byte representation
       * @param {boolean=} unsigned Whether unsigned or not, defaults to signed
       * @returns {Long} The corresponding Long value
       */


      Long.fromBytesLE = function fromBytesLE(bytes, unsigned) {
        return new Long(bytes[0] | bytes[1] << 8 | bytes[2] << 16 | bytes[3] << 24, bytes[4] | bytes[5] << 8 | bytes[6] << 16 | bytes[7] << 24, unsigned);
      };
      /**
       * Creates a Long from its big endian byte representation.
       * @param {!Array.<number>} bytes Big endian byte representation
       * @param {boolean=} unsigned Whether unsigned or not, defaults to signed
       * @returns {Long} The corresponding Long value
       */


      Long.fromBytesBE = function fromBytesBE(bytes, unsigned) {
        return new Long(bytes[4] << 24 | bytes[5] << 16 | bytes[6] << 8 | bytes[7], bytes[0] << 24 | bytes[1] << 16 | bytes[2] << 8 | bytes[3], unsigned);
      };
    }
  };
});

System.register("chunks:///_virtual/rollupPluginModLoBabelHelpers.js", [], function (exports) {
  return {
    execute: function () {
      exports({
        applyDecoratedDescriptor: _applyDecoratedDescriptor,
        arrayLikeToArray: _arrayLikeToArray,
        assertThisInitialized: _assertThisInitialized,
        asyncToGenerator: _asyncToGenerator,
        construct: _construct,
        createClass: _createClass,
        createForOfIteratorHelperLoose: _createForOfIteratorHelperLoose,
        getPrototypeOf: _getPrototypeOf,
        inheritsLoose: _inheritsLoose,
        initializerDefineProperty: _initializerDefineProperty,
        isNativeFunction: _isNativeFunction,
        isNativeReflectConstruct: _isNativeReflectConstruct,
        regeneratorRuntime: _regeneratorRuntime,
        setPrototypeOf: _setPrototypeOf,
        toPrimitive: _toPrimitive,
        toPropertyKey: _toPropertyKey,
        unsupportedIterableToArray: _unsupportedIterableToArray,
        wrapNativeSuper: _wrapNativeSuper
      });

      function _regeneratorRuntime() {
        /*! regenerator-runtime -- Copyright (c) 2014-present, Facebook, Inc. -- license (MIT): https://github.com/facebook/regenerator/blob/main/LICENSE */
        _regeneratorRuntime = exports('regeneratorRuntime', function () {
          return exports$1;
        });

        var exports$1 = {},
            Op = Object.prototype,
            hasOwn = Op.hasOwnProperty,
            defineProperty = Object.defineProperty || function (obj, key, desc) {
          obj[key] = desc.value;
        },
            $Symbol = "function" == typeof Symbol ? Symbol : {},
            iteratorSymbol = $Symbol.iterator || "@@iterator",
            asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator",
            toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

        function define(obj, key, value) {
          return Object.defineProperty(obj, key, {
            value: value,
            enumerable: !0,
            configurable: !0,
            writable: !0
          }), obj[key];
        }

        try {
          define({}, "");
        } catch (err) {
          define = function (obj, key, value) {
            return obj[key] = value;
          };
        }

        function wrap(innerFn, outerFn, self, tryLocsList) {
          var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator,
              generator = Object.create(protoGenerator.prototype),
              context = new Context(tryLocsList || []);
          return defineProperty(generator, "_invoke", {
            value: makeInvokeMethod(innerFn, self, context)
          }), generator;
        }

        function tryCatch(fn, obj, arg) {
          try {
            return {
              type: "normal",
              arg: fn.call(obj, arg)
            };
          } catch (err) {
            return {
              type: "throw",
              arg: err
            };
          }
        }

        exports$1.wrap = wrap;
        var ContinueSentinel = {};

        function Generator() {}

        function GeneratorFunction() {}

        function GeneratorFunctionPrototype() {}

        var IteratorPrototype = {};
        define(IteratorPrototype, iteratorSymbol, function () {
          return this;
        });
        var getProto = Object.getPrototypeOf,
            NativeIteratorPrototype = getProto && getProto(getProto(values([])));
        NativeIteratorPrototype && NativeIteratorPrototype !== Op && hasOwn.call(NativeIteratorPrototype, iteratorSymbol) && (IteratorPrototype = NativeIteratorPrototype);
        var Gp = GeneratorFunctionPrototype.prototype = Generator.prototype = Object.create(IteratorPrototype);

        function defineIteratorMethods(prototype) {
          ["next", "throw", "return"].forEach(function (method) {
            define(prototype, method, function (arg) {
              return this._invoke(method, arg);
            });
          });
        }

        function AsyncIterator(generator, PromiseImpl) {
          function invoke(method, arg, resolve, reject) {
            var record = tryCatch(generator[method], generator, arg);

            if ("throw" !== record.type) {
              var result = record.arg,
                  value = result.value;
              return value && "object" == typeof value && hasOwn.call(value, "__await") ? PromiseImpl.resolve(value.__await).then(function (value) {
                invoke("next", value, resolve, reject);
              }, function (err) {
                invoke("throw", err, resolve, reject);
              }) : PromiseImpl.resolve(value).then(function (unwrapped) {
                result.value = unwrapped, resolve(result);
              }, function (error) {
                return invoke("throw", error, resolve, reject);
              });
            }

            reject(record.arg);
          }

          var previousPromise;
          defineProperty(this, "_invoke", {
            value: function (method, arg) {
              function callInvokeWithMethodAndArg() {
                return new PromiseImpl(function (resolve, reject) {
                  invoke(method, arg, resolve, reject);
                });
              }

              return previousPromise = previousPromise ? previousPromise.then(callInvokeWithMethodAndArg, callInvokeWithMethodAndArg) : callInvokeWithMethodAndArg();
            }
          });
        }

        function makeInvokeMethod(innerFn, self, context) {
          var state = "suspendedStart";
          return function (method, arg) {
            if ("executing" === state) throw new Error("Generator is already running");

            if ("completed" === state) {
              if ("throw" === method) throw arg;
              return doneResult();
            }

            for (context.method = method, context.arg = arg;;) {
              var delegate = context.delegate;

              if (delegate) {
                var delegateResult = maybeInvokeDelegate(delegate, context);

                if (delegateResult) {
                  if (delegateResult === ContinueSentinel) continue;
                  return delegateResult;
                }
              }

              if ("next" === context.method) context.sent = context._sent = context.arg;else if ("throw" === context.method) {
                if ("suspendedStart" === state) throw state = "completed", context.arg;
                context.dispatchException(context.arg);
              } else "return" === context.method && context.abrupt("return", context.arg);
              state = "executing";
              var record = tryCatch(innerFn, self, context);

              if ("normal" === record.type) {
                if (state = context.done ? "completed" : "suspendedYield", record.arg === ContinueSentinel) continue;
                return {
                  value: record.arg,
                  done: context.done
                };
              }

              "throw" === record.type && (state = "completed", context.method = "throw", context.arg = record.arg);
            }
          };
        }

        function maybeInvokeDelegate(delegate, context) {
          var methodName = context.method,
              method = delegate.iterator[methodName];
          if (undefined === method) return context.delegate = null, "throw" === methodName && delegate.iterator.return && (context.method = "return", context.arg = undefined, maybeInvokeDelegate(delegate, context), "throw" === context.method) || "return" !== methodName && (context.method = "throw", context.arg = new TypeError("The iterator does not provide a '" + methodName + "' method")), ContinueSentinel;
          var record = tryCatch(method, delegate.iterator, context.arg);
          if ("throw" === record.type) return context.method = "throw", context.arg = record.arg, context.delegate = null, ContinueSentinel;
          var info = record.arg;
          return info ? info.done ? (context[delegate.resultName] = info.value, context.next = delegate.nextLoc, "return" !== context.method && (context.method = "next", context.arg = undefined), context.delegate = null, ContinueSentinel) : info : (context.method = "throw", context.arg = new TypeError("iterator result is not an object"), context.delegate = null, ContinueSentinel);
        }

        function pushTryEntry(locs) {
          var entry = {
            tryLoc: locs[0]
          };
          1 in locs && (entry.catchLoc = locs[1]), 2 in locs && (entry.finallyLoc = locs[2], entry.afterLoc = locs[3]), this.tryEntries.push(entry);
        }

        function resetTryEntry(entry) {
          var record = entry.completion || {};
          record.type = "normal", delete record.arg, entry.completion = record;
        }

        function Context(tryLocsList) {
          this.tryEntries = [{
            tryLoc: "root"
          }], tryLocsList.forEach(pushTryEntry, this), this.reset(!0);
        }

        function values(iterable) {
          if (iterable) {
            var iteratorMethod = iterable[iteratorSymbol];
            if (iteratorMethod) return iteratorMethod.call(iterable);
            if ("function" == typeof iterable.next) return iterable;

            if (!isNaN(iterable.length)) {
              var i = -1,
                  next = function next() {
                for (; ++i < iterable.length;) if (hasOwn.call(iterable, i)) return next.value = iterable[i], next.done = !1, next;

                return next.value = undefined, next.done = !0, next;
              };

              return next.next = next;
            }
          }

          return {
            next: doneResult
          };
        }

        function doneResult() {
          return {
            value: undefined,
            done: !0
          };
        }

        return GeneratorFunction.prototype = GeneratorFunctionPrototype, defineProperty(Gp, "constructor", {
          value: GeneratorFunctionPrototype,
          configurable: !0
        }), defineProperty(GeneratorFunctionPrototype, "constructor", {
          value: GeneratorFunction,
          configurable: !0
        }), GeneratorFunction.displayName = define(GeneratorFunctionPrototype, toStringTagSymbol, "GeneratorFunction"), exports$1.isGeneratorFunction = function (genFun) {
          var ctor = "function" == typeof genFun && genFun.constructor;
          return !!ctor && (ctor === GeneratorFunction || "GeneratorFunction" === (ctor.displayName || ctor.name));
        }, exports$1.mark = function (genFun) {
          return Object.setPrototypeOf ? Object.setPrototypeOf(genFun, GeneratorFunctionPrototype) : (genFun.__proto__ = GeneratorFunctionPrototype, define(genFun, toStringTagSymbol, "GeneratorFunction")), genFun.prototype = Object.create(Gp), genFun;
        }, exports$1.awrap = function (arg) {
          return {
            __await: arg
          };
        }, defineIteratorMethods(AsyncIterator.prototype), define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
          return this;
        }), exports$1.AsyncIterator = AsyncIterator, exports$1.async = function (innerFn, outerFn, self, tryLocsList, PromiseImpl) {
          void 0 === PromiseImpl && (PromiseImpl = Promise);
          var iter = new AsyncIterator(wrap(innerFn, outerFn, self, tryLocsList), PromiseImpl);
          return exports$1.isGeneratorFunction(outerFn) ? iter : iter.next().then(function (result) {
            return result.done ? result.value : iter.next();
          });
        }, defineIteratorMethods(Gp), define(Gp, toStringTagSymbol, "Generator"), define(Gp, iteratorSymbol, function () {
          return this;
        }), define(Gp, "toString", function () {
          return "[object Generator]";
        }), exports$1.keys = function (val) {
          var object = Object(val),
              keys = [];

          for (var key in object) keys.push(key);

          return keys.reverse(), function next() {
            for (; keys.length;) {
              var key = keys.pop();
              if (key in object) return next.value = key, next.done = !1, next;
            }

            return next.done = !0, next;
          };
        }, exports$1.values = values, Context.prototype = {
          constructor: Context,
          reset: function (skipTempReset) {
            if (this.prev = 0, this.next = 0, this.sent = this._sent = undefined, this.done = !1, this.delegate = null, this.method = "next", this.arg = undefined, this.tryEntries.forEach(resetTryEntry), !skipTempReset) for (var name in this) "t" === name.charAt(0) && hasOwn.call(this, name) && !isNaN(+name.slice(1)) && (this[name] = undefined);
          },
          stop: function () {
            this.done = !0;
            var rootRecord = this.tryEntries[0].completion;
            if ("throw" === rootRecord.type) throw rootRecord.arg;
            return this.rval;
          },
          dispatchException: function (exception) {
            if (this.done) throw exception;
            var context = this;

            function handle(loc, caught) {
              return record.type = "throw", record.arg = exception, context.next = loc, caught && (context.method = "next", context.arg = undefined), !!caught;
            }

            for (var i = this.tryEntries.length - 1; i >= 0; --i) {
              var entry = this.tryEntries[i],
                  record = entry.completion;
              if ("root" === entry.tryLoc) return handle("end");

              if (entry.tryLoc <= this.prev) {
                var hasCatch = hasOwn.call(entry, "catchLoc"),
                    hasFinally = hasOwn.call(entry, "finallyLoc");

                if (hasCatch && hasFinally) {
                  if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0);
                  if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc);
                } else if (hasCatch) {
                  if (this.prev < entry.catchLoc) return handle(entry.catchLoc, !0);
                } else {
                  if (!hasFinally) throw new Error("try statement without catch or finally");
                  if (this.prev < entry.finallyLoc) return handle(entry.finallyLoc);
                }
              }
            }
          },
          abrupt: function (type, arg) {
            for (var i = this.tryEntries.length - 1; i >= 0; --i) {
              var entry = this.tryEntries[i];

              if (entry.tryLoc <= this.prev && hasOwn.call(entry, "finallyLoc") && this.prev < entry.finallyLoc) {
                var finallyEntry = entry;
                break;
              }
            }

            finallyEntry && ("break" === type || "continue" === type) && finallyEntry.tryLoc <= arg && arg <= finallyEntry.finallyLoc && (finallyEntry = null);
            var record = finallyEntry ? finallyEntry.completion : {};
            return record.type = type, record.arg = arg, finallyEntry ? (this.method = "next", this.next = finallyEntry.finallyLoc, ContinueSentinel) : this.complete(record);
          },
          complete: function (record, afterLoc) {
            if ("throw" === record.type) throw record.arg;
            return "break" === record.type || "continue" === record.type ? this.next = record.arg : "return" === record.type ? (this.rval = this.arg = record.arg, this.method = "return", this.next = "end") : "normal" === record.type && afterLoc && (this.next = afterLoc), ContinueSentinel;
          },
          finish: function (finallyLoc) {
            for (var i = this.tryEntries.length - 1; i >= 0; --i) {
              var entry = this.tryEntries[i];
              if (entry.finallyLoc === finallyLoc) return this.complete(entry.completion, entry.afterLoc), resetTryEntry(entry), ContinueSentinel;
            }
          },
          catch: function (tryLoc) {
            for (var i = this.tryEntries.length - 1; i >= 0; --i) {
              var entry = this.tryEntries[i];

              if (entry.tryLoc === tryLoc) {
                var record = entry.completion;

                if ("throw" === record.type) {
                  var thrown = record.arg;
                  resetTryEntry(entry);
                }

                return thrown;
              }
            }

            throw new Error("illegal catch attempt");
          },
          delegateYield: function (iterable, resultName, nextLoc) {
            return this.delegate = {
              iterator: values(iterable),
              resultName: resultName,
              nextLoc: nextLoc
            }, "next" === this.method && (this.arg = undefined), ContinueSentinel;
          }
        }, exports$1;
      }

      function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
        try {
          var info = gen[key](arg);
          var value = info.value;
        } catch (error) {
          reject(error);
          return;
        }

        if (info.done) {
          resolve(value);
        } else {
          Promise.resolve(value).then(_next, _throw);
        }
      }

      function _asyncToGenerator(fn) {
        return function () {
          var self = this,
              args = arguments;
          return new Promise(function (resolve, reject) {
            var gen = fn.apply(self, args);

            function _next(value) {
              asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
            }

            function _throw(err) {
              asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
            }

            _next(undefined);
          });
        };
      }

      function _defineProperties(target, props) {
        for (var i = 0; i < props.length; i++) {
          var descriptor = props[i];
          descriptor.enumerable = descriptor.enumerable || false;
          descriptor.configurable = true;
          if ("value" in descriptor) descriptor.writable = true;
          Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor);
        }
      }

      function _createClass(Constructor, protoProps, staticProps) {
        if (protoProps) _defineProperties(Constructor.prototype, protoProps);
        if (staticProps) _defineProperties(Constructor, staticProps);
        Object.defineProperty(Constructor, "prototype", {
          writable: false
        });
        return Constructor;
      }

      function _inheritsLoose(subClass, superClass) {
        subClass.prototype = Object.create(superClass.prototype);
        subClass.prototype.constructor = subClass;

        _setPrototypeOf(subClass, superClass);
      }

      function _getPrototypeOf(o) {
        _getPrototypeOf = exports('getPrototypeOf', Object.setPrototypeOf ? Object.getPrototypeOf.bind() : function _getPrototypeOf(o) {
          return o.__proto__ || Object.getPrototypeOf(o);
        });
        return _getPrototypeOf(o);
      }

      function _setPrototypeOf(o, p) {
        _setPrototypeOf = exports('setPrototypeOf', Object.setPrototypeOf ? Object.setPrototypeOf.bind() : function _setPrototypeOf(o, p) {
          o.__proto__ = p;
          return o;
        });
        return _setPrototypeOf(o, p);
      }

      function _isNativeReflectConstruct() {
        if (typeof Reflect === "undefined" || !Reflect.construct) return false;
        if (Reflect.construct.sham) return false;
        if (typeof Proxy === "function") return true;

        try {
          Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {}));
          return true;
        } catch (e) {
          return false;
        }
      }

      function _construct(Parent, args, Class) {
        if (_isNativeReflectConstruct()) {
          _construct = exports('construct', Reflect.construct.bind());
        } else {
          _construct = exports('construct', function _construct(Parent, args, Class) {
            var a = [null];
            a.push.apply(a, args);
            var Constructor = Function.bind.apply(Parent, a);
            var instance = new Constructor();
            if (Class) _setPrototypeOf(instance, Class.prototype);
            return instance;
          });
        }

        return _construct.apply(null, arguments);
      }

      function _isNativeFunction(fn) {
        return Function.toString.call(fn).indexOf("[native code]") !== -1;
      }

      function _wrapNativeSuper(Class) {
        var _cache = typeof Map === "function" ? new Map() : undefined;

        _wrapNativeSuper = exports('wrapNativeSuper', function _wrapNativeSuper(Class) {
          if (Class === null || !_isNativeFunction(Class)) return Class;

          if (typeof Class !== "function") {
            throw new TypeError("Super expression must either be null or a function");
          }

          if (typeof _cache !== "undefined") {
            if (_cache.has(Class)) return _cache.get(Class);

            _cache.set(Class, Wrapper);
          }

          function Wrapper() {
            return _construct(Class, arguments, _getPrototypeOf(this).constructor);
          }

          Wrapper.prototype = Object.create(Class.prototype, {
            constructor: {
              value: Wrapper,
              enumerable: false,
              writable: true,
              configurable: true
            }
          });
          return _setPrototypeOf(Wrapper, Class);
        });
        return _wrapNativeSuper(Class);
      }

      function _assertThisInitialized(self) {
        if (self === void 0) {
          throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        }

        return self;
      }

      function _unsupportedIterableToArray(o, minLen) {
        if (!o) return;
        if (typeof o === "string") return _arrayLikeToArray(o, minLen);
        var n = Object.prototype.toString.call(o).slice(8, -1);
        if (n === "Object" && o.constructor) n = o.constructor.name;
        if (n === "Map" || n === "Set") return Array.from(o);
        if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
      }

      function _arrayLikeToArray(arr, len) {
        if (len == null || len > arr.length) len = arr.length;

        for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

        return arr2;
      }

      function _createForOfIteratorHelperLoose(o, allowArrayLike) {
        var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"];
        if (it) return (it = it.call(o)).next.bind(it);

        if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
          if (it) o = it;
          var i = 0;
          return function () {
            if (i >= o.length) return {
              done: true
            };
            return {
              done: false,
              value: o[i++]
            };
          };
        }

        throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
      }

      function _toPrimitive(input, hint) {
        if (typeof input !== "object" || input === null) return input;
        var prim = input[Symbol.toPrimitive];

        if (prim !== undefined) {
          var res = prim.call(input, hint || "default");
          if (typeof res !== "object") return res;
          throw new TypeError("@@toPrimitive must return a primitive value.");
        }

        return (hint === "string" ? String : Number)(input);
      }

      function _toPropertyKey(arg) {
        var key = _toPrimitive(arg, "string");

        return typeof key === "symbol" ? key : String(key);
      }

      function _initializerDefineProperty(target, property, descriptor, context) {
        if (!descriptor) return;
        Object.defineProperty(target, property, {
          enumerable: descriptor.enumerable,
          configurable: descriptor.configurable,
          writable: descriptor.writable,
          value: descriptor.initializer ? descriptor.initializer.call(context) : void 0
        });
      }

      function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
        var desc = {};
        Object.keys(descriptor).forEach(function (key) {
          desc[key] = descriptor[key];
        });
        desc.enumerable = !!desc.enumerable;
        desc.configurable = !!desc.configurable;

        if ('value' in desc || desc.initializer) {
          desc.writable = true;
        }

        desc = decorators.slice().reverse().reduce(function (desc, decorator) {
          return decorator(target, property, desc) || desc;
        }, desc);

        if (context && desc.initializer !== void 0) {
          desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
          desc.initializer = undefined;
        }

        if (desc.initializer === void 0) {
          Object.defineProperty(target, property, desc);
          desc = null;
        }

        return desc;
      }
    }
  };
});

} }; });